INSERT INTO agora.type_element_modele_objet(id_type_element_modele_objet , date_creation , libelle) values (1, CURRENT_TIMESTAMP ,'fonctionnel'); 

INSERT INTO agora.critere VALUES ('Actualité');
INSERT INTO agora.critere VALUES ('Cohérence');
INSERT INTO agora.critere VALUES ('Complétude');
INSERT INTO agora.critere VALUES ('Disponibilité');
INSERT INTO agora.critere VALUES ('Exactitude');
INSERT INTO agora.critere VALUES ('Exhaustivité');
INSERT INTO agora.critere VALUES ('Intégrité');
INSERT INTO agora.critere VALUES ('Précision');
INSERT INTO agora.critere VALUES ('Unicité');
INSERT INTO agora.critere VALUES ('Validité');

INSERT INTO agora.usage_metier
(id_usage_metier, date_creation, id, revision, date_debut_activite, date_fin_activite, id_externe, libelle)
VALUES(1, null, null, 0, null, null, null, 'Gestion opérationnelle (EF)');

INSERT INTO agora.usage_metier
(id_usage_metier, date_creation, id, revision, date_debut_activite, date_fin_activite, id_externe, libelle)
VALUES(2, null, null, 0, null, null, null, 'Horairisation des sillons (SIPH)');

INSERT INTO agora.usage_metier
(id_usage_metier, date_creation, id, revision, date_debut_activite, date_fin_activite, id_externe, libelle)
VALUES(3, null, null, 0, null, null, null, 'Horairisation des sillons (MGOC)');