package sncf.reseau.cdsdpadsir.agora.generic.model;

/**
 * Les etats possible d'une entité du réferenciel 
 * @author PNBE10831 (Nouh BOUANANE)
 *
 */
public enum EtatEnum {
	
	ACTIF, ARCHIVE, SUPPRIME;
}
