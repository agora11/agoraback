package sncf.reseau.cdsdpadsir.agora.generic.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;


/**
 * Ariane spécifique
 * 
 * @author PAMI08191
 *
 */
@Getter
@Setter
@MappedSuperclass
public abstract class ObjetPersistant {

	/**
	 * GUUID
	 */
	@Column(name = "\"id\"")
	@JsonProperty("id")
	protected String id;

	@Column(name = "\"date_creation\"")
	@JsonProperty("dateCreation")
	protected LocalDateTime dateCreation;

	@Column(name = "\"revision\"")
	@JsonProperty("revision")
	protected Integer revision;
}