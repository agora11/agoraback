package sncf.reseau.cdsdpadsir.agora.generic.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
@MappedSuperclass
public abstract class RessourceGouvernanceDonneeAEtat extends RessourceAbstraiteNommee {

	@Column(name = "\"etat\"")
	@JsonProperty("etat")
	protected String etat;

	@Column(name = "\"description\"")
	@JsonProperty("description")
	protected String description;

}