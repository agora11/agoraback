package sncf.reseau.cdsdpadsir.agora.generic.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString(callSuper = true)
@Getter
@Setter
@MappedSuperclass
public abstract class RessourceAbstraiteNommee extends RessourceAbstraite {

	@Column(name = "\"nom\"")
	@JsonProperty("nom")
	protected String nom;

}
