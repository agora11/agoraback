package sncf.reseau.cdsdpadsir.agora.generic.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(exclude = { "idExterne", "dateDebutActivite", "dateFinActivite" }, callSuper = true)
@MappedSuperclass
public abstract class RessourceAbstraite extends ObjetPersistant {

	@Column(name = "\"id_externe\"")
	@JsonProperty("idExterne")
	protected String idExterne;

	@Column(name = "\"date_debut_activite\"")
	@JsonProperty("dateDebutActivite")
	protected LocalDateTime dateDebutActivite;

	@Column(name = "\"date_fin_activite\"")
	@JsonProperty("dateFinActivite")
	protected LocalDateTime dateFinActivite;

}