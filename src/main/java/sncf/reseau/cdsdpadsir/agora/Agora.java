package sncf.reseau.cdsdpadsir.agora;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.modelmapper.ModelMapper;
import org.openapitools.jackson.nullable.JsonNullableModule;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.databind.Module;

import sncf.reseau.cdsdpadsir.agora.entity.Diagram;
import sncf.reseau.cdsdpadsir.agora.model.DiagramDto;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
@ComponentScan(basePackages = { "sncf.reseau.cdsdpadsir.agora", "sncf.reseau.cdsdpadsir.agora.api",
		"sncf.reseau.cdsdpadsir.agora.configuration", "sncf.reseau.cdsdpadsir.agora.entity",
		"sncf.reseau.cdsdpadsir.agora.service","sncf.reseau.cdspadsir.web" })
@EnableJpaRepositories("sncf.reseau.cdsdpadsir.agora.repository")
public class Agora implements CommandLineRunner {

	@Override
	public void run(String... arg0) throws Exception {
		if (arg0.length > 0 && arg0[0].equals("exitcode")) {
			throw new ExitException();
		}
	}

	public static void main(String[] args) throws Exception {
		disableSSLValidation();
		new SpringApplication(Agora.class).run(args);
	}

	static class ExitException extends RuntimeException implements ExitCodeGenerator {
		private static final long serialVersionUID = 1L;

		@Override
		public int getExitCode() {
			return 10;
		}

	}

	@Bean
	public WebMvcConfigurer webConfigurer() {
		return new WebMvcConfigurer() {
			/*
			 * @Override public void addCorsMappings(CorsRegistry registry) {
			 * registry.addMapping("/**") .allowedOrigins("*") .allowedMethods("*")
			 * .allowedHeaders("Content-Type"); }
			 */
		};
	}

	@Bean
	public Module jsonNullableModule() {
		return new JsonNullableModule();
	}

	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.createTypeMap(Diagram.class, DiagramDto.class)
				.addMappings(mapper -> mapper.map(diagram -> diagram.getFiliere().getNom(), DiagramDto::setFiliere));
		return modelMapper;
	}

	public static Boolean disableSSLValidation() throws Exception {
		final SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(null, new TrustManager[] { new X509TrustManager() {
			@Override
			public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
			}

			@Override
			public X509Certificate[] getAcceptedIssuers()

					{
				return new X509Certificate[0];
			}
		} }, null);
		HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
		HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
			@Override
			public boolean verify(String hostname, SSLSession session)

			{
				return true;
			}
		});
		return true;
	}

}
