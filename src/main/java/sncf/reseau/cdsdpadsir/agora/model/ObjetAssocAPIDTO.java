package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObjetAssocAPIDTO {
    @JsonProperty("id")
    private String id;

    @JsonProperty("libelle")
    private String libelle;

    @JsonProperty("definition")
    private String definition;

    @JsonProperty("filiere")
    private FiliereStructApi filiere;
    
    @JsonProperty("famille")
    @JsonSerialize(include = Inclusion.NON_NULL)
    private FamilleStructApi famille;
    
    @JsonProperty("sousFamille")
    @JsonSerialize(include = Inclusion.NON_NULL)	
    private FamilleStructApi sousFamille;
}
