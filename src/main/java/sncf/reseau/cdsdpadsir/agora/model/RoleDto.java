package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * RoleDto
 */
@Getter
@Setter
public class RoleDto extends RessourceAbstraite   {
	
  @JsonProperty("idRole")
  private Long idRole;

  @JsonProperty("codeRoleFonctionnel")
	private String codeRoleFonctionnel;

  @JsonProperty("description")
	private String description;

  @JsonProperty("nom")
	private String nom;

}

