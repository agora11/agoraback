package sncf.reseau.cdsdpadsir.agora.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * RoleWsDTO
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-01-05T10:18:32.903+01:00[Europe/Paris]")
public class RoleWsDTO   {
  @JsonProperty("id")
  private String id;

  @JsonProperty("nom")
  private String nom;

  @JsonProperty("codeRoleFonctionnel")
  private String codeRoleFonctionnel;

  public RoleWsDTO id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  */
  @ApiModelProperty(value = "")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public RoleWsDTO nom(String nom) {
    this.nom = nom;
    return this;
  }

  /**
   * Get nom
   * @return nom
  */
  @ApiModelProperty(value = "")


  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public RoleWsDTO codeRoleFonctionnel(String codeRoleFonctionnel) {
    this.codeRoleFonctionnel = codeRoleFonctionnel;
    return this;
  }

  /**
   * Get codeRoleFonctionnel
   * @return codeRoleFonctionnel
  */
  @ApiModelProperty(value = "")


  public String getCodeRoleFonctionnel() {
    return codeRoleFonctionnel;
  }

  public void setCodeRoleFonctionnel(String codeRoleFonctionnel) {
    this.codeRoleFonctionnel = codeRoleFonctionnel;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RoleWsDTO roleWsDTO = (RoleWsDTO) o;
    return Objects.equals(this.id, roleWsDTO.id) &&
        Objects.equals(this.nom, roleWsDTO.nom) &&
        Objects.equals(this.codeRoleFonctionnel, roleWsDTO.codeRoleFonctionnel);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, nom, codeRoleFonctionnel);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RoleWsDTO {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    nom: ").append(toIndentedString(nom)).append("\n");
    sb.append("    codeRoleFonctionnel: ").append(toIndentedString(codeRoleFonctionnel)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

