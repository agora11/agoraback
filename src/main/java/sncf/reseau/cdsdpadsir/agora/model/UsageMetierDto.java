package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Getter
@Setter
public class UsageMetierDto extends RessourceAbstraite {

	@JsonProperty("idUsageMetier")
	private long idUsageMetier;

	@JsonProperty("libelle")
	private String libelle;

}
