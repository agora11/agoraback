package sncf.reseau.cdsdpadsir.agora.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaquetDto extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("idPaquet")
	private Long idPaquet;

	@JsonProperty("classeDobjets")
	private Set<ClasseDobjetDto> classeDobjets;

	@JsonProperty("paquetParent")
    private PaquetDto paquetParent;
	
	@JsonProperty("version")
	private String version ;
	
	@JsonProperty("nom")
	private String nom ;
	
	@JsonProperty("typeElementModeleObjet")
	private TypeElementModeleObjetDto typeElementModeleObjet;
	
	@JsonProperty("revision_fonctionnel")
	private Timestamp revisionFonctionnel ;

}