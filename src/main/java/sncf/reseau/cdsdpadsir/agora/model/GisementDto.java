package sncf.reseau.cdsdpadsir.agora.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Getter
@Setter
public class GisementDto extends RessourceAbstraite {

	@JsonProperty(value = "idGisement")
	private Long idGisement;

	@JsonProperty(value = "libelle")
	private String libelle;

	@JsonProperty(value = "niveauDeCompletude")
	private String niveauDeCompletude;

	@JsonProperty(value = "nombreDeJeuxDonnees")
	private String nombreDeJeuxDonnees;

	@JsonProperty(value = "nombreDeClients")
	private String nombreDeClients;

	@JsonProperty(value = "nombreDeFournisseurs")
	private String nombreDeFournisseurs;

	@JsonProperty(value = "nombreDeServices")
	private String nombreDeServices;

	@JsonProperty(value = "nombreObjetsReferences")
	private String nombreObjetsReferences;

	@JsonProperty("detailsClientsExternes")
	private String detailsClientsExternes;

	@JsonProperty("nombreClientsInternes")
	private String nombreClientsInternes;

	@JsonProperty("nombreClientsExternes")
	private String nombreClientsExternes;

	@JsonProperty("nombreServicesWebExposesDataLab")
	private String nombreServicesWebExposesDataLab;

	@JsonProperty("nombreObjetsReferencesEnProjet")
	private String nombreObjetsReferencesEnProjet;

	@JsonProperty("nombreServicesEnProjet")
	private String nombreServicesEnProjet;

	@JsonProperty("nombreServicesWebExposesDataLabEnProjet")
	private String nombreServicesWebExposesDataLabEnProjet;

	@JsonProperty(value = "raisonEtre")
	private String raisonEtre;

	@JsonProperty(value = "raisonEtreCourte")
	private String raisonEtreCourte;

	@JsonProperty(value = "servicesDocumentes")
	private String servicesDocumentes;

	@JsonProperty(value = "dateMiseEnService")
	private String dateMiseEnService;

	@JsonProperty(value = "statut")
	private String statut;

	@JsonProperty(value = "filieres")
	private List<FiliereGisementDto> filieres;

	@JsonProperty(value = "acteurs")
	private List<ActeurDto> acteurs;

	@JsonProperty(value = "liensUtiles")
	private List<LienUtileDto> liensUtiles;

	@JsonProperty(value = "description")
	private String description;

}
