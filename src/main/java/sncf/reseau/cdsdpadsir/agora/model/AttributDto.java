package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * AttributDto
 */
@Getter
@Setter
public class AttributDto extends RessourceAbstraite {

	@JsonProperty("id_attribut")
	private Long idAttribut;

	@JsonProperty("libelle")
	private String libelle;

	@JsonProperty("valeur")
	private String valeur;

}
