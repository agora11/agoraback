package sncf.reseau.cdsdpadsir.agora.model;

import java.sql.Timestamp;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RelationClasseDobjetDto extends RessourceAbstraite{
	private static final Long serialVersionUID = 1L;

	@JsonProperty("idRelationClasseDobjet")
	private Long idRelationClasseDobjet;

	@JsonProperty("typeRelation")
	private String typeRelation ;
	
	@JsonProperty("classeDobjetSource")
	private ClasseDobjetDto classeDobjetSource;
	
	@Column(name = "\"estNavigableSource\"")
	private Boolean  estNavigableSource ;
	
	@Column(name = "\"cardinaliteMinSource\"")
	private Integer cardinaliteMinSource ;
	
	@Column(name = "\"cardinaliteMaxSource\"")
	private Integer cardinaliteMaxSource ;

	@JsonProperty("classeDobjetDestination")
	private ClasseDobjetDto classeDobjetDestination;
	
	@JsonProperty("\"cardinaliteMinDestination\"")
	private Integer cardinaliteMinDestination ;
	
	@JsonProperty("\"cardinaliteMaxDestination\"")
	private Integer cardinaliteMaxDestination ;
	
	@JsonProperty("\"estNavigableDestination\"")
	private Boolean  estNavigableDestination ;
	
	@JsonProperty("version")
	private String version ;
	
	@JsonProperty("revision_fonctionnel")
	private Timestamp revisionFonctionnel ;
	
	@JsonProperty("typeElementModeleObjet")
	private TypeElementModeleObjetDto typeElementModeleObjet;
}