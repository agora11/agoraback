package sncf.reseau.cdsdpadsir.agora.model;

import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * UtilisateursCasDUsageDto
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-01-05T10:18:32.903+01:00[Europe/Paris]")
public class UtilisateursCasDUsageDto   {
  @JsonProperty("referentMetier")
  private UtilisateurDto referentMetier;

  @JsonProperty("casDUsage")
  private String casDUsage;

  public UtilisateursCasDUsageDto referentMetier(UtilisateurDto referentMetier) {
    this.referentMetier = referentMetier;
    return this;
  }

  /**
   * Get referentMetier
   * @return referentMetier
  */
  @ApiModelProperty(value = "")

  @Valid

  public UtilisateurDto getReferentMetier() {
    return referentMetier;
  }

  public void setReferentMetier(UtilisateurDto referentMetier) {
    this.referentMetier = referentMetier;
  }

  public UtilisateursCasDUsageDto casDUsage(String casDUsage) {
    this.casDUsage = casDUsage;
    return this;
  }

  /**
   * Get casDUsage
   * @return casDUsage
  */
  @ApiModelProperty(value = "")


  public String getCasDUsage() {
    return casDUsage;
  }

  public void setCasDUsage(String casDUsage) {
    this.casDUsage = casDUsage;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UtilisateursCasDUsageDto utilisateursCasDUsageDto = (UtilisateursCasDUsageDto) o;
    return Objects.equals(this.referentMetier, utilisateursCasDUsageDto.referentMetier) &&
        Objects.equals(this.casDUsage, utilisateursCasDUsageDto.casDUsage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(referentMetier, casDUsage);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UtilisateursCasDUsageDto {\n");
    
    sb.append("    referentMetier: ").append(toIndentedString(referentMetier)).append("\n");
    sb.append("    casDUsage: ").append(toIndentedString(casDUsage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

