package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UtilisateurApiDto {
	
	@JsonProperty("nom")
	private String nom;
	
	@JsonProperty("prenom")
	private String prenom;

}
