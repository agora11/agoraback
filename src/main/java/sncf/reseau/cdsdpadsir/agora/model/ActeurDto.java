package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Getter
@Setter
public class ActeurDto extends RessourceAbstraite {

	@JsonProperty(value = "idActeur")
	private Long idActeur;

	@JsonProperty(value = "contact")
	private String contact;

	@JsonProperty(value = "nom")
	private String nom;
	
	@JsonProperty(value = "role")
	private String role;

}
