package sncf.reseau.cdsdpadsir.agora.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * MappingDto
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-01-05T10:18:32.903+01:00[Europe/Paris]")
public class MappingDto   {
  @JsonProperty("codeCp")
  private String codeCp;

  @JsonProperty("idCaimon")
  private String idCaimon;

  public MappingDto codeCp(String codeCp) {
    this.codeCp = codeCp;
    return this;
  }

  /**
   * Get codeCp
   * @return codeCp
  */
  @ApiModelProperty(value = "")


  public String getCodeCp() {
    return codeCp;
  }

  public void setCodeCp(String codeCp) {
    this.codeCp = codeCp;
  }

  public MappingDto idCaimon(String idCaimon) {
    this.idCaimon = idCaimon;
    return this;
  }

  /**
   * Get idCaimon
   * @return idCaimon
  */
  @ApiModelProperty(value = "")


  public String getIdCaimon() {
    return idCaimon;
  }

  public void setIdCaimon(String idCaimon) {
    this.idCaimon = idCaimon;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MappingDto mappingDto = (MappingDto) o;
    return Objects.equals(this.codeCp, mappingDto.codeCp) &&
        Objects.equals(this.idCaimon, mappingDto.idCaimon);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codeCp, idCaimon);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MappingDto {\n");
    
    sb.append("    codeCp: ").append(toIndentedString(codeCp)).append("\n");
    sb.append("    idCaimon: ").append(toIndentedString(idCaimon)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

