package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

import java.util.List;

@Data
public class DiagramDto extends RessourceAbstraite {

    @JsonProperty("diagramId")
    private Long diagramId;
    @JsonProperty("libelle")
    private String libelle;
    @JsonProperty("description")
    private String description;
    @JsonProperty("filiere")
    private String filiere;
    @JsonProperty("cells")
    private List<String> cells;
    @JsonProperty("associations")
    private List<String> associations;
    @JsonProperty("inheritances")
    private List<String> inheritances;
}
