package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


@Getter
@Setter
public class AssociationProfilAutorisationTypeAutorisationsDto extends RessourceAbstraite {
	
	@JsonProperty("idAssociationProfilAutorisationTypeAutorisation")
	private long idAssociationProfilAutorisationTypeAutorisation;
	
	@JsonProperty("profilAutorisation")
	private ProfilAutorisationDto profilAutorisation;

	
	@JsonProperty("typeAutorisation")
	private TypeAutorisationDto typeAutorisation;

}
