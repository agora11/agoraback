package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Getter
@Setter
public class PolitiqueQualiteParUsageDto extends RessourceAbstraite {
	
	@JsonProperty("idPolitiqueQualiteParUsage")
	private Long idPolitiqueQualiteParUsage;

	@JsonProperty("tauxSouhaite")
	private String tauxSouhaite;

	@JsonProperty("usageMetier")
	private UsageMetierDto usageMetier;


}
