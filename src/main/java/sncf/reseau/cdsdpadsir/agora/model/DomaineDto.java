package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Getter
@Setter
public class DomaineDto extends RessourceAbstraite {

	@JsonProperty("idDomaine")
	private Long idDomaine;

	@JsonProperty("libelle")
	private String libelle;
}
