package sncf.reseau.cdsdpadsir.agora.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class RechercheDto {

	private List<RechercheLigneDto> glossaire;
	private List<RechercheLigneDto> objets;
	private List<RechercheLigneDto> diagrammes;
	private List<RechercheLigneDto> filiere;
	private List<RechercheLigneDto> acteurs;
	private List<RechercheLigneDto> qualite;
	private List<RechercheLigneDto> gisement;

	public RechercheDto() {
		super();
		this.glossaire = new ArrayList<>();
		this.objets = new ArrayList<>();
		this.diagrammes = new ArrayList<>();
		this.filiere = new ArrayList<>();
		this.acteurs = new ArrayList<>();
		this.qualite = new ArrayList<>();
		this.gisement = new ArrayList<>();
	}

}
