package sncf.reseau.cdsdpadsir.agora.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * RoleUtilisateursDto
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-01-05T10:18:32.903+01:00[Europe/Paris]")
public class RoleUtilisateursDto   {
  @JsonProperty("idRole")
  private Long idRole;

  @JsonProperty("utilisateurs")
  @Valid
  private List<SimpleUtilisateurDto> utilisateurs = null;

  public RoleUtilisateursDto idRole(Long idRole) {
    this.idRole = idRole;
    return this;
  }

  /**
   * Get idRole
   * @return idRole
  */
  @ApiModelProperty(value = "")


  public Long getIdRole() {
    return idRole;
  }

  public void setIdRole(Long idRole) {
    this.idRole = idRole;
  }

  public RoleUtilisateursDto utilisateurs(List<SimpleUtilisateurDto> utilisateurs) {
    this.utilisateurs = utilisateurs;
    return this;
  }

  public RoleUtilisateursDto addUtilisateursItem(SimpleUtilisateurDto utilisateursItem) {
    if (this.utilisateurs == null) {
      this.utilisateurs = new ArrayList<>();
    }
    this.utilisateurs.add(utilisateursItem);
    return this;
  }

  /**
   * Get utilisateurs
   * @return utilisateurs
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<SimpleUtilisateurDto> getUtilisateurs() {
    return utilisateurs;
  }

  public void setUtilisateurs(List<SimpleUtilisateurDto> utilisateurs) {
    this.utilisateurs = utilisateurs;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RoleUtilisateursDto roleUtilisateursDto = (RoleUtilisateursDto) o;
    return Objects.equals(this.idRole, roleUtilisateursDto.idRole) &&
        Objects.equals(this.utilisateurs, roleUtilisateursDto.utilisateurs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idRole, utilisateurs);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RoleUtilisateursDto {\n");
    
    sb.append("    idRole: ").append(toIndentedString(idRole)).append("\n");
    sb.append("    utilisateurs: ").append(toIndentedString(utilisateurs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

