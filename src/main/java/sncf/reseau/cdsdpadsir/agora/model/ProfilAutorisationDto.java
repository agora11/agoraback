package sncf.reseau.cdsdpadsir.agora.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Getter
@Setter
public class ProfilAutorisationDto extends RessourceAbstraite {

	@JsonProperty("idProfilAutorisation")
	private long idProfilAutorisation;

	@JsonProperty("libelle")
	private String libelle;

	@JsonProperty("typesAutorisation")
	private List<TypeAutorisationDto> typesAutorisation;
}
