package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FiliereStructApi {

	@JsonProperty("id")
	private String id;
	
	@JsonProperty("libelle")
	private String libelle;
}
