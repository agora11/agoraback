package sncf.reseau.cdsdpadsir.agora.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * UtilisateurFiliereFamilleSousFamilleDto
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-01-05T10:18:32.903+01:00[Europe/Paris]")
public class UtilisateurFiliereFamilleSousFamilleDto   {
  @JsonProperty("idUtilisateur")
  private Long idUtilisateur;

  @JsonProperty("prenom")
  private String prenom;

  @JsonProperty("adresseMail")
  private String adresseMail;

  @JsonProperty("cp")
  private String cp;

  @JsonProperty("idCaimon")
  private String idCaimon;

  @JsonProperty("poste")
  private String poste;

  @JsonProperty("organisation")
  private String organisation;

  @JsonProperty("groupeDeRole")
  private String groupeDeRole;

  @JsonProperty("role")
  private RoleDto role;

  @JsonProperty("filiereList")
  @Valid
  private List<FiliereDto> filiereList = null;

  @JsonProperty("image")
  private String image;

  public UtilisateurFiliereFamilleSousFamilleDto idUtilisateur(Long idUtilisateur) {
    this.idUtilisateur = idUtilisateur;
    return this;
  }

  /**
   * Get idUtilisateur
   * @return idUtilisateur
  */
  @ApiModelProperty(value = "")


  public Long getIdUtilisateur() {
    return idUtilisateur;
  }

  public void setIdUtilisateur(Long idUtilisateur) {
    this.idUtilisateur = idUtilisateur;
  }

  public UtilisateurFiliereFamilleSousFamilleDto prenom(String prenom) {
    this.prenom = prenom;
    return this;
  }

  /**
   * Get prenom
   * @return prenom
  */
  @ApiModelProperty(value = "")


  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public UtilisateurFiliereFamilleSousFamilleDto adresseMail(String adresseMail) {
    this.adresseMail = adresseMail;
    return this;
  }

  /**
   * Get adresseMail
   * @return adresseMail
  */
  @ApiModelProperty(value = "")

@javax.validation.constraints.Email
  public String getAdresseMail() {
    return adresseMail;
  }

  public void setAdresseMail(String adresseMail) {
    this.adresseMail = adresseMail;
  }

  public UtilisateurFiliereFamilleSousFamilleDto cp(String cp) {
    this.cp = cp;
    return this;
  }

  /**
   * Get cp
   * @return cp
  */
  @ApiModelProperty(value = "")


  public String getCp() {
    return cp;
  }

  public void setCp(String cp) {
    this.cp = cp;
  }

  public UtilisateurFiliereFamilleSousFamilleDto idCaimon(String idCaimon) {
    this.idCaimon = idCaimon;
    return this;
  }

  /**
   * Get idCaimon
   * @return idCaimon
  */
  @ApiModelProperty(value = "")


  public String getIdCaimon() {
    return idCaimon;
  }

  public void setIdCaimon(String idCaimon) {
    this.idCaimon = idCaimon;
  }

  public UtilisateurFiliereFamilleSousFamilleDto poste(String poste) {
    this.poste = poste;
    return this;
  }

  /**
   * Get poste
   * @return poste
  */
  @ApiModelProperty(value = "")


  public String getPoste() {
    return poste;
  }

  public void setPoste(String poste) {
    this.poste = poste;
  }

  public UtilisateurFiliereFamilleSousFamilleDto organisation(String organisation) {
    this.organisation = organisation;
    return this;
  }

  /**
   * Get organisation
   * @return organisation
  */
  @ApiModelProperty(value = "")


  public String getOrganisation() {
    return organisation;
  }

  public void setOrganisation(String organisation) {
    this.organisation = organisation;
  }

  public UtilisateurFiliereFamilleSousFamilleDto groupeDeRole(String groupeDeRole) {
    this.groupeDeRole = groupeDeRole;
    return this;
  }

  /**
   * Get groupeDeRole
   * @return groupeDeRole
  */
  @ApiModelProperty(value = "")


  public String getGroupeDeRole() {
    return groupeDeRole;
  }

  public void setGroupeDeRole(String groupeDeRole) {
    this.groupeDeRole = groupeDeRole;
  }

  public UtilisateurFiliereFamilleSousFamilleDto role(RoleDto role) {
    this.role = role;
    return this;
  }

  /**
   * Get role
   * @return role
  */
  @ApiModelProperty(value = "")

  @Valid

  public RoleDto getRole() {
    return role;
  }

  public void setRole(RoleDto role) {
    this.role = role;
  }

  public UtilisateurFiliereFamilleSousFamilleDto filiereList(List<FiliereDto> filiereList) {
    this.filiereList = filiereList;
    return this;
  }

  public UtilisateurFiliereFamilleSousFamilleDto addFiliereListItem(FiliereDto filiereListItem) {
    if (this.filiereList == null) {
      this.filiereList = new ArrayList<>();
    }
    this.filiereList.add(filiereListItem);
    return this;
  }

  /**
   * Get filiereList
   * @return filiereList
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<FiliereDto> getFiliereList() {
    return filiereList;
  }

  public void setFiliereList(List<FiliereDto> filiereList) {
    this.filiereList = filiereList;
  }

  public UtilisateurFiliereFamilleSousFamilleDto image(String image) {
    this.image = image;
    return this;
  }

  /**
   * Get image
   * @return image
  */
  @ApiModelProperty(value = "")


  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UtilisateurFiliereFamilleSousFamilleDto utilisateurFiliereFamilleSousFamilleDto = (UtilisateurFiliereFamilleSousFamilleDto) o;
    return Objects.equals(this.idUtilisateur, utilisateurFiliereFamilleSousFamilleDto.idUtilisateur) &&
        Objects.equals(this.prenom, utilisateurFiliereFamilleSousFamilleDto.prenom) &&
        Objects.equals(this.adresseMail, utilisateurFiliereFamilleSousFamilleDto.adresseMail) &&
        Objects.equals(this.cp, utilisateurFiliereFamilleSousFamilleDto.cp) &&
        Objects.equals(this.idCaimon, utilisateurFiliereFamilleSousFamilleDto.idCaimon) &&
        Objects.equals(this.poste, utilisateurFiliereFamilleSousFamilleDto.poste) &&
        Objects.equals(this.organisation, utilisateurFiliereFamilleSousFamilleDto.organisation) &&
        Objects.equals(this.groupeDeRole, utilisateurFiliereFamilleSousFamilleDto.groupeDeRole) &&
        Objects.equals(this.role, utilisateurFiliereFamilleSousFamilleDto.role) &&
        Objects.equals(this.filiereList, utilisateurFiliereFamilleSousFamilleDto.filiereList) &&
        Objects.equals(this.image, utilisateurFiliereFamilleSousFamilleDto.image);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idUtilisateur, prenom, adresseMail, cp, idCaimon, poste, organisation, groupeDeRole, role, filiereList, image);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UtilisateurFiliereFamilleSousFamilleDto {\n");
    
    sb.append("    idUtilisateur: ").append(toIndentedString(idUtilisateur)).append("\n");
    sb.append("    prenom: ").append(toIndentedString(prenom)).append("\n");
    sb.append("    adresseMail: ").append(toIndentedString(adresseMail)).append("\n");
    sb.append("    cp: ").append(toIndentedString(cp)).append("\n");
    sb.append("    idCaimon: ").append(toIndentedString(idCaimon)).append("\n");
    sb.append("    poste: ").append(toIndentedString(poste)).append("\n");
    sb.append("    organisation: ").append(toIndentedString(organisation)).append("\n");
    sb.append("    groupeDeRole: ").append(toIndentedString(groupeDeRole)).append("\n");
    sb.append("    role: ").append(toIndentedString(role)).append("\n");
    sb.append("    filiereList: ").append(toIndentedString(filiereList)).append("\n");
    sb.append("    image: ").append(toIndentedString(image)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

