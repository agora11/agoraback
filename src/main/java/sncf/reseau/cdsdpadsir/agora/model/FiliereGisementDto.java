package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Getter
@Setter
public class FiliereGisementDto extends RessourceAbstraite {
	
	@JsonProperty("idFiliere")
	private Long idFiliere;

	@JsonProperty("description")
	private String description;

	@JsonProperty("nom")
	private String nom;

}
