package sncf.reseau.cdsdpadsir.agora.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.entity.SuperRessource;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * FiliereDto
 */
@Getter
@Setter
public class FiliereDto extends RessourceAbstraite {

	@JsonProperty("idFiliere")
	private Long idFiliere;

	@JsonProperty("description")
	private String description;

	@JsonProperty("responsableDeFiliere")
	private UtilisateurDto responsableDeFiliere;

	@JsonProperty("responsableDeFiliereDelegue")
	private UtilisateurDto responsableDeFiliereDelegue;

	@JsonProperty("coordinateurDeFiliere")
	private UtilisateurDto coordinateurDeFiliere;

	@JsonProperty("valideurDeFiliere")
	private List<ValideurDto> valideursDeFiliere;

	@JsonProperty("image")
	private String image;

	@JsonProperty("slug")
	private String slug;

	@JsonProperty("familles")
	private List<FamilleDto> familles = null;

	@JsonProperty("termes")
	private List<TermeDto> termes = null;

	@JsonProperty("classeDObjet")
	private List<ObjetDto> classeDObjet = null;

	@JsonProperty("nom")
	private String nom;

	@JsonProperty("superRessource")
	private List<SuperRessource> superRessource;

	@JsonProperty("gisements")
	private List<GisementDto> gisements;
}
