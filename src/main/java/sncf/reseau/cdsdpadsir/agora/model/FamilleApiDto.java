package sncf.reseau.cdsdpadsir.agora.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FamilleApiDto {
	
	@JsonProperty("id")
	private String id;

	@JsonProperty("nom")
	private String nom;
	
	@JsonProperty("sousFamilles")
	private List<FamilleApiDto> sousFamilles;
}
