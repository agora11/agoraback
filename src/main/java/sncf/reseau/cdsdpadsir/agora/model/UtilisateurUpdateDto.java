package sncf.reseau.cdsdpadsir.agora.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * UtilisateurUpdateDto
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-01-05T10:18:32.903+01:00[Europe/Paris]")
public class UtilisateurUpdateDto   {
  @JsonProperty("idUtilisateur")
  private Long idUtilisateur;

  @JsonProperty("prenom")
  private String prenom;

  @JsonProperty("nom")
  private String nom;

  @JsonProperty("adresseMail")
  private String adresseMail;

  @JsonProperty("cp")
  private String cp;

  @JsonProperty("idCaimon")
  private String idCaimon;

  @JsonProperty("poste")
  private String poste;

  @JsonProperty("organisation")
  private String organisation;

  @JsonProperty("groupeDeRole")
  private String groupeDeRole;

  @JsonProperty("role")
  private RoleDto role;

  @JsonProperty("filiereList")
  @Valid
  private List<FiliereDto> filiereList = null;

  public UtilisateurUpdateDto idUtilisateur(Long idUtilisateur) {
    this.idUtilisateur = idUtilisateur;
    return this;
  }

  /**
   * Get idUtilisateur
   * @return idUtilisateur
  */
  @ApiModelProperty(value = "")


  public Long getIdUtilisateur() {
    return idUtilisateur;
  }

  public void setIdUtilisateur(Long idUtilisateur) {
    this.idUtilisateur = idUtilisateur;
  }

  public UtilisateurUpdateDto prenom(String prenom) {
    this.prenom = prenom;
    return this;
  }

  /**
   * Get prenom
   * @return prenom
  */
  @ApiModelProperty(value = "")


  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public UtilisateurUpdateDto nom(String nom) {
    this.nom = nom;
    return this;
  }

  /**
   * Get nom
   * @return nom
  */
  @ApiModelProperty(value = "")


  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public UtilisateurUpdateDto adresseMail(String adresseMail) {
    this.adresseMail = adresseMail;
    return this;
  }

  /**
   * Get adresseMail
   * @return adresseMail
  */
  @ApiModelProperty(value = "")

@javax.validation.constraints.Email
  public String getAdresseMail() {
    return adresseMail;
  }

  public void setAdresseMail(String adresseMail) {
    this.adresseMail = adresseMail;
  }

  public UtilisateurUpdateDto cp(String cp) {
    this.cp = cp;
    return this;
  }

  /**
   * Get cp
   * @return cp
  */
  @ApiModelProperty(value = "")


  public String getCp() {
    return cp;
  }

  public void setCp(String cp) {
    this.cp = cp;
  }

  public UtilisateurUpdateDto idCaimon(String idCaimon) {
    this.idCaimon = idCaimon;
    return this;
  }

  /**
   * Get idCaimon
   * @return idCaimon
  */
  @ApiModelProperty(value = "")


  public String getIdCaimon() {
    return idCaimon;
  }

  public void setIdCaimon(String idCaimon) {
    this.idCaimon = idCaimon;
  }

  public UtilisateurUpdateDto poste(String poste) {
    this.poste = poste;
    return this;
  }

  /**
   * Get poste
   * @return poste
  */
  @ApiModelProperty(value = "")


  public String getPoste() {
    return poste;
  }

  public void setPoste(String poste) {
    this.poste = poste;
  }

  public UtilisateurUpdateDto organisation(String organisation) {
    this.organisation = organisation;
    return this;
  }

  /**
   * Get organisation
   * @return organisation
  */
  @ApiModelProperty(value = "")


  public String getOrganisation() {
    return organisation;
  }

  public void setOrganisation(String organisation) {
    this.organisation = organisation;
  }

  public UtilisateurUpdateDto groupeDeRole(String groupeDeRole) {
    this.groupeDeRole = groupeDeRole;
    return this;
  }

  /**
   * Get groupeDeRole
   * @return groupeDeRole
  */
  @ApiModelProperty(value = "")


  public String getGroupeDeRole() {
    return groupeDeRole;
  }

  public void setGroupeDeRole(String groupeDeRole) {
    this.groupeDeRole = groupeDeRole;
  }

  public UtilisateurUpdateDto role(RoleDto role) {
    this.role = role;
    return this;
  }

  /**
   * Get role
   * @return role
  */
  @ApiModelProperty(value = "")

  @Valid

  public RoleDto getRole() {
    return role;
  }

  public void setRole(RoleDto role) {
    this.role = role;
  }

  public UtilisateurUpdateDto filiereList(List<FiliereDto> filiereList) {
    this.filiereList = filiereList;
    return this;
  }

  public UtilisateurUpdateDto addFiliereListItem(FiliereDto filiereListItem) {
    if (this.filiereList == null) {
      this.filiereList = new ArrayList<>();
    }
    this.filiereList.add(filiereListItem);
    return this;
  }

  /**
   * Get filiereList
   * @return filiereList
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<FiliereDto> getFiliereList() {
    return filiereList;
  }

  public void setFiliereList(List<FiliereDto> filiereList) {
    this.filiereList = filiereList;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UtilisateurUpdateDto utilisateurUpdateDto = (UtilisateurUpdateDto) o;
    return Objects.equals(this.idUtilisateur, utilisateurUpdateDto.idUtilisateur) &&
        Objects.equals(this.prenom, utilisateurUpdateDto.prenom) &&
        Objects.equals(this.nom, utilisateurUpdateDto.nom) &&
        Objects.equals(this.adresseMail, utilisateurUpdateDto.adresseMail) &&
        Objects.equals(this.cp, utilisateurUpdateDto.cp) &&
        Objects.equals(this.idCaimon, utilisateurUpdateDto.idCaimon) &&
        Objects.equals(this.poste, utilisateurUpdateDto.poste) &&
        Objects.equals(this.organisation, utilisateurUpdateDto.organisation) &&
        Objects.equals(this.groupeDeRole, utilisateurUpdateDto.groupeDeRole) &&
        Objects.equals(this.role, utilisateurUpdateDto.role) &&
        Objects.equals(this.filiereList, utilisateurUpdateDto.filiereList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idUtilisateur, prenom, nom, adresseMail, cp, idCaimon, poste, organisation, groupeDeRole, role, filiereList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UtilisateurUpdateDto {\n");
    
    sb.append("    idUtilisateur: ").append(toIndentedString(idUtilisateur)).append("\n");
    sb.append("    prenom: ").append(toIndentedString(prenom)).append("\n");
    sb.append("    nom: ").append(toIndentedString(nom)).append("\n");
    sb.append("    adresseMail: ").append(toIndentedString(adresseMail)).append("\n");
    sb.append("    cp: ").append(toIndentedString(cp)).append("\n");
    sb.append("    idCaimon: ").append(toIndentedString(idCaimon)).append("\n");
    sb.append("    poste: ").append(toIndentedString(poste)).append("\n");
    sb.append("    organisation: ").append(toIndentedString(organisation)).append("\n");
    sb.append("    groupeDeRole: ").append(toIndentedString(groupeDeRole)).append("\n");
    sb.append("    role: ").append(toIndentedString(role)).append("\n");
    sb.append("    filiereList: ").append(toIndentedString(filiereList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

