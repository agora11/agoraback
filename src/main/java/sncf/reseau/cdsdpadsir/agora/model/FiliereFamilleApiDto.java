package sncf.reseau.cdsdpadsir.agora.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FiliereFamilleApiDto {
	
	@JsonProperty("id")
	private String id;

	@JsonProperty("nom")
	private String nom;

	@JsonProperty("familles")
	private List<FamilleApiDto> familles;


}
