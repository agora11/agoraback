package sncf.reseau.cdsdpadsir.agora.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActeurApiDto {

	@JsonProperty("id")
	private String id;
	
	@JsonProperty("cp")
	private String cp;

	@JsonProperty("prenom")
	private String prenom;

	@JsonProperty("nom")
	private String nom;

	@JsonProperty("adresseMail")
	private String adresseMail;

	@JsonProperty("roles")
	private List<RoleApiDto> roles;
	
	@JsonProperty("filieres")
	private List<String> filieres;
	
}
