package sncf.reseau.cdsdpadsir.agora.model;

import java.io.Serializable;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeAttributClasseDto extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@JsonProperty("idTypeAttributClasse")
	private Long idTypeAttributClasse;

	@JsonProperty("code")
	private String code;

	@JsonProperty("type")
	private String type;

	@JsonProperty("attributClasses")
	private Set<AttributClasseDto> attributClasses;

	@JsonProperty("typeElementModeleObjet")
	private TypeElementModeleObjetDto typeElementModeleObjet;

}