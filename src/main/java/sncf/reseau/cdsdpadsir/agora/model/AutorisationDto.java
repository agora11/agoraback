package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Getter
@Setter
public class AutorisationDto extends RessourceAbstraite {
	
	@JsonProperty("idAutorisation")
	private Long idAutorisation;

	@JsonProperty("profilAutorisation")
	private ProfilAutorisationDto profilAutorisation;
	
	@JsonProperty("filiere")
	private FiliereDto filiere;

}
