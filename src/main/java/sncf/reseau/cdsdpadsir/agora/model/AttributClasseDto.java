package sncf.reseau.cdsdpadsir.agora.model;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttributClasseDto extends RessourceAbstraite {
	private static final Long serialVersionUID = 1L;

	@JsonProperty("idAttributClasse")
	private Long idAttributClasse;

	@JsonProperty("libelle")
	private String libelle;

	@JsonProperty("valeur")
	private String valeur;

	@JsonProperty("classeDobjet")
	private ClasseDobjetDto classeDobjet;

	@JsonProperty("typeAttributClasse")
	private TypeAttributClasseDto typeAttributClasse;

	@JsonProperty("version")
	private String version;
	
	@JsonProperty("revision_fonctionnel")
	private Timestamp revisionFonctionnel ;
	
	@JsonProperty("typeElementModeleObjet")
	private TypeElementModeleObjetDto typeElementModeleObjet;

}