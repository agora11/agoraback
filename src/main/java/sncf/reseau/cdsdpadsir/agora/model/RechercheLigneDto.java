package sncf.reseau.cdsdpadsir.agora.model;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class RechercheLigneDto {

	private String motRecheche;
	private Map<String, String> motifRecherche;
	private Object object;

	public RechercheLigneDto() {
		this.motifRecherche = new HashMap<>();
	}

}
