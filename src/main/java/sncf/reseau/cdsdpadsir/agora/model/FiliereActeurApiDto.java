package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FiliereActeurApiDto {
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("nom")
	private String nom;
	
}
