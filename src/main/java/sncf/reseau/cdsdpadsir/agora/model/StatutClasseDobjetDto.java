package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Getter
@Setter
public class StatutClasseDobjetDto extends RessourceAbstraite {

	@JsonProperty("idClasseDobjet")
	private Long idClasseDobjet;

	@JsonProperty("valideur")
	private UtilisateurDto valideur;

	@JsonProperty("suppleant")
	private UtilisateurDto suppleant;

	@JsonProperty("libelle")
	private String libelle;

	@JsonProperty("commentaire")
	private String commentaire;

	@JsonProperty("nom")
	private String nom;

	@JsonProperty("prenom")
	private String prenom;

	public String getNomComplet() {
		return libelle.equals("Validé") || libelle.equals("Refusé") ? prenom + " " + nom
				: valideur.getPrenom() + " " + valideur.getNom();
	}
}
