package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValideurDto extends UtilisateurDto {
	
	@JsonProperty("ordre")
	Integer ordre;

}
