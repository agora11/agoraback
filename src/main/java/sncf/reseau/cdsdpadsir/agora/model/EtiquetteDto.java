package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EtiquetteDto {
	
	@JsonProperty("id_etiquette	")
	private Long idEtiquette;

	@JsonProperty("libelle")
	private String libelle;

}
