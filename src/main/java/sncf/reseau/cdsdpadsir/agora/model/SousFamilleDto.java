package sncf.reseau.cdsdpadsir.agora.model;

import java.util.List;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * SousFamilleDto
 */
@Getter
@Setter
public class SousFamilleDto extends RessourceAbstraite  {
  @JsonProperty("idSousFamille")
  private Long idSousFamille;

  @JsonProperty("idReferent")
  private String idReferent;

  @JsonProperty("referentsMetier")
  @Valid
  private List<UtilisateursCasDUsageDto> referentsMetier = null;
}

