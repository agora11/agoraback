package sncf.reseau.cdsdpadsir.agora.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * FamilleDto
 */
@Getter
@Setter
public class FamilleDto extends RessourceAbstraite implements Comparable {

	@JsonProperty("idFamille")
	private Long idFamille;

	@JsonProperty("nom")
	private String nom;

	@JsonProperty("responsableDeDonnees")
	UtilisateurDto responsableDeDonnees;

	@JsonProperty("objets")
	private List<ObjetDto> objets;

	@JsonProperty("termes")
	private List<ObjetDto> termes;

	@JsonProperty("filiere")
	private FiliereDto filiere;

	@JsonProperty("familleMere")
	private FamilleDto familleMere = null;

	@JsonProperty("sousFamilles")
	private List<FamilleDto> sousFamilles = null;

	@Override
	public int compareTo(Object o) {
		FamilleDto compareTo = (FamilleDto) o;
		return nom.toLowerCase().compareTo(compareTo.nom.toLowerCase());
	}

}
