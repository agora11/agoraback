package sncf.reseau.cdsdpadsir.agora.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClasseDobjetDto extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@JsonProperty("idClasseDobjet")	
	private Long idClasseDobjet;

	@JsonProperty( "abreviation")
	private String abreviation;

	@JsonProperty("commentaire")
	private String commentaire;

	@JsonProperty( "libelle")
	private String libelle;
	
	@JsonProperty("paquet")
	private PaquetDto paquet;
	
	@JsonProperty("version")
	private String version ;
	
	@JsonProperty("revision_fonctionnel")
	private Timestamp revisionFonctionnel ;
	
	@JsonProperty("attributClasses")
	private Set<AttributClasseDto> attributClasses;
		
	@JsonProperty("typeElementModeleObjet")    
	private TypeElementModeleObjetDto typeElementModeleObjet;


	
}