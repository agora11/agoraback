package sncf.reseau.cdsdpadsir.agora.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Getter
@Setter
public class PolitiqueQualiteDto extends RessourceAbstraite {
	
	@JsonProperty("idPolitiqueQualite")
	private Long idPolitiqueQualite;
	
	@JsonProperty("indicateurDeQualite")
	private String indicateurDeQualite;
	
	@JsonProperty("description")
	private String description;

	@JsonProperty("tauxPromis")
	private String tauxPromis;
	
	@JsonProperty("url")
	private String url;

	@JsonProperty("politiqueQualiteParUsages")
	private List<PolitiqueQualiteParUsageDto> politiqueQualiteParUsages;

	@JsonProperty("tauxQualiteMesures")
	private List<TauxQualiteMesureDto> tauxQualiteMesures;
	
	

}
