package sncf.reseau.cdsdpadsir.agora.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.entity.Role;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Getter
@Setter
public class SuperRessourceDto extends RessourceAbstraite {

	@JsonProperty("id_super_ressource")
	private long idSuperRessource;
	
	@JsonProperty("filiere")
	private FiliereDto filiere;
	
	@JsonProperty("profil_autorisation")
	private List<ProfilAutorisationDto> profilAutorisation;
	
	@JsonProperty("roles")
	private List<Role> roles;
	

}
