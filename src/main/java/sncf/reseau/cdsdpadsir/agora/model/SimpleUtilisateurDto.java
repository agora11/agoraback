package sncf.reseau.cdsdpadsir.agora.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * SimpleUtilisateurDto
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-01-05T10:18:32.903+01:00[Europe/Paris]")
public class SimpleUtilisateurDto   {
  @JsonProperty("idUtilisateur")
  private Long idUtilisateur;

  @JsonProperty("prenom")
  private String prenom;

  @JsonProperty("adresseMail")
  private String adresseMail;

  @JsonProperty("cp")
  private String cp;

  @JsonProperty("poste")
  private String poste;

  @JsonProperty("organisation")
  private String organisation;

  public SimpleUtilisateurDto idUtilisateur(Long idUtilisateur) {
    this.idUtilisateur = idUtilisateur;
    return this;
  }

  /**
   * Get idUtilisateur
   * @return idUtilisateur
  */
  @ApiModelProperty(value = "")


  public Long getIdUtilisateur() {
    return idUtilisateur;
  }

  public void setIdUtilisateur(Long idUtilisateur) {
    this.idUtilisateur = idUtilisateur;
  }

  public SimpleUtilisateurDto prenom(String prenom) {
    this.prenom = prenom;
    return this;
  }

  /**
   * Get prenom
   * @return prenom
  */
  @ApiModelProperty(value = "")


  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public SimpleUtilisateurDto adresseMail(String adresseMail) {
    this.adresseMail = adresseMail;
    return this;
  }

  /**
   * Get adresseMail
   * @return adresseMail
  */
  @ApiModelProperty(value = "")

@javax.validation.constraints.Email
  public String getAdresseMail() {
    return adresseMail;
  }

  public void setAdresseMail(String adresseMail) {
    this.adresseMail = adresseMail;
  }

  public SimpleUtilisateurDto cp(String cp) {
    this.cp = cp;
    return this;
  }

  /**
   * Get cp
   * @return cp
  */
  @ApiModelProperty(value = "")


  public String getCp() {
    return cp;
  }

  public void setCp(String cp) {
    this.cp = cp;
  }

  public SimpleUtilisateurDto poste(String poste) {
    this.poste = poste;
    return this;
  }

  /**
   * Get poste
   * @return poste
  */
  @ApiModelProperty(value = "")


  public String getPoste() {
    return poste;
  }

  public void setPoste(String poste) {
    this.poste = poste;
  }

  public SimpleUtilisateurDto organisation(String organisation) {
    this.organisation = organisation;
    return this;
  }

  /**
   * Get organisation
   * @return organisation
  */
  @ApiModelProperty(value = "")


  public String getOrganisation() {
    return organisation;
  }

  public void setOrganisation(String organisation) {
    this.organisation = organisation;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SimpleUtilisateurDto simpleUtilisateurDto = (SimpleUtilisateurDto) o;
    return Objects.equals(this.idUtilisateur, simpleUtilisateurDto.idUtilisateur) &&
        Objects.equals(this.prenom, simpleUtilisateurDto.prenom) &&
        Objects.equals(this.adresseMail, simpleUtilisateurDto.adresseMail) &&
        Objects.equals(this.cp, simpleUtilisateurDto.cp) &&
        Objects.equals(this.poste, simpleUtilisateurDto.poste) &&
        Objects.equals(this.organisation, simpleUtilisateurDto.organisation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idUtilisateur, prenom, adresseMail, cp, poste, organisation);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SimpleUtilisateurDto {\n");
    
    sb.append("    idUtilisateur: ").append(toIndentedString(idUtilisateur)).append("\n");
    sb.append("    prenom: ").append(toIndentedString(prenom)).append("\n");
    sb.append("    adresseMail: ").append(toIndentedString(adresseMail)).append("\n");
    sb.append("    cp: ").append(toIndentedString(cp)).append("\n");
    sb.append("    poste: ").append(toIndentedString(poste)).append("\n");
    sb.append("    organisation: ").append(toIndentedString(organisation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

