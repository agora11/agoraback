package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeElementModeleObjetDto extends RessourceAbstraite {

	@JsonProperty("idTypeElementModeleObjet")
	private long idTypeElementModeleObjet;

	@JsonProperty("libelle")
	private String libelle;

}
