package sncf.reseau.cdsdpadsir.agora.model;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RelationClasseMereDto extends RessourceAbstraite{
	
	@JsonProperty("idRelationClasseMere")
	private Long idRelationClasseMere;

	@JsonProperty("classeDobjeMere")
	private ClasseDobjetDto classeDobjeMere;

	@JsonProperty("classeDobjetFille")
	private ClasseDobjetDto classeDobjetFille;
	
	@JsonProperty("typeElementModeleObjet")    
	private TypeElementModeleObjetDto typeElementModeleObjet;
	
	@JsonProperty("version")
	private String version;
	
	@JsonProperty("revision_fonctionnel")
	private Timestamp revisionFonctionnel ;


}
