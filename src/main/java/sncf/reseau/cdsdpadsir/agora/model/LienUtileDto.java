package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Getter
@Setter
public class LienUtileDto extends RessourceAbstraite {

	@JsonProperty(value = "idLienUtile")
	private long idLienUtile;

	@JsonProperty(value = "libelle")
	private String libelle;

	@JsonProperty(value = "url")
	private String url;
}
