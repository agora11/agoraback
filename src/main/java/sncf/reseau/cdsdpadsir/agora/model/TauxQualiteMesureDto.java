package sncf.reseau.cdsdpadsir.agora.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Getter
@Setter
public class TauxQualiteMesureDto extends RessourceAbstraite implements Comparable<TauxQualiteMesureDto> {

	@JsonProperty("idTauxQualiteMesure")
	private Long idTauxQualiteMesure;

	@JsonProperty("dateDeMesure")
	private String dateDeMesure;

	@JsonProperty("tauxMesure")
	private String tauxMesure;

	@Override
	public int compareTo(TauxQualiteMesureDto o) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		if (this.dateDeMesure == null)
			return 1;

		if (o.dateDeMesure == null)
			return -1;

		try {
			return sdf.parse(this.dateDeMesure).compareTo(sdf.parse(o.dateDeMesure));
		} catch (ParseException e) {
			return 0;
		}
	}

}
