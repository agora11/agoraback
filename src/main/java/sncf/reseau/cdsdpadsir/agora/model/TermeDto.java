package sncf.reseau.cdsdpadsir.agora.model;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * TermeDto
 */
@Getter
@Setter
public class TermeDto extends RessourceAbstraite {

	@JsonProperty("idTerme")
	private Long idTerme;

	@JsonProperty("abreviation")
	private String abreviation;

	@JsonProperty("commentaire")
	private String commentaire;

	@JsonProperty("libelle")
	private String libelle;

	@JsonProperty("objets")
	private Set<ObjetDto> objets;

	@JsonProperty("documents")
	private Set<DocumentDto> documents;

	@JsonProperty("filieres")
	private Set<FiliereDto> filieres;

	@JsonProperty("utilisateurs")
	private Set<UtilisateurDto> utilisateurs;
	
	@JsonProperty("definitions")
	private Set<DefinitionDto> definitions;

	@JsonProperty("statut")
	private StatutTermeDto statut;
	
	@JsonProperty("proprietaire")
	private UtilisateurDto proprietaire;

}
