package sncf.reseau.cdsdpadsir.agora.model;

import java.util.List;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.entity.SuperRessource;

/**
 * UtilisateurDto
 */
@Getter
@Setter
public class UtilisateurDto {
	@JsonProperty("idUtilisateur")
	private Long idUtilisateur;

	@JsonProperty("idIdentiteNumerique")
	private Long idIdentiteNumerique;

	@JsonProperty("prenom")
	private String prenom;

	@JsonProperty("nom")
	private String nom;

	@JsonProperty("nomComplet")
	private String nomComplet;

	@JsonProperty("id")
	private String id;

	@JsonProperty("adresseMail")
	private String adresseMail;

	@JsonProperty("cp")
	private String cp;

	@JsonProperty("revision")
	private String revision;

	@JsonProperty("idCaimon")
	private String idCaimon;

	@JsonProperty("poste")
	private String poste;

	@JsonProperty("organisation")
	private String organisation;

	@JsonProperty("roles")
	private List<RoleDto> roles;

	@JsonProperty("familleRoles")
	private List<GroupeRoleDto> familleRoles;

	@JsonProperty("filieresResponsable")
	@Valid
	private List<FiliereDto> filieresResponsable = null;

	@JsonProperty("filieresResponsableDelegue")
	@Valid
	private List<FiliereDto> filieresResponsableDelegue = null;

	@JsonProperty("filieresCoordinateur")
	@Valid
	private List<FiliereDto> filieresCoordinateur = null;

	@JsonProperty("superRessource")
	@Valid
	private List<SuperRessource> superRessource;

	@JsonProperty("autorisations")
	private List<AutorisationDto> autorisations;
	
	@JsonProperty("isAdmin")
	private boolean isAdmin;

	public UtilisateurDto idUtilisateur(Long idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
		return this;
	}

	public String getNomComplet() {
		return prenom + " " + nom;
	}

}
