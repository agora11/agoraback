package sncf.reseau.cdsdpadsir.agora.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * GroupeRoleRolesUtilisateursDto
 */
public class GroupeRoleRolesUtilisateursDto   {
	
  @JsonProperty("idGroupeRole")
  private Long idGroupeRole;

  @JsonProperty("roleUtilisateurs")
  @Valid
  private List<RoleUtilisateursDto> roleUtilisateurs = null;

  public GroupeRoleRolesUtilisateursDto idGroupeRole(Long idGroupeRole) {
    this.idGroupeRole = idGroupeRole;
    return this;
  }

  /**
   * Get idGroupeRole
   * @return idGroupeRole
  */
  @ApiModelProperty(value = "")


  public Long getIdGroupeRole() {
    return idGroupeRole;
  }

  public void setIdGroupeRole(Long idGroupeRole) {
    this.idGroupeRole = idGroupeRole;
  }

  public GroupeRoleRolesUtilisateursDto roleUtilisateurs(List<RoleUtilisateursDto> roleUtilisateurs) {
    this.roleUtilisateurs = roleUtilisateurs;
    return this;
  }

  public GroupeRoleRolesUtilisateursDto addRoleUtilisateursItem(RoleUtilisateursDto roleUtilisateursItem) {
    if (this.roleUtilisateurs == null) {
      this.roleUtilisateurs = new ArrayList<>();
    }
    this.roleUtilisateurs.add(roleUtilisateursItem);
    return this;
  }

  /**
   * Get roleUtilisateurs
   * @return roleUtilisateurs
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<RoleUtilisateursDto> getRoleUtilisateurs() {
    return roleUtilisateurs;
  }

  public void setRoleUtilisateurs(List<RoleUtilisateursDto> roleUtilisateurs) {
    this.roleUtilisateurs = roleUtilisateurs;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GroupeRoleRolesUtilisateursDto groupeRoleRolesUtilisateursDto = (GroupeRoleRolesUtilisateursDto) o;
    return Objects.equals(this.idGroupeRole, groupeRoleRolesUtilisateursDto.idGroupeRole) &&
        Objects.equals(this.roleUtilisateurs, groupeRoleRolesUtilisateursDto.roleUtilisateurs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idGroupeRole, roleUtilisateurs);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GroupeRoleRolesUtilisateursDto {\n");
    
    sb.append("    idGroupeRole: ").append(toIndentedString(idGroupeRole)).append("\n");
    sb.append("    roleUtilisateurs: ").append(toIndentedString(roleUtilisateurs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

