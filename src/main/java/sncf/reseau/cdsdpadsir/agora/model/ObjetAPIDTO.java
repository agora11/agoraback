package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class ObjetAPIDTO {

    @JsonProperty("id")
    private String id;

    @JsonProperty("libelle")
    private String libelle;

    @JsonProperty("abreviation")
    private String abreviation;

    @JsonProperty("commentaire")
    private String commentaire;

    @JsonProperty("definition")
    private String definition;

    @JsonProperty("filiere")
    private FiliereStructApi filiere;

    @JsonProperty("famille")
    private FamilleStructApi famille;

    @JsonProperty("sous-famille")
    private FamilleStructApi sousFamille;

    @JsonProperty("termes")
    private Set<ObjetAssocAPIDTO> termes;

    @JsonProperty("attributs")
    private Set<AttributAPIDTO> attributClasses;
}
