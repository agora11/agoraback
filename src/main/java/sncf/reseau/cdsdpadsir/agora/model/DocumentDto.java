package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * DocumentDto
 */
@Getter
@Setter
public class DocumentDto extends RessourceAbstraite {
	
	@JsonProperty("id_document")
	private Long idDocument;

	@JsonProperty("url")
	private String url;

	@JsonProperty("titre")
	private String titre;
}
