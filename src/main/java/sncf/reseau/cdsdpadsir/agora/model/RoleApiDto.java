package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleApiDto {

	@JsonProperty("id")
	private String id;

	@JsonProperty("codeRoleFonctionnel")
	private String codeRoleFonctionnel;

	@JsonProperty("libelle")
	private String nom;

}
