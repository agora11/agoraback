package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FiliereApiDto {
	

	@JsonProperty("id")
	private String id;
	
	@JsonProperty("nom")
	private String nom;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("image")
	private String image;
	
	@JsonProperty("responsableDeFiliere")
	private UtilisateurApiDto responsableDeFiliere;

	@JsonProperty("responsableDeFiliereDelegue")
	private UtilisateurApiDto responsableDeFiliereDelegue;

	@JsonProperty("coordinateurDeFiliere")
	private UtilisateurApiDto coordinateurDeFiliere;

	


}
