package sncf.reseau.cdsdpadsir.agora.model;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * ObjetDto
 */
@Getter
@Setter
public class ObjetDto extends RessourceAbstraite {
	
	
	@JsonProperty("idClasseDobjet")
	private Long idClasseDobjet;

	@JsonProperty("abreviation")
	private String abreviation;

	@JsonProperty("commentaire")
	private String commentaire;

	@JsonProperty("libelle")
	private String libelle;

	@JsonProperty("familles")
	private Set<FamilleDto> familles;
	
	@JsonProperty("sousFamilles")
	private Set<FamilleDto> sousFamilles;

	@JsonProperty("filieres")
	private Set<FiliereDto> filieres;

	@JsonProperty("termes")
	private Set<TermeDto> termes;

	@JsonProperty("attributs")
	private Set<AttributDto> attributClasses;

	@JsonProperty("definitions")
	private Set<DefinitionDto> definitions;
	
	@JsonProperty("responsables")
	private Set<UtilisateurDto> responsables;
	
	@JsonProperty("statut")
	private StatutClasseDobjetDto statut;
	
	@JsonProperty("proprietaire")
	private UtilisateurDto proprietaire;
	
	@JsonProperty("critereQualites")
	private Set<CritereQualiteDto> critereQualites; 
	
	@JsonProperty("criteresToDelete")
	private Set<CritereQualiteDto> criteresToDelete; 
	
	
}
