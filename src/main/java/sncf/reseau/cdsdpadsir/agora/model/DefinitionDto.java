package sncf.reseau.cdsdpadsir.agora.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DefinitionDto {
	
	@JsonProperty("id_definition")
	private Long idDefinitionClasseDobjet;

	@JsonProperty("definition")
	private String definition;

}
