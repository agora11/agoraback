package sncf.reseau.cdsdpadsir.agora.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

@Getter
@Setter
public class CritereQualiteDto extends RessourceAbstraite {

	@JsonProperty("idCritereQualite")
	private Long idCritereQualite;

	@JsonProperty("libelle")
	private String libelle;

	@JsonProperty("url")
	private String url;

	@JsonProperty("politiqueQualites")
	private List<PolitiqueQualiteDto> politiqueQualites;

}
