package sncf.reseau.cdsdpadsir.agora.xmi.depot;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import sncf.reseau.cdsdpadsir.agora.xmi.exception.XmiDepotFileException;

@Service
public class LocalRepositoryFile implements RepositoryFileService{

	@Autowired
	private Environment env ;
	
	@Override
	public Optional<List<File>> getXmiFiles() throws XmiDepotFileException {
		
		String pathString = env.getProperty("local_repository") ;
		Optional<List<File>> files;
		try {
			files = FileUtil.getXmlFiles(new File(pathString));
		} catch (IOException e) {
			throw new XmiDepotFileException("erreur de récupération à partir du depot local", e);
		}
		return files ;
		
		
	}

}
