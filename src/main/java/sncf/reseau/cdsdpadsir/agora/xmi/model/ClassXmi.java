package sncf.reseau.cdsdpadsir.agora.xmi.model;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClassXmi implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String classId ;
	private String className; 
	private String classScope;
	private String classType; 
	private String packageId ;
	private String created;
	private String modified ;
	private List<AttributeClassXmi> attributes ;
	private List<LinkClassXmi> links ;
	private VersionXmi version ;

}
