package sncf.reseau.cdsdpadsir.agora.xmi.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttributeClassXmi implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String idAttribut ;
	private String nameProperty ;
	private String scope ;	
	private String typeProperty ;
	private String attachedClassId ; 
	private String created ;
	private String modified ;
	private VersionXmi version ;
	
}
