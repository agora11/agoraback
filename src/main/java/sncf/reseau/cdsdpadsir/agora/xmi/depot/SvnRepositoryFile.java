package sncf.reseau.cdsdpadsir.agora.xmi.depot;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.wc.SVNWCUtil;
import org.tmatesoft.svn.core.wc2.SvnCheckout;
import org.tmatesoft.svn.core.wc2.SvnOperationFactory;
import org.tmatesoft.svn.core.wc2.SvnTarget;

import sncf.reseau.cdsdpadsir.agora.xmi.exception.XmiDepotFileException;

@Service
public class SvnRepositoryFile implements RepositoryFileService {

	private static Logger logger = LoggerFactory.getLogger(SvnRepositoryFile.class);
	private static final  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	
	@Autowired
	private Environment env ;
	
	@Override
	public Optional<List<File>> getXmiFiles() throws XmiDepotFileException {
		Optional<List<File>> files ;
		
		
		File file = new File(DIRETORTY_FILE);				
		try {
			setupLibrary();
			cloneRepository() ;
			files = FileUtil.getXmlFiles(file);
		} catch (IOException | SVNException e) {
			throw new XmiDepotFileException("erreur de récupération à partir du depot svn", e);
		}
		return files ;
		
	}
	
	private void setupLibrary() {
        /*
         * For using over http:// and https://
         */
        DAVRepositoryFactory.setup();
        /*
         * For using over svn:// and svn+xxx://
         */
        SVNRepositoryFactoryImpl.setup();
        
        /*
         * For using over file:///
         */
        FSRepositoryFactory.setup();
    }
	
	private void cloneRepository() throws SVNException {
    	
		logger.info("svn repository start clone at --> {} " ,  LocalDateTime.now().format(formatter));
        String workingCopyDirectory = DIRETORTY_FILE ;
        
        final SvnOperationFactory svnOperationFactory = new SvnOperationFactory();        
        final ISVNAuthenticationManager authenticationManager = SVNWCUtil.createDefaultAuthenticationManager();
        SVNURL svnUrl = SVNURL.fromFile(new File(env.getProperty("svn_repository_remote_url"))) ;
        svnOperationFactory.setAuthenticationManager(authenticationManager);
        try {
            final SvnCheckout checkout = svnOperationFactory.createCheckout();
            
            checkout.getAuthenticationManager();
            checkout.setSingleTarget(SvnTarget.fromFile(new File(workingCopyDirectory)));
            checkout.setSource(SvnTarget.fromURL(svnUrl));
            //... other options
            checkout.run();
        } finally {
            svnOperationFactory.dispose();
        }
        logger.info("svn repository end clone at --> {} " ,  LocalDateTime.now().format(formatter));
    }
	
}
