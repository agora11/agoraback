package sncf.reseau.cdsdpadsir.agora.xmi.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LinkClassXmi implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String linkId ;
	private String linkName;
	private String startId;
	private String endId;
	private String attachedClassId ; 
	private String created ;
	private String modified ;
	private VersionXmi version ;

}
