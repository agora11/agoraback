package sncf.reseau.cdsdpadsir.agora.xmi.depot;

import java.io.File;
import java.util.List;
import java.util.Optional;

import sncf.reseau.cdsdpadsir.agora.xmi.exception.XmiDepotFileException;

public interface RepositoryFileService {
	
	public static final String DIRETORTY_FILE = "xmiRepository" ;
	
	public Optional<List<File>> getXmiFiles() throws XmiDepotFileException;
	

}
