package sncf.reseau.cdsdpadsir.agora.xmi.exception;

import java.util.function.Consumer;
import java.util.function.Function;

public class XmiExceptionUtil {
	
	public static <T , R , E extends Exception>	Function<T, R> wrapperExceptionFunction( FunctionWithException<T, R, Exception> fe) {
		return arg -> {
			try {
				return fe.apply(arg);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		};
		
	}
	
	public static <T , E extends Exception> Consumer<T> wrapperExceptionConsumer(ConsumerWithException<T,E> fe){
		return obj -> {
			try {
				fe.accept(obj);
			} catch (Exception e) {
				throw new RuntimeException(e);				
			}
			
		};
	}
	

}
