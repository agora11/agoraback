package sncf.reseau.cdsdpadsir.agora.xmi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class XmiFileNameException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public XmiFileNameException(String message) {
		super(message);
	}
	
	public XmiFileNameException(String message , Throwable cause) {
		super(message);
	}
	

}
