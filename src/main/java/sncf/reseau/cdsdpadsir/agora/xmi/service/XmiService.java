package sncf.reseau.cdsdpadsir.agora.xmi.service;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import sncf.reseau.cdsdpadsir.agora.model.AttributClasseDto;
import sncf.reseau.cdsdpadsir.agora.model.ClasseDobjetDto;
import sncf.reseau.cdsdpadsir.agora.model.PaquetDto;
import sncf.reseau.cdsdpadsir.agora.model.RelationClasseDobjetDto;
import sncf.reseau.cdsdpadsir.agora.model.RelationClasseMereDto;
import sncf.reseau.cdsdpadsir.agora.model.TypeAttributClasseDto;
import sncf.reseau.cdsdpadsir.agora.model.TypeElementModeleObjetDto;
import sncf.reseau.cdsdpadsir.agora.service.AttributClasseService;
import sncf.reseau.cdsdpadsir.agora.service.ClasseDobjetService;
import sncf.reseau.cdsdpadsir.agora.service.PaquetService;
import sncf.reseau.cdsdpadsir.agora.service.RelationClasseDobjetService;
import sncf.reseau.cdsdpadsir.agora.service.RelationClasseMereService;
import sncf.reseau.cdsdpadsir.agora.xmi.depot.RepositoryFileService;
import sncf.reseau.cdsdpadsir.agora.xmi.depot.WebServiceRepositoryFile;
import sncf.reseau.cdsdpadsir.agora.xmi.exception.XmiDepotFileException;
import sncf.reseau.cdsdpadsir.agora.xmi.exception.XmiExceptionUtil;
import sncf.reseau.cdsdpadsir.agora.xmi.exception.XmiParsingException;
import sncf.reseau.cdsdpadsir.agora.xmi.model.ClassXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.ConnectorBorderXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.ConnectorLinkXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.MutliplicityXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.PackageXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.XmiDetailImport;
import sncf.reseau.cdsdpadsir.agora.xmi.model.XmiInformation;
import sncf.reseau.cdsdpadsir.agora.xmi.util.XmiUtilStream;

@Service
@Transactional
public class XmiService {

	private static Logger logger = LoggerFactory.getLogger(XmiService.class);
	private static String REPOSITORY_FILE_LOCAL ="local" ;
	private static String REPOSITORY_FILE_SVN ="svn" ;
	private static String REPOSITORY_FILE_GIT ="git" ;
	private static String REPOSITORY_FILE_WS ="ws" ;
	
	@Autowired
	private Environment env ;
	
	@Autowired
	private PaquetService paquetService ;

	@Autowired
	private ClasseDobjetService classeDobjetService;

	@Autowired
	private RelationClasseDobjetService relationClasseDobjetService;

	@Autowired
	private RelationClasseMereService relationClasseMereService;	
	
	@Autowired
	private AttributClasseService attributClasseService ;
		
	@Autowired
	@Qualifier(value = "localRepositoryFile")
	private RepositoryFileService respositoryFileServiceLocal ;
	
	@Autowired
	@Qualifier(value = "gitRepositoryFile")
	private RepositoryFileService respositoryFileServiceGit ;
	
	@Autowired
	@Qualifier(value = "svnRepositoryFile")
	private RepositoryFileService respositoryFileServiceSvn ;
	
	@Autowired
	private WebServiceRepositoryFile respositoryFileServiceWs ;
		
	private static final  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	public List<XmiDetailImport> loadXmiStructre(TypeElementModeleObjetDto typeElementModele , MultipartFile[] filesMultipart) throws XmiDepotFileException  , RuntimeException  {
	
		Optional<List<File>> files = null;
		List<XmiDetailImport> listXmiDetailImport = null;
		logger.info("Start load Xmi at ---> {}", LocalDateTime.now().format(formatter));

		if (env.getProperty("repository_file_selected").equals(REPOSITORY_FILE_LOCAL)) {
			files = respositoryFileServiceLocal.getXmiFiles();
		}	
		if (env.getProperty("repository_file_selected").equals(REPOSITORY_FILE_GIT)) {
			files = respositoryFileServiceGit.getXmiFiles();
		}
		if (env.getProperty("repository_file_selected").equals(REPOSITORY_FILE_SVN)) {
			files = respositoryFileServiceSvn.getXmiFiles();
		}
		if (env.getProperty("repository_file_selected").equals(REPOSITORY_FILE_WS)) {
			files = respositoryFileServiceWs.getXmiFiles(filesMultipart);
		}

		if (null !=files && files.isPresent() && !files.get().isEmpty()) {
			listXmiDetailImport = files.get().stream().map(XmiExceptionUtil.wrapperExceptionFunction(file -> {
				XmiDetailImport detail = null;
				try {

					XmiInformation xmiInfo = XmiUtilStream.retriveElements(file);
					List<PaquetDto> packagesDto = loadPackages(xmiInfo.getPackageList(), typeElementModele);
					List<ClasseDobjetDto> classesDto = null;
					if (null != packagesDto) {
						classesDto = loadClasses(xmiInfo.getClassList(), packagesDto, typeElementModele);
					}
					if (!classesDto.isEmpty()) {
						loadAttributsOfClasses(xmiInfo.getClassList(), classesDto, typeElementModele);
					}
					loadRelationsOfClasses(xmiInfo.getClassList(), xmiInfo.getConnectorLinkList(), classesDto, typeElementModele);

					// loadAssociations(xmiInfo.getConnectorLinkList() , typeElementModele) ;

					detail = new XmiDetailImport();
					detail.setNomFichier(xmiInfo.getFileName());
					detail.setTotalClasse(xmiInfo.getClassList().size());
					detail.setTotalPaquet(xmiInfo.getPackageList().size());
					detail.setTotalRelation(xmiInfo.getConnectorLinkList().size());
					detail.setVersionXmi(xmiInfo.getVersionXmi());
				} catch (RuntimeException e) {
					logger.info(" erreur de traitement du fichier xmi  {}", e.getMessage());
					throw new XmiParsingException(e.getMessage(), e);
				}
				return detail;
			})).collect(Collectors.toList());
		}
		logger.info("Fin chargement de Xmi at ---> {}", LocalDateTime.now().format(formatter));
		return listXmiDetailImport;
	}
	
	
	private List<PaquetDto> loadPackages(
			List<PackageXmi> packageListXmi , 
			TypeElementModeleObjetDto typeElementModele) {
		
		logger.info("Start package classes at ---> {}", LocalDateTime.now().format(formatter));			
		List<PaquetDto> packs = mapToPackagesDobjet(packageListXmi ,typeElementModele) ;
		paquetService.saveAll(packs);
		logger.info("Fin d'enregistrement des packages at ---> {}", LocalDateTime.now().format(formatter));
		return packs ;
	
	}
	
	private List<ClasseDobjetDto> loadClasses(
			List<ClassXmi> classList, 
			List<PaquetDto> packagesDto, 
			TypeElementModeleObjetDto typeElementModele) {
		
		logger.info("Début d'enregistrement des  classes at ---> {}", LocalDateTime.now().format(formatter));
		
		List<ClasseDobjetDto> classesDto = null ;
		if(!classList.isEmpty()) {
			classesDto = mapToClassDobjet(classList , packagesDto , typeElementModele);	
			classesDto = classeDobjetService.addAll(classesDto);		
			logger.info("Fin d'enregistrement classes at ---> {}", LocalDateTime.now().format(formatter));
		}
		return classesDto ;
	}
	
	private List<ClasseDobjetDto> loadAttributsOfClasses(
			List<ClassXmi> classList ,
			List<ClasseDobjetDto> classesDto, 
			TypeElementModeleObjetDto typeElementModele) {
		
		logger.info("Début d'enregistrement des attributes at ---> {}", LocalDateTime.now().format(formatter));
		List<ClasseDobjetDto> classesAndAttributsDto = null ;
		if(!classList.isEmpty()) {	
			
			classesAndAttributsDto = classList.stream().map(classeXmi -> {
				List<AttributClasseDto> listAttribut = mapToAttributClasseDobjet(classeXmi , classesDto, typeElementModele) ;
				Optional<ClasseDobjetDto> classeDto = classesDto.stream().filter(clDto -> clDto.getIdExterne().equals(classeXmi.getClassId())).findFirst() ;
				if(classeDto.isPresent()) {
					classeDto.get().setAttributClasses( new HashSet<>(listAttribut));
				}
				return classeDto.get() ;
			}).collect(Collectors.toList()) ;
			
		
			classesAndAttributsDto.stream().forEach(classeDto -> attributClasseService.addAll(classeDto.getAttributClasses()) );
			
			logger.info("Fin d'enregistrement des attributes at ---> {}", LocalDateTime.now().format(formatter));			
		}
		return classesAndAttributsDto ;
	}
	
	private void loadRelationsOfClasses(
			List<ClassXmi> classesXmi ,
			List<ConnectorLinkXmi> connectorsXmi , 
			List<ClasseDobjetDto> classesDto , 
			TypeElementModeleObjetDto typeElementModele) {
		
		logger.info("Début d'enregistrement des Relations at ---> {}", LocalDateTime.now().format(formatter));
		if(!classesXmi.isEmpty()) {	
			
			List<RelationClasseDobjetDto> relationsClasseDobjetDto = classesXmi.stream().map(classeXmi -> {
				List<RelationClasseDobjetDto> listRelation = mapToRelationClasseDobjet(classeXmi, connectorsXmi, classesDto, typeElementModele);

				return listRelation.stream()
						.filter(relation -> !relation.getTypeRelation().equalsIgnoreCase("generalization")
								&& !relation.getTypeRelation().equalsIgnoreCase("notelink")
								&& !relation.getTypeRelation().equalsIgnoreCase("usage"))
						.collect(Collectors.toList());

			}).flatMap(List::stream).collect(Collectors.toList());
			relationClasseDobjetService.addAll(relationsClasseDobjetDto);

			List<RelationClasseMereDto> relationsClasseMereDobjetDto = classesXmi.stream().map(classeXmi -> {
				List<RelationClasseDobjetDto> listRelation = mapToRelationClasseDobjet(classeXmi, connectorsXmi, classesDto, typeElementModele);
				return listRelation.stream()
						.filter(relation -> relation.getTypeRelation().toLowerCase().equals("generalization"))
						.map(this::mapToRelationClasseMere)
						.collect(Collectors.toList());
			}).flatMap(List::stream).collect(Collectors.toList());
			relationClasseMereService.addAll(relationsClasseMereDobjetDto);

			logger.info("Fin d'enregistrement des Relations at ---> {}", LocalDateTime.now().format(formatter));
		}
	}
	
//	private void loadAssociations(List<ConnectorLinkXmi> connectorList , TypeElementModeleObjet typeElementModele) {
//	
//		connectorList.stream().forEach(connector-> {
//			AssociationClasseDobjet associationSource = mapToAssociation(connector , connector.getSource() , AssociationRole.SOURCE , typeElementModele);
//			AssociationClasseDobjet associationTarget = mapToAssociation(connector , connector.getTarget() , AssociationRole.DESTINATION , typeElementModele);
//			if(null != associationSource) {
//				associationClasseDobjetRepository.save(associationSource);
//			}
//			if(null != associationTarget) {
//				associationClasseDobjetRepository.save(associationTarget);
//			}
//		});
//	}
	
//	private AssociationClasseDobjet mapToAssociation(
//			ConnectorLinkXmi relationAssociation , 
//			ConnectorBorderXmi connector  , 
//			AssociationRole role,
//			TypeElementModeleObjet typeElementModele) {
//		
//		AssociationClasseDobjet association = new AssociationClasseDobjet();
//		association.setIdRelationCLasseDobjet(relationAssociation.getConnectorId());
//		association.setIdAssociationClasseDobjet(connector.getId());
//		association.setLibelle(connector.getRole().getName());
//		association.setRole(role.getRole());
//		association.setTypeModele(typeModele.getType());
//		if(null != connector.getModifiers().getIsNavigable()) {				
//			association.setEstNavigable(Boolean.parseBoolean(connector.getModifiers().getIsNavigable()));
//		}
//		
//		List<String> mutiplicites = new ArrayList<String>();
//		Pattern pattern = Pattern.compile("[0-9*]+");
//		if(null != connector.getType().getMultiplicity()) {	
//			mutiplicites.clear();
//			Matcher matcher = pattern.matcher(connector.getType().getMultiplicity());
//			while (matcher.find()) {
//				mutiplicites.add(matcher.group());
//			}
//			
//		}
//		if(null != mutiplicites && mutiplicites.size() > 1) {
//			if(mutiplicites.get(0).matches("[0-9]")) {					
//				association.setCardinaliteMin(Integer.parseInt(mutiplicites.get(0)));
//			}else {
//				association.setCardinaliteMin(-1);
//			}
//			if(mutiplicites.get(mutiplicites.size()-1).matches("[0-9]")) {					
//				association.setCardinaliteMax(Integer.parseInt(mutiplicites.get(mutiplicites.size()-1)));
//			}else {
//				association.setCardinaliteMax(-1);
//			}				
//		}else if(null != mutiplicites && mutiplicites.size() == 1) {
//			if(mutiplicites.get(0).matches("[0-9]")) {									
//				association.setCardinaliteMax(Integer.parseInt(mutiplicites.get(0)));
//			}else {
//				association.setCardinaliteMax(-1);
//			}
//		}			
//		return association;
//	}
//	

	
	
	private List<PaquetDto> mapToPackagesDobjet(
			List<PackageXmi> packageListXmi , 
			TypeElementModeleObjetDto typeElementModele) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		
		List<PaquetDto> paquetDtoList = packageListXmi.stream()
				.map(packXmi -> {
					PaquetDto pack = toPaquetEntity( typeElementModele, formatter, packXmi);	
			
					if(null != packXmi.getPackageParentId()) {
						Optional<PackageXmi> optionalPaquetXmi = packageListXmi
								.stream()
								.filter(p -> p.getId().equals(packXmi.getPackageParentId())).findFirst();
						if(optionalPaquetXmi.isPresent()) {
							PaquetDto mapped = toPaquetEntity(typeElementModele, formatter, optionalPaquetXmi.get());	
							pack.setPaquetParent(mapped);
						}
					}
					return pack;
		}).collect(Collectors.toList());
			
		return paquetDtoList ;
	}


	
	private PaquetDto toPaquetEntity(
			TypeElementModeleObjetDto typeElementModele,
			DateTimeFormatter formatter, 
			PackageXmi packXmi) {
		
		PaquetDto pack = new PaquetDto();		
		pack.setId(packXmi.getId());
		pack.setIdExterne(packXmi.getId());
		pack.setNom(packXmi.getName());
		logger.debug("pack {} , parent {} " ,packXmi.getId(), packXmi.getPackageParentId());			
		pack.setTypeElementModeleObjet(typeElementModele);			
		if(null != packXmi.getCreated() ) {
			LocalDateTime dateTime = LocalDateTime.parse(packXmi.getCreated(), formatter);
			pack.setDateDebutActivite(dateTime);
		}
		if(null != packXmi.getModified() ) {
			LocalDateTime dateTime = LocalDateTime.parse(packXmi.getModified(), formatter);
			pack.setDateFinActivite(dateTime);
		}
		
		pack.setVersion(packXmi.getVersion().getVersion());
		return pack;
	}
	
	
	private List<ClasseDobjetDto> mapToClassDobjet(
			List<ClassXmi> classListXmi , 
			List<PaquetDto> packagesDto, 
			TypeElementModeleObjetDto typeElementModele) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return classListXmi.stream()
				.map(classXmi -> {					
					ClasseDobjetDto classe = new ClasseDobjetDto();
					classe.setLibelle(classXmi.getClassName());
					classe.setId(classXmi.getClassId());	
					classe.setIdExterne(classXmi.getClassId());
					classe.setVersion(classXmi.getVersion().getVersion());
					classe.setTypeElementModeleObjet(typeElementModele);
					Optional<PaquetDto> packageClasse = packagesDto
							.stream()
							.filter(p -> p.getIdExterne().equals(classXmi.getPackageId())).findFirst();
					if(packageClasse.isPresent()) {
						classe.setPaquet(packageClasse.get());
					}
					if(null != classXmi.getCreated()) {		
						LocalDateTime dateTime = LocalDateTime.parse(classXmi.getCreated(), formatter);
						classe.setDateDebutActivite(dateTime);			
					}
					if(null != classXmi.getModified()) {		
						LocalDateTime dateTime = LocalDateTime.parse(classXmi.getModified(), formatter);
						classe.setDateFinActivite(dateTime);			
					}
					return  classe ;
		}).collect(Collectors.toList());
				
	}

	private List<RelationClasseDobjetDto> mapToRelationClasseDobjet(
			ClassXmi classXmi , 
			List<ConnectorLinkXmi> connectorsXmi, 
			List<ClasseDobjetDto> classesDto, 
			TypeElementModeleObjetDto typeElementModele) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		
		return classXmi.getLinks().stream().map(link -> {
			logger.debug("linkXmi Relation id {} , idStart  {} , idEnd {} , type {}" , link.getLinkId() ,link.getStartId() , link.getEndId() , link.getLinkName());
			RelationClasseDobjetDto relation = new RelationClasseDobjetDto();
			relation.setId(link.getLinkId());
			relation.setIdExterne(link.getLinkId());			
			relation.setVersion(classXmi.getVersion().getVersion());
			relation.setTypeRelation(link.getLinkName());	
			relation.setTypeElementModeleObjet(typeElementModele);
											
			Optional<ClasseDobjetDto> classeSource = classesDto.stream().filter(cl -> cl.getIdExterne().equals(link.getStartId())).findFirst();
			if(classeSource.isPresent()) {					
				relation.setClasseDobjetSource(classeSource.get());
			}
			
			Optional<ClasseDobjetDto> classeDestination = classesDto.stream().filter(cl -> cl.getIdExterne().equals(link.getEndId())).findFirst();
			if(classeDestination.isPresent()) {					
				relation.setClasseDobjetDestination(classeDestination.get());
			}
			
			if(null != classXmi.getCreated()) {	
				LocalDateTime dateTime = LocalDateTime.parse(classXmi.getCreated(), formatter);
				relation.setDateDebutActivite(dateTime);
			}
			
			if(null != classXmi.getModified()) {		
				LocalDateTime dateTime = LocalDateTime.parse(classXmi.getModified(), formatter);
				relation.setDateFinActivite(dateTime);						
			}
			
			Optional<ConnectorLinkXmi> connectorXmi =connectorsXmi.stream()
				.filter(connector -> connector.getConnectorId().equals(relation.getIdExterne()))
				.findFirst();
			
			if(connectorXmi.isPresent()) {
				if(null != connectorXmi.get().getSource()) {
					ConnectorBorderXmi connectorBorderSourceXmi =  connectorXmi.get().getSource() ;
					if(null != connectorBorderSourceXmi.getModifiers() && null != connectorBorderSourceXmi.getModifiers().getIsNavigable()) {
						relation.setEstNavigableSource(Boolean.parseBoolean(connectorBorderSourceXmi.getModifiers().getIsNavigable()) );											
					}					
					if(null != connectorBorderSourceXmi.getType().getMultiplicity()) {
						MutliplicityXmi mutliplicityXmi = getMutlipliciteToRelation(connectorBorderSourceXmi.getType().getMultiplicity());
						relation.setCardinaliteMinSource(mutliplicityXmi.getMin());
						relation.setCardinaliteMinDestination(mutliplicityXmi.getMax());						
					}
									
				}
				if(null != connectorXmi.get().getTarget()) {
					ConnectorBorderXmi connectorBorderTargetXmi =  connectorXmi.get().getTarget() ;
					if(null != connectorBorderTargetXmi.getModifiers() && null != connectorBorderTargetXmi.getModifiers().getIsNavigable()) {
						relation.setEstNavigableDestination(Boolean.parseBoolean(connectorBorderTargetXmi.getModifiers().getIsNavigable()) );											
					}					
					if(null != connectorBorderTargetXmi.getType().getMultiplicity()) {
						MutliplicityXmi mutliplicityXmi = getMutlipliciteToRelation(connectorBorderTargetXmi.getType().getMultiplicity());
						relation.setCardinaliteMinDestination(mutliplicityXmi.getMin());
						relation.setCardinaliteMaxDestination(mutliplicityXmi.getMax());						
					}	
				}
			}		
			
			return relation;
		}).collect(Collectors.toList());

	}
	
	private MutliplicityXmi getMutlipliciteToRelation(String mutilplicitesStr ) {
		List<String> mutiplicites = new ArrayList<String>();
		MutliplicityXmi mutliplicity = new MutliplicityXmi(-1 , -1);
		Pattern pattern = Pattern.compile("[0-9*]+");
		Matcher matcher = pattern.matcher(mutilplicitesStr);
		while (matcher.find()) {
			mutiplicites.add(matcher.group());
		}
		if(mutiplicites != null && mutiplicites.size() > 1) {
			if(mutiplicites.get(0).matches("[0-9]")) {					
				mutliplicity.setMin(Integer.parseInt(mutiplicites.get(0)));
			}else {
				mutliplicity.setMin(-1);
			}
			if(mutiplicites.get(mutiplicites.size()-1).matches("[0-9]")) {					
				mutliplicity.setMax(Integer.parseInt(mutiplicites.get(mutiplicites.size()-1)));
			}else {
				mutliplicity.setMax(-1);
			}				
		}else if(null != mutiplicites && mutiplicites.size() == 1) {
			if(mutiplicites.get(0).matches("[0-9]")) {									
				mutliplicity.setMax(Integer.parseInt(mutiplicites.get(0)));
			}else {
				mutliplicity.setMax(-1);
			}
		}	
		return mutliplicity ;
	}



	private RelationClasseMereDto mapToRelationClasseMere(RelationClasseDobjetDto link ) {

		RelationClasseMereDto relation = new RelationClasseMereDto();
		relation.setId(link.getId());
		relation.setIdExterne(link.getIdExterne());
							
		if(null != link.getClasseDobjetSource()) {					
			relation.setClasseDobjetFille(link.getClasseDobjetSource());
		}
		
		if(null != link.getClasseDobjetDestination()) {					
			relation.setClasseDobjeMere(link.getClasseDobjetDestination());
		}
		
		relation.setTypeElementModeleObjet(link.getTypeElementModeleObjet());

		if(null != link.getDateDebutActivite()) {	
			relation.setDateDebutActivite(link.getDateDebutActivite());
		}
		if(null != link.getDateFinActivite()) {		
			relation.setDateFinActivite(link.getDateFinActivite());						
		}
		
		relation.setVersion(link.getVersion());
		return relation;

	}
	
	private List<AttributClasseDto> mapToAttributClasseDobjet(
			ClassXmi classeXmi , 
			List<ClasseDobjetDto> classesDto , 
			TypeElementModeleObjetDto typeElementModele) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		
		return classeXmi.getAttributes().stream().map(attribut -> {
			AttributClasseDto attr = new AttributClasseDto();
			attr.setId(attribut.getIdAttribut());
			attr.setIdExterne(attribut.getIdAttribut());
			attr.setLibelle(attribut.getNameProperty());
			attr.setVersion(classeXmi.getVersion().getVersion());
			if(null != attribut.getTypeProperty()) {
				TypeAttributClasseDto typeAttr = new TypeAttributClasseDto();
				typeAttr.setType(attribut.getTypeProperty());
				attr.setTypeAttributClasse(typeAttr);
			}
			
			if(null != attribut.getAttachedClassId()) {
				Optional<ClasseDobjetDto> classeDto = classesDto.stream().filter(cl -> cl.getIdExterne().equals(attribut.getAttachedClassId())).findFirst();
				if(classeDto.isPresent()) {					
					attr.setClasseDobjet(classeDto.get());	
				}
			}
			attr.setTypeElementModeleObjet(typeElementModele);
			if(null != attribut.getCreated()) {
				LocalDateTime dateTime = LocalDateTime.parse(attribut.getCreated(), formatter);
				attr.setDateDebutActivite(dateTime);
			}
			if(null != attribut.getModified()) {
				LocalDateTime dateTime = LocalDateTime.parse(attribut.getModified(), formatter);
				attr.setDateFinActivite(dateTime);
			}
			return attr;
		}).collect(Collectors.toList());
		
	}
	
	


}
