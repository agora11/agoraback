package sncf.reseau.cdsdpadsir.agora.xmi.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VersionXmi implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String version ;
	private String revision ;
	private String dateImport ;
	
}
