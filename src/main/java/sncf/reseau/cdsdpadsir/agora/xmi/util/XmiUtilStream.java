package sncf.reseau.cdsdpadsir.agora.xmi.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import sncf.reseau.cdsdpadsir.agora.xmi.exception.XmiExceptionUtil;
import sncf.reseau.cdsdpadsir.agora.xmi.exception.XmiFileNameException;
import sncf.reseau.cdsdpadsir.agora.xmi.exception.XmiParsingException;
import sncf.reseau.cdsdpadsir.agora.xmi.model.AttributeClassXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.ClassXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.ConnectorBorderXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.ConnectorLabelsXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.ConnectorLinkXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.ConnectorPropertyXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.LinkClassXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.ModelXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.ModifiersXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.PackageXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.RoleXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.TypeXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.VersionXmi;
import sncf.reseau.cdsdpadsir.agora.xmi.model.XmiInformation;

public class XmiUtilStream {
	
	private static Logger logger = LoggerFactory.getLogger(XmiUtilStream.class);
	private static final  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	
	public static final String REGEX_VERSION_REVISION = "(ariane)_(.*).xml";
	public static final String ERROR_NOM_FICHIER = "Le format de nom de fichier n'est pas correct exp : ariane_version.xmi";
	 
	
//	Élément syntaxique       Type de chemin                 Exemples de chemin          Exemples de chemin répondant au chemin indiqué à gauche
//	------------------------------------------------------------------------------------------------------------------------------------------------
//	balise                   Nom d'élément                  hopital                     <hopital>…</hopital>
//	------------------------------------------------------------------------------------------------------------------------------------------------
//	                                                        hopital/personne            <hopital>
//	/                        Sépare enfants directs                                        <personne>…</personne>
//	                                                                                    </hopital>
//	------------------------------------------------------------------------------------------------------------------------------------------------
//    //	                 Descendant                     hopital//relations          <hopital>                             <hopital>
//															----                          <pensionnaires>                        <personne>
//															//nom                           <patient>                              <nom></nom>
//																						      <relations>…</relations>           </personne>
//																						    </patient>                         <hopital>
//																						  </pensionnaires>
//																						</hopital>
//																					    => recherche n'importe où dans le schéma
//	------------------------------------------------------------------------------------------------------------------------------------------------
//	*                       « wildcard »                    */title                      <bla><title>..</title> ou <bli><title>…</title>
//	------------------------------------------------------------------------------------------------------------------------------------------------
//	                                                        title|head                   <title>…</title> ou <head> …</head>
//	|                        opérateur « ou »                ----                         ----
//															*|/|@*                       (tous les éléments : les enfants, la racine et les attributs de la racine)
//	------------------------------------------------------------------------------------------------------------------------------------------------
//	.                       élément courant                 .                             
//	------------------------------------------------------------------------------------------------------------------------------------------------
//	../                     élément supérieur               ../problem                    <project>
//	------------------------------------------------------------------------------------------------------------------------------------------------
//	                                                        @id                           <xyz id="test">...</xyz>
//	@                       nom d'attribut                  ----                          ----
//															project/@id                   <project id="test" ...> ... </project>
//	------------------------------------------------------------------------------------------------------------------------------------------------
//	@attr='type'            Valeur d'attribut               list[@type='ol']              <list type="ol"> ...... </list>
//	------------------------------------------------------------------------------------------------------------------------------------------------
//	
	/**
	 * 
	 * @param file
	 * @return
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws Exception
	 */
	public static Document getDocumentXmlFromSource(File file) throws ParserConfigurationException, SAXException, IOException  {
		FileInputStream fileIS = new FileInputStream(file);
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = builderFactory.newDocumentBuilder();
		Document xmlDocument = builder.parse(fileIS);
		return xmlDocument ;
	}
	
	public static VersionXmi getVersionXmi(File file) throws XmiFileNameException {
		String fileName = file.getName();
		String regEx = REGEX_VERSION_REVISION;
		Pattern patternRegex = Pattern.compile(regEx);
		Matcher matcherRegex = patternRegex.matcher(fileName);
		VersionXmi version = null;
		if (matcherRegex.matches()) {
			version = new VersionXmi();
			version.setVersion(matcherRegex.group(2));
        } else {
            throw new XmiFileNameException(ERROR_NOM_FICHIER);
        }		
		return version ;
	}
	
	public static XmiInformation retriveElements(File file) throws  XmiFileNameException , XmiParsingException    {
		
		Document xmlDocument;
		XmiInformation xmiInformation = new XmiInformation();
		xmiInformation.setFileName(file.getName());
		xmiInformation.setVersionXmi(getVersionXmi( file));

		try {
			xmlDocument = getDocumentXmlFromSource(file);
			logger.info(" Loading Package at ---> {}", LocalDateTime.now().format(formatter)); 
			xmiInformation.setPackageList(getPackages(xmlDocument , xmiInformation.getVersionXmi()));
			logger.info(" nom fichier ---> {} , chemin {}", file.getName() , file.getPath() );
			logger.info(" Package loaded at ---> {}", LocalDateTime.now().format(formatter));
			
			logger.info("loading Class at ---> {}", LocalDateTime.now().format(formatter));
			xmiInformation.setClassList(getClasses(xmlDocument , xmiInformation.getVersionXmi()));
			logger.info(" Class loaded at ---> {}", LocalDateTime.now().format(formatter));
			
			logger.info(" Loading Connector at ---> {}", LocalDateTime.now().format(formatter));
			xmiInformation.setConnectorLinkList(getConnectors(xmlDocument , xmiInformation.getVersionXmi()));
			logger.info(" Connector loaded at ---> {}", LocalDateTime.now());
		} catch ( ParserConfigurationException | SAXException | IOException e) {
			throw new XmiParsingException("erreur de parsing de fichier xmi" ,e);
		} 
		
		return xmiInformation ;
	}
	
	public static List<ConnectorLinkXmi> getConnectors(Node xmlDocument , VersionXmi version) throws XmiParsingException  {
		
		XPath xPath = XPathFactory.newInstance().newXPath();	
		String expression = "//connector";
		NodeList nodeConnectorList;
		try {
			nodeConnectorList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {			
			throw new XmiParsingException("error parsing connecor xmi",e);
		}
		
		Stream<Node> nodeStreamConnectorList = IntStream.range(0, nodeConnectorList.getLength()).mapToObj(nodeConnectorList::item);
		
		return nodeStreamConnectorList.parallel().map(
				XmiExceptionUtil.wrapperExceptionFunction(item -> {
					ConnectorLinkXmi connector = new ConnectorLinkXmi();
					connector.setConnectorId(item.getAttributes().getNamedItem("xmi:idref").getNodeValue());	
					connector.setSource(getConnectorBorderSource(item));
					connector.setTarget(getConnectorBorderTarget(item));
					connector.setProperties(getConnectorProperties(item));
					connector.setLabels(getConnectorLabels(item));
					connector.setVersion(version);								
					return connector ;
				}) ).collect(Collectors.toList());
	
		
	}
	
	
	
	public static List<PackageXmi> getPackages(Node xmlDocument , VersionXmi version) throws XmiParsingException {
		XPath xPath = XPathFactory.newInstance().newXPath();
		//String expression = "//element[@type='uml:Package']" ;
		String expression = "//element[starts-with(@idref, 'EAPK_')]" ;
		//*[starts-with(@id, 'sometext') and ends-with(@id, '_text')]
		NodeList nodeList;
		try {
			nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing package xmi",e);
		}
		Stream<Node> nodeStreamPackage = IntStream.range(0, nodeList.getLength()).mapToObj(nodeList::item);
		
		return nodeStreamPackage.parallel().map(XmiExceptionUtil.wrapperExceptionFunction(
				item ->{
					
					PackageXmi pack = new PackageXmi();
							
					if(null != item.getAttributes().getNamedItem("xmi:idref")) {
						pack.setId(item.getAttributes().getNamedItem("xmi:idref").getNodeValue());
					}
					if(null != item.getAttributes().getNamedItem("name")) {				
						pack.setName(item.getAttributes().getNamedItem("name").getNodeValue());
					}
					if(null != item.getAttributes().getNamedItem("scope")) {
						pack.setScope(item.getAttributes().getNamedItem("scope").getNodeValue());				
					}
							
					Node nodeModel = (Node) xPath.evaluate("./model", item, XPathConstants.NODE);
					if(null != nodeModel && null != nodeModel.getAttributes().getNamedItem("package")) {
						pack.setPackageParentId(nodeModel.getAttributes().getNamedItem("package").getNodeValue());
					}
					Node nodeProject = (Node) xPath.evaluate("./project", item, XPathConstants.NODE);
//					if(null != nodeProject && null != nodeProject.getAttributes().getNamedItem("version")) {
//						pack.setVersion(nodeProject.getAttributes().getNamedItem("version").getNodeValue());
//					}
					
					if(null != nodeProject && null != nodeProject.getAttributes().getNamedItem("created")) {
						pack.setCreated(nodeProject.getAttributes().getNamedItem("created").getNodeValue());
					}
					
					if(null != nodeProject && null != nodeProject.getAttributes().getNamedItem("modified")) {
						pack.setModified(nodeProject.getAttributes().getNamedItem("modified").getNodeValue());
					}					
					pack.setVersion(version);
					return pack ;

				}))
				.collect(Collectors.toList());
		
		
	}
	public static List<ClassXmi> getClasses(Node xmlDocument , VersionXmi version) throws XmiParsingException  {
		
		XPath xPath = XPathFactory.newInstance().newXPath();
		String expression = "//element[@type='uml:Class']";	
		NodeList nodeList = null ;
		try {
			nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing classes xmi",e);
		}
		Stream<Node> nodeStreamClass = IntStream.range(0, nodeList.getLength()).mapToObj(nodeList::item);
		
		return nodeStreamClass.parallel().map(XmiExceptionUtil.wrapperExceptionFunction( 
				item -> {
												
					ClassXmi clazz = null;
					// info class
					clazz = infoClass(item , version);
					// info Attributs Class
					attributesClass(item , clazz , version);		
					// link 
					linkClass(item , clazz , version) ;								
					return clazz ;
				}))
				.collect(Collectors.toList());	
	}

	public static ModelXmi getModel(Node nodeBorder) throws XmiParsingException  {
		XPath xPath = XPathFactory.newInstance().newXPath();
		Node nodeModel = null;
		try {
			nodeModel = (Node) xPath.evaluate("./model", nodeBorder, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing model xmi",e);
		}
		ModelXmi model = new ModelXmi();
		if(null != nodeModel && null != nodeModel.getAttributes().getNamedItem("name")) {			
			model.setName(nodeModel.getAttributes().getNamedItem("name").getNodeValue());
		}
		if(null != nodeModel && null != nodeModel.getAttributes().getNamedItem("type") ) {			
			model.setType(nodeModel.getAttributes().getNamedItem("type").getNodeValue());		
		}
		return model ;
	}
	
	public static ModifiersXmi getModifiers(Node nodeBorder) throws XmiParsingException   {
		XPath xPath = XPathFactory.newInstance().newXPath();
		Node nodeModel = null;
		try {
			nodeModel = (Node) xPath.evaluate("./modifiers", nodeBorder, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing modifiers xmi",e);
		}
		ModifiersXmi modifiers = new ModifiersXmi();
		if(null != nodeModel && null != nodeModel.getAttributes().getNamedItem("isOrdered")) {
			modifiers.setIsOrdered(nodeModel.getAttributes().getNamedItem("isOrdered").getNodeValue());			
		}
		
		if(null != nodeModel && null != nodeModel.getAttributes().getNamedItem("changeable")) {
			modifiers.setChangeable(nodeModel.getAttributes().getNamedItem("changeable").getNodeValue());					
		}
		
		if(null != nodeModel && null != nodeModel.getAttributes().getNamedItem("isNavigable")) {
			modifiers.setIsNavigable(nodeModel.getAttributes().getNamedItem("isNavigable").getNodeValue());					
		}
		return modifiers ;
		
	}
	
	public static RoleXmi getRole( Node nodeBorder) throws XmiParsingException   {
		XPath xPath = XPathFactory.newInstance().newXPath();
		Node nodeRole;
		try {
			nodeRole = (Node) xPath.evaluate("./role", nodeBorder, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing role xmi",e);
		}
		RoleXmi role = new RoleXmi();
		if(null != nodeRole && null != nodeRole.getAttributes().getNamedItem("targetScope")) {
			role.setTargetScope(nodeRole.getAttributes().getNamedItem("targetScope").getNodeValue());
		}
		if(null != nodeRole && null != nodeRole.getAttributes().getNamedItem("visibility")) {			
			role.setVisibility(nodeRole.getAttributes().getNamedItem("visibility").getNodeValue());
		}
		if(null != nodeRole && null != nodeRole.getAttributes().getNamedItem("name")) {			
			role.setName(nodeRole.getAttributes().getNamedItem("name").getNodeValue());
		}
		return role ;
	}
	
	public static TypeXmi getType(Node nodeBorder) throws XmiParsingException   {
		XPath xPath = XPathFactory.newInstance().newXPath();
		Node nodeType;
		try {
			nodeType = (Node) xPath.evaluate("./type", nodeBorder, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing type xmi",e);
		}
		TypeXmi type = new TypeXmi();
		if(null == nodeType) {
			return type ;
		}
		if( null != nodeType.getAttributes().getNamedItem("aggregation") ) {			
			type.setAggregation(nodeType.getAttributes().getNamedItem("aggregation").getNodeValue());
		}
		if( null != nodeType.getAttributes().getNamedItem("multiplicity")) {			
			type.setMultiplicity(nodeType.getAttributes().getNamedItem("multiplicity").getNodeValue());
		}
		if( null != nodeType.getAttributes().getNamedItem("containment")) {			
			type.setContainment(nodeType.getAttributes().getNamedItem("containment").getNodeValue());
		}
		return type ;
		
	}
	
	public static ConnectorLabelsXmi getConnectorLabels(Node nodeConnector) throws XmiParsingException  {
		XPath xPath = XPathFactory.newInstance().newXPath();
		Node nodeLabels;
		try {
			nodeLabels = (Node) xPath.evaluate("./labels",  nodeConnector, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing lables xmi",e);
		}		
		ConnectorLabelsXmi labels = new ConnectorLabelsXmi() ;
		
		if(null == nodeLabels) {
			return labels;
		}
		if(null != nodeLabels.getAttributes().getNamedItem("rb")) {
			labels.setRb(nodeLabels.getAttributes().getNamedItem("rb").getNodeValue());
		}
		if(null != nodeLabels.getAttributes().getNamedItem("rt")) {
			labels.setRt(nodeLabels.getAttributes().getNamedItem("rt").getNodeValue());
		}
		return labels ;
	}
	
	public static ConnectorBorderXmi getConnectorBorder(Node nodeBorder) throws  XmiParsingException {
		ConnectorBorderXmi connectorBorder = new ConnectorBorderXmi();
		
		String nodeSourceId = nodeBorder.getAttributes().getNamedItem("xmi:idref").getNodeValue();
		connectorBorder.setId(nodeSourceId);
		connectorBorder.setModel(getModel(nodeBorder));		
		connectorBorder.setModifiers(getModifiers( nodeBorder));
		connectorBorder.setRole(getRole(nodeBorder));
		
		connectorBorder.setType(getType(nodeBorder));
		
		return connectorBorder ;
		
	}
	
	public static ConnectorBorderXmi getConnectorBorderSource( Node nodeConnector) throws XmiParsingException  {
		XPath xPath = XPathFactory.newInstance().newXPath();
		Node nodeSource = null;
		try {
			nodeSource = (Node) xPath.evaluate("./source", nodeConnector, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing source xmi",e);
		}		
		return getConnectorBorder(nodeSource) ;		
		
	}
	
	public static ConnectorBorderXmi getConnectorBorderTarget(Node nodeConnector) throws XmiParsingException  {		
		XPath xPath = XPathFactory.newInstance().newXPath();
		Node nodeSource;
		try {
			nodeSource = (Node) xPath.evaluate("./target", nodeConnector, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing target xmi",e);
		}		
		return getConnectorBorder(nodeSource) ;		
		
	}	
	
	public static ConnectorPropertyXmi getConnectorProperties(Node nodeConnector) throws XmiParsingException {
		XPath xPath = XPathFactory.newInstance().newXPath();
		Node nodeProperties;
		try {
			nodeProperties = (Node) xPath.evaluate("./properties", nodeConnector, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing properties xmi",e);
		}
		ConnectorPropertyXmi properties = new ConnectorPropertyXmi();
		
		if(null != nodeProperties && null != nodeProperties.getAttributes().getNamedItem("ea_type")) {
			properties.setEaType(nodeProperties.getAttributes().getNamedItem("ea_type").getNodeValue());
		}
		if(null != nodeProperties && null != nodeProperties.getAttributes().getNamedItem("direction")) {
			properties.setDirection(nodeProperties.getAttributes().getNamedItem("direction").getNodeValue());
		}
		return properties ;
	}
	
	
	public static ClassXmi infoClass(Node nodeClass , VersionXmi version) throws XmiParsingException  {

		XPath xPath = XPathFactory.newInstance().newXPath();
		
		ClassXmi clazz = new ClassXmi();
		if( null !=nodeClass.getAttributes().getNamedItem("xmi:idref")) {			
			clazz.setClassId(nodeClass.getAttributes().getNamedItem("xmi:idref").getNodeValue());
		}
		if(null != nodeClass.getAttributes().getNamedItem("name")) {			
			clazz.setClassName(nodeClass.getAttributes().getNamedItem("name").getNodeValue());
		}
		if(null != nodeClass.getAttributes().getNamedItem("scope")) {
			clazz.setClassScope(nodeClass.getAttributes().getNamedItem("scope").getNodeValue());
		}
		if(null != nodeClass.getAttributes().getNamedItem("xmi:type")) {
			clazz.setClassType(nodeClass.getAttributes().getNamedItem("xmi:type").getNodeValue());
		}
		
		Node nodeProject = null;
		try {
			nodeProject = (Node) xPath.evaluate("./project", nodeClass, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing project xmi",e);
		}
//		if(null != nodeProject && null != nodeProject.getAttributes().getNamedItem("version")) {
//			clazz.setVersion(nodeProject.getAttributes().getNamedItem("version").getNodeValue());
//		}
		
		if(null != nodeProject && null != nodeProject.getAttributes().getNamedItem("created")) {
			clazz.setCreated(nodeProject.getAttributes().getNamedItem("created").getNodeValue());
		}
		
		if(null != nodeProject && null != nodeProject.getAttributes().getNamedItem("modified")) {
			clazz.setModified(nodeProject.getAttributes().getNamedItem("modified").getNodeValue());
		}
		
		Node nodeModel;
		try {
			nodeModel = (Node) xPath.evaluate("./model", nodeClass, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing model xmi",e);
		}	
		
		if(null != nodeModel && null != nodeModel.getAttributes().getNamedItem("package")) {
			clazz.setPackageId(nodeModel.getAttributes().getNamedItem("package").getNodeValue());
		}
		clazz.setVersion(version);
		return clazz ;
	}
	
	public static List<AttributeClassXmi> attributesClass(Node nodeClass , ClassXmi attachedClass , VersionXmi version ) throws XmiParsingException  { 
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodeAttributes;
		try {
			nodeAttributes = (NodeList) xPath.evaluate("attributes/attribute", nodeClass, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing attributes xmi",e);
		}
		
		Node nodeProject;
		try {
			nodeProject = (Node) xPath.evaluate("./project", nodeClass, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing project xmi",e);
		}
		final String created = null != nodeProject && null !=nodeProject.getAttributes().getNamedItem("created").getNodeValue() ? nodeProject.getAttributes().getNamedItem("created").getNodeValue() : null  ;
		final String modified = null != nodeProject && null != nodeProject.getAttributes().getNamedItem("modified").getNodeValue() ?  nodeProject.getAttributes().getNamedItem("modified").getNodeValue() : null ;
		
		
		Stream<Node> nodeStreamAttribute = IntStream.range(0, nodeAttributes.getLength()).mapToObj(nodeAttributes::item);
		List<AttributeClassXmi> attrs = nodeStreamAttribute.parallel().map(XmiExceptionUtil.wrapperExceptionFunction(
				item -> {			
					AttributeClassXmi attr = new AttributeClassXmi();
					if(null != item.getAttributes().getNamedItem("xmi:idref")) {				
						attr.setIdAttribut(item.getAttributes().getNamedItem("xmi:idref").getNodeValue());
					}
					if(null != item.getAttributes().getNamedItem("name")) {				
						attr.setNameProperty(item.getAttributes().getNamedItem("name").getNodeValue());
					}
					if(null != item.getAttributes().getNamedItem("scope")) {				
						attr.setScope(item.getAttributes().getNamedItem("scope").getNodeValue());
					}
								
					Node nodeProperties = (Node) xPath.evaluate("./properties", item, XPathConstants.NODE);
					if(null !=nodeProperties && null!= nodeProperties.getAttributes().getNamedItem("type")) {
						attr.setTypeProperty(nodeProperties.getAttributes().getNamedItem("type").getNodeValue());
					}
					attr.setAttachedClassId(attachedClass.getClassId());
					
					if( null != created) {
						attr.setCreated(created);
					}
					if(null != modified) {
						attr.setModified(modified);
					}
					attr.setVersion(version);			
					return attr ;
					
				}))
			.collect(Collectors.toList());
		
		attachedClass.setAttributes(attrs);
		return attrs ;
	}
	
	public static void linkClass(Node nodeClass ,ClassXmi attachedClass ,VersionXmi version ) throws XmiParsingException  { 
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodeLinks = null;
		try {
			nodeLinks = (NodeList) xPath.evaluate("links//*", nodeClass, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing link xmi",e);
		}
		
		Node nodeProject = null;
		try {
			nodeProject = (Node) xPath.evaluate("./project", nodeClass, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new XmiParsingException("error parsing projetc xmi",e);
		}
		final String created = null != nodeProject && null !=nodeProject.getAttributes().getNamedItem("created").getNodeValue() ? nodeProject.getAttributes().getNamedItem("created").getNodeValue() : null  ;
		final String modified = null != nodeProject && null != nodeProject.getAttributes().getNamedItem("modified").getNodeValue() ?  nodeProject.getAttributes().getNamedItem("modified").getNodeValue() : null ;
		
		Stream<Node> nodeStreamLink = IntStream.range(0, nodeLinks.getLength()).mapToObj(nodeLinks::item);	
		List<LinkClassXmi> links = nodeStreamLink.parallel().map(item -> {
			Node node = (Node) item ;			
			LinkClassXmi link = new LinkClassXmi();
			link.setLinkName(node.getNodeName());
			if(null != node.getAttributes().getNamedItem("xmi:id")) {
				link.setLinkId(node.getAttributes().getNamedItem("xmi:id").getNodeValue());
			}
			if(null != node.getAttributes().getNamedItem("start")) {
				link.setStartId(node.getAttributes().getNamedItem("start").getNodeValue());
			}
			if(null != node.getAttributes().getNamedItem("end")) {
				link.setEndId(node.getAttributes().getNamedItem("end").getNodeValue());
			}
			link.setAttachedClassId(attachedClass.getClassId());
			
			if(null != created) {
				link.setCreated(created);
			}
			if(null != modified) {
				link.setModified(modified);
			}			
			link.setVersion(version);
			
			return link ;
		}).collect(Collectors.toList());
		
		attachedClass.setLinks(links);
	}

}
