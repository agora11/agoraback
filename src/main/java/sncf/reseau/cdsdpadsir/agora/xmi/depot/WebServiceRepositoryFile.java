package sncf.reseau.cdsdpadsir.agora.xmi.depot;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import sncf.reseau.cdsdpadsir.agora.xmi.exception.FileStorageException;
import sncf.reseau.cdsdpadsir.agora.xmi.exception.XmiDepotFileException;
import sncf.reseau.cdsdpadsir.agora.xmi.exception.XmiFileNameException;
import sncf.reseau.cdsdpadsir.agora.xmi.util.XmiUtilStream;

@Service
public class WebServiceRepositoryFile {

	private Environment env;

	private final Path fileStorageLocation;

	@Autowired
	public WebServiceRepositoryFile(Environment env) {
		this.env = env;
		this.fileStorageLocation = Paths.get(env.getProperty("file.upload-dir")).toAbsolutePath().normalize();

		this.createDirectory();
	}

	public void createDirectory() {
		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception ex) {
			throw new FileStorageException(
					"Imposible de creer le dossier ou il faut envoyer les fichiers à enregistrer", ex);
		}

	}

	public String storeFile(MultipartFile file) {
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		File directory = new File(this.fileStorageLocation.toString());
		if (!directory.isDirectory()) {
			this.createDirectory();
		}

		try {
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				throw new FileStorageException(
						"Desolé! le nom de fichier contient un chemain invalid invalid " + fileName);
			}

			Pattern patternRegex = Pattern.compile(XmiUtilStream.REGEX_VERSION_REVISION);
			Matcher matcherRegex = patternRegex.matcher(fileName);
			if (!matcherRegex.matches()) {
				throw new XmiFileNameException(XmiUtilStream.ERROR_NOM_FICHIER);
			}

			// Copy file to the target location (Replacing existing file with the same name)
			Path targetLocation = this.fileStorageLocation.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

			return fileName;
		} catch (IOException ex) {
			throw new FileStorageException(
					"Imposible de stocker le fichier " + fileName + ". Veuillez réessayer plus tard!", ex);
		}
	}

	public void clearFolder() {
		File directory = new File(this.fileStorageLocation.toString());
		if (directory.isDirectory()) {
			for (File file : directory.listFiles()) {
				file.delete();
			}
		}
	}

	public Optional<List<File>> getXmiFiles(MultipartFile[] filesMultipart) throws XmiDepotFileException {

		// delete all file
		this.clearFolder();

		// uploade files
		Arrays.asList(filesMultipart).stream().map(this::storeFile).collect(Collectors.toList());

		String pathString = env.getProperty("file.upload-dir");
		Optional<List<File>> files;
		try {
			files = FileUtil.getXmlFiles(new File(pathString));
		} catch (IOException e) {
			throw new XmiDepotFileException("erreur de récupération à partir du depot local", e);
		}
		return files;

	}

}
