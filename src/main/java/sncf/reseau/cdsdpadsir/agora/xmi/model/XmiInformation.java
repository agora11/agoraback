package sncf.reseau.cdsdpadsir.agora.xmi.model;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class XmiInformation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<PackageXmi> packageList ;
	private List<ClassXmi> classList ;
	private List<ConnectorLinkXmi> connectorLinkList ;	
	private VersionXmi versionXmi ;
	private String fileName ;
	
}
