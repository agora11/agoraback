package sncf.reseau.cdsdpadsir.agora.xmi.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConnectorLinkXmi implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String connectorId;
	private ConnectorBorderXmi source ;
	private ConnectorBorderXmi target ;
	private ConnectorPropertyXmi properties ;
	private ConnectorLabelsXmi labels ;
	private VersionXmi version ;
	
	

}
