package sncf.reseau.cdsdpadsir.agora.xmi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MutliplicityXmi {
	
	private Integer min ;
	private Integer max ;

}
