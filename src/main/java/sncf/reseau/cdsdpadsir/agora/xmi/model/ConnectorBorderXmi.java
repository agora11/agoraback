package sncf.reseau.cdsdpadsir.agora.xmi.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConnectorBorderXmi implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id ;
	private ModelXmi model ;
	private RoleXmi role ;
	private TypeXmi type ;
	private ModifiersXmi modifiers ;
	private VersionXmi version ;

}
