package sncf.reseau.cdsdpadsir.agora.xmi.depot;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class FileUtil {
	
	public static final String PATH_FILE = "xmiRepository";
			
	public static void search(final String pattern, final File folder, List<String> result) {
        for (final File f : folder.listFiles()) {

            if (f.isDirectory()) {
                search(pattern, f, result);
            }

            if (f.isFile()) {
                if (f.getName().matches(pattern)) {
                    result.add(f.getAbsolutePath());
                }
            }

        }
    }
	
	public static Optional<List<File>> getXmlFiles(File file) throws IOException {

		Path path = Paths.get(file.getAbsolutePath());
		List<File> paths = null ;
		if(file.exists()) {		
			paths = Files.walk(path)
					.filter(Files::isRegularFile)
					.filter(p -> p.getFileName().toString().startsWith("ariane") && (p.getFileName().toString().endsWith(".xml")|| p.getFileName().toString().endsWith(".xmi")))
					.map(item -> new File(item.toAbsolutePath().toString())).collect(Collectors.toList());
		}
		return Optional.ofNullable(paths) ;
	}
	
	public static void clearDirectory( File file ) throws IOException {
		 Files.walk(file.toPath())
	      .sorted(Comparator.reverseOrder())
	      .map(Path::toFile)
	      .forEach(File::delete);

	}

}
