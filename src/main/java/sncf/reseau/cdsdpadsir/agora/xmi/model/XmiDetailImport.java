package sncf.reseau.cdsdpadsir.agora.xmi.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class XmiDetailImport {
	
	@JsonProperty("totalPaquet")	
	private Integer totalPaquet ;
	
	@JsonProperty("totalClasse")	
	private Integer totalClasse ;
	
	@JsonProperty("totalRelation")	
	private Integer totalRelation ;	
	
	@JsonProperty("versionXmi")	
	private VersionXmi versionXmi ;
	
	@JsonProperty("nomFichier")	
	private String nomFichier ;

}
