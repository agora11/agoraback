package sncf.reseau.cdsdpadsir.agora.xmi.depot;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ResetCommand.ResetType;
import org.eclipse.jgit.api.errors.CheckoutConflictException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import sncf.reseau.cdsdpadsir.agora.xmi.exception.XmiDepotFileException;

@Service
public class GitRepositoryFile implements RepositoryFileService{
	
	
	@Autowired
	private Environment env ; 
	
		
	public Optional<List<File>> getXmiFiles() throws XmiDepotFileException {
		Optional<List<File>> files ;
		try {
			pullRepository();
			File file = new File(DIRETORTY_FILE);	
			files =  FileUtil.getXmlFiles(file);			
		} catch (GitAPIException | IOException | InterruptedException e) {
			throw new XmiDepotFileException("erreur de récupération à partir du depot git", e);
		}
		
		return files ;
		
	}
	
	public void pullRepository() throws InvalidRemoteException, TransportException, GitAPIException, IOException, InterruptedException {
		File file = new File(DIRETORTY_FILE) ;
		if(!file.exists()) {
			cloneDepot();		
		}else {
			pullDepot();
		}		
	}
	
	public void cloneDepot() throws InvalidRemoteException, TransportException, GitAPIException, IOException, InterruptedException {
		File file = new File(DIRETORTY_FILE) ;
		if(file.exists()) {
			FileUtil.clearDirectory(file);
		}
		Git git =Git.cloneRepository()
				.setURI(env.getProperty("git_repository_remote_url"))
				.setDirectory(file)
				.setCloneAllBranches(true)
				//.setBranchesToClone(Arrays.asList("refs/heads/master"))
				.setBranch(env.getProperty("git_repository_default_branch"))
				.call();
	}
	
	public void pullDepot() throws IOException, CheckoutConflictException, GitAPIException, InterruptedException {
		FileRepositoryBuilder repositoryBuilder = new FileRepositoryBuilder();
		Repository repository = null ;
		//file.getPath()
		File file = new File(DIRETORTY_FILE+"\\.git");
		if(file.delete()) {
			File fileDelete = new File(DIRETORTY_FILE);
			FileUtil.clearDirectory(fileDelete);
			cloneDepot();
		}else {
			repository = repositoryBuilder.setGitDir(file)
					.readEnvironment() // scan environment GIT_* variables
					.findGitDir() // scan up the file system tree
					.setMustExist(true)
					.build();
			Git git = new Git(repository);
			git.reset().setMode(ResetType.HARD).call();
			repository.close();			
		}
	}
			

}
