package sncf.reseau.cdsdpadsir.agora.xmi.exception;

public class XmiDepotFileException extends Exception  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public XmiDepotFileException() {
		super();
	}

	public XmiDepotFileException(String message) {
		super(message);
	}
	
	public XmiDepotFileException(String message , Throwable cause) {
		super(message , cause);
	}
	
}
