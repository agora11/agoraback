package sncf.reseau.cdsdpadsir.agora.xmi.util;

public enum TypeModele {

	FONCTIONNEL("fonctionnel"),
	METIER("metier");
	private final String type;

	TypeModele(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
