package sncf.reseau.cdsdpadsir.agora.xmi.util;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

public class DistinctByKey<T> implements Predicate<T> {

	private Function<T, Object> function;
	private Set<Object> seenObjects;

	public DistinctByKey(Function<T, Object> function) {
		this.function = function;
		this.seenObjects = new HashSet<>();
	}

	public boolean test(T t) {
		return seenObjects.add(function.apply(t));
	}

}
