package sncf.reseau.cdsdpadsir.agora.xmi.exception;

@FunctionalInterface
public interface ConsumerWithException<Target , E extends Exception>{

	void accept(Target t) throws E ;
}
