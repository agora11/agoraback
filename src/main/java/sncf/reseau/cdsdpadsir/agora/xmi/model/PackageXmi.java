package sncf.reseau.cdsdpadsir.agora.xmi.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PackageXmi implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String name; 
	private String scope;
	private String packageParentId;
	private String created ;
	private String modified ;
	private VersionXmi version ;
	
}
