package sncf.reseau.cdsdpadsir.agora.xmi.util;

public enum AssociationRole {

	SOURCE("source"),
	DESTINATION("destination");
	private final String role;

	AssociationRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
