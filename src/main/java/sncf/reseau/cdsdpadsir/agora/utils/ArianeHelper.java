package sncf.reseau.cdsdpadsir.agora.utils;

import java.time.LocalDateTime;

import com.sncf.reseau.tec.Uuid;

import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * Class util pour les données en relation avec le model ariane
 * 
 * @author PNBE10831
 *
 */
public class ArianeHelper{
	
	public static final Integer FIRST_REVISION = 1;
	
	/**
	 * Max date personalisée, celle de LocalDateTime ne rentre pas dans l'intervalle authorisé par le SGBD
	 */
	public static final LocalDateTime FUTURE_DATE = LocalDateTime.of(99999, 10, 30, 00, 00, 00);

	public static final String STARS = "********";

	public static <S extends RessourceAbstraite> void setNewEntityDetails(S dto) {

		if (dto != null) {
			dto.setId(Uuid.generateOneTextUUID());
			dto.setDateCreation(LocalDateTime.now());
			dto.setRevision(FIRST_REVISION);
			dto.setDateDebutActivite(LocalDateTime.now());
			dto.setDateFinActivite(ArianeHelper.FUTURE_DATE);
		}
	}
	
	public static <S extends RessourceAbstraite> void updateEntityDetails(S dto, S response) {

		if (dto != null && response != null) {
			response.setId(dto.getId());
			response.setDateCreation(dto.getDateCreation());
			response.setRevision(null != dto.getRevision() ? dto.getRevision().intValue()+1 : FIRST_REVISION);
			response.setDateDebutActivite(LocalDateTime.now());
			response.setDateFinActivite(FUTURE_DATE);
		}
	}
	
	
	public static LocalDateTime getCurrentDate() {
		return LocalDateTime.now();
	}
	
}
