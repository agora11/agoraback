package sncf.reseau.cdsdpadsir.agora.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Join;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Or;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import sncf.reseau.cdsdpadsir.agora.entity.Terme;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.model.ObjetDto;
import sncf.reseau.cdsdpadsir.agora.model.StatutTermeDto;
import sncf.reseau.cdsdpadsir.agora.model.TermeAPIDTO;
import sncf.reseau.cdsdpadsir.agora.model.TermeDto;
import sncf.reseau.cdsdpadsir.agora.service.ConflictException;
import sncf.reseau.cdsdpadsir.agora.service.TermeService;
import sncf.reseau.cdsdpadsir.agora.service.XlsxService;
import sncf.reseau.cdsdpadsir.agora.specification.Specifications;

@RestController
public class TermeController {

	@Autowired
	private TermeService termeService;
	@Autowired
	XlsxService xlsxService;

	/**
	 * GET /v1/termes : Termes liste des termes
	 *
	 * @param page   numéro de la page courante (optional)
	 * @param sort   Le tri est défini comme la détermination de l’ordre dans lequel
	 *               nous souhaitons récupérer les ressources (optional)
	 * @param size   nombre d’éléments par page (optional)
	 * @param filter filtrage avancé (optional)
	 * @param fields Le consommateur du service spécifie dans un paramètre de
	 *               requête fields les champs de la ressource qu’il souhaite
	 *               récupérer exclusivement au sein de la réponse. Ces paramètres
	 *               seront séparés par une virgule (optional)
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Termes", nickname = "getV1Termes", notes = "liste des termes", response = TermeDto.class, responseContainer = "List", tags = {
			"termes", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = TermeDto.class, responseContainer = "List") })
	@GetMapping(value = "/v1/termes", produces = { "application/json" })
	public ResponseEntity<List<TermeDto>> getV1Termes(
			@ApiParam(value = "numéro de la page courante") @Valid @RequestParam(value = "page", required = false) BigDecimal page,
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@ApiParam(value = "nombre d’éléments par page") @Valid @RequestParam(value = "size", required = false) BigDecimal size,
			@ApiParam(value = "filtrage avancé") @Valid @RequestParam(value = "filter", required = false) List<String> filter,
			@ApiParam(value = "Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule") @Valid @RequestParam(value = "fields", required = false) String fields,
			@ApiParam(value = "nom Filiere") @Valid @RequestParam(value = "filiere", required = false) String filiere,
			@Join(path = "proprietaire", alias = "prop") @Join(path = "statutTermes", alias = "statut") @Join(path = "statut.valeurStatutTerme", alias = "valeurStatut") @Join(path = "definitionTermes", alias = "definition") @Join(path = "statut.valideur", alias = "valideur") @Join(path = "statut.suppleant", alias = "suppleant") @Or({
					@Spec(path = "valideur.idIdentiteNumerique", params = "idValideur", spec = Equal.class),
					@Spec(path = "suppleant.idIdentiteNumerique", params = "idSuppleant", spec = Equal.class) }) @And({
							@Spec(path = "idTerme", spec = Equal.class),
							@Spec(path = "abreviation", spec = Equal.class),
							@Spec(path = "commentaire", spec = Equal.class),
							@Spec(path = "libelle", spec = Equal.class),
							@Spec(path = "definition.definition", params = "definition", spec = Equal.class),
							@Spec(path = "valeurStatut.libelle", params = "status", spec = Equal.class),
							@Spec(path = "prop.idIdentiteNumerique", params = "proprietaire", spec = Equal.class),
							@Spec(path = "id", spec = Equal.class) })

			Specification<Terme> termeSpec) {

		Specification<Terme> activeTerme = Specification.where(termeSpec).and(Specifications.getTermesByRevision());

		if (filiere != null) {
			List<String> filieres = Arrays.asList(filiere.split(","));
			activeTerme = activeTerme.and(Specifications.getTermesByFiliere(filieres));
		}
		final PagingResponse<TermeDto> response = termeService.findAll(activeTerme, sort, page, size);
		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}

	@ApiOperation(value = "Termes", nickname = "getV1TermesApi", notes = "liste des termes", response = TermeAPIDTO.class, responseContainer = "List", tags = {
			"termes", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = TermeDto.class, responseContainer = "List") })
	@GetMapping(value = "/v3/connaissance/glossaire/termes", produces = { "application/json" })
	public ResponseEntity<List<TermeAPIDTO>> getV1TermesApi(
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@And({}) Specification<Terme> termeSpec) {

		Specification<Terme> activeTerme = Specification.where(termeSpec).and(Specifications.getTermesByRevision());

		final PagingResponse<TermeAPIDTO> response = termeService.findAllTermesAPI(activeTerme, sort);
		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}

	/**
	 * POST /v1/termes : Add terme ajouter un terme
	 *
	 * @param termeDto
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Add terme", nickname = "postV1Termes", notes = "ajouter un terme", response = TermeDto.class, tags = {
			"termes", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = TermeDto.class) })
	@PostMapping(value = "/v1/termes", produces = { "application/json" }, consumes = { "application/json" })
	public ResponseEntity<TermeDto> postV1Termes(
			@ApiParam(value = "") @Valid @RequestBody(required = true) TermeDto termeDto) {

		if (termeService.checkUniquenessByFiliere(termeDto)) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		} else if (termeService.checkUniqueness(termeDto)) {
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.set("warning", "Le terme ajouté existe dans une autre filière avec le même libellé");
			return new ResponseEntity<>(termeService.add(termeDto), responseHeaders, HttpStatus.OK);
		}
		return new ResponseEntity<>(termeService.add(termeDto), HttpStatus.OK);
	}

	/**
	 * PUT /v1/termes : Update terme mettre à jour un terme
	 *
	 * @param termeDto (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "Update terme", nickname = "putV1TermesId", notes = "mettre à jour un terme", tags = {
			"termes", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Not Found") })
	@PutMapping(value = "/v1/termes")
	public ResponseEntity<TermeDto> putV1TermesId(
			@ApiParam(value = "") @RequestBody(required = true) TermeDto termeDto) {
		try {
			HttpHeaders responseHeaders = new HttpHeaders();
			if (termeService.checkUniquenessForEdit(termeDto)) {
				responseHeaders.set("warning", "Le terme ajouté existe dans une autre filière avec le même libelle");
			}
			return new ResponseEntity<>(termeService.update(termeDto), responseHeaders, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (ConflictException e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}

	}

	/**
	 * DELETE /v1/termes/{id} : delete terme supprimer un terme
	 *
	 * @param id identifiant terme (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "delete terme", nickname = "deleteV1TermesId", notes = "supprimer un terme", response = TermeDto.class, tags = {
			"termes", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = TermeDto.class),
			@ApiResponse(code = 404, message = "Not Found") })
	@DeleteMapping(value = "/v1/termes/{id}", produces = { "application/json" })
	public ResponseEntity<Object> deleteV1TermesId(
			@ApiParam(value = "identifiant terme", required = true) @PathVariable("id") BigInteger id) {
		try {
			return new ResponseEntity<>("Success: " + termeService.delete(id), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * GET /v1/termes/{id} : Terme récupérer un terme avec son id
	 *
	 * @param id identifiant terme (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "Terme", nickname = "getV1TermesId", notes = "récupérer un terme avec son id", response = TermeDto.class, tags = {
			"termes", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = TermeDto.class),
			@ApiResponse(code = 404, message = "Not Found") })
	@GetMapping(value = "/v1/termes/{id}", produces = { "application/json" })
	public ResponseEntity<Object> getV1TermesId(
			@ApiParam(value = "identifiant terme", required = true) @PathVariable("id") Long id) {

		try {
			return new ResponseEntity<>(termeService.findByIdTerme(id), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}

	}

	/**
	 * GET /v1/termes/statut/{id} : récupérer les statuts par id(guid)
	 *
	 * @param id guid statut (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "statut", nickname = "getV1StatutId", notes = "récupérer les statut par id(guid)", response = StatutTermeDto.class, tags = {
			"termes", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ObjetDto.class),
			@ApiResponse(code = 404, message = "Not Found") })
	@GetMapping(value = "/v1/termes/statut/{id}", produces = { "application/json" })
	public ResponseEntity<?> getV1Statut(
			@ApiParam(value = "guid statut", required = true) @PathVariable("id") String id) {
		try {
			return new ResponseEntity<>(termeService.getStatuts(id), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(value = "/v1/termes/file", produces = { "application/json" })
	public ResponseEntity<?> loadFile(@RequestParam String nom, @RequestParam String prenom,
			@RequestParam String filiere, @RequestParam String nomFichier) {

		try {
			xlsxService.loadTermes(filiere, nom, prenom, nomFichier);
			return new ResponseEntity<>(true, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}
