package sncf.reseau.cdsdpadsir.agora.api;

import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import sncf.reseau.cdsdpadsir.agora.entity.Gisement;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.model.ActeurDto;
import sncf.reseau.cdsdpadsir.agora.model.GisementDto;
import sncf.reseau.cdsdpadsir.agora.service.ConflictException;
import sncf.reseau.cdsdpadsir.agora.service.GisementService;
import sncf.reseau.cdsdpadsir.agora.specification.Specifications;

@RestController
public class GisementController {

	@Autowired
	private GisementService gisementService;

	@ApiOperation(value = "Gisements", nickname = "getV1Gisements", notes = "liste des gisements", response = GisementDto.class, responseContainer = "List", tags = {
			"gisements", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = GisementDto.class, responseContainer = "List") })
	@GetMapping(value = {"/v1/gisements","/v3/connaissance/gouvernance/gisements"}, produces = { "application/json" })
	public ResponseEntity<List<GisementDto>> getV1Gisements(
			@ApiParam(value = "numéro de la page courante") @Valid @RequestParam(value = "page", required = false) BigDecimal page,
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@ApiParam(value = "nombre d’éléments par page") @Valid @RequestParam(value = "size", required = false) BigDecimal size,
			@ApiParam(value = "filtrage avancé") @Valid @RequestParam(value = "filter", required = false) List<String> filter,
			@ApiParam(value = "Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule") @Valid @RequestParam(value = "fields", required = false) String fields,
			@ApiParam(value = "nom Filiere") @Valid @RequestParam(value = "filiere", required = false) String filiere,
			@And({ @Spec(path = "idGisement", spec = Equal.class), @Spec(path = "libelle", spec = Equal.class),
					@Spec(path = "id", spec = Equal.class) })

			Specification<Gisement> gisementSpec) {

		Specification<Gisement> activeGisement = Specification.where(gisementSpec)
				.and(Specifications.getGisementsByRevision());

		final PagingResponse<GisementDto> response = gisementService.findAll(activeGisement, sort, page, size);
		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}

	/**
	 * POST /v1/gisements : Add gisement ajouter un gisement
	 *
	 * @param gisementDto
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Add gisement", nickname = "postV1Gisements", notes = "ajouter un gisement", response = GisementDto.class, tags = {
			"gisements", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = GisementDto.class) })
	@PostMapping(value = "/v1/gisements", produces = { "application/json" }, consumes = { "application/json" })
	public ResponseEntity<GisementDto> postV1Gisements(
			@ApiParam(value = "") @Valid @RequestBody(required = true) GisementDto gisementDto) {

		return new ResponseEntity<>(gisementService.add(gisementDto), HttpStatus.OK);
	}

	/**
	 * PUT /v1/gisements : Update gisement mettre à jour un gisement
	 *
	 * @param gisementDto (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "Update gisement", nickname = "putV1GisementsId", notes = "mettre à jour un gisement", tags = {
			"gisements", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Not Found") })
	@PutMapping(value = "/v1/gisements")
	public ResponseEntity<GisementDto> putV1GisementsId(
			@ApiParam(value = "") @RequestBody(required = true) GisementDto gisementDto) {
		try {
			HttpHeaders responseHeaders = new HttpHeaders();
			return new ResponseEntity<>(gisementService.update(gisementDto), responseHeaders, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (ConflictException e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}

	}

	/**
	 * DELETE /v1/gisements/{id} : delete gisement supprimer un gisement
	 *
	 * @param id identifiant gisement (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "delete gisement", nickname = "deleteV1GisementsId", notes = "supprimer un gisement", response = GisementDto.class, tags = {
			"gisements", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = GisementDto.class),
			@ApiResponse(code = 404, message = "Not Found") })
	@DeleteMapping(value = "/v1/gisements/{id}", produces = { "application/json" })
	public ResponseEntity<Object> deleteV1GisementsId(
			@ApiParam(value = "identifiant gisement", required = true) @PathVariable("id") Long id) {
		try {
			return new ResponseEntity<>("Success: " + gisementService.delete(id), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * DELETE /v1/acteur/{id} : delete acteur supprimer un acteur
	 *
	 * @param id identifiant acteur (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "delete acteur", nickname = "deleteV1GisementsId", notes = "supprimer un acteur", response = ActeurDto.class, tags = {
			"gisements", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = GisementDto.class),
			@ApiResponse(code = 404, message = "Not Found") })
	@DeleteMapping(value = "/v1/gisements/acteurs/{id}", produces = { "application/json" })
	public ResponseEntity<Object> deleteV1ActeurGisement(
			@ApiParam(value = "identifiant acteur", required = true) @PathVariable("id") Long id) {
		try {
			return new ResponseEntity<>("Success: " + gisementService.deleteActeur(id), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * DELETE /v1/lienUtile/{id} : delete lienUtile supprimer un lienUtile
	 *
	 * @param id identifiant lienUtile (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "delete lienUtile", nickname = "deleteV1GisementsId", notes = "supprimer un lienUtile", response = ActeurDto.class, tags = {
			"gisements", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = GisementDto.class),
			@ApiResponse(code = 404, message = "Not Found") })
	@DeleteMapping(value = "/v1/gisements/lienUtiles/{id}", produces = { "application/json" })
	public ResponseEntity<Object> deleteV1LienUtileGisement(
			@ApiParam(value = "identifiant lienUtile", required = true) @PathVariable("id") Long id) {
		try {
			return new ResponseEntity<>("Success: " + gisementService.deleteLienUtile(id), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

}
