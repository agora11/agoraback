package sncf.reseau.cdsdpadsir.agora.api;

import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import sncf.reseau.cdsdpadsir.agora.entity.Famille;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.model.FamilleDto;
import sncf.reseau.cdsdpadsir.agora.model.TermeDto;
import sncf.reseau.cdsdpadsir.agora.service.FamilleService;
import sncf.reseau.cdsdpadsir.agora.specification.Specifications;

@RestController
public class FamilleController {

	@Autowired
	private FamilleService familleService;

	/**
	 * GET /v1/familles : Liste des familles Liste des familles
	 *
	 * @param page   numéro de la page courante (optional)
	 * @param size   nombre d’éléments par page (optional)
	 * @param sort   Le tri est défini comme la détermination de l’ordre dans lequel
	 *               nous souhaitons récupérer les ressources (optional)
	 * @param filter filtrage avancé (optional)
	 * @param fields Le consommateur du service spécifie dans un paramètre de
	 *               requête fields les champs de la ressource qu’il souhaite
	 *               récupérer exclusivement au sein de la réponse. Ces paramètres
	 *               seront séparés par une virgule (optional)
	 * @return OK (status code 200) or Partial Content (status code 206)
	 */
	@ApiOperation(value = "Liste des familles", nickname = "getV1Familles", notes = "Liste des familles", response = FamilleDto.class, responseContainer = "List", tags = {
			"familles", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = FamilleDto.class, responseContainer = "List"),
			@ApiResponse(code = 206, message = "Partial Content", response = FamilleDto.class, responseContainer = "List") })
	@GetMapping(value = "/v1/familles", produces = { "application/json" })
	public ResponseEntity<List<FamilleDto>> getV1Familles(
			@ApiParam(value = "numéro de la page courante") @Valid @RequestParam(value = "page", required = false) BigDecimal page,
			@ApiParam(value = "nombre d’éléments par page") @Valid @RequestParam(value = "size", required = false) BigDecimal size,
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@ApiParam(value = "filtrage avancé") @Valid @RequestParam(value = "filter", required = false) String filter,
			@ApiParam(value = "Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule") @Valid @RequestParam(value = "fields", required = false) String fields) {
		Specification<Famille> activeFamille = Specification.where(Specifications.getFamillesByRevision());
		final PagingResponse<FamilleDto> response = familleService.findAll(activeFamille, sort, page, size);
		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}
	
	/**
	 * POST /v1/familles : Add famille ajouter une famille
	 *
	 * @param familleDto
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Add famille", nickname = "postV1Familles", notes = "ajouter une famille", response = TermeDto.class, tags = {
			"familles", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = FamilleDto.class) })
	@PostMapping(value = "/v1/familles", produces = { "application/json" }, consumes = { "application/json" })
	public ResponseEntity<FamilleDto> postV1Familles(
			@ApiParam(value = "") @Valid @RequestBody(required = true) FamilleDto familleDto) {

		return new ResponseEntity<>(familleService.add(familleDto), HttpStatus.OK);
	}

	/**
	 * PUT /v1/familles : Update famille mettre à jour une famille
	 *
	 * @param familleDto (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "Update famille", nickname = "putV1Famille", notes = "mettre à jour une famille", tags = {
			"familles", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Not Found") })
	@PutMapping(value = "/v1/familles")
	public ResponseEntity<FamilleDto> putV1Familles(
			@ApiParam(value = "") @RequestBody(required = true) FamilleDto familleDto) {
		try {
			return new ResponseEntity<>(familleService.update(familleDto), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}
	
	/**
	 * PUT /v1/familles : Delete famille archiver une famille
	 *
	 * @param familleDto (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "Delete famille", nickname = "putV1Famille", notes = "archiver une famille", tags = {
			"familles", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Not Found") })
	@PutMapping(value = "/v1/familles/delete")
	public ResponseEntity<FamilleDto> deleteV1Familles(
			@ApiParam(value = "") @RequestBody(required = true) FamilleDto familleDto) {
		try {
			return new ResponseEntity<>(familleService.delete(familleDto), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}
}
