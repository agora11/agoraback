package sncf.reseau.cdsdpadsir.agora.api;

import java.math.BigDecimal;

import javax.persistence.criteria.JoinType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.kaczmarzyk.spring.data.jpa.domain.LikeIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Join;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Or;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import sncf.reseau.cdsdpadsir.agora.entity.ClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.entity.Filiere;
import sncf.reseau.cdsdpadsir.agora.entity.Gisement;
import sncf.reseau.cdsdpadsir.agora.entity.Terme;
import sncf.reseau.cdsdpadsir.agora.entity.Utilisateur;
import sncf.reseau.cdsdpadsir.agora.model.RechercheDto;
import sncf.reseau.cdsdpadsir.agora.service.RechercheService;
import sncf.reseau.cdsdpadsir.agora.specification.Specifications;

@RestController
public class RechercheController {

	@Autowired
	RechercheService recherceService;

	@GetMapping(value = "/v1/search", produces = { "application/json" })
	ResponseEntity<RechercheDto> getSearchResult(@RequestParam(value = "page", required = false) BigDecimal page,
			Sort sort, @RequestParam(value = "size", required = false) BigDecimal size, @RequestParam String keyWord,
			@Join(path = "definitionClasseDobjets", alias = "definition", type = JoinType.LEFT)

			@Join(path = "proprietaire", alias = "proprietaireId")

			@Join(path = "proprietaireId.utilisateur", alias = "proprietaire")

			@Join(path = "statutClasseDobjets", alias = "statutClasseDobjet")

			@Join(path = "statutClasseDobjet.valeurStatutClasseDobjet", alias = "valeurStatut")

			@Join(path = "statutClasseDobjet.valideur", alias = "valideurId", type = JoinType.LEFT)

			@Join(path = "valideurId.utilisateur", alias = "valideur", type = JoinType.LEFT)

			@Join(path = "statutClasseDobjet.suppleant", alias = "suppleantId", type = JoinType.LEFT)

			@Join(path = "suppleantId.utilisateur", alias = "suppleant", type = JoinType.LEFT)

			@Join(path = "associationClasseDobjetFamilles", alias = "associationClasseDobjetFamillesAlias", type = JoinType.LEFT)

			@Join(path = "associationClasseDobjetFamillesAlias.famille", alias = "famille", type = JoinType.LEFT)

			@Join(path = "associationClasseDobjetSousFamilles", alias = "associationClasseDobjetSousFamillesAlias", type = JoinType.LEFT)

			@Join(path = "associationClasseDobjetSousFamillesAlias.famille", alias = "sousFamille", type = JoinType.LEFT)

			@Join(path = "associationClasseDobjetFilieres", alias = "associationClasseDobjetFilieresAlias", type = JoinType.LEFT)

			@Join(path = "associationClasseDobjetFilieresAlias.filiere", alias = "filiere", type = JoinType.LEFT)

			@Or({ @Spec(path = "famille.nom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "sousFamille.nom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "filiere.nom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "abreviation", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "definition.definition", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "valeurStatut.libelle", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "commentaire", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "proprietaire.nom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "proprietaire.prenom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "libelle", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "valideur.nom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "valideur.prenom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "suppleant.nom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "suppleant.prenom", params = "keyWord", spec = LikeIgnoreCase.class) }) Specification<ClasseDobjet> objetSpec,

			@Join(path = "proprietaire", alias = "propId")

			@Join(path = "propId.utilisateur", alias = "prop")

			@Join(path = "statutTermes", alias = "statut")

			@Join(path = "statut.valeurStatutTerme", alias = "valeurStatut")

			@Join(path = "definitionTermes", alias = "definition", type = JoinType.LEFT)

			@Join(path = "statut.valideur", alias = "valideurId", type = JoinType.LEFT)

			@Join(path = "valideurId.utilisateur", alias = "valideur", type = JoinType.LEFT)

			@Join(path = "statut.suppleant", alias = "suppleantId", type = JoinType.LEFT)

			@Join(path = "suppleantId.utilisateur", alias = "suppleant", type = JoinType.LEFT)

			@Join(path = "associationTermeFilieres", alias = "associationTermeFilieresId", type = JoinType.LEFT)

			@Join(path = "associationTermeFilieresId.filiere", alias = "filiereId", type = JoinType.LEFT)

			@Join(path = "associationTermeDocuments", alias = "associationTermeDocumentId", type = JoinType.LEFT)

			@Join(path = "associationTermeDocumentId.document", alias = "documentId", type = JoinType.LEFT)

			@Or({ @Spec(path = "valideur.nom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "valideur.prenom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "suppleant.nom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "suppleant.prenom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "abreviation", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "commentaire", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "libelle", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "definition.definition", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "valeurStatut.libelle", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "prop.nom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "prop.prenom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "filiereId.nom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "documentId.url", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "documentId.titre", params = "keyWord", spec = LikeIgnoreCase.class)}) Specification<Terme> termeSpec,
			@Join(path = "filiereFamilles", alias = "filiereFamilles", type = JoinType.LEFT)

			@Join(path = "filiereFamilles.famille", alias = "famille", type = JoinType.LEFT)

			@Join(path = "famille.familles", alias = "sousFamille", type = JoinType.LEFT)

			@Join(path = "filiereResponsables", alias = "filiereResponsablesId", type = JoinType.LEFT)

			@Join(path = "filiereResponsablesId.coordinateur", alias = "coordinateurId", type = JoinType.LEFT)

			@Join(path = "coordinateurId.utilisateur", alias = "coordinateurUser", type = JoinType.LEFT)

			@Join(path = "filiereResponsablesId.responsableDelegue", alias = "responsableDelegueId", type = JoinType.LEFT)

			@Join(path = "responsableDelegueId.utilisateur", alias = "responsableDelegue", type = JoinType.LEFT)

			@Join(path = "filiereResponsablesId.responsable", alias = "responsableId", type = JoinType.LEFT)

			@Join(path = "responsableId.utilisateur", alias = "responsable", type = JoinType.LEFT)

			@Or({ @Spec(path = "nom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "description", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "famille.nom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "sousFamille.nom", params = "keyWord", spec = LikeIgnoreCase.class),

					@Spec(path = "coordinateurUser.nom", params = "keyWord", spec = LikeIgnoreCase.class),

					@Spec(path = "responsableDelegue.nom", params = "keyWord", spec = LikeIgnoreCase.class),

					@Spec(path = "responsable.nom", params = "keyWord", spec = LikeIgnoreCase.class),

					@Spec(path = "coordinateurUser.prenom", params = "keyWord", spec = LikeIgnoreCase.class),

					@Spec(path = "responsableDelegue.prenom", params = "keyWord", spec = LikeIgnoreCase.class),

					@Spec(path = "responsable.prenom", params = "keyWord", spec = LikeIgnoreCase.class)

			}) Specification<Filiere> filiereSpec,
			@Or({ @Spec(path = "nom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "prenom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "organisation", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "cp", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "adresseMail", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "codeRoleFonctionnel", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "description", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "poste", params = "keyWord", spec = LikeIgnoreCase.class) }) Specification<Utilisateur> utilisateurSpec,

			@Join(path = "typeElementModeleObjet", alias = "typeElementModeleObjetId")

			@Join(path = "critereQualites", alias = "critereQualitesId", type = JoinType.LEFT)

			@Join(path = "critereQualitesId.politiqueQualites", alias = "politiqueQualitesId", type = JoinType.LEFT)

			@Join(path = "politiqueQualitesId.tauxQualiteMesures", alias = "tauxQualiteMesuresId", type = JoinType.LEFT)

			@Join(path = "politiqueQualitesId.politiqueQualiteParUsages", alias = "politiqueQualiteParUsagesId", type = JoinType.LEFT)

			@Or({ @Spec(path = "libelle", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "critereQualitesId.libelle", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "politiqueQualitesId.indicateurDeQualite", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "politiqueQualitesId.description", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "politiqueQualitesId.url", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "politiqueQualitesId.tauxPromis", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "tauxQualiteMesuresId.dateDeMesure", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "tauxQualiteMesuresId.tauxMesure", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "politiqueQualiteParUsagesId.tauxSouhaite", params = "keyWord", spec = LikeIgnoreCase.class) }) Specification<ClasseDobjet> objetFonctionnelSpec,
			@Join(path = "associationActeurGisements", alias = "associationActeurGisementsId", type = JoinType.LEFT)

			@Join(path = "associationActeurGisementsId.acteur", alias = "acteurId", type = JoinType.LEFT)

			@Join(path = "associationFiliereGisements", alias = "associationFiliereGisementsId", type = JoinType.LEFT)

			@Join(path = "associationFiliereGisementsId.filiere", alias = "filiereId", type = JoinType.LEFT)

			@Join(path = "associationLienUtileGisements", alias = "associationLienUtileGisementsId", type = JoinType.LEFT)

			@Join(path = "associationLienUtileGisementsId.lienUtile", alias = "lienUtileId", type = JoinType.LEFT)

			@Or({ @Spec(path = "libelle", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "niveauDeCompletude", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "nombreDeJeuxDonnees", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "nombreDeClients", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "nombreDeFournisseurs", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "nombreDeServices", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "detailsClientsExternes", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "nombreClientsInternes", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "nombreClientsExternes", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "nombreObjetsReferences", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "nombreServicesWebExposesDataLab", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "nombreObjetsReferencesEnProjet", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "nombreServicesEnProjet", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "nombreServicesWebExposesDataLabEnProjet", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "raisonEtre", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "raisonEtreCourte", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "servicesDocumentes", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "statut", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "description", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "dateMiseEnService", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "acteurId.contact", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "acteurId.nom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "acteurId.role", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "filiereId.nom", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "lienUtileId.libelle", params = "keyWord", spec = LikeIgnoreCase.class),
					@Spec(path = "lienUtileId.url", params = "keyWord", spec = LikeIgnoreCase.class)

			})

			Specification<Gisement> gisementSpec) {

		Specification<Utilisateur> activeUtilisateur = Specification.where(utilisateurSpec)
				.and(Specifications.getUtilisateursByRevision());

		Specification<ClasseDobjet> activeObjet = Specification.where(objetSpec)
				.and(Specifications.getObjetsByRevision());

		Specification<ClasseDobjet> activeObjetFonctionnel = Specification.where(objetFonctionnelSpec)
				.and(Specifications.getObjetsByRevision());

		Specification<Terme> activeTerme = Specification.where(termeSpec).and(Specifications.getTermesByRevision());
		Specification<Filiere> activeFiliere = Specification.where(filiereSpec)
				.and(Specifications.getFilieresByRevision());

		Specification<Gisement> activeGisement = Specification.where(gisementSpec)
				.and(Specifications.getGisementsByRevision());

		RechercheDto result = recherceService.search(keyWord, sort, page, size, activeGisement, activeFiliere,
				activeObjet, activeObjetFonctionnel, activeTerme, activeUtilisateur);

		return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.OK);

	}

}
