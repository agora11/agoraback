package sncf.reseau.cdsdpadsir.agora.api;

import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.NativeWebRequest;

import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingHeaders;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ApiUtil {

	private ApiUtil() {
		super();
	}

	public static void setExampleResponse(NativeWebRequest req, String contentType, String example) {
		try {
			HttpServletResponse res = req.getNativeResponse(HttpServletResponse.class);
			res.setCharacterEncoding("UTF-8");
			res.addHeader("Content-Type", contentType);
			res.getWriter().print(example);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static HttpHeaders returnHttpHeaders(PagingResponse response) {
		HttpHeaders headers = new HttpHeaders();
		headers.set(PagingHeaders.COUNT.getName(), String.valueOf(response.getCount()));
		headers.set(PagingHeaders.PAGE_SIZE.getName(), String.valueOf(response.getPageSize()));
		headers.set(PagingHeaders.PAGE_OFFSET.getName(), String.valueOf(response.getPageOffset()));
		headers.set(PagingHeaders.PAGE_NUMBER.getName(), String.valueOf(response.getPageNumber()));
		headers.set(PagingHeaders.PAGE_TOTAL.getName(), String.valueOf(response.getPageTotal()));
		return headers;
	}
}
