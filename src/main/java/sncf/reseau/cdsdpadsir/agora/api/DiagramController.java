package sncf.reseau.cdsdpadsir.agora.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sncf.reseau.cdsdpadsir.agora.model.DiagramDto;
import sncf.reseau.cdsdpadsir.agora.service.DiagramService;

import java.util.List;

@RestController
public class DiagramController {

    @Autowired
    DiagramService diagramService;


    @PostMapping(value = "/save-diagram", consumes = {"application/json"}, produces = {"application/json"})
    public DiagramDto saveDiagram(@RequestBody DiagramDto diagram) {

        return diagramService.saveDiagram(diagram);
    }

    @GetMapping(value = "/get-diagram/{id}", produces = {"application/json"})
    public DiagramDto getDiagram(@PathVariable("id") Long id) {
        return diagramService.getDiagram(id);
    }


    @GetMapping(value = "/get-all-diagrams", produces = {"application/json"})
    public List<DiagramDto> getAllDiagrams() {
        return diagramService.getAllDiagrams();
    }

    @PutMapping(value = "/update-diagram", consumes = {"application/json"}, produces = {"application/json"})
    public DiagramDto updateDiagram(@RequestBody DiagramDto diagram) {
        return diagramService.updateDiagram(diagram);
    }

    @PatchMapping(value = "/update-diagram-meta-data", consumes = {"application/json"}, produces = {"application/json"})
    public DiagramDto upadateDiagramMetaData(@RequestBody DiagramDto diagram) {
        return diagramService.updateDiagramMetaData(diagram);
    }
    @PatchMapping(value = "/archive-diagram", consumes = {"application/json"}, produces = {"application/json"})
    public DiagramDto archiveDiagram(@RequestBody DiagramDto diagram) {
        return diagramService.archiveDiagram(diagram);
    }
}
