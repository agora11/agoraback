package sncf.reseau.cdsdpadsir.agora.api;

import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import sncf.reseau.cdsdpadsir.agora.entity.Filiere;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.model.FiliereApiDto;
import sncf.reseau.cdsdpadsir.agora.model.FiliereDto;
import sncf.reseau.cdsdpadsir.agora.model.FiliereFamilleApiDto;
import sncf.reseau.cdsdpadsir.agora.model.ObjetDto;
import sncf.reseau.cdsdpadsir.agora.service.FiliereService;
import sncf.reseau.cdsdpadsir.agora.specification.Specifications;

@RestController
public class FiliereController {

	@Autowired
	FiliereService filiereService;

	/**
	 * GET /v1/filieres : Filieres Récupérer toutes les filières
	 *
	 * @param page   numéro de la page courante (optional)
	 * @param size   nombre d’éléments par page (optional)
	 * @param sort   Le tri est défini comme la détermination de l’ordre dans lequel
	 *               nous souhaitons récupérer les ressources (optional)
	 * @param filter filtrage avancé (optional)
	 * @param fields Le consommateur du service spécifie dans un paramètre de
	 *               requête fields les champs de la ressource qu’il souhaite
	 *               récupérer exclusivement au sein de la réponse. Ces paramètres
	 *               seront séparés par une virgule (optional)
	 * @return OK (status code 200) or Created (status code 201)
	 */
	@ApiOperation(value = "Filieres", nickname = "getV1Filieres", notes = "Récupérer toutes les filières", response = FiliereDto.class, responseContainer = "List", tags = {
			"Filieres", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = FiliereDto.class, responseContainer = "List"),
			@ApiResponse(code = 201, message = "Created") })
	@GetMapping(value = "/v1/filieres", produces = { "application/json" })
	ResponseEntity<List<FiliereDto>> getV1Filieres(
			@ApiParam(value = "numéro de la page courante") @Valid @RequestParam(value = "page", required = false) BigDecimal page,
			@ApiParam(value = "nombre d’éléments par page") @Valid @RequestParam(value = "size", required = false) BigDecimal size,
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@ApiParam(value = "filtrage avancé") @Valid @RequestParam(value = "filter", required = false) String filter,
			@ApiParam(value = "Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule") @Valid @RequestParam(value = "fields", required = false) String fields,
			@And({ @Spec(path = "id_filiere", spec = Equal.class), @Spec(path = "nom", spec = Equal.class),
					@Spec(path = "id", spec = Equal.class),
					@Spec(path = "description", spec = Equal.class) }) Specification<Filiere> filiereSpec) {

		Specification<Filiere> activeObjet = Specification.where(filiereSpec)
				.and(Specifications.getFilieresByRevision());
		final PagingResponse<FiliereDto> response = filiereService.findAll(activeObjet, sort, page, size);
		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}

	/**
	 * GET /v1/filieres : Filieres Récupérer toutes les filières
	 *
	 * @param page   numéro de la page courante (optional)
	 * @param size   nombre d’éléments par page (optional)
	 * @param sort   Le tri est défini comme la détermination de l’ordre dans lequel
	 *               nous souhaitons récupérer les ressources (optional)
	 * @param filter filtrage avancé (optional)
	 * @param fields Le consommateur du service spécifie dans un paramètre de
	 *               requête fields les champs de la ressource qu’il souhaite
	 *               récupérer exclusivement au sein de la réponse. Ces paramètres
	 *               seront séparés par une virgule (optional)
	 * @return OK (status code 200) or Created (status code 201)
	 */
	@ApiOperation(value = "Filieres", nickname = "getV1Filieres", notes = "Récupérer toutes les filières", response = FiliereDto.class, responseContainer = "List", tags = {
			"Filieres", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = FiliereDto.class, responseContainer = "List"),
			@ApiResponse(code = 201, message = "Created") })
	@GetMapping(value = "/v3/connaissance/gouvernance/filieres", produces = { "application/json" })
	ResponseEntity<List<FiliereApiDto>> getV1ApiFilieres(
			@ApiParam(value = "numéro de la page courante") @Valid @RequestParam(value = "page", required = false) BigDecimal page,
			@ApiParam(value = "nombre d’éléments par page") @Valid @RequestParam(value = "size", required = false) BigDecimal size,
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@ApiParam(value = "filtrage avancé") @Valid @RequestParam(value = "filter", required = false) String filter,
			@ApiParam(value = "Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule") @Valid @RequestParam(value = "fields", required = false) String fields,
			@And({ @Spec(path = "id_filiere", spec = Equal.class), @Spec(path = "nom", spec = Equal.class),
					@Spec(path = "id", spec = Equal.class),
					@Spec(path = "description", spec = Equal.class) }) Specification<Filiere> filiereSpec) {

		Specification<Filiere> activeObjet = Specification.where(filiereSpec)
				.and(Specifications.getFilieresByRevision());
		final PagingResponse<FiliereApiDto> response = filiereService.findAllApi(activeObjet, sort, page, size);
		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}

	/**
	 * GET /v1/filieres : Filieres Récupérer toutes les filières
	 *
	 * @param page   numéro de la page courante (optional)
	 * @param size   nombre d’éléments par page (optional)
	 * @param sort   Le tri est défini comme la détermination de l’ordre dans lequel
	 *               nous souhaitons récupérer les ressources (optional)
	 * @param filter filtrage avancé (optional)
	 * @param fields Le consommateur du service spécifie dans un paramètre de
	 *               requête fields les champs de la ressource qu’il souhaite
	 *               récupérer exclusivement au sein de la réponse. Ces paramètres
	 *               seront séparés par une virgule (optional)
	 * @return OK (status code 200) or Created (status code 201)
	 */
	@ApiOperation(value = "Filieres", nickname = "getV1Filieres", notes = "Récupérer toutes les filières", response = FiliereDto.class, responseContainer = "List", tags = {
			"Filieres", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = FiliereDto.class, responseContainer = "List"),
			@ApiResponse(code = 201, message = "Created") })
	@GetMapping(value = "/v3/connaissance/gouvernance/familles", produces = { "application/json" })
	ResponseEntity<List<FiliereFamilleApiDto>> getV1ApiFilieresFamilles(
			@ApiParam(value = "numéro de la page courante") @Valid @RequestParam(value = "page", required = false) BigDecimal page,
			@ApiParam(value = "nombre d’éléments par page") @Valid @RequestParam(value = "size", required = false) BigDecimal size,
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@ApiParam(value = "filtrage avancé") @Valid @RequestParam(value = "filter", required = false) String filter,
			@ApiParam(value = "Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule") @Valid @RequestParam(value = "fields", required = false) String fields,
			@And({ @Spec(path = "id_filiere", spec = Equal.class), @Spec(path = "nom", spec = Equal.class),
					@Spec(path = "id", spec = Equal.class),
					@Spec(path = "description", spec = Equal.class) }) Specification<Filiere> filiereSpec) {

		Specification<Filiere> activeObjet = Specification.where(filiereSpec)
				.and(Specifications.getFilieresByRevision());
		final PagingResponse<FiliereFamilleApiDto> response = filiereService.findAllFiliereFamilleApi(activeObjet, sort,
				page, size);
		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}

	/**
	 * GET /v1/filieres/{id}
	 *
	 * @param id identifiant filiere (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "Objet", nickname = "getV1FilieresId", notes = "récupérer une filiere par id", response = FiliereDto.class, tags = {
			"filieres", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ObjetDto.class),
			@ApiResponse(code = 404, message = "Not Found") })
	@GetMapping(value = "/v1/filieres/{id}", produces = { "application/json" })
	ResponseEntity<Object> getV1FilieresId(
			@ApiParam(value = "identifiant filiere", required = true) @PathVariable("id") Long id) {
		try {
			return new ResponseEntity<>(filiereService.get(id), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}

	}

	/**
	 * POST /v1/filiere : Add filiere ajouter une filiere
	 *
	 * @param filiereDto
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Add filiere", nickname = "postV1Filieres", notes = "ajouter une filiere", response = FiliereDto.class, tags = {
			"filieres", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = FiliereDto.class) })
	@PostMapping(value = "/v1/filieres", produces = { "application/json" }, consumes = { "application/json" })
	public ResponseEntity<FiliereDto> postV1Filiere(
			@ApiParam(value = "") @Valid @RequestBody(required = true) FiliereDto filiereDto) {

		return new ResponseEntity<>(filiereService.add(filiereDto), HttpStatus.OK);
	}

	/**
	 * PUT /v1/filieres : Update filiere mettre à jour une filiere
	 *
	 * @param filiereDto (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "Update filiere", nickname = "putV1TermesId", notes = "mettre à jour une filiere", tags = {
			"filieres", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Not Found") })
	@PutMapping(value = "/v1/filieres")
	public ResponseEntity<FiliereDto> putV1Filiere(
			@ApiParam(value = "") @RequestBody(required = true) FiliereDto filiereDto) {
		try {
			return new ResponseEntity<>(filiereService.update(filiereDto), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	/**
	 * POST /v1/filieres/intervenant : Add intervenant ajouter un intervenant
	 *
	 * @param filiereDto
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Add filiere", nickname = "postV1Filieres", notes = "ajouter un intervenant", response = FiliereDto.class, tags = {
			"filieres", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = FiliereDto.class) })
	@PostMapping(value = "/v1/filieres/intervenant", produces = { "application/json" }, consumes = {
			"application/json" })
	public ResponseEntity<FiliereDto> postV1Intervenant(
			@ApiParam(value = "") @Valid @RequestBody(required = true) FiliereDto filiereDto) {

		return new ResponseEntity<>(filiereService.addIntervenant(filiereDto), HttpStatus.OK);
	}

	/**
	 * DELETE /v1/filieres : Delete filiere supprimer une filiere
	 *
	 * @param id (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "Delete filiere", nickname = "deleteV1Filiere", notes = "supprimer une filiere", tags = {
			"filieres", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Not Found") })
	@DeleteMapping(value = "/v1/filieres/{id}")
	public ResponseEntity<FiliereDto> deleteV1Filiere(
			@ApiParam(value = "identifiant filiere", required = true) @PathVariable("id") Long id) {
		try {
			return new ResponseEntity<>(filiereService.delete(id), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	/**
	 * PUT /v1/filieres/intervenant : Update intervenant maj un intervenant
	 *
	 * @param filiereDto
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Update filiere", nickname = "putV1Filieres", notes = "maj un intervenant", response = FiliereDto.class, tags = {
			"filieres", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = FiliereDto.class) })
	@PutMapping(value = "/v1/filieres/intervenant", produces = { "application/json" }, consumes = {
			"application/json" })
	public ResponseEntity<FiliereDto> putV1Intervenant(
			@ApiParam(value = "") @Valid @RequestBody(required = true) FiliereDto filiereDto) {

		return new ResponseEntity<>(filiereService.updateIntervenant(filiereDto, null), HttpStatus.OK);
	}

	/**
	 * DELETE /v1/filieres/intervenant : delete intervenant supprimer un intervenant
	 *
	 * @param filiereDto
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Delete filiere", nickname = "deleteV1Filieres", notes = "supprimer un intervenant", response = FiliereDto.class, tags = {
			"filieres", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = FiliereDto.class) })
	@PutMapping(value = "/v1/filieres/intervenant/archiver", produces = { "application/json" }, consumes = {
			"application/json" })
	public ResponseEntity<FiliereDto> deleteV1Intervenant(
			@ApiParam(value = "") @Valid @RequestBody(required = true) FiliereDto filiereDto) {
		try {
			filiereService.endDateIntervenant(filiereDto, false);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
