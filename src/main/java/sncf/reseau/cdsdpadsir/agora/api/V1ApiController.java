package sncf.reseau.cdsdpadsir.agora.api;

import java.util.Optional;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-01-05T10:18:32.903+01:00[Europe/Paris]")

@RequestMapping("${openapi.agora.base-path:}")
public class V1ApiController implements V1Api {

    private final NativeWebRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public V1ApiController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

}
