package sncf.reseau.cdsdpadsir.agora.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Join;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Or;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import sncf.reseau.cdsdpadsir.agora.entity.ClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.model.ObjetDto;
import sncf.reseau.cdsdpadsir.agora.model.ObjetAPIDTO;
import sncf.reseau.cdsdpadsir.agora.model.StatutClasseDobjetDto;
import sncf.reseau.cdsdpadsir.agora.service.ConflictException;
import sncf.reseau.cdsdpadsir.agora.service.ObjetService;
import sncf.reseau.cdsdpadsir.agora.service.XlsxService;
import sncf.reseau.cdsdpadsir.agora.specification.Specifications;

@RestController
public class ObjetController {

	@Autowired
	private ObjetService objetService;
	@Autowired
	XlsxService xlsxService;

	/**
	 * GET /v1/objets : Objets liste des objets
	 *
	 * @param page   numéro de la page courante (optional)
	 * @param sort   Le tri est défini comme la détermination de l’ordre dans lequel
	 *               nous souhaitons récupérer les ressources (optional)
	 * @param size   nombre d’éléments par page (optional)
	 * @param filter filtrage avancé (optional)
	 * @param fields Le consommateur du service spécifie dans un paramètre de
	 *               requête fields les champs de la ressource qu’il souhaite
	 *               récupérer exclusivement au sein de la réponse. Ces paramètres
	 *               seront séparés par une virgule (optional)
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Objets", nickname = "getV1Objets", notes = "liste des objets", response = ObjetDto.class, responseContainer = "List", tags = {
			"objets", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = ObjetDto.class, responseContainer = "List") })
	@GetMapping(value = "/v1/objets", produces = { "application/json" })
	ResponseEntity<List<ObjetDto>> getV1Objets(
			@ApiParam(value = "numéro de la page courante") @Valid @RequestParam(value = "page", required = false) BigDecimal page,
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@ApiParam(value = "nombre d’éléments par page") @Valid @RequestParam(value = "size", required = false) BigDecimal size,
			@ApiParam(value = "filtrage avancé") @Valid @RequestParam(value = "filter", required = false) String filter,
			@ApiParam(value = "Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule") @Valid @RequestParam(value = "fields", required = false) String fields,
			@ApiParam(value = "nom Filiere") @Valid @RequestParam(value = "filiere", required = false) String filiere,
			@Join(path = "definitionClasseDobjets", alias = "definition") 
			@Join(path = "proprietaire", alias = "proprietaire") 
			@Join(path = "statutClasseDobjets", alias = "statutClasseDobjet") 
			@Join(path = "statutClasseDobjet.valeurStatutClasseDobjet", alias = "valeurStatut") 
			@Join(path = "statutClasseDobjet.valideur", alias = "valideur")
			@Join(path = "statutClasseDobjet.suppleant", alias = "suppleant")
			@Or({
				@Spec(path = "valideur.idIdentiteNumerique", params = "idValideur", spec = Equal.class),
				@Spec(path = "suppleant.idIdentiteNumerique", params = "idSuppleant", spec = Equal.class)
			})
			@And({
					@Spec(path = "idClasseDobjet", spec = Equal.class), 
					@Spec(path = "abreviation", spec = Equal.class),
					@Spec(path = "definition.definition", params = "definition", spec = Equal.class),
					@Spec(path = "valeurStatut.libelle", params = "status", spec = Equal.class),
					@Spec(path = "commentaire", spec = Equal.class),
					@Spec(path = "proprietaire.idIdentiteNumerique", params = "proprietaire", spec = Equal.class),
					@Spec(path = "id", spec = Equal.class),
					@Spec(path = "libelle", spec = Equal.class) }) Specification<ClasseDobjet> objetSpec) {

		Specification<ClasseDobjet> activeObjet = Specification.where(objetSpec)
				.and(Specifications.getObjetsByRevision());

		if (filiere != null) {
			List<String> filieres = Arrays.asList(filiere.split(","));
			activeObjet = activeObjet.and(Specifications.getObjetsByFiliere(filieres));
		}
		final PagingResponse<ObjetDto> response = objetService.findAll(activeObjet, sort, page, size);
		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}

	@ApiOperation(value = "Objets", nickname = "getV1ObjetsApi", notes = "liste des objets", response = ObjetAPIDTO.class, responseContainer = "List", tags = {
			"objets", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = ObjetDto.class, responseContainer = "List") })
	@GetMapping(value = "/v3/connaissance/objet/objets", produces = { "application/json" })
	ResponseEntity<List<ObjetAPIDTO>> getV1ObjetsApi(
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@And({}) Specification<ClasseDobjet> objetSpec) {

		Specification<ClasseDobjet> activeObjet = Specification.where(objetSpec)
				.and(Specifications.getObjetsByRevision());

		final PagingResponse<ObjetAPIDTO> response = objetService.findAllObjetAPI(activeObjet, sort);

		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}

	/**
	 * POST /v1/objets : Add objet ajouter un objet
	 *
	 * @param objetDto (optional)
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Add objet", nickname = "postV1Objets", notes = "ajouter un objet", response = ObjetDto.class, tags = {
			"objets", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ObjetDto.class) })
	@PostMapping(value = "/v1/objets", produces = { "application/json" }, consumes = { "application/json" })
	ResponseEntity<ObjetDto> postV1Objets(
			@ApiParam(value = "") @Valid @RequestBody(required = false) ObjetDto objetDto) {

		if (objetService.checkUniquenessByFiliere(objetDto)) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		} else if (objetService.checkUniqueness(objetDto)) {
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.set("warning", "L'objet ajouté existe dans une autre filière avec le même libellé");
			return new ResponseEntity<>(objetService.add(objetDto), responseHeaders, HttpStatus.OK);
		}
		return new ResponseEntity<>(objetService.add(objetDto), HttpStatus.OK);

	}

	/**
	 * GET /v1/objets/{id} : Objet récupérer un objet par id
	 *
	 * @param id identifiant objet (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "Objet", nickname = "getV1ObjetsId", notes = "récupérer un objet par id", response = ObjetDto.class, tags = {
			"objets", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ObjetDto.class),
			@ApiResponse(code = 404, message = "Not Found") })
	@GetMapping(value = "/v1/objets/{id}", produces = { "application/json" })
	ResponseEntity<Object> getV1ObjetsId(
			@ApiParam(value = "identifiant objet", required = true) @PathVariable("id") Long id) {
		try {
			return new ResponseEntity<>(objetService.get(id), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}

	}

	/**
	 * PUT /v1/objets/{id} : Update Objet modifier un objet
	 *
	 * @param id       identifiant objet (required)
	 * @param objetDto (optional)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "Update Objet", nickname = "putV1Objet", notes = "modifier un objet", response = ObjetDto.class, tags = {
			"objets", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ObjetDto.class),
			@ApiResponse(code = 404, message = "Not Found") })
	@PutMapping(value = "/v1/objets", produces = { "application/json" }, consumes = { "application/json" })
	ResponseEntity<ObjetDto> putV1ObjetsId(
			@ApiParam(value = "") @Valid @RequestBody(required = false) ObjetDto objetDto) {

		try {
			HttpHeaders responseHeaders = new HttpHeaders();
			if (objetService.checkUniquenessForEdit(objetDto)) {
				responseHeaders.set("warning", "L'objet ajouté existe dans une autre filière avec le même libelle");
			}
			return new ResponseEntity<>(objetService.update(objetDto), responseHeaders, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (ConflictException e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}

	}

	/**
	 * DELETE /v1/objets/{id} : Delete Objet supprimer un objet
	 *
	 * @param id identifiant objet (required)
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Delete Objet", nickname = "deleteV1ObjetsId", notes = "supprimer un objet", response = ObjetDto.class, tags = {
			"objets", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ObjetDto.class) })
	@DeleteMapping(value = "/v1/objets/{id}", produces = { "application/json" })
	ResponseEntity<String> deleteV1ObjetsId(
			@ApiParam(value = "identifiant objet", required = true) @PathVariable("id") Long id) {
		try {
			return new ResponseEntity<>("Success: " + objetService.delete(id), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}

	}
	
	
	/**
	 * GET /v1/objets/statut/{id} : récupérer les statuts par id(guid)
	 *
	 * @param id guid statut (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "statut", nickname = "getV1StatutId", notes = "récupérer les statut par id(guid)", response = StatutClasseDobjetDto.class, tags = {
			"objets", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ObjetDto.class),
			@ApiResponse(code = 404, message = "Not Found") })
	@GetMapping(value = "/v1/objets/statut/{id}", produces = { "application/json" })
	ResponseEntity<?> getV1Statut(
			@ApiParam(value = "guid statut", required = true) @PathVariable("id") String id) {
		try {
			return new ResponseEntity<>(objetService.getStatuts(id), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}

	}
	
	@GetMapping(value = "/v1/objets/file", produces = { "application/json" })
	public ResponseEntity<?> loadFile() {

		try {
			xlsxService.loadObjets();
			return new ResponseEntity<>(true, HttpStatus.OK);
		} catch (IOException e) {
			return new ResponseEntity<>(e.getStackTrace(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}
