package sncf.reseau.cdsdpadsir.agora.api;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import sncf.reseau.cdsdpadsir.agora.entity.Etiquette;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.model.EtiquetteDto;
import sncf.reseau.cdsdpadsir.agora.service.TagService;
import sncf.reseau.cdsdpadsir.agora.specification.Specifications;

@RestController
public class TagController {

	@Autowired
	private TagService tagService;

	/**
	 * GET /v1/tags : Liste des tags récupérer la liste des tags
	 *
	 * @param page   numéro de la page courante (optional)
	 * @param size   nombre d’éléments par page (optional)
	 * @param sort   Le tri est défini comme la détermination de l’ordre dans lequel
	 *               nous souhaitons récupérer les ressources (optional)
	 * @param filter filtrage avancé (optional)
	 * @param fields Le consommateur du service spécifie dans un paramètre de
	 *               requête fields les champs de la ressource qu’il souhaite
	 *               récupérer exclusivement au sein de la réponse. Ces paramètres
	 *               seront séparés par une virgule (optional)
	 * @return OK (status code 200) or Partial Content (status code 206)
	 */
	@ApiOperation(value = "Liste des tags", nickname = "getV1Tags", notes = "récupérer la liste des tags", response = EtiquetteDto.class, responseContainer = "List", tags = {
			"tags", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = EtiquetteDto.class, responseContainer = "List"),
			@ApiResponse(code = 206, message = "Partial Content", response = EtiquetteDto.class, responseContainer = "List") })
	@GetMapping(value = "/v1/etiquettes", produces = { "application/json" })
	public ResponseEntity<List<EtiquetteDto>> getV1Tags(
			@ApiParam(value = "numéro de la page courante") @Valid @RequestParam(value = "page", required = false) BigDecimal page,
			@ApiParam(value = "nombre d’éléments par page") @Valid @RequestParam(value = "size", required = false) BigDecimal size,
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@ApiParam(value = "filtrage avancé") @Valid @RequestParam(value = "filter", required = false) String filter,
			@ApiParam(value = "Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule") @Valid @RequestParam(value = "fields", required = false) String fields,
			@And({ @Spec(path = "id_etiquette", spec = Equal.class), @Spec(path = "id", spec = Equal.class),
					@Spec(path = "libelle", spec = Equal.class) }) Specification<Etiquette> tagSpec) {

		Specification<Etiquette> activeTerme = Specification.where(tagSpec).and(Specifications.getTagsByRevision());
		final PagingResponse<EtiquetteDto> response = tagService.findAll(activeTerme, sort, page, size);
		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}
}
