package sncf.reseau.cdsdpadsir.agora.api;

import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Join;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import sncf.reseau.cdsdpadsir.agora.configuration.CpError;
import sncf.reseau.cdsdpadsir.agora.entity.Role;
import sncf.reseau.cdsdpadsir.agora.entity.Utilisateur;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.model.ActeurApiDto;
import sncf.reseau.cdsdpadsir.agora.model.GroupeRoleDto;
import sncf.reseau.cdsdpadsir.agora.model.RoleApiDto;
import sncf.reseau.cdsdpadsir.agora.model.RoleDto;
import sncf.reseau.cdsdpadsir.agora.model.TermeDto;
import sncf.reseau.cdsdpadsir.agora.model.UtilisateurDto;
import sncf.reseau.cdsdpadsir.agora.service.UtilisateurService;
import sncf.reseau.cdsdpadsir.agora.specification.Specifications;

@RestController
public class UtilisateurController {

	@Autowired
	public UtilisateurService utilisateurService;

	/**
	 * GET /v1/utilisateurs : Liste des utilisateurs
	 *
	 * @param page   numéro de la page courante (optional)
	 * @param size   nombre d’éléments par page (optional)
	 * @param sort   Le tri est défini comme la détermination de l’ordre dans lequel
	 *               nous souhaitons récupérer les ressources (optional)
	 * @param filter filtrage avancé (optional)
	 * @param fields Le consommateur du service spécifie dans un paramètre de
	 *               requête fields les champs de la ressource qu’il souhaite
	 *               récupérer exclusivement au sein de la réponse. Ces paramètres
	 *               seront séparés par une virgule (optional)
	 * @return OK (status code 200) or Partial Content (status code 206)
	 */
	@ApiOperation(value = "Liste des utilisateurs", nickname = "getV1Utilisateurs", notes = "liste des utilisateur", response = UtilisateurDto.class, responseContainer = "List", tags = {
			"Utilisateurs", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = UtilisateurDto.class, responseContainer = "List"),
			@ApiResponse(code = 206, message = "Partial Content", response = UtilisateurDto.class, responseContainer = "List") })
	@GetMapping(value = "/v1/utilisateurs", produces = { "application/json" })
	public ResponseEntity<List<UtilisateurDto>> getV1Utilisateurs(
			@ApiParam(value = "numéro de la page courante") @Valid @RequestParam(value = "page", required = false) BigDecimal page,
			@ApiParam(value = "nombre d’éléments par page") @Valid @RequestParam(value = "size", required = false) BigDecimal size,
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@ApiParam(value = "filtrage avancé") @Valid @RequestParam(value = "filter", required = false) String filter,
			@ApiParam(value = "Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule") @Valid @RequestParam(value = "fields", required = false) String fields,
//			@Join(path = "mapping", alias = "mapping")
			@And({ @Spec(path = "nom", spec = Equal.class), @Spec(path = "prenom", spec = Equal.class),
					@Spec(path = "cp", spec = Equal.class) }) Specification<Utilisateur> utilisateurSpec) {
		Specification<Utilisateur> activeUtilisateur = Specification.where(utilisateurSpec)
				.and(Specifications.getUtilisateursByRevision());
		final PagingResponse<UtilisateurDto> response = utilisateurService.findAll(activeUtilisateur, sort, page, size);
		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}

	/**
	 * GET /v1/utilisateurs : Liste des utilisateurs
	 *
	 * @param page   numéro de la page courante (optional)
	 * @param size   nombre d’éléments par page (optional)
	 * @param sort   Le tri est défini comme la détermination de l’ordre dans lequel
	 *               nous souhaitons récupérer les ressources (optional)
	 * @param filter filtrage avancé (optional)
	 * @param fields Le consommateur du service spécifie dans un paramètre de
	 *               requête fields les champs de la ressource qu’il souhaite
	 *               récupérer exclusivement au sein de la réponse. Ces paramètres
	 *               seront séparés par une virgule (optional)
	 * @return OK (status code 200) or Partial Content (status code 206)
	 */
	@ApiOperation(value = "Liste des utilisateurs", nickname = "getV1Utilisateurs", notes = "liste des utilisateur", response = UtilisateurDto.class, responseContainer = "List", tags = {
			"Utilisateurs", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = UtilisateurDto.class, responseContainer = "List"),
			@ApiResponse(code = 206, message = "Partial Content", response = UtilisateurDto.class, responseContainer = "List") })
	@GetMapping(value = "/v3/connaissance/gouvernance/acteurs", produces = { "application/json" })
	public ResponseEntity<List<ActeurApiDto>> getV1ApiUtilisateurs(
			@ApiParam(value = "numéro de la page courante") @Valid @RequestParam(value = "page", required = false) BigDecimal page,
			@ApiParam(value = "nombre d’éléments par page") @Valid @RequestParam(value = "size", required = false) BigDecimal size,
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@ApiParam(value = "filtrage avancé") @Valid @RequestParam(value = "filter", required = false) String filter,
			@ApiParam(value = "Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule") @Valid @RequestParam(value = "fields", required = false) String fields,
//			@Join(path = "mapping", alias = "mapping")
			@And({ @Spec(path = "nom", spec = Equal.class), @Spec(path = "prenom", spec = Equal.class),
					@Spec(path = "cp", spec = Equal.class) }) Specification<Utilisateur> utilisateurSpec) {
		Specification<Utilisateur> activeUtilisateur = Specification.where(utilisateurSpec)
				.and(Specifications.getUtilisateursByRevision());
		final PagingResponse<ActeurApiDto> response = utilisateurService.findAllApi(activeUtilisateur, sort, page,
				size);
		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}

	/**
	 * POST /v1/utilisateurs: Add utilisateur ajouter un utilisateur
	 *
	 * @param utilisateurDto
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Add utilisateur", nickname = "postV1utilisateurs", notes = "ajouter un utilisateur", response = TermeDto.class, tags = {
			"utilisateurs", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = UtilisateurDto.class) })
	@PostMapping(value = "/v1/utilisateurs", produces = { "application/json" }, consumes = { "application/json" })
	public ResponseEntity<?> postV1utilisateurs(
			@ApiParam(value = "") @Valid @RequestBody(required = true) UtilisateurDto utilisateurDto) {

		try {
			return new ResponseEntity<>(utilisateurService.add(utilisateurDto), HttpStatus.OK);
		} catch (CpError e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
		}
	}

	/**
	 * PUT /v1/utilisateurs: Update utilisateur mettre à jour un utilisateur
	 *
	 * @param utilisateurDto (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "Update utilisateur", nickname = "putV1utilisateur", notes = "mettre à jour une utilisateur", tags = {
			"utilisateurs", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Not Found") })
	@PutMapping(value = "/v1/utilisateurs")
	public ResponseEntity<?> putV1utilisateurs(
			@ApiParam(value = "") @RequestBody(required = true) UtilisateurDto utilisateurDto) {
		try {
			return new ResponseEntity<>(utilisateurService.update(utilisateurDto), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (CpError e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
		}

	}

	/**
	 * PUT /v1/utilisateurs: Delete utilisateur archiver une utilisateur
	 *
	 * @param utilisateurDto (required)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	@ApiOperation(value = "Delete utilisateur", nickname = "putV1utilisateur", notes = "archiver une utilisateur", tags = {
			"utilisateurs", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Not Found") })
	@DeleteMapping(value = "/v1/utilisateurs/delete")
	public ResponseEntity<String> deleteV1utilisateurs(@RequestParam("id") String id) {
		try {
			return new ResponseEntity<>(utilisateurService.delete(id), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	/**
	 * GET /v1/famille_role : Liste des familles_roles
	 *
	 * @param page   numéro de la page courante (optional)
	 * @param size   nombre d’éléments par page (optional)
	 * @param sort   Le tri est défini comme la détermination de l’ordre dans lequel
	 *               nous souhaitons récupérer les ressources (optional)
	 * @param filter filtrage avancé (optional)
	 * @param fields Le consommateur du service spécifie dans un paramètre de
	 *               requête fields les champs de la ressource qu’il souhaite
	 *               récupérer exclusivement au sein de la réponse. Ces paramètres
	 *               seront séparés par une virgule (optional)
	 * @return OK (status code 200) or Partial Content (status code 206)
	 */
	@ApiOperation(value = "Liste des familles_roles", nickname = "getV1familleRole", notes = "liste des familles_roles", response = GroupeRoleDto.class, responseContainer = "List", tags = {
			"FamillesRoles", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = UtilisateurDto.class, responseContainer = "List"),
			@ApiResponse(code = 206, message = "Partial Content", response = UtilisateurDto.class, responseContainer = "List") })
	@GetMapping(value = "/v1/familles_roles", produces = { "application/json" })
	public ResponseEntity<List<GroupeRoleDto>> getV1FamillesRoles(
			@ApiParam(value = "numéro de la page courante") @Valid @RequestParam(value = "page", required = false) BigDecimal page,
			@ApiParam(value = "nombre d’éléments par page") @Valid @RequestParam(value = "size", required = false) BigDecimal size,
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@ApiParam(value = "filtrage avancé") @Valid @RequestParam(value = "filter", required = false) String filter,
			@ApiParam(value = "Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule") @Valid @RequestParam(value = "fields", required = false) String fields) {
		final PagingResponse<GroupeRoleDto> response = utilisateurService.findAllFamillesRoles(sort);
		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}

	/**
	 * GET /v1/role : Liste des roles
	 *
	 * @param page   numéro de la page courante (optional)
	 * @param size   nombre d’éléments par page (optional)
	 * @param sort   Le tri est défini comme la détermination de l’ordre dans lequel
	 *               nous souhaitons récupérer les ressources (optional)
	 * @param filter filtrage avancé (optional)
	 * @param fields Le consommateur du service spécifie dans un paramètre de
	 *               requête fields les champs de la ressource qu’il souhaite
	 *               récupérer exclusivement au sein de la réponse. Ces paramètres
	 *               seront séparés par une virgule (optional)
	 * @return OK (status code 200) or Partial Content (status code 206)
	 */
	@ApiOperation(value = "Liste des roles", nickname = "getV1Role", notes = "liste des roles", response = RoleDto.class, responseContainer = "List", tags = {
			"FamillesRoles", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = RoleDto.class, responseContainer = "List"),
			@ApiResponse(code = 206, message = "Partial Content", response = RoleDto.class, responseContainer = "List") })
	@GetMapping(value = "/v1/roles", produces = { "application/json" })
	public ResponseEntity<List<RoleDto>> getV1Roles(
			@ApiParam(value = "numéro de la page courante") @Valid @RequestParam(value = "page", required = false) BigDecimal page,
			@ApiParam(value = "nombre d’éléments par page") @Valid @RequestParam(value = "size", required = false) BigDecimal size,
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@ApiParam(value = "filtrage avancé") @Valid @RequestParam(value = "filter", required = false) String filter,
			@ApiParam(value = "Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule") @Valid @RequestParam(value = "fields", required = false) String fields,
			@Join(path = "groupeRoleRoles", alias = "groupeRoleRoles") @Join(path = "groupeRoleRoles.groupeRole", alias = "groupeRole") @Spec(path = "groupeRole.idGroupeRole", params = "idGroupeRole", spec = Equal.class)

			Specification<Role> roleSpec) {
		final PagingResponse<RoleDto> response = utilisateurService.findAllRoles(roleSpec, sort);
		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}

	/**
	 * GET /v1/role : Liste des roles
	 *
	 * @param page   numéro de la page courante (optional)
	 * @param size   nombre d’éléments par page (optional)
	 * @param sort   Le tri est défini comme la détermination de l’ordre dans lequel
	 *               nous souhaitons récupérer les ressources (optional)
	 * @param filter filtrage avancé (optional)
	 * @param fields Le consommateur du service spécifie dans un paramètre de
	 *               requête fields les champs de la ressource qu’il souhaite
	 *               récupérer exclusivement au sein de la réponse. Ces paramètres
	 *               seront séparés par une virgule (optional)
	 * @return OK (status code 200) or Partial Content (status code 206)
	 */
	@ApiOperation(value = "Liste des roles", nickname = "getV1Role", notes = "liste des roles", response = RoleDto.class, responseContainer = "List", tags = {
			"FamillesRoles", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = RoleDto.class, responseContainer = "List"),
			@ApiResponse(code = 206, message = "Partial Content", response = RoleDto.class, responseContainer = "List") })
	@GetMapping(value = "/v3/connaissance/gouvernance/roles", produces = { "application/json" })
	public ResponseEntity<List<RoleApiDto>> getV1ApiRoles(
			@ApiParam(value = "numéro de la page courante") @Valid @RequestParam(value = "page", required = false) BigDecimal page,
			@ApiParam(value = "nombre d’éléments par page") @Valid @RequestParam(value = "size", required = false) BigDecimal size,
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@ApiParam(value = "filtrage avancé") @Valid @RequestParam(value = "filter", required = false) String filter,
			@ApiParam(value = "Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule") @Valid @RequestParam(value = "fields", required = false) String fields,
			@Join(path = "groupeRoleRoles", alias = "groupeRoleRoles") @Join(path = "groupeRoleRoles.groupeRole", alias = "groupeRole") @Spec(path = "groupeRole.idGroupeRole", params = "idGroupeRole", spec = Equal.class)

			Specification<Role> roleSpec) {
		final PagingResponse<RoleApiDto> response = utilisateurService.findAllApiRoles(roleSpec, sort);
		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}

}
