package sncf.reseau.cdsdpadsir.agora.api;

import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Join;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Or;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import sncf.reseau.cdsdpadsir.agora.entity.ClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.entity.UsageMetier;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.model.ObjetDto;
import sncf.reseau.cdsdpadsir.agora.repository.ObjetRepository;
import sncf.reseau.cdsdpadsir.agora.service.QualiteService;

@RestController
public class QualiteController {

	private QualiteService qualiteService;

	private ObjetRepository objetRepository;

	@Autowired
	public QualiteController(QualiteService qualiteService, ObjetRepository objetRepository) {
		super();
		this.qualiteService = qualiteService;
		this.objetRepository = objetRepository;
	}

	/**
	 * GET /qualite/ : Objets liste des objets fontionel
	 *
	 * @param page   numéro de la page courante (optional)
	 * @param sort   Le tri est défini comme la détermination de l’ordre dans lequel
	 *               nous souhaitons récupérer les ressources (optional)
	 * @param size   nombre d’éléments par page (optional)
	 * @param filter filtrage avancé (optional)
	 * @param fields Le consommateur du service spécifie dans un paramètre de
	 *               requête fields les champs de la ressource qu’il souhaite
	 *               récupérer exclusivement au sein de la réponse. Ces paramètres
	 *               seront séparés par une virgule (optional)
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Objets", nickname = "getV1Objets", notes = "liste des objets", response = ObjetDto.class, responseContainer = "List", tags = {
			"objets", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = ObjetDto.class, responseContainer = "List") })
	@GetMapping(value = "/v1/qualite/objets", produces = { "application/json" })
	public ResponseEntity<List<ObjetDto>> getV1Objets(
			@ApiParam(value = "numéro de la page courante") @Valid @RequestParam(value = "page", required = false) BigDecimal page,
			@ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
			@ApiParam(value = "nombre d’éléments par page") @Valid @RequestParam(value = "size", required = false) BigDecimal size,
			@ApiParam(value = "filtrage avancé") @Valid @RequestParam(value = "filter", required = false) String filter,
			@ApiParam(value = "Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule") @Valid @RequestParam(value = "fields", required = false) String fields,
			@ApiParam(value = "nom Filiere") @Valid @RequestParam(value = "filiere", required = false) String filiere,
			@Join(path = "definitionClasseDobjets", alias = "definition") @Join(path = "proprietaire", alias = "proprietaire") @Join(path = "statutClasseDobjets", alias = "statutClasseDobjet") @Join(path = "statutClasseDobjet.valeurStatutClasseDobjet", alias = "valeurStatut") @Join(path = "statutClasseDobjet.valideur", alias = "valideur") @Join(path = "statutClasseDobjet.suppleant", alias = "suppleant") @Or({
					@Spec(path = "valideur.idIdentiteNumerique", params = "idValideur", spec = Equal.class),
					@Spec(path = "suppleant.idIdentiteNumerique", params = "idSuppleant", spec = Equal.class) }) @And({
							@Spec(path = "idClasseDobjet", spec = Equal.class),
							@Spec(path = "abreviation", spec = Equal.class),
							@Spec(path = "definition.definition", params = "definition", spec = Equal.class),
							@Spec(path = "valeurStatut.libelle", params = "status", spec = Equal.class),
							@Spec(path = "commentaire", spec = Equal.class),
							@Spec(path = "proprietaire.idIdentiteNumerique", params = "proprietaire", spec = Equal.class),
							@Spec(path = "id", spec = Equal.class),
							@Spec(path = "libelle", spec = Equal.class) }) Specification<ClasseDobjet> objetSpec) {

		final PagingResponse<ObjetDto> response = qualiteService.findAll(null, sort, page, size);
		return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
				response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

	}

	@Transactional
	public ObjetDto persistCritere(ObjetDto objetDto) throws NotFoundException {
		qualiteService.addCritere(objetDto);
		Optional<ClasseDobjet> objet = objetRepository.findById(objetDto.getIdClasseDobjet());
		
		if(objet.isPresent()) {
			return qualiteService.mapObjet(objetRepository.findById(objetDto.getIdClasseDobjet()).get());
		}
		
		return null;
	}

	/**
	 * POST /v1/qualite/objets : Add objet ajouter un objet fonctionnel
	 *
	 * @param objetDto (optional)
	 * @return OK (status code 200)
	 * @throws NotFoundException
	 */
	@ApiOperation(value = "Add objet fonctionnel", nickname = "postV1Objets", notes = "ajouter un objet", response = ObjetDto.class, tags = {
			"objets", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ObjetDto.class) })
	@PutMapping(value = "/v1/qualite/objets", produces = { "application/json" }, consumes = { "application/json" })
	public ResponseEntity<ObjetDto> postV1Objets(@ApiParam(value = "") @Valid @RequestBody(required = false) ObjetDto objetDto)
			throws NotFoundException {

		return new ResponseEntity<>(persistCritere(objetDto), HttpStatus.OK);

	}

	/**
	 * PUT /v1/qualite/objets/{id} : Update Objet modifier un objet fonctionnel
	 *
	 * @param id       identifiant objet (required)
	 * @param objetDto (optional)
	 * @return OK (status code 200) or Not Found (status code 404)
	 */
	/*
	 * @ApiOperation(value = "Update Objet", nickname = "putV1Objet", notes =
	 * "modifier un objet fonctionnel", response = ObjetDto.class, tags = {
	 * "objets", })
	 * 
	 * @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response =
	 * ObjetDto.class),
	 * 
	 * @ApiResponse(code = 404, message = "Not Found") })
	 * 
	 * @PutMapping(value = "/v1/qualite/objets", produces = { "application/json" },
	 * consumes = { "application/json" }) ResponseEntity<ObjetDto> putV1ObjetsId(
	 * 
	 * @ApiParam(value = "") @Valid @RequestBody(required = false) ObjetDto
	 * objetDto) {
	 * 
	 * HttpHeaders responseHeaders = new HttpHeaders();
	 * 
	 * return new ResponseEntity<>(qualiteService.updateCritere(objetDto),
	 * responseHeaders, HttpStatus.OK);
	 * 
	 * }
	 */

	/**
	 * DELETE /v1/qualite/objets/{id} : Delete Objet supprimer les criteres d'un
	 * objet
	 *
	 * @param id identifiant objet (required)
	 * @return OK (status code 200)
	 */
	@ApiOperation(value = "Delete Objet", nickname = "deleteV1ObjetsId", notes = "supprimer les criteres d'un objet", response = ObjetDto.class, tags = {
			"objets", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ObjetDto.class) })
	@DeleteMapping(value = "/v1/qualite/objets/{id}", produces = { "application/json" })
	public ResponseEntity<String> deleteV1ObjetsId(
			@ApiParam(value = "identifiant objet", required = true) @PathVariable("id") Long id) {
		try {
			return new ResponseEntity<>("Success: " + qualiteService.archiver(id), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping(value = "/v1/qualite/criteres", produces = { "application/json" })
	public ResponseEntity<List<String>> getCriteres() {
		return new ResponseEntity<>(qualiteService.findAllCriteres(), HttpStatus.OK);

	}

	@GetMapping(value = "/v1/qualite/usages", produces = { "application/json" })
	public ResponseEntity<List<UsageMetier>> getUsages() {
		return new ResponseEntity<>(qualiteService.findAllUsage(), HttpStatus.OK);

	}
}
