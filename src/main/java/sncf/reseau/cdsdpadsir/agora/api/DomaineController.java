package sncf.reseau.cdsdpadsir.agora.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sncf.reseau.cdsdpadsir.agora.entity.Domaine;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.model.DomaineDto;
import sncf.reseau.cdsdpadsir.agora.service.DomaineService;
import sncf.reseau.cdsdpadsir.agora.specification.Specifications;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

@RestController
public class DomaineController {

    @Autowired
    private DomaineService domaineService;

    /**
     * GET /v1/domaines : Liste des domaines
     *
     * @param page   numéro de la page courante (optional)
     * @param size   nombre d’éléments par page (optional)
     * @param sort   Le tri est défini comme la détermination de l’ordre dans lequel
     *               nous souhaitons récupérer les ressources (optional)
     * @param filter filtrage avancé (optional)
     * @param fields Le consommateur du service spécifie dans un paramètre de
     *               requête fields les champs de la ressource qu’il souhaite
     *               récupérer exclusivement au sein de la réponse. Ces paramètres
     *               seront séparés par une virgule (optional)
     * @return OK (status code 200) or Partial Content (status code 206)
     */
    @ApiOperation(value = "Liste des domaines", nickname = "getV1Domaines", notes = "Liste des domaines", response = DomaineDto.class, responseContainer = "List", tags = {
            "domaines", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = DomaineDto.class, responseContainer = "List"),
            @ApiResponse(code = 206, message = "Partial Content", response = DomaineDto.class, responseContainer = "List") })
    @GetMapping(value = "/v1/domaines", produces = { "application/json" })
    ResponseEntity<List<DomaineDto>> getV1Domaines(
            @ApiParam(value = "numéro de la page courante") @Valid @RequestParam(value = "page", required = false) BigDecimal page,
            @ApiParam(value = "nombre d’éléments par page") @Valid @RequestParam(value = "size", required = false) BigDecimal size,
            @ApiParam(value = "Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources") Sort sort,
            @ApiParam(value = "filtrage avancé") @Valid @RequestParam(value = "filter", required = false) String filter,
            @ApiParam(value = "Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule") @Valid @RequestParam(value = "fields", required = false) String fields) {
        Specification<Domaine> activeDomaine = Specification.where(Specifications.getDomainesByRevision());
        final PagingResponse<DomaineDto> response = domaineService.findAll(activeDomaine, sort, page, size);
        return new ResponseEntity<>(response.getElements(), ApiUtil.returnHttpHeaders(response),
                response.getPageTotal().equals(0L) ? HttpStatus.OK : HttpStatus.PARTIAL_CONTENT);

    }
}
