package sncf.reseau.cdsdpadsir.agora.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import sncf.reseau.cdsdpadsir.agora.entity.ClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.model.TypeElementModeleObjetDto;
import sncf.reseau.cdsdpadsir.agora.repository.ObjetRepository;
import sncf.reseau.cdsdpadsir.agora.service.QualiteService;
import sncf.reseau.cdsdpadsir.agora.service.TypeElementModeleObjetService;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;
import sncf.reseau.cdsdpadsir.agora.xmi.exception.XmiDepotFileException;
import sncf.reseau.cdsdpadsir.agora.xmi.exception.XmiFileNameException;
import sncf.reseau.cdsdpadsir.agora.xmi.exception.XmiParsingException;
import sncf.reseau.cdsdpadsir.agora.xmi.model.XmiDetailImport;
import sncf.reseau.cdsdpadsir.agora.xmi.service.XmiService;
import sncf.reseau.cdsdpadsir.agora.xmi.util.TypeModele;

@RestController
@RequestMapping("/api/v1/structure-classes")
public class StructureFromXmiController {

	private static final Logger logger = LoggerFactory.getLogger(StructureFromXmiController.class);

	@Autowired
	private XmiService xmiService;

	@Autowired
	private ObjetRepository objetRepository;

	@Autowired
	private QualiteService qualiteService;

	@Autowired
	private TypeElementModeleObjetService typeElementModeleObjetService;

	@ApiOperation(value = "Xmi", nickname = "loadXmiStructreAriane", notes = "xmi to sql", response = ResponseEntity.class, responseContainer = "ResponseEntity", tags = {
			"xmi", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = ResponseEntity.class, responseContainer = "ResponseEntity") })
	@PostMapping("/loadXmiStruture/ariane")
	@Transactional
	public ResponseEntity<Map<String, Object>> loadXmiStructreAriane(@RequestParam("files") MultipartFile[] files) {
		// curl -i -X POST -H "Content-Type: multipart/form-data" -F
		// 'files=@C:\dev-tools\xmiRepository\t1\ariane_AV3_3.0.1_20210701.xml'
		// http://localhost:8080/agora/api/v1/structure-classes/loadXmiStruture/ariane
		logger.info(" start load Xmi Structre ");
		Map<String, Object> map = new HashMap<>();
		List<ClasseDobjet> oldObjects = objetRepository.findObjetsFonctionnels(ArianeHelper.FUTURE_DATE);

		TypeElementModeleObjetDto typeElement = typeElementModeleObjetService
				.getByLibelle(TypeModele.FONCTIONNEL.getType());
		if (null == typeElement) {
			map.put("message", "chargement est interrompu du problème au niveau de typeElementModele non trouvé");
			logger.error("chargement est interrompu du problème au niveau de typeElementModele non trouvé");
			return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
		}

		try {
			List<XmiDetailImport> detailsImport = xmiService.loadXmiStructre(typeElement, files);
			map.put("message", "chargement est terminé");
			map.put("detailImport", detailsImport);
			List<ClasseDobjet> newObjects = objetRepository.findObjetsFonctionnels(ArianeHelper.FUTURE_DATE);
			
			for (ClasseDobjet oldObject : oldObjects) {
				List<ClasseDobjet> matches = newObjects.stream()
						.filter(newObject -> newObject.getId().equals(oldObject.getId())).collect(Collectors.toList());
				if (!matches.isEmpty()) {
					qualiteService.updateJoins(oldObject, matches.get(0));
				}
			}

			logger.info(" finish load Xmi Structre ");
			return new ResponseEntity<>(map, HttpStatus.OK);
		} catch (XmiDepotFileException e) {
			String errorMessage = "chargement est interrompu du au problème du depot de fichier";
			map.put("message", "chargement est interrompu du au problème du depot de fichier , " + e.getMessage());
			logger.error(errorMessage + " => {} cause => {}", e.getMessage(), e.getCause());
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage, e);
		} catch (RuntimeException e) {
			String errorMessage = null;
			if (e.getCause().getClass().equals(XmiFileNameException.class)) {
				errorMessage = "chargement est interrompu du au problème de nom de fichier , " + e.getMessage();
				logger.error(errorMessage + " => {} cause => {}", e.getMessage(), e.getCause());
			} else if (e.getCause().getClass().equals(XmiParsingException.class)) {
				errorMessage = " chargement est interrompu du au problème de parsing de fichier Xmi , "
						+ e.getMessage();
				logger.error(errorMessage + " => {} cause => {}", e.getMessage(), e.getCause());
			} else {
				errorMessage = "chargement est interrompu du au problème technique , " + e.getMessage();
				logger.error(errorMessage + " => {} cause => {}", e.getMessage(), e.getCause());
			}
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
		}

	}

}
