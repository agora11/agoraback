package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "acteur" database table.
 * 
 */
@Entity
@Table(name="\"acteur\"")
@Getter
@Setter
public class Acteur extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ACTEUR_GENERATOR", sequenceName="SEQUENCE_ACTEUR")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACTEUR_GENERATOR")
	@Column(name="\"id_acteur\"")
	private Long idActeur;

	@Column(name="\"contact\"")
	private String contact;

	@Column(name="\"nom\"")
	private String nom;
	
	@Column(name="\"role\"")
	private String role;

	//bi-directional many-to-one association to AssociationActeurGisement
	@OneToMany(mappedBy="acteur")
	private Set<AssociationActeurGisement> associationActeurGisements;


}