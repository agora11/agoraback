package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "groupe_role_roles" database table.
 * 
 */
@Entity
@Table(name="\"groupe_role_roles\"")
@Getter
@Setter
public class GroupeRoleRole extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="GROUPE_ROLE_ROLES_IDGROUPEROLEROLES_GENERATOR", sequenceName="groupe_role_roles_sequence", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GROUPE_ROLE_ROLES_IDGROUPEROLEROLES_GENERATOR")
	@Column(name="\"id_groupe_role_roles\"")
	private Long idGroupeRoleRoles;

	@Column(name="\"code_role_fonctionnel\"")
	private String codeRoleFonctionnel;

	@Column(name="\"description\"")
	private String description;

	@Column(name="\"etat\"")
	private String etat;

	@Column(name="\"nom\"")
	private String nom;

	//bi-directional many-to-one association to GroupeRole
	@ManyToOne
	@JoinColumn(name = "\"groupe_role_id_groupe_role\"", referencedColumnName = "\"id_groupe_role\"")
	private GroupeRole groupeRole;

	//bi-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name = "\"role_id_role\"", referencedColumnName = "\"id_role\"")
	private Role role;

}