package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "element_modele_objet" database table.
 * 
 */
@Entity
@Table(name="\"element_modele_objet\"")
@Getter
@Setter
public class ElementModeleObjet extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ELEMENT_MODELE_OBJET_GENERATOR", sequenceName="SEQUENCE_ELEMENT_MODELE_OBJET")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ELEMENT_MODELE_OBJET_GENERATOR")
	@Column(name="\"id_element_modele_objet\"")
	private long idElementModeleObjet;

	//bi-directional many-to-one association to TypeElementModeleObjet
	@ManyToOne
	@JoinColumn(name = "\"id_type_element_modele_objet\"", referencedColumnName = "\"id_type_element_modele_objet\"")
	private TypeElementModeleObjet typeElementModeleObjet;

	//bi-directional many-to-one association to AssociationTermeElementModeleObjet
	@OneToMany(mappedBy="elementModeleObjet")
	private Set<AssociationTermeElementModeleObjet> associationTermeElementModeleObjets;
	
	@OneToMany(mappedBy = "elementModeleObjet")
	private Set<ClasseDobjet> classeDobjets;


}