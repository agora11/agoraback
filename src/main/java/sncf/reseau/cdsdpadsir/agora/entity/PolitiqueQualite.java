package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "politique_qualite" database table.
 * 
 */
@Entity
@Table(name="\"politique_qualite\"")
@Getter
@Setter
public class PolitiqueQualite extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="POLITIQUE_QUALITE_GENERATOR", sequenceName="SEQUENCE_POLITIQUE_QUALITE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="POLITIQUE_QUALITE_GENERATOR")
	@Column(name="\"id_politique_qualite\"")
	private Long idPolitiqueQualite;
	
	@Column(name="\"indicateur_de_qualite\"")
	private String indicateurDeQualite;
	
	@Column(name="\"description\"")
	private String description;
	
	@Column(name="\"url\"")
	private String url;

	@Column(name="\"taux_promis\"")
	private String tauxPromis;

	//bi-directional many-to-one association to CritereQualite
	@ManyToOne
	@JoinColumn(name = "\"id_critere_qualite\"", referencedColumnName = "\"id_critere_qualite\"")
	private CritereQualite critereQualite;

	//bi-directional many-to-one association to PolitiqueQualiteParUsage
	@OneToMany(mappedBy="politiqueQualite")
	private Set<PolitiqueQualiteParUsage> politiqueQualiteParUsages;

	//bi-directional many-to-one association to TauxQualiteMesure
	@OneToMany(mappedBy="politiqueQualite")
	private Set<TauxQualiteMesure> tauxQualiteMesures;

	public PolitiqueQualite() {
	}
	
}