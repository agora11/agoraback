package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "association_profil_autorisation_abstrait_role" database table.
 * 
 */
@Entity
@Table(name="\"association_profil_autorisation_abstrait_role\"")
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class AssociationProfilAutorisationAbstraitRole extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ASSOCIATION_PROFIL_AUTORISATION_ABSTRAIT_ROLE_GENERATOR", sequenceName="SEQUENCE_ASSOCIATION_PROFIL_AUTORISATION_ABSTRAIT_ROLE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ASSOCIATION_PROFIL_AUTORISATION_ABSTRAIT_ROLE_GENERATOR")
	@Column(name="\"id_association_profil_autorisation_abstrait_role\"")
	private long idAssociationProfilAutorisationAbstraitRole;

	//bi-directional many-to-one association to ProfilAutorisationAbstrait
	@ManyToOne
	@JoinColumn(name = "\"id_profil_autorisation_abstrait\"", referencedColumnName = "\"id_profil_autorisation_abstrait\"")
	private ProfilAutorisationAbstrait profilAutorisationAbstrait;
	
	@ManyToOne
	@JoinColumn(name = "\"id_role\"", referencedColumnName = "\"id_role\"")
	private Role role;

}