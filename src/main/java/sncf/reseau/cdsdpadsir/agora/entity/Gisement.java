package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * The persistent class for the "gisement" database table.
 * 
 */
@Entity
@Table(name = "\"gisement\"")
@Getter
@Setter
public class Gisement extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "GISEMENT_GENERATOR", sequenceName = "SEQUENCE_GISEMENT")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GISEMENT_GENERATOR")
	@Column(name = "\"id_gisement\"")
	private Long idGisement;

	@Column(name = "\"libelle\"")
	private String libelle;

	@Column(name = "\"niveau_de_completude\"")
	private String niveauDeCompletude;

	@Column(name = "\"nombre_de_jeux_donnees\"")
	private String nombreDeJeuxDonnees;

	@Column(name = "\"nombre_de_clients\"")
	private String nombreDeClients;

	@Column(name = "\"nombre_de_fournisseurs\"")
	private String nombreDeFournisseurs;

	@Column(name = "\"nombre_de_services\"")
	private String nombreDeServices;

	@Column(name = "\"details_clients_externes\"")
	private String detailsClientsExternes;

	@Column(name = "\"nombre_clients_internes\"")
	private String nombreClientsInternes;

	@Column(name = "\"nombre_clients_externes\"")
	private String nombreClientsExternes;

	@Column(name = "\"nombre_objets_references\"")
	private String nombreObjetsReferences;

	@Column(name = "\"nombre_services_web_esposes_data_lab\"")
	private String nombreServicesWebExposesDataLab;

	@Column(name = "\"nombre_objets_references_en_projet\"")
	private String nombreObjetsReferencesEnProjet;

	@Column(name = "\"nombre_services_en_projet\"")
	private String nombreServicesEnProjet;

	@Column(name = "\"nombre_services_web_exposes_data_lab_en_projet\"")
	private String nombreServicesWebExposesDataLabEnProjet;

	@Column(name = "\"raison_etre\"")
	private String raisonEtre;

	@Column(name = "\"raison_etre_courte\"")
	private String raisonEtreCourte;

	@Column(name = "\"services_documentes\"")
	private String servicesDocumentes;

	@Column(name = "\"statut\"")
	private String statut;

	@Column(name = "\"description\"")
	private String description;

	@Column(name = "\"date_mise_en_service\"")
	private String dateMiseEnService;

	// bi-directional many-to-one association to Filiere
	@ManyToOne
	@JoinColumns({ @JoinColumn(name = "\"id_filiere\"", referencedColumnName = "\"id_filiere\"") })
	private Filiere filiere;

	// bi-directional many-to-one association to AssociationActeurGisement
	@OneToMany(mappedBy = "gisement")
	private Set<AssociationActeurGisement> associationActeurGisements;

	// bi-directional many-to-one association to AssociationFiliereGisement
	@OneToMany(mappedBy = "gisement")
	private Set<AssociationFiliereGisement> associationFiliereGisements;

	// bi-directional many-to-one association to AssociationLienUtileGisement
	@OneToMany(mappedBy = "gisement")
	private Set<AssociationLienUtileGisement> associationLienUtileGisements;

}