package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "famille_responsables" database table.
 * 
 */
@Entity
@Table(name="\"famille_responsables\"")
@Getter
@Setter
public class FamilleResponsable extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FAMILLE_RESPONSABLES_IDFAMILLERESPONSABLES_GENERATOR", sequenceName="famille_responsables_sequence", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FAMILLE_RESPONSABLES_IDFAMILLERESPONSABLES_GENERATOR")
	@Column(name="\"id_famille_responsables\"")
	private Long idFamilleResponsables;

	@Column(name="\"code_role_fonctionnel\"")
	private String codeRoleFonctionnel;

	@Column(name="\"description\"")
	private String description;

	@Column(name="\"etat\"")
	private String etat;

	@Column(name="\"nom\"")
	private String nom;

	//bi-directional many-to-one association to Famille
	@ManyToOne
	@JoinColumn(name = "\"famille_id_famille\"", referencedColumnName = "\"id_famille\"")
	private Famille famille;

	//bi-directional many-to-one association to ID Numérique
	@ManyToOne
	@JoinColumn(name = "\"responsable_de_donnees_id_identite_numerique\"", referencedColumnName = "\"id_utilisateur\"")
	private IdentiteNumerique identiteNumerique;


}