package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;
import sncf.reseau.cdsdpadsir.agora.model.RelationClasseDobjetDto;

/**
 * The persistent class for the "relation_classe_dobjet" database table.
 * 
 */
@Entity
@Table(name = "\"relation_classe_dobjet\"")
@Getter
@Setter
public class RelationClasseDobjet extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "RELATION_CLASSE_DOBJET_IDRELATIONCLASSEDOBJET_GENERATOR", sequenceName = "SEQUENCE_RELATION_CLASSE_DOBJET", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RELATION_CLASSE_DOBJET_IDRELATIONCLASSEDOBJET_GENERATOR")
	@Column(name = "\"id_relation_classe_dobjet\"")
	private Long idRelationClasseDobjet;

	@Column(name = "\"type_relation\"")
	private String typeRelation;

	// bi-directional many-to-one association to ClasseDobjet
	@ManyToOne
	@JoinColumn(name = "\"id_classe_dobjet_source\"", referencedColumnName = "\"id_classe_dobjet\"")
	private ClasseDobjet classeDobjetSource;

	@Column(name = "\"est_navigable_source\"")
	private Boolean estNavigableSource;

	@Column(name = "\"cardinalite_min_source\"")
	private Integer cardinaliteMinSource;

	@Column(name = "\"cardinalite_max_source\"")
	private Integer cardinaliteMaxSource;

	@ManyToOne
	@JoinColumn(name = "\"id_classe_dobjet_destination\"", referencedColumnName = "\"id_classe_dobjet\"")
	private ClasseDobjet classeDobjetDestination;

	@Column(name = "\"cardinalite_min_destination\"")
	private Integer cardinaliteMinDestination;

	@Column(name = "\"cardinalite_max_destination\"")
	private Integer cardinaliteMaxDestination;

	@Column(name = "\"est_navigable_destination\"")
	private Boolean estNavigableDestination;

	@Column(name = "\"version\"")
	private String version;

	@Column(name = "\"revision_fonctionnel\"")
	private Timestamp revisionFonctionnel;

	@ManyToOne
	@JoinColumn(name = "\"id_type_element_modele_objet\"", nullable = true)
	private TypeElementModeleObjet typeElementModeleObjet;

	public RelationClasseDobjetDto mapToDto() {
		RelationClasseDobjetDto dto = new RelationClasseDobjetDto();
		dto.setCardinaliteMaxDestination(this.getCardinaliteMaxDestination());
		dto.setCardinaliteMaxSource(this.getCardinaliteMaxSource());
		dto.setCardinaliteMinSource(this.getCardinaliteMinSource());
		dto.setClasseDobjetDestination(
				null != this.getClasseDobjetDestination() ? this.getClasseDobjetDestination().mapToDto() : null);
		dto.setDateCreation(this.getDateCreation());
		dto.setDateDebutActivite(dto.getDateDebutActivite());
		dto.setDateFinActivite(this.getDateFinActivite());
		dto.setEstNavigableDestination(this.getEstNavigableDestination());
		dto.setEstNavigableSource(this.getEstNavigableSource());
		dto.setId(this.getId());
		dto.setIdExterne(this.getIdExterne());
		dto.setIdRelationClasseDobjet(this.getIdRelationClasseDobjet());
		dto.setRevision(this.getRevision());
		dto.setRevisionFonctionnel(this.getRevisionFonctionnel());
		dto.setTypeElementModeleObjet(
				null != this.getTypeElementModeleObjet() ? this.getTypeElementModeleObjet().mapToDto() : null);
		dto.setTypeRelation(this.getTypeRelation());
		dto.setVersion(this.getVersion());
		return dto;
	}

}