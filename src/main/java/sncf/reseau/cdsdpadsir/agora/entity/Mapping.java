package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;


/**
 * The persistent class for the "mapping" database table.
 * 
 */
@Entity
@Table(name="\"mapping\"")
@Getter
@Setter
public class Mapping implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="\"code_cp\"")
	private String codeCp;

	@Column(name="\"id_caimon\"")
	private String idCaimon;
	
//	@ManyToOne
//	@JoinColumns({ @JoinColumn(name = "\"code_cp\"", referencedColumnName = "\"cp\"") })
//	private Utilisateur utilisateur;


}