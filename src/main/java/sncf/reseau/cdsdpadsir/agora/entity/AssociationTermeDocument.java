package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "association_terme_document" database table.
 * 
 */
@Entity
@Table(name="\"association_terme_document\"")
@Getter
@Setter
public class AssociationTermeDocument extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ASSOCIATION_TERME_DOCUMENT_IDTERMEDOCUMENT_GENERATOR", sequenceName="SEQUENCE_ASSOCIATION_TERME_DOCUMENT", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ASSOCIATION_TERME_DOCUMENT_IDTERMEDOCUMENT_GENERATOR")
	@Column(name="\"id_terme_document\"")
	private Long idTermeDocument;

	//bi-directional many-to-one association to Document
	@ManyToOne
	@JoinColumn(name = "\"id_document\"", referencedColumnName = "\"id_document\"")
	private Document document;

	//bi-directional many-to-one association to Terme
	@ManyToOne
	@JoinColumn(name = "\"id_terme\"", referencedColumnName = "\"id_terme\"")
	private Terme terme;

	}