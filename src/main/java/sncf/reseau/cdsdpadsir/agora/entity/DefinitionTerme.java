package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "definition_terme" database table.
 * 
 */
@Entity
@Table(name="\"definition_terme\"")
@Getter
@Setter
public class DefinitionTerme extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DEFINITION_TERME_IDDEFINITIONTERME_GENERATOR", sequenceName="SEQUENCE_DEFINITION_TERME", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DEFINITION_TERME_IDDEFINITIONTERME_GENERATOR")
	@Column(name="\"id_definition_terme\"")
	private Long idDefinitionTerme;

	@Column(name="\"definition\"")
	private String definition;

	//bi-directional many-to-one association to Terme
	@ManyToOne
	@JoinColumn(name = "\"id_terme\"", referencedColumnName = "\"id_terme\"")
	private Terme terme;

}