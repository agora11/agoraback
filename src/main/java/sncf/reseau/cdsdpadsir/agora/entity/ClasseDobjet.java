package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;
import sncf.reseau.cdsdpadsir.agora.model.ClasseDobjetDto;

/**
 * The persistent class for the "classe_dobjet" database table.
 * 
 */
@Entity
@Table(name = "\"classe_dobjet\"")
@Getter
@Setter
public class ClasseDobjet extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "CLASSE_DOBJET_IDCLASSEDOBJET_GENERATOR", sequenceName = "SEQUENCE_CLASSE_DOBJET", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLASSE_DOBJET_IDCLASSEDOBJET_GENERATOR")
	@Column(name = "\"id_classe_dobjet\"")
	private Long idClasseDobjet;

	@Column(name = "\"abreviation\"")
	private String abreviation;

	@Column(name = "\"commentaire\"")
	private String commentaire;

	@Column(name = "\"libelle\"")
	private String libelle;

	// bi-directional many-to-one association to AssociationClasseDobjetDomaine
	@OneToMany(mappedBy = "classeDobjet")
	private Set<AssociationClasseDobjetDomaine> associationClasseDobjetDomaines;

	// bi-directional many-to-one association to AssociationClasseDobjetEtiquette
	@OneToMany(mappedBy = "classeDobjet")
	private Set<AssociationClasseDobjetEtiquette> associationClasseDobjetEtiquettes;

	// bi-directional many-to-one association to AssociationClasseDobjetFamille
	@OneToMany(mappedBy = "classeDobjet")
	private Set<AssociationClasseDobjetFamille> associationClasseDobjetFamilles;

	// bi-directional many-to-one association to AssociationClasseDobjetFamille
	@OneToMany(mappedBy = "classeDobjet")
	private Set<AssociationClasseDobjetSousFamille> associationClasseDobjetSousFamilles;

	// bi-directional many-to-one association to AssociationClasseDobjetFiliere
	@OneToMany(mappedBy = "classeDobjet")
	private Set<AssociationClasseDobjetFiliere> associationClasseDobjetFilieres;

	// bi-directional many-to-one association to AssociationClasseDobjetUtilisateur
	@OneToMany(mappedBy = "classeDobjet")
	private Set<AssociationClasseDobjetIdentiteNumerique> associationClasseDobjetUtilisateurs;

	// bi-directional many-to-one association to AssociationTermeClasseDobjet
	@OneToMany(mappedBy = "classeDobjet")
	private Set<AssociationTermeClasseDobjet> associationTermeClasseDobjets;

	// bi-directional many-to-one association to AttributClasse
	@OneToMany(mappedBy = "classeDobjet")
	private Set<AttributClasse> attributClasses;

	// bi-directional many-to-one association to AttributClasseNomenclature
	@OneToMany(mappedBy = "classeDobjet")
	private Set<AttributClasseNomenclature> attributClasseNomenclatures;

	// bi-directional many-to-one association to DefinitionClasseDobjet
	@OneToMany(mappedBy = "classeDobjet")
	private Set<DefinitionClasseDobjet> definitionClasseDobjets;

	// bi-directional many-to-one association to RelationClasseDobjet
	@OneToMany(mappedBy = "classeDobjetSource")
	private Set<RelationClasseDobjet> relationsClasseDobjetsSource;

	// bi-directional many-to-one association to RelationClasseDobjet
	@OneToMany(mappedBy = "classeDobjetDestination")
	private Set<RelationClasseDobjet> relationsClasseDobjetsDestination;

	// bi-directional many-to-one association to RelationClasseMere
	@OneToMany(mappedBy = "classeDobjeMere")
	private Set<RelationClasseMere> classeDobjetMeres;

	// bi-directional many-to-one association to RelationClasseMere
	@OneToMany(mappedBy = "classeDobjetFille")
	private Set<RelationClasseMere> classeDobjetFilles;

	// bi-directional many-to-one association to StatutClasseDobjet
	@OneToMany(mappedBy = "classeDobjet")
	private Set<StatutClasseDobjet> statutClasseDobjets;

	@OneToMany(mappedBy = "classeDobjet")
	private Set<CritereQualite> critereQualites;

	// bi-directional many-to-one association to Terme
	@ManyToOne()
	@JoinColumn(name = "\"proprietaire\"", referencedColumnName = "\"id_identite_numerique\"")
	private IdentiteNumerique proprietaire;

	@ManyToOne
	@JoinColumn(name = "\"id_element_modele_objet\"", referencedColumnName = "\"id_element_modele_objet\"")
	private ElementModeleObjet elementModeleObjet;

	@ManyToOne
	@JoinColumn(name = "\"id_paquet\"", referencedColumnName = "\"id_paquet\"")
	private Paquet paquet;

	@Column(name = "\"version\"")
	private String version;

	@Column(name = "\"revision_fonctionnel\"")
	private Timestamp revisionFonctionnel;

	@ManyToOne(optional = true)
	@JoinColumn(name = "\"id_type_element_modele_objet\"")
	private TypeElementModeleObjet typeElementModeleObjet;

	public ClasseDobjetDto mapToDto() {
		ClasseDobjetDto dto = new ClasseDobjetDto();
		dto.setDateCreation(getDateCreation());
		dto.setDateDebutActivite(getDateDebutActivite());
		dto.setDateFinActivite(getDateFinActivite());
		dto.setId(getId());
		dto.setIdClasseDobjet(getIdClasseDobjet());
		dto.setIdExterne(getIdExterne());
		dto.setLibelle(getLibelle());
		dto.setPaquet(null != this.getPaquet() ? getPaquet().mapToDto() : null);
		dto.setRevision(getRevision());
		dto.setTypeElementModeleObjet(
				null != this.getTypeElementModeleObjet() ? this.getTypeElementModeleObjet().mapToDto() : null);
		dto.setVersion(getVersion());
		dto.setRevisionFonctionnel(this.getRevisionFonctionnel());

		return dto;
	}
}
