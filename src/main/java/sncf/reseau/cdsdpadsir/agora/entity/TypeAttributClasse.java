package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;
import sncf.reseau.cdsdpadsir.agora.model.TypeAttributClasseDto;


/**
 * The persistent class for the "type_attribut_classe" database table.
 * 
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="\"type_attribut_classe\"")
public class TypeAttributClasse extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TYPE_ATTRIBUT_CLASSE_IDTYPEATTRIBUTCLASSE_GENERATOR", sequenceName="SEQUENCE_TYPE_ATTRIBUT_CLASSE", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TYPE_ATTRIBUT_CLASSE_IDTYPEATTRIBUTCLASSE_GENERATOR")
	@Column(name="\"id_type_attribut_classe\"")
	private Long idTypeAttributClasse;

	@Column(name="\"code\"")
	private String code;

	@Column(name="\"type\"")
	private String type;

	//bi-directional many-to-one association to AttributClasse
	@OneToMany(mappedBy="typeAttributClasse" ,cascade = CascadeType.ALL , orphanRemoval = true)
	private Set<AttributClasse> attributClasses;
	
	@ManyToOne
	@JoinColumn(name = "\"id_type_element_modele_objet\"")
	private TypeElementModeleObjet typeElementModeleObjet;
	
	public TypeAttributClasseDto mapToDto() {
		TypeAttributClasseDto dto  = new TypeAttributClasseDto();		
		dto.setId(this.getId());
		dto.setCode(this.getCode());
		dto.setDateCreation(this.getDateCreation());
		dto.setDateDebutActivite(this.getDateDebutActivite());
		dto.setDateFinActivite(this.getDateFinActivite());
		dto.setIdExterne(this.getIdExterne());
		dto.setIdTypeAttributClasse(this.getIdTypeAttributClasse());
		dto.setRevision(this.getRevision());
		dto.setType(this.getType());	
		dto.setTypeElementModeleObjet(null != this.getTypeElementModeleObjet() ? this.getTypeElementModeleObjet().mapToDto() : null );
		return dto;
	}

}