package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "association_classe_dobjet_domaine" database table.
 * 
 */
@Entity
@Table(name="\"association_classe_dobjet_domaine\"")
@Getter
@Setter
public class AssociationClasseDobjetDomaine extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ASSOCIATION_CLASSE_DOBJET_DOMAINE_IDASSOCIATIONCLASSEDOBJETDOMAINE_GENERATOR", sequenceName="SEQUENCE_ASSOCIATION_CLASSE_DOBJET_DOMAINE", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ASSOCIATION_CLASSE_DOBJET_DOMAINE_IDASSOCIATIONCLASSEDOBJETDOMAINE_GENERATOR")
	@Column(name="\"id_association_classe_dobjet_domaine\"")
	private Long idAssociationClasseDobjetDomaine;


	//bi-directional many-to-one association to ClasseDobjet
	@ManyToOne
	@JoinColumn(name = "\"id_classe_dobjet\"", referencedColumnName = "\"id_classe_dobjet\"")
	private ClasseDobjet classeDobjet;

	//bi-directional many-to-one association to Domaine
	@ManyToOne
	@JoinColumn(name = "\"id_domaine\"", referencedColumnName = "\"id_domaine\"")
	private Domaine domaine;

}