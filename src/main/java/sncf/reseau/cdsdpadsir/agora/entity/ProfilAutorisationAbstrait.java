package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "profil_autorisation_abstrait" database table.
 * 
 */
@Entity
@Table(name="\"profil_autorisation_abstrait\"")
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class ProfilAutorisationAbstrait extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PROFIL_AUTORISATION_ABSTRAIT_GENERATOR", sequenceName="SEQUENCE_PROFIL_AUTORISATION_ABSTRAIT")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PROFIL_AUTORISATION_ABSTRAIT_GENERATOR")
	@Column(name="\"id_profil_autorisation_abstrait\"")
	private long idProfilAutorisationAbstrait;

	//bi-directional many-to-one association to AssociationProfilAutorisationAbstraitRole
	@OneToMany(mappedBy="profilAutorisationAbstrait")
	private Set<AssociationProfilAutorisationAbstraitRole> associationProfilAutorisationAbstraitRoles;

	//bi-directional many-to-one association to Autorisation
	@OneToMany(mappedBy="profilAutorisationAbstrait")
	private Set<Autorisation> autorisations;

	//bi-directional many-to-one association to ProfilAutorisation
	@ManyToOne
	@JoinColumn(name = "\"id_profil_autorisation\"", referencedColumnName = "\"id_profil_autorisation\"")
	private ProfilAutorisation profilAutorisation;

}