package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;
import sncf.reseau.cdsdpadsir.agora.model.RelationClasseMereDto;

/**
 * The persistent class for the "relation_classe_mere" database table.
 * 
 */
@Entity
@Table(name = "\"relation_classe_mere\"")
@Getter
@Setter
public class RelationClasseMere extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "RELATION_CLASSE_MERE_IDRELATIONCLASSEMERE_GENERATOR", sequenceName = "SEQUENCE_RELATION_CLASSE_MERE", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RELATION_CLASSE_MERE_IDRELATIONCLASSEMERE_GENERATOR")
	@Column(name = "\"id_relation_classe_mere\"")
	private Long idRelationClasseMere;

	// bi-directional many-to-one association to ClasseDobjet
	@ManyToOne
	@JoinColumn(name = "\"id_classe_dobjet_mere\"", referencedColumnName = "\"id_classe_dobjet\"")
	private ClasseDobjet classeDobjeMere;

	// bi-directional many-to-one association to ClasseDobjet
	@ManyToOne
	@JoinColumn(name = "\"id_classe_dobjet_fille\"", referencedColumnName = "\"id_classe_dobjet\"")
	private ClasseDobjet classeDobjetFille;

	@ManyToOne
	@JoinColumn(name = "\"id_type_element_modele_objet\"", nullable = true)
	private TypeElementModeleObjet typeElementModeleObjet;

	@Column(name = "\"version\"")
	private String version;

	@Column(name = "\"revision_fonctionnel\"")
	private Timestamp revisionFonctionnel;

	public RelationClasseMereDto mapToDto() {
		RelationClasseMereDto dto = new RelationClasseMereDto();
		dto.setClasseDobjeMere(null != this.getClasseDobjeMere() ? this.getClasseDobjeMere().mapToDto() : null);
		dto.setClasseDobjetFille(null != this.getClasseDobjetFille() ? this.getClasseDobjetFille().mapToDto() : null);
		dto.setDateCreation(this.getDateCreation());
		dto.setDateDebutActivite(dto.getDateDebutActivite());
		dto.setDateFinActivite(this.getDateFinActivite());
		dto.setId(this.getId());
		dto.setIdExterne(this.getIdExterne());
		dto.setIdRelationClasseMere(this.getIdRelationClasseMere());
		dto.setRevision(this.getRevision());
		dto.setRevisionFonctionnel(this.getRevisionFonctionnel());
		dto.setTypeElementModeleObjet(
				null != this.getTypeElementModeleObjet() ? this.getTypeElementModeleObjet().mapToDto() : null);
		dto.setVersion(this.getVersion());
		return dto;
	}

}