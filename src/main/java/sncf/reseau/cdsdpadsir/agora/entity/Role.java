package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * The persistent class for the "role" database table.
 * 
 */
@Entity
@Table(name = "\"role\"")
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class Role extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@Column(name = "\"id_role\"")
	private Long idRole;

	@Column(name = "\"code_role_fonctionnel\"")
	private String codeRoleFonctionnel;

	@Column(name = "\"description\"")
	private String description;

	@Column(name = "\"etat\"")
	private String etat;

	@Column(name = "\"nom\"")
	private String nom;

	// bi-directional many-to-one association to GroupeRoleRole
	@OneToMany(mappedBy = "role")
	private Set<GroupeRoleRole> groupeRoleRoles;

	// bi-directional many-to-one association to UtilisateurRole
	@OneToMany(mappedBy = "role")
	private Set<UtilisateurRole> utilisateurRoles;

	@OneToMany(mappedBy = "role")
	private Set<AssociationProfilAutorisationAbstraitRole> associationProfilAutorisationAbstraitRoles;
}