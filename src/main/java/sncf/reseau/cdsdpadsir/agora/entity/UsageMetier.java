package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "usage_metier" database table.
 * 
 */
@Entity
@Table(name="\"usage_metier\"")
@Getter
@Setter
public class UsageMetier extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="USAGE_METIER_GENERATOR", sequenceName="SEQUENCE_USAGE_METIER")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USAGE_METIER_GENERATOR")
	@Column(name="\"id_usage_metier\"")
	private long idUsageMetier;

	@Column(name="\"libelle\"")
	private String libelle;

	//bi-directional many-to-one association to PolitiqueQualiteParUsage
	@OneToMany(mappedBy="usageMetier")
	@JsonIgnore
	private Set<PolitiqueQualiteParUsage> politiqueQualiteParUsages;

	public UsageMetier() {
	}

}