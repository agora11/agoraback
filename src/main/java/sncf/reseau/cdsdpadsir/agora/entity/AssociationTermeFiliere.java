package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "association_terme_filiere" database table.
 * 
 */
@Entity
@Table(name="\"association_terme_filiere\"")
@Getter
@Setter
public class AssociationTermeFiliere extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ASSOCIATION_TERME_FILIERE_IDTERMEFILIERE_GENERATOR", sequenceName="SEQUENCE_ASSOCIATION_TERME_FILIERE", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ASSOCIATION_TERME_FILIERE_IDTERMEFILIERE_GENERATOR")
	@Column(name="\"id_terme_filiere\"")
	private Long idTermeFiliere;

	//bi-directional many-to-one association to Filiere
	@ManyToOne
	@JoinColumn(name = "\"id_filiere\"", referencedColumnName = "\"id_filiere\"")
	private Filiere filiere;

	//bi-directional many-to-one association to Terme
	@ManyToOne
	@JoinColumn(name = "\"id_terme\"", referencedColumnName = "\"id_terme\"")
	private Terme terme;

}