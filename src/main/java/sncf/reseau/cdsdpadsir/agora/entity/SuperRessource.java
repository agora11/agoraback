package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "super_ressource" database table.
 * 
 */
@Entity
@Table(name="\"super_ressource\"")
@JsonInclude(Include.NON_NULL)
@Setter
@Getter
public class SuperRessource extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SUPER_RESSOURCE_GENERATOR", sequenceName="SEQUENCE_SUPER_RESSOURCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SUPER_RESSOURCE_GENERATOR")
	@Column(name="\"id_super_ressource\"")
	private Long idSuperRessource;

	//bi-directional many-to-one association to ProfilAutorisation
	@OneToMany(mappedBy="superRessource")
	private Set<ProfilAutorisation> profilAutorisations;

	//bi-directional many-to-one association to TypeRessource
	@ManyToOne
	@JoinColumn(name = "\"id_type_ressource\"", referencedColumnName = "\"id_type_ressource\"")
	private TypeRessource typeRessource;
	
	@ManyToOne
	@JoinColumn(name = "\"id_filiere\"", referencedColumnName = "\"id_filiere\"")
	private Filiere filiere;

	/*@ManyToOne
	@JoinColumn(name = "\"id_identite_numerique\"", referencedColumnName = "\"id_identite_numerique\"")
	private IdentiteNumerique identiteNumerique;*/
	
}