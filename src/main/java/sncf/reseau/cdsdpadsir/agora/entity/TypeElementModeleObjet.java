package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;
import sncf.reseau.cdsdpadsir.agora.model.TypeElementModeleObjetDto;

/**
 * The persistent class for the "type_element_modele_objet" database table.
 * 
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "\"type_element_modele_objet\"")
public class TypeElementModeleObjet extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TYPE_ELEMENT_MODELE_OBJET_GENERATOR", sequenceName = "SEQUENCE_TYPE_ELEMENT_MODELE_OBJET")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TYPE_ELEMENT_MODELE_OBJET_GENERATOR")
	@Column(name = "\"id_type_element_modele_objet\"")
	private long idTypeElementModeleObjet;

	@Column(name = "\"libelle\"")
	private String libelle;

	// bi-directional many-to-one association to ElementModeleObjet
	@OneToMany(mappedBy = "typeElementModeleObjet")
	private Set<ElementModeleObjet> elementModeleObjets;
	
	public TypeElementModeleObjetDto mapToDto() {
		TypeElementModeleObjetDto dto = new TypeElementModeleObjetDto();
		dto.setDateCreation(getDateCreation());
		dto.setDateDebutActivite(getDateDebutActivite());
		dto.setDateFinActivite(getDateFinActivite());
		dto.setId(getId());
		dto.setIdExterne(dto.getIdExterne());
		dto.setIdTypeElementModeleObjet(getIdTypeElementModeleObjet());
		dto.setLibelle(getLibelle());
		dto.setRevision(getRevision());
		return dto ;	
	}
}