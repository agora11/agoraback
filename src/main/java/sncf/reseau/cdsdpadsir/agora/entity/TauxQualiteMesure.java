package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "taux_qualite_mesure" database table.
 * 
 */
@Entity
@Table(name="\"taux_qualite_mesure\"")
@Getter
@Setter
public class TauxQualiteMesure extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAUX_QUALITE_MESURE_GENERATOR", sequenceName="SEQUENCE_TAUX_QUALITE_MESURE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAUX_QUALITE_MESURE_GENERATOR")
	@Column(name="\"id_taux_qualite_mesure\"")
	private Long idTauxQualiteMesure;

	@Column(name="\"date_de_mesure\"")
	private String dateDeMesure;

	@Column(name="\"taux_mesure\"")
	private String tauxMesure;

	//bi-directional many-to-one association to PolitiqueQualite
	@ManyToOne
	@JoinColumn(name = "\"id_politique_qualite\"", referencedColumnName = "\"id_politique_qualite\"")
	private PolitiqueQualite politiqueQualite;

	public TauxQualiteMesure() {
	}

}