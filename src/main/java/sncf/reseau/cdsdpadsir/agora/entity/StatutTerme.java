package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * The persistent class for the "statut_terme" database table.
 * 
 */
@Entity
@Getter
@Setter
@Table(name = "\"statut_terme\"")
public class StatutTerme extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "STATUT_TERME_IDSTATUTTERME_GENERATOR", sequenceName = "SEQUENCE_STATUT_TERME", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STATUT_TERME_IDSTATUTTERME_GENERATOR")
	@Column(name = "\"id_statut_terme\"")
	private Long idStatutTerme;

	@Column(name = "\"commentaire\"")
	private String commentaire;

	// bi-directional many-to-one association to Terme
	@ManyToOne
	@JoinColumn(name = "\"id_terme\"", referencedColumnName = "\"id_terme\"")
	private Terme terme;

	// bi-directional many-to-one association to Utilisateur
	@ManyToOne
	@JoinColumn(name = "\"id_valideur\"", referencedColumnName = "\"id_identite_numerique\"")
	private IdentiteNumerique valideur;

	// bi-directional many-to-one association to Utilisateur
	@ManyToOne
	@JoinColumn(name = "\"id_suppleant\"", referencedColumnName = "\"id_identite_numerique\"")
	private IdentiteNumerique suppleant;
	
	@Column(name = "\"nom\"")
	private String nom;

	@Column(name = "\"prenom\"")
	private String prenom;

	// bi-directional many-to-one association to ValeurStatutTerme
	@ManyToOne
	@JoinColumn(name = "\"id_valeur_statut_terme\"", referencedColumnName = "\"id_valeur_statut_terme\"")
	private ValeurStatutTerme valeurStatutTerme;

}