package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "document" database table.
 * 
 */
@Entity
@Table(name="\"document\"")
@Getter
@Setter
public class Document extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DOCUMENT_IDDOCUMENT_GENERATOR", sequenceName="SEQUENCE_DOCUMENT", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DOCUMENT_IDDOCUMENT_GENERATOR")
	@Column(name="\"id_document\"")
	private Long idDocument;

	@Column(name="\"url\"")
	private String url;

	@Column(name="\"titre\"")
	private String titre;

	//bi-directional many-to-one association to AssociationTermeDocument
	@OneToMany(mappedBy="document")
	private Set<AssociationTermeDocument> associationTermeDocuments;

}