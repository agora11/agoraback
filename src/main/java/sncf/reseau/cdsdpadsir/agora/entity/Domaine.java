package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "domaine" database table.
 * 
 */
@Entity
@Table(name="\"domaine\"")
@Getter
@Setter
public class Domaine extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DOMAINE_IDDOMAINE_GENERATOR", sequenceName="SEQUENCE_DOMAINE" , allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DOMAINE_IDDOMAINE_GENERATOR")
	@Column(name="\"id_domaine\"")
	private Long idDomaine;

	@Column(name="\"libelle\"")
	private String libelle;

	//bi-directional many-to-one association to AssociationClasseDobjetDomaine
	@OneToMany(mappedBy="domaine")
	private Set<AssociationClasseDobjetDomaine> associationClasseDobjetDomaines;

	//bi-directional many-to-one association to AssociationTermeDomaine
	@OneToMany(mappedBy="domaine")
	private Set<AssociationTermeDomaine> associationTermeDomaines;

}