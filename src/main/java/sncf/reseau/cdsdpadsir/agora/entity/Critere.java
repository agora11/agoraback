package sncf.reseau.cdsdpadsir.agora.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "\"critere\"")
@Getter
@Setter
public class Critere {
	
	@Id
	@Column(name="\"libelle\"")
	private String libelle;

}
