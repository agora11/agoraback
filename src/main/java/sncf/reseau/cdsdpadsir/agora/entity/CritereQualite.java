package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "critere_qualite" database table.
 * 
 */
@Entity
@Table(name="\"critere_qualite\"")
@Getter
@Setter
public class CritereQualite extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CRITERE_QUALITE_GENERATOR", sequenceName="SEQUENCE_CRITERE_QUALITE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CRITERE_QUALITE_GENERATOR")
	@Column(name="\"id_critere_qualite\"")
	private Long idCritereQualite;

	@Column(name="\"libelle\"")
	private String libelle;

	//bi-directional many-to-one association to PolitiqueQualite
	@OneToMany(mappedBy="critereQualite")
	private Set<PolitiqueQualite> politiqueQualites;
	
	@ManyToOne
	@JoinColumn(name = "\"id_classe_dobjet\"", referencedColumnName = "\"id_classe_dobjet\"")
	private ClasseDobjet classeDobjet;

	public CritereQualite() {
	}
}