package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "famille_sous_familles" database table.
 * 
 */
@Entity
@Table(name="\"famille_sous_familles\"")
@Getter
@Setter
public class FamilleSousFamille extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FAMILLE_SOUS_FAMILLES_IDFAMILLESOUSFAMILLE_GENERATOR", sequenceName="famille_sous_familles_sequence", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FAMILLE_SOUS_FAMILLES_IDFAMILLESOUSFAMILLE_GENERATOR")
	@Column(name="\"id_famille_sous_famille\"")
	private Long idFamilleSousFamille;

	@Column(name="\"code_role_fonctionnel\"")
	private String codeRoleFonctionnel;

	@Column(name="\"description\"")
	private String description;

	@Column(name="\"etat\"")
	private String etat;

	@Column(name="\"nom\"")
	private String nom;

	//bi-directional many-to-one association to Famille
	@ManyToOne
	@JoinColumn(name = "\"famille_id_famille\"", referencedColumnName = "\"id_famille\"")
	private Famille famille;

	//bi-directional many-to-one association to SousFamille
	@ManyToOne
	@JoinColumn(name = "\"sous_famille_id_sous_famille\"", referencedColumnName = "\"id_sous_famille\"")
	private SousFamille sousFamille;

}