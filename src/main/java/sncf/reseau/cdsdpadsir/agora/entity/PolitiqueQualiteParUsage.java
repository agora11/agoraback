package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "politique_qualite_par_usage" database table.
 * 
 */
@Entity
@Table(name="\"politique_qualite_par_usage\"")
@Getter
@Setter
public class PolitiqueQualiteParUsage extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="POLITIQUE_QUALITE_PAR_USAGE_GENERATOR", sequenceName="SEQUENCE_POLITIQUE_QUALITE_PAR_USAGE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="POLITIQUE_QUALITE_PAR_USAGE_GENERATOR")
	@Column(name="\"id_politique_qualite_par_usage\"")
	private Long idPolitiqueQualiteParUsage;

	@Column(name="\"taux_souhaite\"")
	private String tauxSouhaite;

	//bi-directional many-to-one association to PolitiqueQualite
	@ManyToOne
	@JoinColumn(name = "\"id_politique_qualite\"", referencedColumnName = "\"id_politique_qualite\"")
	private PolitiqueQualite politiqueQualite;

	//bi-directional many-to-one association to UsageMetier
	@ManyToOne
	@JoinColumn(name = "\"id_usage_metier\"", referencedColumnName = "\"id_usage_metier\"")
	private UsageMetier usageMetier;

	public PolitiqueQualiteParUsage() {
	}

}