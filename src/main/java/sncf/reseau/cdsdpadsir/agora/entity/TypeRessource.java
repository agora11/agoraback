package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "type_ressource" database table.
 * 
 */
@Entity
@Table(name="\"type_ressource\"")
@Getter
@Setter
public class TypeRessource extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TYPE_RESSOURCE_GENERATOR", sequenceName="SEQUENCE_TYPE_RESSOURCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TYPE_RESSOURCE_GENERATOR")
	@Column(name="\"id_type_ressource\"")
	private long idTypeRessource;

	//bi-directional many-to-one association to SuperRessource
	@OneToMany(mappedBy="typeRessource")
	private Set<SuperRessource> superRessources;
}