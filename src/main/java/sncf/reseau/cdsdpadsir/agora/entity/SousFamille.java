package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "sous_famille" database table.
 * 
 */
@Entity
@Table(name="\"sous_famille\"")
@Getter
@Setter
public class SousFamille extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@Column(name="\"id_sous_famille\"")
	private Long idSousFamille;

	@Column(name="\"code_role_fonctionnel\"")
	private String codeRoleFonctionnel;

	@Column(name="\"description\"")
	private String description;

	@Column(name="\"etat\"")
	private String etat;

	@Column(name="\"nom\"")
	private String nom;

	//bi-directional many-to-one association to FamilleSousFamille
	@OneToMany(mappedBy="sousFamille")
	private Set<FamilleSousFamille> familleSousFamilles;

	//bi-directional many-to-one association to SousFamilleResponsable
	@OneToMany(mappedBy="sousFamille")
	private Set<SousFamilleResponsable> sousFamilleResponsables;

}