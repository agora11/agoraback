package sncf.reseau.cdsdpadsir.agora.entity.utils;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * DTO using for pagination
 */
@Getter
@Setter
@AllArgsConstructor
public class PagingResponse<S> {

	/**
	 * entity count
	 */
	private Long count;
	/**
	 * page number, 0 indicate the first page.
	 */
	private Long pageNumber;
	/**
	 * size of page, 0 indicate infinite-sized.
	 */
	private Long pageSize;
	/**
	 * Offset from the of pagination.
	 */
	private Long pageOffset;
	/**
	 * the number total of pages.
	 */
	private Long pageTotal;
	/**
	 * elements of page.
	 */
	private List<S> elements;
}