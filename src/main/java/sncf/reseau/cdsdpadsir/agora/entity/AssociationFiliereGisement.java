package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * The persistent class for the "association_filiere_gisement" database table.
 * 
 */
@Entity
@Table(name = "\"association_filiere_gisement\"")
@Getter
@Setter
public class AssociationFiliereGisement extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ASSOCIATION_FILIERE_GISEMENT_GENERATOR", sequenceName = "SEQUENCE_GISEMENT")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ASSOCIATION_FILIERE_GISEMENT_GENERATOR")
	@Column(name = "\"id_association_filiere_gisement\"")
	private long idAssociationFiliereGisement;

	// bi-directional many-to-one association to Gisement
	@ManyToOne
	@JoinColumn(name = "\"id_gisement\"", referencedColumnName = "\"id_gisement\"")
	private Gisement gisement;

	// bi-directional many-to-one association to Gisement
	@ManyToOne
	@JoinColumn(name = "\"id_filiere\"", referencedColumnName = "\"id_filiere\"")
	private Filiere filiere;

}