package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "type_autorisation" database table.
 * 
 */
@Entity
@Table(name="\"type_autorisation\"")
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class TypeAutorisation extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TYPE_AUTORISATION_GENERATOR", sequenceName="SEQUENCE_TYPE_AUTORISATION")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TYPE_AUTORISATION_GENERATOR")
	@Column(name="\"id_type_autorisation\"")
	private long idTypeAutorisation;

	@Column(name="\"libelle\"")
	private String libelle;

	//bi-directional many-to-one association to AssociationProfilAutorisationTypeAutorisation
	@OneToMany(mappedBy="typeAutorisation")
	private Set<AssociationProfilAutorisationTypeAutorisation> associationProfilAutorisationTypeAutorisations;

}