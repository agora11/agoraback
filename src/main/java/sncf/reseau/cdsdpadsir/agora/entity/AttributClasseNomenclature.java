package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "attribut_classe_nomenclature" database table.
 * 
 */
@Entity
@Table(name="\"attribut_classe_nomenclature\"")
@Getter
@Setter
public class AttributClasseNomenclature extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ATTRIBUT_CLASSE_NOMENCLATURE_IDATTRIBUTCLASSENOMENCLATURE_GENERATOR", sequenceName="SEQUENCE_ATTRIBUT_CLASSE_NOMENCLATURE", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ATTRIBUT_CLASSE_NOMENCLATURE_IDATTRIBUTCLASSENOMENCLATURE_GENERATOR")
	@Column(name="\"id_attribut_classe_nomenclature\"")
	private Long idAttributClasseNomenclature;

	//bi-directional many-to-one association to ClasseDobjet
	@ManyToOne
	@JoinColumn(name = "\"id_classe_dobjet\"", referencedColumnName = "\"id_classe_dobjet\"")
	private ClasseDobjet classeDobjet;

	//bi-directional many-to-one association to NomenclatureDattribut
	@ManyToOne
	@JoinColumn(name = "\"id_nomenclature_dattribut\"", referencedColumnName = "\"id_nomenclature_dattribut\"")
	private NomenclatureDattribut nomenclatureDattribut;

	}