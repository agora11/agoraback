package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * The persistent class for the "filiere_responsables" database table.
 * 
 */
@Entity
@Table(name = "\"filiere_responsables\"")
@Getter
@Setter
public class FiliereResponsable extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "FILIERE_RESPONSABLES_IDFILIERERESPONSABLES_GENERATOR", sequenceName = "filiere_responsables_sequence", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FILIERE_RESPONSABLES_IDFILIERERESPONSABLES_GENERATOR")
	@Column(name = "\"id_filiere_responsables\"")
	private Long idFiliereResponsables;

	@Column(name = "\"code_role_fonctionnel\"")
	private String codeRoleFonctionnel;

	@Column(name = "\"description\"")
	private String description;

	@Column(name = "\"etat\"")
	private String etat;

	@Column(name = "\"nom\"")
	private String nom;
	
	@Column(name = "\"ordre\"")
	private Integer ordre;
	

	// bi-directional many-to-one association to Filiere
	@ManyToOne
	@JoinColumn(name = "\"filiere_id_filiere\"", referencedColumnName = "\"id_filiere\"")
	private Filiere filiere;

	// bi-directional many-to-one association to ID Numerique
	@ManyToOne
	@JoinColumn(name = "\"coordinateur_de_filiere_id_identite_numerique\"", referencedColumnName = "\"id_utilisateur\"")
	private IdentiteNumerique coordinateur;

	// bi-directional many-to-one association to ID Numerique
	@ManyToOne
	@JoinColumn(name = "\"responsable_de_filiere_delegue_id_identite_numerique\"", referencedColumnName = "\"id_utilisateur\"")
	private IdentiteNumerique responsableDelegue;

	// bi-directional many-to-one association to ID Numerique
	@ManyToOne
	@JoinColumn(name = "\"responsable_de_filiere_id_identite_numerique\"", referencedColumnName = "\"id_utilisateur\"")
	private IdentiteNumerique responsable;

	// bi-directional many-to-one association to ID Numerique
	@ManyToOne
	@JoinColumn(name = "\"valideur_de_filiere_id_identite_numerique\"", referencedColumnName = "\"id_utilisateur\"")
	private IdentiteNumerique valideur;

}