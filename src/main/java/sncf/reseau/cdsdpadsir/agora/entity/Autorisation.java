package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "autorisation" database table.
 * 
 */
@Entity
@Table(name="\"autorisation\"")
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class Autorisation extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="AUTORISATION_GENERATOR", sequenceName="SEQUENCE_AUTORISATION")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AUTORISATION_GENERATOR")
	@Column(name="\"id_autorisation\"")
	private Long idAutorisation;

	//bi-directional many-to-one association to ProfilAutorisationAbstrait
	@ManyToOne
	@JoinColumn(name = "\"id_profil_autorisation_abstrait\"", referencedColumnName = "\"id_profil_autorisation_abstrait\"")
	private ProfilAutorisationAbstrait profilAutorisationAbstrait;
	
	@ManyToOne
	@JoinColumn(name = "\"id_identite_numerique\"", referencedColumnName = "\"id_identite_numerique\"")
	private IdentiteNumerique identiteNumerique;

}