package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "groupe_role" database table.
 * 
 */
@Entity
@Table(name="\"groupe_role\"")
@Getter
@Setter
public class GroupeRole extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="GROUPE_ROLE_IDGROUPEROLE_GENERATOR", sequenceName="groupe_role_sequence", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GROUPE_ROLE_IDGROUPEROLE_GENERATOR")
	@Column(name="\"id_groupe_role\"")
	private Long idGroupeRole;

	@Column(name="\"code_role_fonctionnel\"")
	private String codeRoleFonctionnel;

	@Column(name="\"description\"")
	private String description;

	@Column(name="\"etat\"")
	private String etat;

	@Column(name="\"nom\"")
	private String nom;

	//bi-directional many-to-one association to GroupeRoleRole
	@OneToMany(mappedBy="groupeRole")
	private Set<GroupeRoleRole> groupeRoleRoles;

}