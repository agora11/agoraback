package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "valeur_statut_classe_dobjet" database table.
 * 
 */
@Entity
@Getter
@Setter
@Table(name="\"valeur_statut_classe_dobjet\"")
public class ValeurStatutClasseDobjet extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VALEUR_STATUT_CLASSE_DOBJET_IDVALEURSTATUTCLASSEDOBJET_GENERATOR", sequenceName="SEQUENCE_VALEUR_STATUT_CLASSE_DOBJET", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VALEUR_STATUT_CLASSE_DOBJET_IDVALEURSTATUTCLASSEDOBJET_GENERATOR")
	@Column(name="\"id_valeur_statut_classe_dobjet\"")
	private Long idValeurStatutClasseDobjet;

	@Column(name="\"code\"")
	private Long code;

	@Column(name="\"libelle\"")
	private String libelle;

	//bi-directional many-to-one association to StatutClasseDobjet
	@OneToMany(mappedBy="valeurStatutClasseDobjet")
	private Set<StatutClasseDobjet> statutClasseDobjets;

}