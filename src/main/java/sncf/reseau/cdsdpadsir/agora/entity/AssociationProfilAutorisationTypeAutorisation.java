package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * The persistent class for the
 * "association_profil_autorisation_type_autorisation" database table.
 * 
 */
@Entity
@Table(name = "\"association_profil_autorisation_type_autorisation\"")
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class AssociationProfilAutorisationTypeAutorisation extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ASSOCIATION_PROFIL_AUTORISATION_TYPE_AUTORISATION_GENERATOR", sequenceName = "SEQUENCE_ASSOCIATION_PROFIL_AUTORISATION_TYPE_AUTORISATION")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ASSOCIATION_PROFIL_AUTORISATION_TYPE_AUTORISATION_GENERATOR")
	@Column(name = "\"id_association_profil_autorisation_type_autorisationcolumn\"")
	private long idAssociationProfilAutorisationTypeAutorisation;

	// bi-directional many-to-one association to ProfilAutorisation
	@ManyToOne
	@JoinColumn(name = "\"id_profil_autorisation\"", referencedColumnName = "\"id_profil_autorisation\"")
	private ProfilAutorisation profilAutorisation;

	// bi-directional many-to-one association to TypeAutorisation
	@ManyToOne
	@JoinColumn(name = "\"id_type_autorisation\"", referencedColumnName = "\"id_type_autorisation\"")
	private TypeAutorisation typeAutorisation;

}