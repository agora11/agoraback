package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "lien_utile" database table.
 * 
 */
@Entity
@Table(name="\"lien_utile\"")
@Getter
@Setter
public class LienUtile extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="LIEN_UTILE_GENERATOR", sequenceName="SEQUENCE_LIEN_UTILE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="LIEN_UTILE_GENERATOR")
	@Column(name="\"id_lien_utile\"")
	private long idLienUtile;

	@Column(name="\"libelle\"")
	private String libelle;

	@Column(name="\"url\"")
	private String url;

	//bi-directional many-to-one association to AssociationLienUtileGisement
	@OneToMany(mappedBy="lienUtile")
	private Set<AssociationLienUtileGisement> associationLienUtileGisements;

}