package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "profil_autorisation" database table.
 * 
 */
@Entity
@Table(name="\"profil_autorisation\"")
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class ProfilAutorisation extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PROFIL_AUTORISATION_GENERATOR", sequenceName="SEQUENCE_PROFIL_AUTORISATION")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PROFIL_AUTORISATION_GENERATOR")
	@Column(name="\"id_profil_autorisation\"")
	private long idProfilAutorisation;
	
	@Column(name="\"libelle\"")
	private String libelle;

	//bi-directional many-to-one association to AssociationProfilAutorisationTypeAutorisation
	@OneToMany(mappedBy="profilAutorisation")
	private Set<AssociationProfilAutorisationTypeAutorisation> associationProfilAutorisationTypeAutorisations;

	//bi-directional many-to-one association to SuperRessource
	@ManyToOne
	@JoinColumn(name = "\"id_super_ressource\"", referencedColumnName = "\"id_super_ressource\"")
	private SuperRessource superRessource;

	//bi-directional many-to-one association to ProfilAutorisationAbstrait
	@OneToMany(mappedBy="profilAutorisation")
	private Set<ProfilAutorisationAbstrait> profilAutorisationAbstraits;

}