package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;
import sncf.reseau.cdsdpadsir.agora.model.AttributClasseDto;

/**
 * The persistent class for the "attribut_classe" database table.
 * 
 */
@Entity
@Table(name = "\"attribut_classe\"")
@Getter
@Setter
public class AttributClasse extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ATTRIBUT_CLASSE_IDATTRIBUTCLASSE_GENERATOR", sequenceName = "SEQUENCE_ATTRIBUT_CLASSE", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ATTRIBUT_CLASSE_IDATTRIBUTCLASSE_GENERATOR")
	@Column(name = "\"id_attribut_classe\"")
	private Long idAttributClasse;

	@Column(name = "\"libelle\"")
	private String libelle;

	@Column(name = "\"valeur\"")
	private String valeur;

	// bi-directional many-to-one association to ClasseDobjet
	@ManyToOne
	@JoinColumn(name = "\"id_classe_dobjet\"", referencedColumnName = "\"id_classe_dobjet\"")
	private ClasseDobjet classeDobjet;

	// bi-directional many-to-one association to TypeAttributClasse
	@ManyToOne
	@JoinColumn(name = "\"id_type_attribut_classe\"", referencedColumnName = "\"id_type_attribut_classe\"")
	private TypeAttributClasse typeAttributClasse;

	@Column(name = "\"version\"")
	private String version;
	
	@Column(name = "\"revision_fonctionnel\"")
	private Timestamp revisionFonctionnel ;
	
	@ManyToOne
	@JoinColumn(name = "\"id_type_element_modele_objet\"")
	private TypeElementModeleObjet typeElementModeleObjet;
	
	public AttributClasseDto mapToDto() {
		AttributClasseDto dto = new AttributClasseDto();
		dto.setClasseDobjet(null != this.classeDobjet ? this.classeDobjet.mapToDto() : null);
		dto.setDateCreation(this.getDateCreation());
		dto.setDateDebutActivite(this.getDateDebutActivite());
		dto.setDateFinActivite(this.getDateFinActivite());
		dto.setId(this.getId());
		dto.setIdAttributClasse(this.getIdAttributClasse());
		dto.setIdExterne(this.getIdExterne());
		dto.setLibelle(this.getLibelle());
		dto.setRevision(this.getRevision());
		dto.setRevisionFonctionnel(this.getRevisionFonctionnel());
		dto.setTypeAttributClasse(null != this.getTypeAttributClasse() ? this.getTypeAttributClasse().mapToDto(): null);
		dto.setTypeElementModeleObjet(null != this.getTypeElementModeleObjet() ? this.getTypeElementModeleObjet().mapToDto() : null);
		dto.setValeur(this.getValeur());
		dto.setVersion(this.getVersion());
		return dto ;
	}

}