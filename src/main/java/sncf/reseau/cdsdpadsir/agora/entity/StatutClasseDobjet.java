package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * The persistent class for the "statut_classe_dobjet" database table.
 * 
 */
@Entity
@Getter
@Setter
@Table(name = "\"statut_classe_dobjet\"")
public class StatutClasseDobjet extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "STATUT_CLASSE_DOBJET_IDSTATUTRESSOUECE_GENERATOR", sequenceName = "SEQUENCE_STATUT_CLASSE_DOBJET", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STATUT_CLASSE_DOBJET_IDSTATUTRESSOUECE_GENERATOR")
	@Column(name = "\"id_statut_ressouece\"")
	private Long idStatutRessouece;

	@Column(name = "\"nom\"")
	private String nom;

	@Column(name = "\"prenom\"")
	private String prenom;

	@Column(name = "\"commentaire\"")
	private String commentaire;

	// bi-directional many-to-one association to ClasseDobjet
	@ManyToOne
	@JoinColumn(name = "\"id_classe_dobjet\"", referencedColumnName = "\"id_classe_dobjet\"")
	private ClasseDobjet classeDobjet;

	// bi-directional many-to-one association to Utilisateur
	@ManyToOne
	@JoinColumn(name = "\"id_valideur\"", referencedColumnName = "\"id_identite_numerique\"")
	private IdentiteNumerique valideur;

	// bi-directional many-to-one association to Utilisateur
	@ManyToOne
	@JoinColumn(name = "\"id_suppleant\"", referencedColumnName = "\"id_identite_numerique\"")
	private IdentiteNumerique suppleant;

	// bi-directional many-to-one association to ValeurStatutClasseDobjet
	@ManyToOne
	@JoinColumn(name = "\"id_valeur_statut_classe_dobjet\"", referencedColumnName = "\"id_valeur_statut_classe_dobjet\"")
	private ValeurStatutClasseDobjet valeurStatutClasseDobjet;

}