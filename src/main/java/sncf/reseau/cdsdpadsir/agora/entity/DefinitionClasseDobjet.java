package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "definition_classe_dobjet" database table.
 * 
 */
@Entity
@Table(name="\"definition_classe_dobjet\"")
@Getter
@Setter
public class DefinitionClasseDobjet extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DEFINITION_CLASSE_DOBJET_IDDEFINITIONCLASSEDOBJET_GENERATOR", sequenceName="SEQUENCE_DEFINITION_CLASSE_DOBJET", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DEFINITION_CLASSE_DOBJET_IDDEFINITIONCLASSEDOBJET_GENERATOR")
	@Column(name="\"id_definition_classe_dobjet\"")
	private Long idDefinitionClasseDobjet;

	@Column(name="\"definition\"")
	private String definition;

	//bi-directional many-to-one association to ClasseDobjet
	@ManyToOne
	@JoinColumn(name = "\"id_classe_dobjet\"", referencedColumnName = "\"id_classe_dobjet\"")
	private ClasseDobjet classeDobjet;

}