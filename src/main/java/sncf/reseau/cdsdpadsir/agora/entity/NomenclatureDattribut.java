package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "nomenclature_dattribut" database table.
 * 
 */
@Entity
@Table(name="\"nomenclature_dattribut\"")
@Getter
@Setter
@NamedQuery(name="NomenclatureDattribut.findAll", query="SELECT n FROM NomenclatureDattribut n")
public class NomenclatureDattribut extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="NOMENCLATURE_DATTRIBUT_IDNOMENCLATUREDATTRIBUT_GENERATOR", sequenceName="SEQUENCE_NOMENCLATURE_DATTRIBUT", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="NOMENCLATURE_DATTRIBUT_IDNOMENCLATUREDATTRIBUT_GENERATOR")
	@Column(name="\"id_nomenclature_dattribut\"")
	private Long idNomenclatureDattribut;

	//bi-directional many-to-one association to AttributClasseNomenclature
	@OneToMany(mappedBy="nomenclatureDattribut")
	private Set<AttributClasseNomenclature> attributClasseNomenclatures;

	//bi-directional many-to-one association to ElementNomenclature
	@OneToMany(mappedBy="nomenclatureDattribut")
	private Set<ElementNomenclature> elementNomenclatures;

}