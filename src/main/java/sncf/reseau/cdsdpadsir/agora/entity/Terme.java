package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * The persistent class for the "terme" database table.
 * 
 */
@Entity
@Getter
@Setter
@Table(name = "\"terme\"")
public class Terme extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TERME_IDTERME_GENERATOR", sequenceName = "SEQUENCE_TERME", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TERME_IDTERME_GENERATOR")
	@Column(name = "\"id_terme\"")
	private Long idTerme;

	@Column(name = "\"abreviation\"")
	private String abreviation;

	@Column(name = "\"commentaire\"")
	private String commentaire;

	@Column(name = "\"libelle\"")
	private String libelle;

	// bi-directional many-to-one association to AssociationTermeClasseDobjet
	@OneToMany(mappedBy = "terme")
	private Set<AssociationTermeClasseDobjet> associationTermeClasseDobjets;

	// bi-directional many-to-one association to AssociationTermeDocument
	@OneToMany(mappedBy = "terme")
	private Set<AssociationTermeDocument> associationTermeDocuments;

	// bi-directional many-to-one association to AssociationTermeDomaine
	@OneToMany(mappedBy = "terme")
	private Set<AssociationTermeDomaine> associationTermeDomaines;

	// bi-directional many-to-one association to AssociationTermeEtiquette
	@OneToMany(mappedBy = "terme")
	private Set<AssociationTermeEtiquette> associationTermeEtiquettes;

	// bi-directional many-to-one association to AssociationTermeFiliere
	@OneToMany(mappedBy = "terme")
	private Set<AssociationTermeFiliere> associationTermeFilieres;

	// bi-directional many-to-one association to AssociationTermeUtilisateur
	@OneToMany(mappedBy = "terme")
	private Set<AssociationTermeIdentiteNumerique> associationTermeUtilisateurs;

	// bi-directional many-to-one association to DefinitionTerme
	@OneToMany(mappedBy = "terme")
	private Set<DefinitionTerme> definitionTermes;

	// bi-directional many-to-one association to StatutTerme
	@OneToMany(mappedBy = "terme")
	private Set<StatutTerme> statutTermes;
	
	@OneToMany(mappedBy = "terme")
	private Set<AssociationTermeElementModeleObjet> associationTermeElementModeleObjets;

	// bi-directional many-to-one association to Terme
	@ManyToOne
	@JoinColumn(name = "\"proprietaire\"", referencedColumnName = "\"id_identite_numerique\"")
	private IdentiteNumerique proprietaire;

}