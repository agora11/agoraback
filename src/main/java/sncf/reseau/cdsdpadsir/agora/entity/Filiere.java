package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * The persistent class for the "filiere" database table.
 * 
 */
@Entity
@Table(name = "\"filiere\"")
@Getter
@Setter
public class Filiere extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "FILIERE_IDFILIERE_GENERATOR", sequenceName = "filiere_sequence", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FILIERE_IDFILIERE_GENERATOR")
	@Column(name = "\"id_filiere\"")
	private Long idFiliere;

	@Column(name = "\"code_role_fonctionnel\"")
	private String codeRoleFonctionnel;

	@Column(name = "\"description\"")
	private String description;

	@Column(name = "\"etat\"")
	private String etat;

	@Column(name = "\"image\"")
	private String image;

	@Column(name = "\"nom\"")
	private String nom;

	@Column(name = "\"slug\"")
	private String slug;

	// bi-directional many-to-one association to AssociationClasseDobjetFiliere
	@OneToMany(mappedBy = "filiere")
	private Set<AssociationClasseDobjetFiliere> associationClasseDobjetFilieres;

	// bi-directional many-to-one association to AssociationTermeFiliere
	@OneToMany(mappedBy = "filiere")
	private Set<AssociationTermeFiliere> associationTermeFilieres;

	// bi-directional many-to-one association to FiliereFamille
	@OneToMany(mappedBy = "filiere")
	private Set<FiliereFamille> filiereFamilles;

	// bi-directional many-to-one association to FiliereResponsable
	@OneToMany(mappedBy = "filiere")
	private Set<FiliereResponsable> filiereResponsables;

	@OneToMany(mappedBy = "filiere")
	private Set<SuperRessource> superRessources;

	// bi-directional many-to-one association to AssociationFiliereGisement
	@OneToMany(mappedBy = "filiere")
	private Set<AssociationFiliereGisement> associationFiliereGisements;

}