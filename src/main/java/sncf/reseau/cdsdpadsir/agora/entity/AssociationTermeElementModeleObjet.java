package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "association_terme_element_modele_objet" database table.
 * 
 */
@Entity
@Table(name="\"association_terme_element_modele_objet\"")
@Getter
@Setter
public class AssociationTermeElementModeleObjet extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ASSOCIATION_TERME_ELEMENT_MODELE_OBJET_GENERATOR", sequenceName="SEQUENCE_ASSOCIATION_TERME_ELEMENT_MODELE_OBJET")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ASSOCIATION_TERME_ELEMENT_MODELE_OBJET_GENERATOR")
	@Column(name="\"id_association_terme_element_modele_objet\"")
	private long idAssociationTermeElementModeleObjet;
	
	@ManyToOne
	@JoinColumn(name = "\"id_terme\"", referencedColumnName = "\"id_terme\"")
	private Terme terme;
	
	@ManyToOne
	@JoinColumn(name = "\"id_element_modele_objet\"", referencedColumnName = "\"id_element_modele_objet\"")
	private ElementModeleObjet elementModeleObjet;

}