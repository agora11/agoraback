package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * The persistent class for the "sous_famille_responsables" database table.
 * 
 */
@Entity
@Getter
@Setter
@Table(name = "\"sous_famille_responsables\"")
public class SousFamilleResponsable extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@Column(name = "\"id_sous_famille_responsables\"")
	private Long idSousFamilleResponsables;

	@Column(name = "\"casdusage\"")
	private String casdusage;

	@Column(name = "\"code_role_fonctionnel\"")
	private String codeRoleFonctionnel;

	@Column(name = "\"description\"")
	private String description;

	@Column(name = "\"etat\"")
	private String etat;

	@Column(name = "\"nom\"")
	private String nom;

	// bi-directional many-to-one association to SousFamille
	@ManyToOne
	@JoinColumn(name = "\"sous_famille_id_sous_famille\"", referencedColumnName = "\"id_sous_famille\"")
	private SousFamille sousFamille;

	// bi-directional many-to-one association to Utilisateur
	@ManyToOne
	@JoinColumn(name = "\"referent_metier_id_utilisateur\"", referencedColumnName = "\"id_utilisateur\"")
	private Utilisateur utilisateur;

}