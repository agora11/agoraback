package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * The persistent class for the "IdentiteNumerique" database table.
 * 
 */
@Entity
@Table(name = "\"identite_numerique\"")
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class IdentiteNumerique extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IDIDENTITENUMERIQUE_GENERATOR", sequenceName = "SEQUENCE_IdentiteNumerique", allocationSize = 1, initialValue = 748)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDIDENTITENUMERIQUE_GENERATOR")
	@Column(name = "\"id_identite_numerique\"")
	private Long idIdentiteNumerique;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "\"id_utilisateur\"", referencedColumnName = "\"id_utilisateur\"")
	private Utilisateur utilisateur;

	// bi-directional many-to-one association to AssociationClasseDobjetUtilisateur
	@OneToMany(mappedBy = "identiteNumerique")
	private Set<AssociationClasseDobjetIdentiteNumerique> associationClasseDobjetUtilisateurs;

	// bi-directional one-to-one association to AssociationTermeUtilisateur
	@OneToOne(mappedBy = "identiteNumerique")
	private AssociationTermeIdentiteNumerique associationTermeUtilisateur;

	// bi-directional many-to-one association to FamilleResponsable
	@OneToMany(mappedBy = "identiteNumerique")
	private Set<FamilleResponsable> familleResponsables;

	// bi-directional many-to-one association to FiliereResponsable
	@OneToMany(mappedBy = "coordinateur")
	private Set<FiliereResponsable> coordinateurs;

	// bi-directional many-to-one association to FiliereResponsable
	@OneToMany(mappedBy = "responsableDelegue")
	private Set<FiliereResponsable> responsablesDelegues;

	// bi-directional many-to-one association to FiliereResponsable
	@OneToMany(mappedBy = "responsable")
	private Set<FiliereResponsable> responsables;

	// bi-directional many-to-one association to FiliereResponsable
	@OneToMany(mappedBy = "valideur")
	private Set<FiliereResponsable> valideurs;

	// bi-directional many-to-one association to StatutClasseDobjet
	@OneToMany(mappedBy = "valideur")
	private Set<StatutClasseDobjet> statutValideurClasseDobjets;

	// bi-directional many-to-one association to StatutClasseDobjet
	@OneToMany(mappedBy = "suppleant")
	private Set<StatutClasseDobjet> statutSuppleantClasseDobjets;

	// bi-directional many-to-one association to StatutTerme
	@OneToMany(mappedBy = "valideur")
	private Set<StatutTerme> statutValideurTermes;
	
	// bi-directional many-to-one association to StatutTerme
		@OneToMany(mappedBy = "suppleant")
		private Set<StatutTerme> statutSuppleantTermes;

	// bi-directional many-to-one association to UtilisateurRole
	@OneToMany(mappedBy = "identiteNumerique")
	private Set<UtilisateurRole> utilisateurRoles;

	@OneToMany(mappedBy = "proprietaire")
	private Set<Terme> termes;

	@OneToMany(mappedBy = "proprietaire")
	private Set<ClasseDobjet> objets;

	/*@OneToMany(mappedBy = "identiteNumerique")
	private Set<SuperRessource> superRessources;*/

	@OneToMany(mappedBy = "identiteNumerique")
	private Set<Autorisation> autorisations;

}