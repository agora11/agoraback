package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "element_nomenclature" database table.
 * 
 */
@Entity
@Table(name="\"element_nomenclature\"")
@Getter
@Setter
public class ElementNomenclature extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="ELEMENT_NOMENCLATURE_IDELEMENTNOMENCLATURE_GENERATOR", sequenceName="SEQUENCE_ELEMENT_NOMENCLATURE", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ELEMENT_NOMENCLATURE_IDELEMENTNOMENCLATURE_GENERATOR")
	@Column(name="\"id_element_nomenclature\"")
	private Long idElementNomenclature;

	//bi-directional many-to-one association to NomenclatureDattribut
	@ManyToOne
	@JoinColumn(name = "\"id_nomenclature_dattribut\"", referencedColumnName = "\"id_nomenclature_dattribut\"")
	private NomenclatureDattribut nomenclatureDattribut;

}