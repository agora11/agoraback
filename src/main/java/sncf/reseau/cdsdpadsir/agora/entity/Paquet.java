package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;
import sncf.reseau.cdsdpadsir.agora.model.PaquetDto;

/**
 * The persistent class for the "paquet" database table.
 * 
 */
@Entity
@Table(name = "\"paquet\"")
@Getter
@Setter
public class Paquet extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "PAQUET_GENERATOR", sequenceName = "SEQUENCE_PAQUET", allocationSize = 1,  initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PAQUET_GENERATOR")
	@Column(name = "\"id_paquet\"", nullable=false)
	private Long idPaquet;

	@OneToMany(mappedBy = "paquet")
	private Set<ClasseDobjet> classeDobjets;
	
	////@OneToOne(cascade = {CascadeType.ALL})
	@OneToOne(fetch = FetchType.LAZY, orphanRemoval = false)
    @JoinColumn( name="\"id_paquet_parent\"", nullable=true, insertable = true )		
    private Paquet paquetParent;
	
	@Column(name = "\"version\"")
	private String version ;
	
//	@Temporal( TemporalType.TIMESTAMP)
//	@Column(name = "\"revision_fonctionnel\"")
//	private Timestamp revisionFonctionnel ;
	
	@Column(name = "\"revision_fonctionnel\"")
	private Timestamp revisionFonctionnel ;
	
	
	@Column(name = "\"nom\"")
	private String nom ;
			
	@ManyToOne(optional = true)
    @JoinColumn(name="\"id_type_element_modele_objet\"")    
	private TypeElementModeleObjet typeElementModeleObjet;
	
//	@OneToOne
//	@JoinColumns({ @JoinColumn(name = "\"paquet_mere\"", referencedColumnName = "\"id_paquet\"") })
//	private Paquet paquet;
//
//	@OneToMany(mappedBy = "paquet", fetch = FetchType.EAGER)
//	private Set<Paquet> paquets;
	
	public PaquetDto mapToDto() {
		PaquetDto dto = new PaquetDto();
		dto.setId(getId());
		dto.setIdExterne(getIdExterne());
		dto.setIdPaquet(getIdPaquet());
		if(null != getPaquetParent()) {			
//			dto.setPaquetParent(getPaquetParent().mapToDot());
//			treeParent(getPaquetParent() , this.getPaquetParent().mapToDto() );
			dto.setPaquetParent(treeParent(this));
		}
		dto.setNom(getNom());
		dto.setRevision(getRevision());
		if(null!= getTypeElementModeleObjet()) {
			dto.setTypeElementModeleObjet(getTypeElementModeleObjet().mapToDto());
		}
		dto.setDateCreation(getDateCreation());
		dto.setDateDebutActivite(getDateDebutActivite());
		dto.setDateFinActivite(getDateFinActivite());
		
		dto.setVersion(this.getVersion());
		dto.setRevisionFonctionnel(this.getRevisionFonctionnel());
		return dto ;
	}
	
	public PaquetDto treeParent(Paquet paquet ) {
//		if(null != paquet && null != paquet.getPaquetParent()) {
//			paquetDto.setPaquetParent(paquet.getPaquetParent().mapToDto());
//			treeParent( paquet.getPaquetParent() ,paquetDto.getPaquetParent());
//		}
		
		if(null != paquet && null != paquet.getPaquetParent()) {
			return paquet.getPaquetParent().mapToDto();
		}else {
			return null ;
		}
	}
	
//	@PrePersist()
//	private void onCreate() {
//		this.revisionFonctionnel = Timestamp.valueOf(LocalDateTime.now());
//	}

}