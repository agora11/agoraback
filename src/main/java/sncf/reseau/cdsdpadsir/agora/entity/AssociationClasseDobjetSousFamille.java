package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * The persistent class for the "association_classe_dobjet_famille" database table.
 * 
 */
@Entity
@Table(name="\"association_classe_dobjet_sous_famille\"")
@Getter
@Setter
public class AssociationClasseDobjetSousFamille extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ASSOCIATION_CLASSE_DOBJET_SOUS_FAMILLE_IDCLASSEDOBJETFAMILLE_GENERATOR", sequenceName="SEQUENCE_ASSOCIATION_CLASSE_DOBJET_SOUS_FAMILLE", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ASSOCIATION_CLASSE_DOBJET_SOUS_FAMILLE_IDCLASSEDOBJETFAMILLE_GENERATOR")
	@Column(name="\"id_classe_dobjet_sous_famille\"")
	private Long idClasseDobjetsSousFamille;


	//bi-directional many-to-one association to ClasseDobjet
	@ManyToOne
	@JoinColumn(name = "\"id_classe_dobjet\"", referencedColumnName = "\"id_classe_dobjet\"")
	private ClasseDobjet classeDobjet;

	//bi-directional many-to-one association to Famille
	@ManyToOne
	@JoinColumn(name = "\"id_famille\"", referencedColumnName = "\"id_famille\"")
	private Famille famille;

}
