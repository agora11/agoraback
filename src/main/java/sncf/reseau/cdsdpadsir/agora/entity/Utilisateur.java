package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * The persistent class for the "utilisateur" database table.
 * 
 */
@Entity
@Getter
@Setter
@Table(name = "\"utilisateur\"")
@JsonInclude(Include.NON_NULL)
public class Utilisateur extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "UTILISATEUR_IDUTILISATEUR_GENERATOR", sequenceName = "utilisateur_sequence", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UTILISATEUR_IDUTILISATEUR_GENERATOR")
	@Column(name = "\"id_utilisateur\"")
	private Long idUtilisateur;

	@Column(name = "\"adresse_mail\"")
	private String adresseMail;

	@Column(name = "\"code_role_fonctionnel\"")
	private String codeRoleFonctionnel;

	@Column(name = "\"cp\"")
	private String cp;

	@Column(name = "\"description\"")
	private String description;

	@Column(name = "\"etat\"")
	private String etat;

	@Column(name = "\"nom\"")
	private String nom;

	@Column(name = "\"organisation\"")
	private String organisation;

	@Column(name = "\"password\"")
	private String password;

	@Column(name = "\"poste\"")
	private String poste;

	@Column(name = "\"prenom\"")
	private String prenom;

	@OneToOne(mappedBy = "utilisateur",fetch = FetchType.EAGER)
	private IdentiteNumerique identiteNumerique;

}