package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "utilisateur_role" database table.
 * 
 */
@Entity
@Getter
@Setter
@Table(name="\"identite_numerique_role\"")
public class UtilisateurRole extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="UTILISATEUR_ROLE_IDUTILISATEURROLE_GENERATOR", sequenceName="utilisateur_role_sequence", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="UTILISATEUR_ROLE_IDUTILISATEURROLE_GENERATOR")
	@Column(name="\"id_utilisateur_role\"")
	private Long idUtilisateurRole;

	@Column(name="\"code_role_fonctionnel\"")
	private String codeRoleFonctionnel;

	@Column(name="\"description\"")
	private String description;

	@Column(name="\"etat\"")
	private String etat;

	@Column(name="\"nom\"")
	private String nom;

	//bi-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name = "\"role_id_role\"", referencedColumnName = "\"id_role\"")
	private Role role;

	//bi-directional many-to-one association to Utilisateur
	@ManyToOne
	@JoinColumn(name = "\"identite_numerique_id_identite_numerique\"", referencedColumnName = "\"id_identite_numerique\"")
	private IdentiteNumerique identiteNumerique;

}