package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "association_terme_domaine" database table.
 * 
 */
@Entity
@Table(name="\"association_terme_domaine\"")
@Getter
@Setter
public class AssociationTermeDomaine extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ASSOCIATION_TERME_DOMAINE_IDASSOCIATIONTERMEDOMAINE_GENERATOR", sequenceName="SEQUENCE_ASSOCIATION_TERME_DOMAINE", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ASSOCIATION_TERME_DOMAINE_IDASSOCIATIONTERMEDOMAINE_GENERATOR")
	@Column(name="\"id_association_terme_domaine\"")
	private Long idAssociationTermeDomaine;

	//bi-directional many-to-one association to Domaine
	@ManyToOne
	@JoinColumn(name = "\"id_domaine\"", referencedColumnName = "\"id_domaine\"")
	private Domaine domaine;

	//bi-directional many-to-one association to Terme
	@ManyToOne
	@JoinColumn(name = "\"id_terme\"", referencedColumnName = "\"id_terme\"")
	private Terme terme;

	
}