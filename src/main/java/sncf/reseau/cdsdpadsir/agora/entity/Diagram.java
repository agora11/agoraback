package sncf.reseau.cdsdpadsir.agora.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name="diagram")
public class Diagram extends RessourceAbstraite {

    @Id
    @SequenceGenerator(name="DIAGRAM_ID_GENERATOR", sequenceName="DIAGRAM_SEQUENCE", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DIAGRAM_ID_GENERATOR")
    @Column(name = "diagram_id")
    private Long diagramId;
    @Column(name = "libelle")
    private String libelle;
    @Column(name = "description")
    private String description;
    @OneToOne
    @JoinColumn(name = "filiere_id")
    private Filiere filiere;

    @OneToMany(mappedBy = "diagram", cascade = CascadeType.ALL)
    @JsonManagedReference("cells_list")
    private List<Cell> cells ;

}
