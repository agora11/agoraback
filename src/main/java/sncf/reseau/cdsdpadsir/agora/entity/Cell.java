package sncf.reseau.cdsdpadsir.agora.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "cell")
@Getter
@Setter
public class Cell {

    @Id
    @SequenceGenerator(name = "CELL_ID_SEQUENCE", sequenceName = "CELL_SEQUENCE", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CELL_ID_SEQUENCE")
    @Column(name = "cell_id")
    private Long id;

    @ManyToOne(targetEntity = Diagram.class)
    @JsonBackReference("cells_list")
    @JoinColumn(name = "diagram_id")
    private Diagram diagram;

    @Column(name = "cell", columnDefinition="text")
    private String cell;
}
