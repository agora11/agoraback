package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "association_classe_dobjet" database table.
 * 
 */
@Entity
@Table(name="\"association_classe_dobjet\"")
@Getter
@Setter
public class AssociationClasseDobjet extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ASSOCIATION_CLASSE_DOBJET_IDASSOCIATIONDOBJET_GENERATOR", sequenceName="SEQUENCE_ASSOCIATION_CLASSE_DOBJET", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ASSOCIATION_CLASSE_DOBJET_IDASSOCIATIONDOBJET_GENERATOR")
	@Column(name="\"id_association_dobjet\"")
	private Long idAssociationDobjet;

	//bi-directional many-to-one association to RelationClasseDobjet
	@ManyToOne
	@JoinColumn(name = "\"id_relation_destination\"", referencedColumnName = "\"id_relation_classe_dobjet\"")
	private RelationClasseDobjet relationClasseDobjet1;

	//bi-directional many-to-one association to RelationClasseDobjet
	@ManyToOne
	@JoinColumn(name = "\"id_relation_source\"", referencedColumnName = "\"id_relation_classe_dobjet\"")
	private RelationClasseDobjet relationClasseDobjet2;
	
	@Column(name="\"cardinalite_max\"")
	private int cardinaliteMax;

	@Column(name="\"cardinalite_min\"")
	private int cardinaliteMin;

	@Column(name="\"est_navigable\"")
	private boolean estNavigable;
	
	@ManyToOne
    @JoinColumn(name="\"type_element_modele_objet\"")    
	private TypeElementModeleObjet typeElementModeleObjet;

}