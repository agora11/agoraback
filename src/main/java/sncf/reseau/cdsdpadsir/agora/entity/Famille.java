package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * The persistent class for the "famille" database table.
 * 
 */
@Entity
@Table(name = "\"famille\"")
@Getter
@Setter
public class Famille extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "FAMILLE_IDFAMILLE_GENERATOR", sequenceName = "famille_sequence", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FAMILLE_IDFAMILLE_GENERATOR")
	@Column(name = "\"id_famille\"", nullable = true)
	private Long idFamille;

	@Column(name = "\"code_role_fonctionnel\"")
	private String codeRoleFonctionnel;

	@Column(name = "\"description\"")
	private String description;

	@Column(name = "\"etat\"")
	private String etat;

	@Column(name = "\"nom\"")
	private String nom;

	// bi-directional many-to-one association to AssociationClasseDobjetFamille
	@OneToMany(mappedBy = "famille")
	private Set<AssociationClasseDobjetFamille> associationClasseDobjetFamilles;

	// bi-directional many-to-one association to AssociationClasseDobjetSousFamille
	@OneToMany(mappedBy = "famille")
	private Set<AssociationClasseDobjetSousFamille> associationClasseDobjetSousFamilles;

	// bi-directional many-to-one association to FamilleResponsable
	@OneToMany(mappedBy = "famille")
	private Set<FamilleResponsable> familleResponsables;

	// bi-directional many-to-one association to FamilleSousFamille
	@OneToMany(mappedBy = "famille")
	private Set<FamilleSousFamille> familleSousFamilles;

	// bi-directional many-to-one association to FiliereFamille
	@OneToMany(mappedBy = "famille")
	private Set<FiliereFamille> filiereFamilles;

	// bi-directional many-to-one association to FamilleBis
	@ManyToOne
	@JoinColumn(name = "\"famille_mere\"", referencedColumnName = "\"id_famille\"")
	private Famille famille;

	// bi-directional many-to-one association to FamilleBis
	@OneToMany(mappedBy = "famille", fetch = FetchType.EAGER)
	private Set<Famille> familles;

}