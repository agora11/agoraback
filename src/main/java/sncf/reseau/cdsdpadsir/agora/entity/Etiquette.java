package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "etiquette" database table.
 * 
 */
@Entity
@Table(name="\"etiquette\"")
@Getter
@Setter
public class Etiquette extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ETIQUETTE_IDETIQUETTE_GENERATOR", sequenceName="SEQUENCE_ETIQUETTE", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ETIQUETTE_IDETIQUETTE_GENERATOR")
	@Column(name="\"id_etiquette\"")
	private Long idEtiquette;

	@Column(name="\"libelle\"")
	private String libelle;

	//bi-directional many-to-one association to AssociationClasseDobjetEtiquette
	@OneToMany(mappedBy="etiquette")
	private Set<AssociationClasseDobjetEtiquette> associationClasseDobjetEtiquettes;

	//bi-directional many-to-one association to AssociationTermeEtiquette
	@OneToMany(mappedBy="etiquette")
	private Set<AssociationTermeEtiquette> associationTermeEtiquettes;

}