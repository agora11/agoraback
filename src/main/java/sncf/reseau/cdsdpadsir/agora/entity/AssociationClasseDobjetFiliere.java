package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;


/**
 * The persistent class for the "association_classe_dobjet_filiere" database table.
 * 
 */
@Entity
@Table(name="\"association_classe_dobjet_filiere\"")
@Getter
@Setter
public class AssociationClasseDobjetFiliere extends RessourceAbstraite implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ASSOCIATION_CLASSE_DOBJET_FILIERE_IDASSOCIATIONCLASSEDOBJETFILIERE_GENERATOR", sequenceName="SEQUENCE_ASSOCIATION_CLASSE_DOBJET_FILIERE", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ASSOCIATION_CLASSE_DOBJET_FILIERE_IDASSOCIATIONCLASSEDOBJETFILIERE_GENERATOR")
	@Column(name="\"id_association_classe_dobjet_filiere\"")
	private Long idAssociationClasseDobjetFiliere;

	//bi-directional many-to-one association to ClasseDobjet
	@ManyToOne
	@JoinColumn(name = "\"id_classe_dobjet\"", referencedColumnName = "\"id_classe_dobjet\"")
	private ClasseDobjet classeDobjet;

	//bi-directional many-to-one association to Filiere
	@ManyToOne
	@JoinColumn(name = "\"id_filiere\"", referencedColumnName = "\"id_filiere\"")
	private Filiere filiere;

	}