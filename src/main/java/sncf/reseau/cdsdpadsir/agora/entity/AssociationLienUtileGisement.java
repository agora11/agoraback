package sncf.reseau.cdsdpadsir.agora.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;

/**
 * The persistent class for the "association_lien_utile_gisement" database
 * table.
 * 
 */
@Entity
@Table(name = "\"association_lien_utile_gisement\"")
@Getter
@Setter
public class AssociationLienUtileGisement extends RessourceAbstraite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ASSOCIATION_LIEN_UTILE_GISEMENT_GENERATOR", sequenceName = "SEQUENCE__GISEMENT")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ASSOCIATION_LIEN_UTILE_GISEMENT_GENERATOR")
	@Column(name = "\"id_association_lien_utile_gisement\"")
	private long idAssociationLienUtileGisement;

	// bi-directional many-to-one association to Gisement
	@ManyToOne
	@JoinColumn(name = "\"id_gisement\"", referencedColumnName = "\"id_gisement\"")
	private Gisement gisement;

	// bi-directional many-to-one association to LienUtile
	@ManyToOne
	@JoinColumn(name = "\"id_lien_utile\"", referencedColumnName = "\"id_lien_utile\"")
	private LienUtile lienUtile;

}