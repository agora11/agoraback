package sncf.reseau.cdsdpadsir.agora.configuration;

public class CpError extends Exception {
	public CpError(String errorMessage) {
		super(errorMessage);
	}
}
