package sncf.reseau.cdsdpadsir.agora.repository;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.Famille;

@Repository
public interface FamilleRepository extends JpaRepository<Famille, Long>, JpaSpecificationExecutor<Famille> {

	Optional<Famille> findByIdFamille(Long idFamille);

	Set<Famille> findByFamilleAndDateFinActivite(Famille famille, LocalDateTime futureDate);

	Famille findByNomAndDateFinActivite(String string, LocalDateTime futureDate);

	

}
