package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import sncf.reseau.cdsdpadsir.agora.entity.FiliereFamille;

public interface FiliereFamilleRepository extends JpaRepository<FiliereFamille, Long>, JpaSpecificationExecutor<FiliereFamille> {

}
