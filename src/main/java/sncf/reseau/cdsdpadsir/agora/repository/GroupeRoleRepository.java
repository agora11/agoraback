package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import sncf.reseau.cdsdpadsir.agora.entity.GroupeRole;

public interface GroupeRoleRepository extends JpaRepository<GroupeRole, Long>, JpaSpecificationExecutor<GroupeRole> {

}
