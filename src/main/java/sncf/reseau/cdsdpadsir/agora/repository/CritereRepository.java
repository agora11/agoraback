package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.Critere;

public interface CritereRepository extends JpaRepository<Critere, Long> {

}
