package sncf.reseau.cdsdpadsir.agora.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import sncf.reseau.cdsdpadsir.agora.entity.ValeurStatutClasseDobjet;

public interface ValeurStatutClasseDobjetRepository extends JpaRepository<ValeurStatutClasseDobjet, Long>, JpaSpecificationExecutor<ValeurStatutClasseDobjet> {

	Optional<ValeurStatutClasseDobjet> findByLibelle(String libelle);

}
