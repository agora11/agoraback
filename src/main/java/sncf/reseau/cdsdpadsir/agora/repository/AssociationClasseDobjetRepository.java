package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.AssociationClasseDobjet;

public interface AssociationClasseDobjetRepository  extends JpaRepository<AssociationClasseDobjet, Long>{

}
