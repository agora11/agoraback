package sncf.reseau.cdsdpadsir.agora.repository;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.Terme;

@Repository
public interface TermeRepository extends JpaRepository<Terme, Long>, JpaSpecificationExecutor<Terme> {

	Optional<Terme> findByIdTerme(Long idTerme);

	@Query(value = "select * from {h-schema}terme t \r\n"
			+ "inner join {h-schema}association_terme_filiere atf on t.id_terme = atf.id_terme \r\n"
			+ "inner join {h-schema}filiere f on  atf.id_filiere = f.id_filiere \r\n"
			+ "where t.date_fin_activite =?2 and upper(t.libelle) = upper(?1) and f.nom = ?3 limit 1", nativeQuery = true)
	Optional<Terme> findFirstByLibelleAndDateFinActiviteByFiliere(String libelle, LocalDateTime date, String filiere);

	Optional<Terme> findFirstByLibelleIgnoreCaseAndDateFinActivite(String libelle, LocalDateTime date);

}
