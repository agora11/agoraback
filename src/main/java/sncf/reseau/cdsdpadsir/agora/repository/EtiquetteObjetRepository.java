package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.AssociationClasseDobjetEtiquette;

@Repository
public interface EtiquetteObjetRepository  extends JpaRepository<AssociationClasseDobjetEtiquette, Long> {

}
