package sncf.reseau.cdsdpadsir.agora.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import sncf.reseau.cdsdpadsir.agora.entity.GroupeRoleRole;
import sncf.reseau.cdsdpadsir.agora.entity.Role;

public interface GroupeRoleRoleRepository
		extends JpaRepository<GroupeRoleRole, Long>, JpaSpecificationExecutor<GroupeRoleRole> {

	Optional<GroupeRoleRole> findByRole(Role role);

}
