package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.PolitiqueQualiteParUsage;

public interface PolitiqueQualiteParUsageRepository extends JpaRepository<PolitiqueQualiteParUsage, Long> {

}
