package sncf.reseau.cdsdpadsir.agora.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.AssociationTermeEtiquette;
import sncf.reseau.cdsdpadsir.agora.entity.Etiquette;
import sncf.reseau.cdsdpadsir.agora.entity.Terme;

@Repository
public interface EtiquetteTermeRepository extends JpaRepository<AssociationTermeEtiquette, Long> {

	List<AssociationTermeEtiquette> findByTerme(Terme terme);

	List<AssociationTermeEtiquette> findByTermeAndEtiquette(Terme terme, Etiquette etiquette);

}
