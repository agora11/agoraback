package sncf.reseau.cdsdpadsir.agora.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.AssociationTermeFiliere;
import sncf.reseau.cdsdpadsir.agora.entity.Filiere;
import sncf.reseau.cdsdpadsir.agora.entity.Terme;

@Repository
public interface FiliereTermeRepository extends JpaRepository<AssociationTermeFiliere, Long> {

	List<AssociationTermeFiliere> findByTerme(Terme terme);

	List<AssociationTermeFiliere> findByTermeAndFiliere(Terme terme, Filiere filiere);

}
