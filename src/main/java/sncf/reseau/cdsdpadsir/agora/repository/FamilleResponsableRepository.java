package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import sncf.reseau.cdsdpadsir.agora.entity.FamilleResponsable;

public interface FamilleResponsableRepository extends JpaRepository<FamilleResponsable, Long>, JpaSpecificationExecutor<FamilleResponsable> {

}
