package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.FiliereResponsable;

public interface FiliereResponsableRepository extends JpaRepository<FiliereResponsable, Long> {

}
