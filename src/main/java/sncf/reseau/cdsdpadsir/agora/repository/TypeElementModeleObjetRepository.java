package sncf.reseau.cdsdpadsir.agora.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.TypeElementModeleObjet;

@Repository
public interface TypeElementModeleObjetRepository extends JpaRepository<TypeElementModeleObjet, Long>  ,JpaSpecificationExecutor<TypeElementModeleObjet> {

	@Query(value = "SELECT * FROM {h-schema}type_element_modele_objet type WHERE  libelle =:libelle " , nativeQuery = true)
	Optional<TypeElementModeleObjet> findByLibelle(@Param("libelle") String libelle);
	
}
