package sncf.reseau.cdsdpadsir.agora.repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sncf.reseau.cdsdpadsir.agora.entity.RelationClasseDobjet;

@Repository
public interface RelationClasseDobjetRepository extends JpaRepository<RelationClasseDobjet, Long>  {

	@Query(value = "SELECT * FROM {h-schema}relation_classe_dobjet r WHERE  id_externe=:idExterne  and version=:version and revision_fonctionnel=:revisionFonctionnel order by revision_fonctionnel desc " , nativeQuery = true)
	List<RelationClasseDobjet> findByIdExterneVersionRevisionFonctionnel(
			@Param("idExterne") String idExterne ,
			@Param("version") String version ,
			@Param("revisionFonctionnel") Timestamp revisionFonctionnel) ;

	@Query(value = "SELECT * FROM {h-schema}relation_classe_dobjet r WHERE  id_externe=:idExterne order by revision_fonctionnel desc limit 1 " , nativeQuery = true)
	List<RelationClasseDobjet> findByIdExterneAndMaxRevisionFonctionnelle(
			@Param("idExterne") String idExterne ) ;

	@Query(value = "SELECT * FROM {h-schema}relation_classe_dobjet r WHERE  id_externe=:idExterne " , nativeQuery = true)
	List<RelationClasseDobjet> findByIdExterne(@Param("idExterne") String idExterne);
	void deleteByIdIn(List<String> ids);

	Optional<RelationClasseDobjet> findTopByIdOrderByIdRelationClasseDobjetDesc(String id);
	List<RelationClasseDobjet> findAllByClasseDobjetSource_IdClasseDobjet(Long idClasseDobjet);
	List<RelationClasseDobjet> findAllByClasseDobjetDestination_IdClasseDobjet(Long idClasseDobjet);




}
