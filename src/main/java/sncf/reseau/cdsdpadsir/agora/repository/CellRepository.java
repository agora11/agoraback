package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sncf.reseau.cdsdpadsir.agora.entity.Cell;

@Repository
public interface CellRepository extends JpaRepository<Cell, Long> {

     void deleteByDiagram_DiagramId(Long diagramId);
}
