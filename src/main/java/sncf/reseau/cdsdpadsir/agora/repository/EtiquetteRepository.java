package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.Etiquette;

@Repository
public interface EtiquetteRepository   extends JpaRepository<Etiquette, Long>, JpaSpecificationExecutor<Etiquette> {

}
