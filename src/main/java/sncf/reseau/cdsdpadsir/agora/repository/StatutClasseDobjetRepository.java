package sncf.reseau.cdsdpadsir.agora.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import sncf.reseau.cdsdpadsir.agora.entity.StatutClasseDobjet;

public interface StatutClasseDobjetRepository extends JpaRepository<StatutClasseDobjet, Long>, JpaSpecificationExecutor<StatutClasseDobjet> {

	List<StatutClasseDobjet> findById(String id);
}
