package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.AssociationProfilAutorisationAbstraitRole;

public interface AssociationProfilAutorisationAbstraitRoleRepository extends JpaRepository<AssociationProfilAutorisationAbstraitRole , Long> {

}
