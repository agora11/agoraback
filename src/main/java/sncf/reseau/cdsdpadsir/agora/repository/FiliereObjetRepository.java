package sncf.reseau.cdsdpadsir.agora.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.AssociationClasseDobjetFiliere;
import sncf.reseau.cdsdpadsir.agora.entity.ClasseDobjet;

@Repository
public interface FiliereObjetRepository extends JpaRepository<AssociationClasseDobjetFiliere, Long> {

	List<AssociationClasseDobjetFiliere> findByClasseDobjet(ClasseDobjet objet);

}
