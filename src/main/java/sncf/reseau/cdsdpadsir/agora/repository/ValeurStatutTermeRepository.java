package sncf.reseau.cdsdpadsir.agora.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.ValeurStatutTerme;

public interface ValeurStatutTermeRepository extends JpaRepository<ValeurStatutTerme, Long> {
	
	Optional<ValeurStatutTerme> findByLibelle(String libelle);

}
