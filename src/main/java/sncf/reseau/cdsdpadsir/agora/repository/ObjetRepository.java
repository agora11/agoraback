package sncf.reseau.cdsdpadsir.agora.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.ClasseDobjet;

@Repository
public interface ObjetRepository extends JpaRepository<ClasseDobjet, Long>, JpaSpecificationExecutor<ClasseDobjet> {

	Optional<ClasseDobjet> findByIdClasseDobjet(Long id);

	@Query(value = "select * from {h-schema}classe_dobjet cd \r\n"
			+ "inner join {h-schema}association_classe_dobjet_filiere acdf on cd.id_classe_dobjet = acdf.id_classe_dobjet \r\n"
			+ "inner join {h-schema}filiere f on  acdf.id_filiere = f.id_filiere \r\n"
			+ "where cd.date_fin_activite = ?2 and upper(cd.libelle) = upper(?1) and f.nom = ?3 limit 1;", nativeQuery = true)
	Optional<ClasseDobjet> findFirstByLibelleAndDateFinActiviteByFiliere(String libelle, LocalDateTime futureDate,
			String filiere);

	@Query(value = "select * from {h-schema}classe_dobjet cd \r\n"
			+ "inner join {h-schema}type_element_modele_objet type on cd.id_type_element_modele_objet = type.id_type_element_modele_objet where cd.date_fin_activite = ?1", nativeQuery = true)
	Page<ClasseDobjet> findObjetsFonctionnels(LocalDateTime future, Pageable pageable);

	@Query(value = "select * from {h-schema}classe_dobjet cd \r\n"
			+ "inner join {h-schema}type_element_modele_objet type on cd.id_type_element_modele_objet = type.id_type_element_modele_objet where cd.date_fin_activite = ?1", nativeQuery = true)
	List<ClasseDobjet> findObjetsFonctionnels(LocalDateTime future);

	Optional<ClasseDobjet> findFirstByLibelleIgnoreCaseAndDateFinActivite(String libelle, LocalDateTime futureDate);

	Optional<ClasseDobjet> findTopByIdOrderByIdClasseDobjetDesc(String id);

}
