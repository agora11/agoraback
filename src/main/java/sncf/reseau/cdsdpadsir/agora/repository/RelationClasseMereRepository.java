package sncf.reseau.cdsdpadsir.agora.repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.RelationClasseMere;

@Repository
public interface RelationClasseMereRepository  extends JpaRepository<RelationClasseMere, Long>  {
	
	
	@Query(value = "SELECT * FROM {h-schema}relation_classe_mere r WHERE  id_externe=:idExterne  order by date_creation" , nativeQuery = true)
	public List<RelationClasseMere> findByIdExterne(@Param("idExterne") String idExterne) ;
	
	@Query(value = "SELECT * FROM {h-schema}relation_classe_mere r WHERE  id_externe=:idExterne and version=:version and revision_fonctionnel=:revisionFonctionnel order by revision_fonctionnel desc " , nativeQuery = true)
	public List<RelationClasseMere> findByIdExterneVersionRevisionFonctionnel(
			@Param("idExterne") String idExterne ,
			@Param("version") String version , 
			@Param("revisionFonctionnel") Timestamp revisionFonctionnel) ;

	@Query(value = "SELECT * FROM {h-schema}relation_classe_mere r WHERE  id_externe=:idExterne order by revision_fonctionnel desc limit 1 " , nativeQuery = true)
	public List<RelationClasseMere> findByIdExterneAndMaxRevisionFonctionnelle(
			@Param("idExterne") String idExterne ) ;
	void deleteByIdIn(List<String> ids);
	Optional<RelationClasseMere> findTopByIdOrderByIdRelationClasseMereDesc(String id);
	List<RelationClasseMere> findAllByClasseDobjeMere_IdClasseDobjet(Long idClasseDobjet);
	List<RelationClasseMere> findAllByClasseDobjetFille_IdClasseDobjet(Long idClasseDobjet);

}
