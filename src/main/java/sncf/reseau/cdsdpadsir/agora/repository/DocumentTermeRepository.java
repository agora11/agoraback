package sncf.reseau.cdsdpadsir.agora.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.AssociationTermeDocument;
import sncf.reseau.cdsdpadsir.agora.entity.Document;
import sncf.reseau.cdsdpadsir.agora.entity.Terme;

@Repository
public interface DocumentTermeRepository extends JpaRepository<AssociationTermeDocument, Long> {

	List<AssociationTermeDocument> findByTerme(Terme terme);

	List<AssociationTermeDocument> findByTermeAndDocument(Terme terme, Document document);

}
