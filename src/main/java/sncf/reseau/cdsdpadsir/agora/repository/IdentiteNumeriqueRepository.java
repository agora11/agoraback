package sncf.reseau.cdsdpadsir.agora.repository;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import sncf.reseau.cdsdpadsir.agora.entity.IdentiteNumerique;
import sncf.reseau.cdsdpadsir.agora.entity.Utilisateur;

public interface IdentiteNumeriqueRepository
		extends JpaRepository<IdentiteNumerique, Long>, JpaSpecificationExecutor<IdentiteNumerique> {

	Optional<IdentiteNumerique> findByUtilisateur(Utilisateur map);

	Optional<IdentiteNumerique> findByUtilisateurAndDateFinActivite(Utilisateur map, LocalDateTime futureDate);

	Optional<IdentiteNumerique> findByIdIdentiteNumerique(Long idUtilisateur);

}
