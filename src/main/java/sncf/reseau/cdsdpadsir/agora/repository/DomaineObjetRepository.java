package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sncf.reseau.cdsdpadsir.agora.entity.AssociationClasseDobjetDomaine;

public interface DomaineObjetRepository extends JpaRepository<AssociationClasseDobjetDomaine, Long> {
}
