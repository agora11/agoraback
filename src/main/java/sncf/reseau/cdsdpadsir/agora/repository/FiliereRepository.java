package sncf.reseau.cdsdpadsir.agora.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.Filiere;

@Repository
public interface FiliereRepository extends JpaRepository<Filiere, Long>, JpaSpecificationExecutor<Filiere> {

	Optional<Filiere> findByIdFiliere(Long idFiliere);
	Optional<Filiere> findTopByNomOrderByDateFinActiviteDesc(String nom);
	Optional<Filiere> findTopByNomOrderByNom(String nom);
	Optional<Filiere> findTopByNomOrderByIdFiliereDesc(String nom);

}
