package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.UsageMetier;

public interface UsageMetierRepository extends JpaRepository<UsageMetier, Long> {

}
