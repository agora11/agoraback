package sncf.reseau.cdsdpadsir.agora.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.Gisement;

@Repository
public interface GisementRepository extends JpaRepository<Gisement, Long>, JpaSpecificationExecutor<Gisement> {

	Optional<Gisement> findByIdGisement(long idGisement);

}
