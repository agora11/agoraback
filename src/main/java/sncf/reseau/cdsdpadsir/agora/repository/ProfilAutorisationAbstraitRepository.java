package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.ProfilAutorisationAbstrait;

public interface ProfilAutorisationAbstraitRepository extends JpaRepository<ProfilAutorisationAbstrait, Long> {

}
