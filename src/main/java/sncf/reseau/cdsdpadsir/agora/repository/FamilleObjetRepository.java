package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.AssociationClasseDobjetFamille;

@Repository
public interface FamilleObjetRepository extends JpaRepository<AssociationClasseDobjetFamille, Long> {

}
