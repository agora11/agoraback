package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.ProfilAutorisation;

public interface ProfilAutorisationRepository extends JpaRepository<ProfilAutorisation, Long> {

}
