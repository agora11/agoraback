package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.PolitiqueQualite;

public interface PolitiqueQualiteRepository extends JpaRepository<PolitiqueQualite, Long> {

}
