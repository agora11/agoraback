package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.DefinitionClasseDobjet;

@Repository
public interface DefinitionRepository extends JpaRepository<DefinitionClasseDobjet, Long>{

}
