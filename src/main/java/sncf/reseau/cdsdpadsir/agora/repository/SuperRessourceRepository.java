package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.SuperRessource;

public interface SuperRessourceRepository extends JpaRepository<SuperRessource, Long> {

}
