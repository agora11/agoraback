package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import sncf.reseau.cdsdpadsir.agora.entity.UtilisateurRole;

public interface UtilisateurRoleRepository extends JpaRepository<UtilisateurRole, Long>, JpaSpecificationExecutor<UtilisateurRole> {

}
