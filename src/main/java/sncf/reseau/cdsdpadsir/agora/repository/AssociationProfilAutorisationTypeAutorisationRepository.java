package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.AssociationProfilAutorisationTypeAutorisation;

public interface AssociationProfilAutorisationTypeAutorisationRepository extends JpaRepository<AssociationProfilAutorisationTypeAutorisation , Long> {

}
