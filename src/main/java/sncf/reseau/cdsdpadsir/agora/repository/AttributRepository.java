package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.AttributClasse;

@Repository
public interface AttributRepository  extends JpaRepository<AttributClasse, Long> {

}
