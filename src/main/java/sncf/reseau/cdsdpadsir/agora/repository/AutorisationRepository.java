package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.Autorisation;

public interface AutorisationRepository extends JpaRepository<Autorisation, Long> {

}
