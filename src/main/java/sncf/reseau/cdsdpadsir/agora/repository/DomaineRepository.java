package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import sncf.reseau.cdsdpadsir.agora.entity.Domaine;

public interface DomaineRepository extends JpaRepository<Domaine, Long>, JpaSpecificationExecutor<Domaine> {
}
