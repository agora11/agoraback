package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.AssociationClasseDobjetSousFamille;

public interface SousFamilleObjetRepository extends JpaRepository<AssociationClasseDobjetSousFamille, Long> {

}
