package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.TauxQualiteMesure;

public interface TauxQualiteMesureRepository extends JpaRepository<TauxQualiteMesure, Long> {

}
