package sncf.reseau.cdsdpadsir.agora.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sncf.reseau.cdsdpadsir.agora.entity.AttributClasse;

public interface AttributClasseRepository extends JpaRepository<AttributClasse, Long>{
	
	@Query(value = "SELECT * FROM {h-schema}attribut_classe a WHERE  id_externe=:idExterne and version=:version and revision_fonctionnel=:revisionFonctionnel order by revision_fonctionnel desc " , nativeQuery = true)
	public List<AttributClasse> findByIdExterneVersionRevisionFonctionnel(
			@Param("idExterne") String idExterne ,
			@Param("version") String version , 
			@Param("revisionFonctionnel") Timestamp revisionFonctionnel) ;
	
	@Query(value = "SELECT * FROM {h-schema}attribut_classe a WHERE  id_externe=:idExterne order by revision_fonctionnel desc limit 1 " , nativeQuery = true)
	public List<AttributClasse> findByIdExterneAndMaxRevisionFonctionnelle(@Param("idExterne") String idExterne) ;

	@Query(value = "SELECT * FROM {h-schema}attribut_classe a WHERE  id_externe=:idExterne  " , nativeQuery = true)
	public List<AttributClasse> findByIdExterne(@Param("idExterne") String idExterne);

}
