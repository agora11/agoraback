package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.Acteur;

public interface ActeurRepository  extends JpaRepository<Acteur, Long> {

}
