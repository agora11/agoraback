package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.CritereQualite;

public interface CritereQualiteRepository extends JpaRepository<CritereQualite, Long> {

}
