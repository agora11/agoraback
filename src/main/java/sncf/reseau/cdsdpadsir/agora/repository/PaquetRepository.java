package sncf.reseau.cdsdpadsir.agora.repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.Paquet;

@Repository
public interface PaquetRepository extends JpaRepository<Paquet, Long>  {
	
	@Query(value = "SELECT * FROM {h-schema}paquet p WHERE  id_externe=:idExterne order by version desc " , nativeQuery = true)
	public List<Paquet> findByIdExterne(@Param("idExterne") String idExterne) ;
	
	@Query(value = "SELECT * FROM {h-schema}paquet p WHERE  id_externe=:idExterne order by revision_fonctionnel desc limit 1 " , nativeQuery = true)
	public List<Paquet> findByIdExterneAndMaxRevisionFonctionnelle(@Param("idExterne") String idExterne) ;
	
	@Query(value = "SELECT * FROM {h-schema}paquet p WHERE  id_externe=:idExterne and version=:version and revision_fonctionnel=:revisionFonctionnel order by version desc " , nativeQuery = true)
	public List<Paquet> findByIdExterneVersionRevisionFonctionnel(
			@Param("idExterne") String idExterne ,
			@Param("version") String version , 
			@Param("revisionFonctionnel") Timestamp revisionFonctionnel) ;
	
	@Query(value = "SELECT * FROM {h-schema}paquet p WHERE  id_externe = :idExterne and version = :version" , nativeQuery = true)
	public List<Paquet> findByIdExterneAndVersion(@Param("idExterne") String idExterne , @Param("version")String version) ;
	
	@Query(value = "SELECT * FROM {h-schema}paquet p WHERE  id_externe = :idExterne and version = :version and nom = :nom" , nativeQuery = true)
	public Optional<Paquet> findByIdExterneAndVersionAndNom(@Param("idExterne") String idExterne , @Param("version")String version ,  @Param("nom")String nom) ;
}
