package sncf.reseau.cdsdpadsir.agora.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.StatutTerme;

public interface StatutTermeRepository extends JpaRepository<StatutTerme, Long> {

	List<StatutTerme> findById(String id);

}
