package sncf.reseau.cdsdpadsir.agora.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.TypeAttributClasse;

@Repository
public interface TypeAttributClasseRepository  extends JpaRepository<TypeAttributClasse, Long>{
	
	@Query(value = "SELECT * FROM {h-schema}type_attribut_classe tdc WHERE type = :type " , nativeQuery = true)
	public Optional<TypeAttributClasse> getByType(String type);

}
