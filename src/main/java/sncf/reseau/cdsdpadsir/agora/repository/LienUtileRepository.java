package sncf.reseau.cdsdpadsir.agora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sncf.reseau.cdsdpadsir.agora.entity.LienUtile;

public interface LienUtileRepository extends JpaRepository<LienUtile, Long> {

}
