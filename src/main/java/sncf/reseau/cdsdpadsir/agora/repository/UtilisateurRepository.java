package sncf.reseau.cdsdpadsir.agora.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import sncf.reseau.cdsdpadsir.agora.entity.Utilisateur;

@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long>, JpaSpecificationExecutor<Utilisateur> {

	List<Utilisateur> findById(String id);

	Optional<Utilisateur> findByCpAndDateFinActivite(String cp, LocalDateTime date);

	Optional<Utilisateur> findByNomAndPrenomAndDateFinActivite(String nom, String prenom, LocalDateTime date);

	Optional<Utilisateur> findByNomIgnoreCaseAndPrenomIgnoreCaseAndDateFinActivite(String nomStatut, String prenomStatut,
			LocalDateTime futureDate);

}
