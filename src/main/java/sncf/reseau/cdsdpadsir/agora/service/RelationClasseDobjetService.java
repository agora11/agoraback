package sncf.reseau.cdsdpadsir.agora.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sncf.reseau.cdsdpadsir.agora.entity.RelationClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.model.ClasseDobjetDto;
import sncf.reseau.cdsdpadsir.agora.model.RelationClasseDobjetDto;
import sncf.reseau.cdsdpadsir.agora.repository.RelationClasseDobjetRepository;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;
import sncf.reseau.cdsdpadsir.agora.xmi.util.DistinctByKey;

@Service
public class RelationClasseDobjetService {

	Logger log = LoggerFactory.getLogger(RelationClasseDobjetService.class);
	private RelationClasseDobjetRepository relationClasseDobjetRepository;
	private ClasseDobjetService classeDobjetService;
	private ModelMapper modelMapper;

	@Autowired
	public RelationClasseDobjetService(RelationClasseDobjetRepository relationClasseDobjetRepository,
			ClasseDobjetService classeDobjetService, ModelMapper modelMapper) {

		this.relationClasseDobjetRepository = relationClasseDobjetRepository;
		this.classeDobjetService = classeDobjetService;
		this.modelMapper = modelMapper;
		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	public List<RelationClasseDobjetDto> addAll(List<RelationClasseDobjetDto> relationsDto) {

		return relationsDto.stream().filter(new DistinctByKey<>(RelationClasseDobjetDto::getIdExterne))
				.map(relationDto -> {

					if (null != relationDto.getClasseDobjetSource()) {
						List<ClasseDobjetDto> classesDobjetSourceDto = classeDobjetService
								.getByIdExterneAndMaxRevisionFonctionnelle(
										relationDto.getClasseDobjetSource().getIdExterne());
						if (null != classesDobjetSourceDto && !classesDobjetSourceDto.isEmpty()) {
							relationDto.setClasseDobjetSource(classesDobjetSourceDto.get(0));
						}
					}

					if (null != relationDto.getClasseDobjetDestination()) {
						List<ClasseDobjetDto> classesDobjetDestinationDto = classeDobjetService
								.getByIdExterneAndMaxRevisionFonctionnelle(
										relationDto.getClasseDobjetDestination().getIdExterne());
						if (null != classesDobjetDestinationDto && !classesDobjetDestinationDto.isEmpty()) {
							relationDto.setClasseDobjetDestination(classesDobjetDestinationDto.get(0));
						}
					}
					return add(relationDto);

				}).collect(Collectors.toList());
	}

	@Transactional
	public RelationClasseDobjetDto add(RelationClasseDobjetDto relationDto) {

		// get id agora id exist
		List<RelationClasseDobjet> relationsClasseDobjet = relationClasseDobjetRepository
				.findByIdExterneAndMaxRevisionFonctionnelle(relationDto.getIdExterne());

		if (null != relationsClasseDobjet && !relationsClasseDobjet.isEmpty()) {
			ArianeHelper.updateEntityDetails(relationsClasseDobjet.get(0), relationDto);
			relationsClasseDobjet.get(0).setDateFinActivite(LocalDateTime.now());
			relationClasseDobjetRepository.save(relationsClasseDobjet.get(0));
		} else {
			ArianeHelper.setNewEntityDetails(relationDto);
		}
		relationDto.setRevisionFonctionnel(Timestamp.valueOf(LocalDateTime.now()));

		RelationClasseDobjet relationClasseDobjet = relationClasseDobjetRepository
				.save(modelMapper.map(relationDto, RelationClasseDobjet.class));
		return relationClasseDobjet != null ? relationClasseDobjet.mapToDto() : null;

	}
}
