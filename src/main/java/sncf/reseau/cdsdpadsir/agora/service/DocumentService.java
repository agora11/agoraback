package sncf.reseau.cdsdpadsir.agora.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import sncf.reseau.cdsdpadsir.agora.entity.Document;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.model.DocumentDto;
import sncf.reseau.cdsdpadsir.agora.repository.DocumentRepository;

@Service
public class DocumentService {
	
	private DocumentRepository documentRepository;

	private ModelMapper modelMapper;
	
	
	
	@Autowired
	public DocumentService(DocumentRepository documentRepository, ModelMapper modelMapper) {
		super();
		this.documentRepository = documentRepository;
		this.modelMapper = modelMapper;
		
		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	public PagingResponse<DocumentDto> findAll(Specification<Document> documentSpec, Sort sort, @Valid BigDecimal page,
			@Valid BigDecimal size) {

		if (isRequestPaged(page, size))
			return findAll(documentSpec, buildPageRequest(page, size, sort));

		List<DocumentDto> entities = findAll(documentSpec, sort);
		return new PagingResponse<>((long) entities.size(), 0L, 0L, 0L, 0L, entities);
	}

	private PagingResponse<DocumentDto> findAll(Specification<Document> spec, Pageable pageable) {
		Page<Document> page = documentRepository.findAll(spec, pageable);
		List<Document> content = page.getContent();
		return new PagingResponse<>(page.getTotalElements(), (long) page.getNumber(), (long) page.getNumberOfElements(),
				pageable.getOffset(), (long) page.getTotalPages(),
				content.stream().map(mapper -> modelMapper.map(mapper, DocumentDto.class)).collect(Collectors.toList()));
	}

	public List<DocumentDto> findAll(Specification<Document> spec, Sort sort) {
		return documentRepository.findAll(spec, sort).stream().map(mapper -> modelMapper.map(mapper, DocumentDto.class))
				.collect(Collectors.toList());
	}

	private boolean isRequestPaged(BigDecimal page, BigDecimal size) {
		return (page != null && size != null);
	}

	private Pageable buildPageRequest(BigDecimal page, BigDecimal size, Sort sort) {
		return PageRequest.of(page.intValue(), size.intValue(), sort);
	}
}
