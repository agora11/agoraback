package sncf.reseau.cdsdpadsir.agora.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import sncf.reseau.cdsdpadsir.agora.entity.Etiquette;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.model.EtiquetteDto;
import sncf.reseau.cdsdpadsir.agora.repository.EtiquetteRepository;

@Service
public class TagService {
	
	private EtiquetteRepository etiquetteRepository;

	private ModelMapper modelMapper;
	
	
	
	@Autowired
	public TagService(EtiquetteRepository etiquetteRepository, ModelMapper modelMapper) {
		super();
		this.etiquetteRepository = etiquetteRepository;
		this.modelMapper = modelMapper;
		
		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	public PagingResponse<EtiquetteDto> findAll(Specification<Etiquette> objetSpec, Sort sort, @Valid BigDecimal page,
			@Valid BigDecimal size) {

		if (isRequestPaged(page, size))
			return findAll(objetSpec, buildPageRequest(page, size, sort));

		List<EtiquetteDto> entities = findAll(objetSpec, sort);
		return new PagingResponse<>((long) entities.size(), 0L, 0L, 0L, 0L, entities);
	}

	private PagingResponse<EtiquetteDto> findAll(Specification<Etiquette> spec, Pageable pageable) {
		Page<Etiquette> page = etiquetteRepository.findAll(spec, pageable);
		List<Etiquette> content = page.getContent();
		return new PagingResponse<>(page.getTotalElements(), (long) page.getNumber(), (long) page.getNumberOfElements(),
				pageable.getOffset(), (long) page.getTotalPages(),
				content.stream().map(mapper -> modelMapper.map(mapper, EtiquetteDto.class)).collect(Collectors.toList()));
	}

	public List<EtiquetteDto> findAll(Specification<Etiquette> spec, Sort sort) {
		return etiquetteRepository.findAll(spec, sort).stream().map(mapper -> modelMapper.map(mapper, EtiquetteDto.class))
				.collect(Collectors.toList());
	}

	private boolean isRequestPaged(BigDecimal page, BigDecimal size) {
		return (page != null && size != null);
	}

	private Pageable buildPageRequest(BigDecimal page, BigDecimal size, Sort sort) {
		return PageRequest.of(page.intValue(), size.intValue(), sort);
	}	

}
