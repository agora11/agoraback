package sncf.reseau.cdsdpadsir.agora.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sncf.reseau.cdsdpadsir.agora.entity.TypeElementModeleObjet;
import sncf.reseau.cdsdpadsir.agora.model.TypeElementModeleObjetDto;
import sncf.reseau.cdsdpadsir.agora.repository.TypeElementModeleObjetRepository;

@Service
public class TypeElementModeleObjetService {

	private TypeElementModeleObjetRepository typeElementModeleObjetRepository;
	private ModelMapper modelMapper;
	
	@Autowired
	public TypeElementModeleObjetService(
			TypeElementModeleObjetRepository typeElementModeleObjetRepository,
			ModelMapper modelMapper
			) {
		this.typeElementModeleObjetRepository = typeElementModeleObjetRepository ;
		this.modelMapper = modelMapper ;
		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}
	
	public TypeElementModeleObjetDto getById(Long id ) {
		return modelMapper.map(typeElementModeleObjetRepository.getOne(id), TypeElementModeleObjetDto.class) ;
	}
	
	public TypeElementModeleObjetDto getByLibelle(String libelle ) {
		Optional<TypeElementModeleObjet>  type = typeElementModeleObjetRepository.findByLibelle(libelle) ;	
		if(!type.isPresent()) {
			return null ;
		}
		return  modelMapper.map(type.get(), TypeElementModeleObjetDto.class) ;
																   
	} 
	
	@Transactional
	public TypeElementModeleObjetDto add(TypeElementModeleObjetDto type) {
		TypeElementModeleObjet typeEntiry = modelMapper.map(type, TypeElementModeleObjet.class) ;
		return modelMapper.map(typeElementModeleObjetRepository.save(typeEntiry) , TypeElementModeleObjetDto.class) ;
		 	
	}
	
	
}
