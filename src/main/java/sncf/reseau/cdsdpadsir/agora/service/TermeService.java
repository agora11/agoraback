package sncf.reseau.cdsdpadsir.agora.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sncf.reseau.cdsdpadsir.agora.entity.AssociationTermeClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.entity.AssociationTermeDocument;
import sncf.reseau.cdsdpadsir.agora.entity.AssociationTermeFiliere;
import sncf.reseau.cdsdpadsir.agora.entity.ClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.entity.DefinitionTerme;
import sncf.reseau.cdsdpadsir.agora.entity.Document;
import sncf.reseau.cdsdpadsir.agora.entity.Filiere;
import sncf.reseau.cdsdpadsir.agora.entity.IdentiteNumerique;
import sncf.reseau.cdsdpadsir.agora.entity.StatutTerme;
import sncf.reseau.cdsdpadsir.agora.entity.Terme;
import sncf.reseau.cdsdpadsir.agora.entity.Utilisateur;
import sncf.reseau.cdsdpadsir.agora.entity.ValeurStatutTerme;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;
import sncf.reseau.cdsdpadsir.agora.model.*;
import sncf.reseau.cdsdpadsir.agora.repository.DefinitionTermeRepository;
import sncf.reseau.cdsdpadsir.agora.repository.DocumentRepository;
import sncf.reseau.cdsdpadsir.agora.repository.DocumentTermeRepository;
import sncf.reseau.cdsdpadsir.agora.repository.FiliereRepository;
import sncf.reseau.cdsdpadsir.agora.repository.FiliereTermeRepository;
import sncf.reseau.cdsdpadsir.agora.repository.IdentiteNumeriqueRepository;
import sncf.reseau.cdsdpadsir.agora.repository.StatutTermeRepository;
import sncf.reseau.cdsdpadsir.agora.repository.TermeClasseDobjetRepository;
import sncf.reseau.cdsdpadsir.agora.repository.TermeRepository;
import sncf.reseau.cdsdpadsir.agora.repository.TermeUtilisateurRepository;
import sncf.reseau.cdsdpadsir.agora.repository.ValeurStatutTermeRepository;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;

@Service
public class TermeService {

	private TermeRepository termeRepository;

	private FiliereTermeRepository filiereTermeRepository;

	private DocumentTermeRepository documentTermeRepository;

	private DocumentRepository documentRepository;

	private DefinitionTermeRepository definitionTermeRepository;

	private TermeUtilisateurRepository termeUtilisateurRepository;

	private TermeClasseDobjetRepository termeClasseDobjetRepository;

	private IdentiteNumeriqueRepository identiteNumeriqueRepository;

	private StatutTermeRepository statutTermeRepository;

	private ValeurStatutTermeRepository valeurStatutTermeRepository;

	private FiliereRepository filiereRepository;

	private SharedService sharedService;

	private ModelMapper modelMapper;

	@Autowired
	public TermeService(TermeRepository termeRepository, FiliereTermeRepository filiereTermeRepository,
			DocumentTermeRepository documentTermeRepository, DocumentRepository documentRepository,
			DefinitionTermeRepository definitionTermeRepository, TermeUtilisateurRepository termeUtilisateurRepository,
			TermeClasseDobjetRepository termeClasseDobjetRepository, ModelMapper modelMapper,
			IdentiteNumeriqueRepository identiteNumeriqueRepository, StatutTermeRepository statutTermeRepository,
			ValeurStatutTermeRepository valeurStatutTermeRepository, SharedService sharedService,
			FiliereRepository filiereRepository) {
		super();
		this.termeRepository = termeRepository;
		this.filiereTermeRepository = filiereTermeRepository;
		this.documentTermeRepository = documentTermeRepository;
		this.documentRepository = documentRepository;
		this.definitionTermeRepository = definitionTermeRepository;
		this.termeUtilisateurRepository = termeUtilisateurRepository;
		this.termeClasseDobjetRepository = termeClasseDobjetRepository;
		this.identiteNumeriqueRepository = identiteNumeriqueRepository;
		this.statutTermeRepository = statutTermeRepository;
		this.valeurStatutTermeRepository = valeurStatutTermeRepository;
		this.filiereRepository = filiereRepository;
		this.modelMapper = modelMapper;
		this.sharedService = sharedService;

		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	public PagingResponse<TermeDto> findAll(Specification<Terme> termeSpec, Sort sort, @Valid BigDecimal page,
			@Valid BigDecimal size) {

		if (isRequestPaged(page, size))
			return findAll(termeSpec, buildPageRequest(page, size, sort));

		List<TermeDto> entities = findAll(termeSpec, sort);
		return new PagingResponse<>((long) entities.size(), 0L, 0L, 0L, 0L, entities);
	}

	private PagingResponse<TermeDto> findAll(Specification<Terme> spec, Pageable pageable) {
		Page<Terme> page = termeRepository.findAll(spec, pageable);
		List<Terme> content = page.getContent();
		return new PagingResponse<>(page.getTotalElements(), (long) page.getNumber(), (long) page.getNumberOfElements(),
				pageable.getOffset(), (long) page.getTotalPages(),
				content.stream().map(this::mapTerme).collect(Collectors.toList()));
	}

	public TermeDto mapTerme(Terme mapper) {
		TermeDto termeDto = modelMapper.map(mapper, TermeDto.class);
		termeDto.setFilieres(new HashSet<>());
		termeDto.setDocuments(new HashSet<>());
		termeDto.setDefinitions(new HashSet<>());
		termeDto.setObjets(new HashSet<>());

		if (mapper.getProprietaire() != null) {
			termeDto.setProprietaire(modelMapper.map(mapper.getProprietaire().getUtilisateur(), UtilisateurDto.class));
		}

		if (mapper.getAssociationTermeFilieres() != null)
			mapper.getAssociationTermeFilieres().forEach(item -> {
				if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
					termeDto.getFilieres().add(modelMapper.map(item.getFiliere(), FiliereDto.class));
				}
			});

		if (mapper.getAssociationTermeDocuments() != null)
			mapper.getAssociationTermeDocuments().forEach(item -> {
				if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
					termeDto.getDocuments().add(modelMapper.map(item.getDocument(), DocumentDto.class));
				}
			});

		if (mapper.getDefinitionTermes() != null)
			mapper.getDefinitionTermes().forEach(item -> {
				if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
					termeDto.getDefinitions().add(modelMapper.map(item, DefinitionDto.class));
				}
			});

		if (mapper.getAssociationTermeUtilisateurs() != null)
			mapper.getAssociationTermeUtilisateurs().forEach(item -> {
				if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
					termeDto.getUtilisateurs()
							.add(modelMapper.map(item.getIdentiteNumerique().getUtilisateur(), UtilisateurDto.class));
				}
			});

		if (mapper.getAssociationTermeClasseDobjets() != null)
			mapper.getAssociationTermeClasseDobjets().forEach(item -> {
				if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
					termeDto.getObjets().add(sharedService.mapObjet(item.getClasseDobjet()));
				}
			});

		for (StatutTerme item : mapper.getStatutTermes()) {
			StatutTermeDto statut = modelMapper.map(item, StatutTermeDto.class);
			statut.setValideur(modelMapper.map(item.getValideur().getUtilisateur(), UtilisateurDto.class));
			statut.setSuppleant(modelMapper.map(item.getSuppleant().getUtilisateur(), UtilisateurDto.class));
			statut.setLibelle(item.getValeurStatutTerme().getLibelle());
			termeDto.setStatut(statut);
		}

		return termeDto;
	}

	public List<TermeDto> findAll(Specification<Terme> spec, Sort sort) {
		return termeRepository.findAll(spec, sort).stream().map(this::mapTerme).collect(Collectors.toList());
	}

	public TermeDto findByIdTerme(Long idTerme) {
		Optional<Terme> entity = termeRepository.findByIdTerme(idTerme);
		if (entity.isPresent()) {
			return entity.map(this::mapTerme).get();
		} else {
			throw new NoSuchElementException();
		}
	}

	private boolean isRequestPaged(BigDecimal page, BigDecimal size) {
		return (page != null && size != null);
	}

	private Pageable buildPageRequest(BigDecimal page, BigDecimal size, Sort sort) {
		return PageRequest.of(page.intValue(), size.intValue(), sort);
	}

	@Transactional
	public TermeDto add(@Valid TermeDto termeDto) {
		ArianeHelper.setNewEntityDetails(termeDto);
		Terme model = modelMapper.map(termeDto, Terme.class);
		model.setProprietaire(identiteNumeriqueRepository
				.findByUtilisateur(modelMapper.map(termeDto.getProprietaire(), Utilisateur.class)).get());
		Terme terme = termeRepository.save(model);
		updateJoins(termeDto, model, terme);
		return mapTerme(model);
	}

	private void updateJoins(TermeDto termeDto, Terme model, Terme terme) {

		boolean update = false;
		/*
		 * avec l'ajout ces associations ne sont pas encore initialisées donc on peut
		 * les utilisées pour tester si c'est un ajout ou modification.
		 */
		if (model.getAssociationTermeFilieres() != null || model.getAssociationTermeDocuments() != null
				|| model.getDefinitionTermes() != null) {
			update = true;
		}
		final boolean flag = update;

		model.setStatutTermes(new HashSet<>());
		StatutTerme statut = new StatutTerme();
		Optional<IdentiteNumerique> idValideur = identiteNumeriqueRepository
				.findByUtilisateur(modelMapper.map(termeDto.getStatut().getValideur(), Utilisateur.class));
		Optional<IdentiteNumerique> idSuppleant = identiteNumeriqueRepository
				.findByUtilisateur(modelMapper.map(termeDto.getStatut().getSuppleant(), Utilisateur.class));

		ariane(termeDto, idValideur.get(), flag);
		ariane(termeDto, idSuppleant.get(), flag);

		ValeurStatutTerme valeurStatutTerme = valeurStatutTermeRepository
				.findByLibelle(termeDto.getStatut().getLibelle()).get();
		ariane(termeDto, valeurStatutTerme, flag);

		statut.setValideur(idValideur.get());
		statut.setSuppleant(idSuppleant.get());

		statut.setNom(termeDto.getStatut().getNom());
		statut.setPrenom(termeDto.getStatut().getPrenom());
		statut.setCommentaire(termeDto.getStatut().getCommentaire());
		statut.setValeurStatutTerme(valeurStatutTerme);
		statut.setTerme(model);
		ariane(termeDto, statut, flag);

		model.getStatutTermes().add(statutTermeRepository.save(statut));

		model.setAssociationTermeFilieres(new HashSet<>());
		termeDto.getFilieres().forEach(item -> {
			if (item.getIdFiliere() != null) {
				AssociationTermeFiliere filiereTerme = new AssociationTermeFiliere();
				filiereTerme.setTerme(terme);
				filiereTerme.setFiliere(modelMapper.map(item, Filiere.class));
				ariane(termeDto, filiereTerme, flag);
				filiereTermeRepository.save(filiereTerme);
				model.getAssociationTermeFilieres().add(filiereTerme);
			}
		});

		model.setAssociationTermeDocuments(new HashSet<>());
		termeDto.getDocuments().forEach(item -> {
			AssociationTermeDocument documentTerme = new AssociationTermeDocument();
			documentTerme.setTerme(terme);
			Document document = modelMapper.map(item, Document.class);
			ariane(termeDto, document, flag);
			document = documentRepository.save(document);
			documentTerme.setDocument(document);
			ariane(termeDto, documentTerme, flag);
			documentTermeRepository.save(documentTerme);
			model.getAssociationTermeDocuments().add(documentTerme);
		});

		model.setDefinitionTermes(new HashSet<>());
		termeDto.getDefinitions().forEach(item -> {
			DefinitionTerme definitionTerme = new DefinitionTerme();
			definitionTerme.setTerme(terme);
			definitionTerme.setDefinition(item.getDefinition());
			ariane(termeDto, definitionTerme, flag);
			definitionTermeRepository.save(definitionTerme);
			model.getDefinitionTermes().add(definitionTerme);
		});

		/*
		 * model.setAssociationTermeUtilisateurs(new HashSet<>());
		 * termeDto.getUtilisateurs().forEach(item -> { AssociationTermeUtilisateur
		 * utilisateurTerme = new AssociationTermeUtilisateur();
		 * utilisateurTerme.setTerme(terme);
		 * utilisateurTerme.setUtilisateur(modelMapper.map(item, Utilisateur.class));
		 * ariane(termeDto, utilisateurTerme, flag);
		 * termeUtilisateurRepository.save(utilisateurTerme);
		 * model.getAssociationTermeUtilisateurs().add(utilisateurTerme); });
		 */

		model.setAssociationTermeClasseDobjets(new HashSet<>());
		termeDto.getObjets().forEach(item -> {
			AssociationTermeClasseDobjet objetTerme = new AssociationTermeClasseDobjet();
			objetTerme.setTerme(terme);
			objetTerme.setClasseDobjet(modelMapper.map(item, ClasseDobjet.class));
			ariane(termeDto, objetTerme, flag);
			termeClasseDobjetRepository.save(objetTerme);
			model.getAssociationTermeClasseDobjets().add(objetTerme);
		});
	}

	@Transactional
	public TermeDto update(@Valid TermeDto termeDto) throws ConflictException {
		Optional<Terme> entity = termeRepository.findByIdTerme(termeDto.getIdTerme());
		Filiere filiere = filiereRepository.findById(termeDto.getFilieres().iterator().next().getIdFiliere()).get();
		Optional<Terme> termeByLibelle = termeRepository.findFirstByLibelleAndDateFinActiviteByFiliere(
				termeDto.getLibelle(), ArianeHelper.FUTURE_DATE, filiere.getNom());

		if (entity.isPresent()) {
			if (termeByLibelle.isPresent() && !termeByLibelle.get().getId().equals(entity.get().getId())) {
				throw new ConflictException();
			}
			Terme terme = new Terme();
			modelMapper.map(entity.get(), terme);
			terme.setIdTerme(null);
			terme.setLibelle(termeDto.getLibelle());
			terme.setCommentaire(termeDto.getCommentaire());
			terme.setAbreviation(termeDto.getAbreviation());
			terme.setProprietaire(identiteNumeriqueRepository
					.findByUtilisateur(modelMapper.map(termeDto.getProprietaire(), Utilisateur.class)).get());
			ArianeHelper.updateEntityDetails(termeDto, terme);
			terme = termeRepository.save(terme);
			updateJoins(termeDto, terme, terme);
			updateEndDate(entity);

			return mapTerme(terme);
		} else {
			throw new NoSuchElementException();
		}

	}

	public boolean delete(BigInteger id) {
		Optional<Terme> entity = termeRepository.findByIdTerme(id.longValue());
		if (entity.isPresent()) {
			updateEndDate(entity);
			return true;
		} else {
			throw new NoSuchElementException();
		}
	}

	<T extends RessourceAbstraite> void ariane(T obj, T response, boolean flag) {
		if (!flag) {
			ArianeHelper.setNewEntityDetails(response);
		} else {
			ArianeHelper.updateEntityDetails(obj, response);
		}
	}

	private void updateEndDate(Optional<Terme> entity) {

		entity.get().getAssociationTermeFilieres().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
		filiereTermeRepository.saveAll(entity.get().getAssociationTermeFilieres());

		entity.get().getAssociationTermeUtilisateurs().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
		termeUtilisateurRepository.saveAll(entity.get().getAssociationTermeUtilisateurs());

		entity.get().getDefinitionTermes().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
		definitionTermeRepository.saveAll(entity.get().getDefinitionTermes());

		entity.get().getAssociationTermeDocuments().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
		documentTermeRepository.saveAll(entity.get().getAssociationTermeDocuments());

		entity.get().getAssociationTermeClasseDobjets().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
		termeClasseDobjetRepository.saveAll(entity.get().getAssociationTermeClasseDobjets());

		entity.get().getStatutTermes().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
		statutTermeRepository.saveAll(entity.get().getStatutTermes());

		entity.get().setDateFinActivite(LocalDateTime.now());
		termeRepository.save(entity.get());

	}

	public boolean checkUniquenessByFiliere(@Valid TermeDto termeDto) {

		if (termeDto.getFilieres().isEmpty() || termeDto.getFilieres().iterator().next().getIdFiliere() == null) {
			return false;
		}
		Optional<Filiere> filiere = filiereRepository.findById(termeDto.getFilieres().iterator().next().getIdFiliere());
		return filiere.isPresent()
				&& termeRepository.findFirstByLibelleAndDateFinActiviteByFiliere(termeDto.getLibelle(),
						ArianeHelper.FUTURE_DATE, filiere.get().getNom()).isPresent();
	}

	public boolean checkUniqueness(@Valid TermeDto termeDto) {
		return termeRepository
				.findFirstByLibelleIgnoreCaseAndDateFinActivite(termeDto.getLibelle(), ArianeHelper.FUTURE_DATE)
				.isPresent();
	}

	public boolean checkUniquenessForEdit(TermeDto termeDto) {
		Optional<Terme> entity = termeRepository.findByIdTerme(termeDto.getIdTerme());
		Optional<Terme> termeByLibelle = termeRepository
				.findFirstByLibelleIgnoreCaseAndDateFinActivite(termeDto.getLibelle(), ArianeHelper.FUTURE_DATE);

		return (entity.isPresent() && termeByLibelle.isPresent()
				&& !termeByLibelle.get().getId().equals(entity.get().getId()));

	}

	public List<StatutTermeDto> getStatuts(String id) {
		List<StatutTermeDto> list = statutTermeRepository.findById(id).stream().map(item -> {
			StatutTermeDto statut = modelMapper.map(item, StatutTermeDto.class);
			statut.setValideur(modelMapper.map(item.getValideur().getUtilisateur(), UtilisateurDto.class));
			statut.setSuppleant(modelMapper.map(item.getSuppleant().getUtilisateur(), UtilisateurDto.class));
			statut.setLibelle(item.getValeurStatutTerme().getLibelle());
			return statut;
		}).collect(Collectors.toList());

		Collections.sort(list, (o1, o2) -> o1.getDateFinActivite().compareTo(o2.getDateFinActivite()));

		return list;

	}

	public PagingResponse<TermeAPIDTO> findAllTermesAPI(Specification<Terme> activeTerme, Sort sort) {
		List<TermeAPIDTO> entities = findAll(activeTerme, sort).stream()
				.filter(terme -> terme.getStatut().getLibelle().equals("Publié")).map(mapper -> mapTermeAPI(mapper))
				.collect(Collectors.toList());
		return new PagingResponse<>((long) entities.size(), 0L, 0L, 0L, 0L, entities);
	}

	private TermeAPIDTO mapTermeAPI(TermeDto terme) {
		TermeAPIDTO termeAPI = modelMapper.map(terme, TermeAPIDTO.class);
		termeAPI.setObjets(new HashSet<>());
		if (terme.getDefinitions() != null && !terme.getDefinitions().isEmpty()) {
			termeAPI.setDefinition(terme.getDefinitions().stream().findFirst().get().getDefinition());
		}
		if (terme.getFilieres() != null && !terme.getFilieres().isEmpty()) {
			FiliereStructApi struct = new FiliereStructApi();
			struct.setId(terme.getFilieres().stream().findFirst().get().getId());
			struct.setLibelle(terme.getFilieres().stream().findFirst().get().getNom());
			termeAPI.setFiliere(struct);
		}

		if (terme.getObjets() != null && !terme.getObjets().isEmpty())
			termeAPI.setObjets(
					terme.getObjets().stream().filter(objet -> objet.getStatut().getLibelle().equals("Publié"))
							.map(objet -> sharedService.mapObjetAPi(objet)).collect(Collectors.toSet()));

		return termeAPI;
	}
}
