package sncf.reseau.cdsdpadsir.agora.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sncf.reseau.cdsdpadsir.agora.entity.AssociationClasseDobjetFiliere;
import sncf.reseau.cdsdpadsir.agora.entity.AssociationFiliereGisement;
import sncf.reseau.cdsdpadsir.agora.entity.AssociationProfilAutorisationAbstraitRole;
import sncf.reseau.cdsdpadsir.agora.entity.AssociationProfilAutorisationTypeAutorisation;
import sncf.reseau.cdsdpadsir.agora.entity.AssociationTermeFiliere;
import sncf.reseau.cdsdpadsir.agora.entity.Autorisation;
import sncf.reseau.cdsdpadsir.agora.entity.ClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.entity.Famille;
import sncf.reseau.cdsdpadsir.agora.entity.FamilleResponsable;
import sncf.reseau.cdsdpadsir.agora.entity.Filiere;
import sncf.reseau.cdsdpadsir.agora.entity.FiliereFamille;
import sncf.reseau.cdsdpadsir.agora.entity.FiliereResponsable;
import sncf.reseau.cdsdpadsir.agora.entity.Gisement;
import sncf.reseau.cdsdpadsir.agora.entity.IdentiteNumerique;
import sncf.reseau.cdsdpadsir.agora.entity.ProfilAutorisation;
import sncf.reseau.cdsdpadsir.agora.entity.ProfilAutorisationAbstrait;
import sncf.reseau.cdsdpadsir.agora.entity.SuperRessource;
import sncf.reseau.cdsdpadsir.agora.entity.Terme;
import sncf.reseau.cdsdpadsir.agora.entity.TypeAutorisation;
import sncf.reseau.cdsdpadsir.agora.entity.Utilisateur;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;
import sncf.reseau.cdsdpadsir.agora.model.FamilleDto;
import sncf.reseau.cdsdpadsir.agora.model.FiliereApiDto;
import sncf.reseau.cdsdpadsir.agora.model.FiliereDto;
import sncf.reseau.cdsdpadsir.agora.model.FiliereFamilleApiDto;
import sncf.reseau.cdsdpadsir.agora.model.GisementDto;
import sncf.reseau.cdsdpadsir.agora.model.ObjetDto;
import sncf.reseau.cdsdpadsir.agora.model.TermeDto;
import sncf.reseau.cdsdpadsir.agora.model.UtilisateurDto;
import sncf.reseau.cdsdpadsir.agora.model.ValideurDto;
import sncf.reseau.cdsdpadsir.agora.repository.AssociationFiliereGisementRepository;
import sncf.reseau.cdsdpadsir.agora.repository.AssociationProfilAutorisationAbstraitRoleRepository;
import sncf.reseau.cdsdpadsir.agora.repository.AssociationProfilAutorisationTypeAutorisationRepository;
import sncf.reseau.cdsdpadsir.agora.repository.AutorisationRepository;
import sncf.reseau.cdsdpadsir.agora.repository.FamilleRepository;
import sncf.reseau.cdsdpadsir.agora.repository.FiliereFamilleRepository;
import sncf.reseau.cdsdpadsir.agora.repository.FiliereObjetRepository;
import sncf.reseau.cdsdpadsir.agora.repository.FiliereRepository;
import sncf.reseau.cdsdpadsir.agora.repository.FiliereResponsableRepository;
import sncf.reseau.cdsdpadsir.agora.repository.FiliereTermeRepository;
import sncf.reseau.cdsdpadsir.agora.repository.IdentiteNumeriqueRepository;
import sncf.reseau.cdsdpadsir.agora.repository.ProfilAutorisationAbstraitRepository;
import sncf.reseau.cdsdpadsir.agora.repository.ProfilAutorisationRepository;
import sncf.reseau.cdsdpadsir.agora.repository.SuperRessourceRepository;
import sncf.reseau.cdsdpadsir.agora.repository.UtilisateurRepository;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;

@Service
public class FiliereService {

	private FiliereRepository filiereRepository;

	private FiliereResponsableRepository filiereResponsableRepository;

	private ModelMapper modelMapper;

	private FiliereObjetRepository filiereObjetRepository;

	private FiliereTermeRepository filiereTermeRepository;

	private IdentiteNumeriqueRepository identiteNumeriqueRepository;

	private FiliereFamilleRepository filiereFamilleRepository;

	private FamilleRepository familleRepository;

	private SuperRessourceRepository superRessourceRepository;

	private ProfilAutorisationRepository profilAutorisationRepository;

	private AssociationProfilAutorisationTypeAutorisationRepository associationProfilAutorisationTypeAutorisationRepository;

	private ProfilAutorisationAbstraitRepository profilAutorisationAbstraitRepository;

	private AssociationProfilAutorisationAbstraitRoleRepository associationProfilAutorisationAbstraitRoleRepository;

	private AutorisationRepository autorisationRepository;

	private UtilisateurRepository utilisateurRepository;

	private AssociationFiliereGisementRepository associationFiliereGisementRepository;

	@Autowired
	public FiliereService(FiliereRepository filiereRepository, ModelMapper modelMapper,
			FiliereResponsableRepository filiereResponsableRepository, FiliereObjetRepository filiereObjetRepository,
			FiliereTermeRepository filiereTermeRepository, IdentiteNumeriqueRepository identiteNumeriqueRepository,
			FiliereFamilleRepository filiereFamilleRepository, FamilleRepository familleRepository,
			SuperRessourceRepository superRessourceRepository, UtilisateurRepository utilisateurRepository,
			ProfilAutorisationRepository profilAutorisationRepository,
			AssociationProfilAutorisationTypeAutorisationRepository associationProfilAutorisationTypeAutorisationRepository,
			ProfilAutorisationAbstraitRepository profilAutorisationAbstraitRepository,
			AssociationProfilAutorisationAbstraitRoleRepository associationProfilAutorisationAbstraitRoleRepository,
			AutorisationRepository autorisationRepository,
			AssociationFiliereGisementRepository associationFiliereGisementRepository) {
		super();
		this.filiereRepository = filiereRepository;
		this.utilisateurRepository = utilisateurRepository;
		this.filiereResponsableRepository = filiereResponsableRepository;
		this.filiereObjetRepository = filiereObjetRepository;
		this.filiereTermeRepository = filiereTermeRepository;
		this.identiteNumeriqueRepository = identiteNumeriqueRepository;
		this.filiereFamilleRepository = filiereFamilleRepository;
		this.familleRepository = familleRepository;
		this.superRessourceRepository = superRessourceRepository;
		this.profilAutorisationRepository = profilAutorisationRepository;
		this.associationProfilAutorisationTypeAutorisationRepository = associationProfilAutorisationTypeAutorisationRepository;
		this.profilAutorisationAbstraitRepository = profilAutorisationAbstraitRepository;
		this.associationProfilAutorisationAbstraitRoleRepository = associationProfilAutorisationAbstraitRoleRepository;
		this.autorisationRepository = autorisationRepository;
		this.associationFiliereGisementRepository = associationFiliereGisementRepository;
		this.modelMapper = modelMapper;

		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	public PagingResponse<FiliereDto> findAll(Specification<Filiere> filiereSpec, Sort sort, @Valid BigDecimal page,
			@Valid BigDecimal size) {

		if (isRequestPaged(page, size))
			return findAll(filiereSpec, buildPageRequest(page, size, sort));

		List<FiliereDto> entities = findAll(filiereSpec, sort);
		return new PagingResponse<>((long) entities.size(), 0L, 0L, 0L, 0L, entities);
	}

	public PagingResponse<FiliereApiDto> findAllApi(Specification<Filiere> filiereSpec, Sort sort,
			@Valid BigDecimal page, @Valid BigDecimal size) {
		PagingResponse<FiliereDto> response = findAll(filiereSpec, sort, page, size);
		List<FiliereApiDto> listApi = response.getElements().stream()
				.map(item -> modelMapper.map(item, FiliereApiDto.class)).collect(Collectors.toList());
		return new PagingResponse<>((long) response.getCount(), response.getPageNumber(), response.getPageSize(),
				response.getPageOffset(), response.getPageTotal(), listApi);
	}

	public PagingResponse<FiliereFamilleApiDto> findAllFiliereFamilleApi(Specification<Filiere> filiereSpec, Sort sort,
			@Valid BigDecimal page, @Valid BigDecimal size) {
		PagingResponse<FiliereDto> response = findAll(filiereSpec, sort, page, size);
		List<FiliereFamilleApiDto> listApi = response.getElements().stream()
				.map(item -> modelMapper.map(item, FiliereFamilleApiDto.class)).collect(Collectors.toList());
		return new PagingResponse<>((long) response.getCount(), response.getPageNumber(), response.getPageSize(),
				response.getPageOffset(), response.getPageTotal(), listApi);
	}

	private PagingResponse<FiliereDto> findAll(Specification<Filiere> spec, Pageable pageable) {
		Page<Filiere> page = filiereRepository.findAll(spec, pageable);
		List<Filiere> content = page.getContent();
		return new PagingResponse<>(page.getTotalElements(), (long) page.getNumber(), (long) page.getNumberOfElements(),
				pageable.getOffset(), (long) page.getTotalPages(),
				content.stream().map(mapper -> mapFiliere(mapper)).collect(Collectors.toList()));
	}

	public List<FiliereDto> findAll(Specification<Filiere> spec, Sort sort) {
		return filiereRepository.findAll(spec, sort).stream().map(this::mapFiliere).collect(Collectors.toList());
	}

	private boolean isRequestPaged(BigDecimal page, BigDecimal size) {
		return (page != null && size != null);
	}

	private Pageable buildPageRequest(BigDecimal page, BigDecimal size, Sort sort) {
		return PageRequest.of(page.intValue(), size.intValue(), sort);
	}

	@Transactional
	public FiliereDto add(@Valid FiliereDto filiereDto) {
		ArianeHelper.setNewEntityDetails(filiereDto);
		Filiere model = modelMapper.map(filiereDto, Filiere.class);
		model.setImage(filiereDto.getImage());
		Filiere filiere = filiereRepository.save(model);
		updateJoins(filiereDto, model, filiere);
		return mapFiliere(filiere);
	}

	@Transactional
	public FiliereDto update(FiliereDto filiereDto) {
		Optional<Filiere> entity = filiereRepository.findByIdFiliere(filiereDto.getIdFiliere());

		if (entity.isPresent()) {
			Filiere filiere = new Filiere();
			modelMapper.map(entity.get(), filiere);
			filiere.setIdFiliere(null);
			filiere.setDescription(filiereDto.getDescription());
			filiere.setNom(filiereDto.getNom());
			filiere.setImage(filiereDto.getImage());
			ArianeHelper.updateEntityDetails(filiereDto, filiere);
			filiere = filiereRepository.save(filiere);
			updateJoins(filiereDto, filiere, filiere);
			updateIntervenant(filiereDto, filiere);
			updateEndDate(entity);

			return mapFiliere(filiereRepository.findByIdFiliere(filiere.getIdFiliere()).get());
		} else {
			throw new NoSuchElementException();
		}
	}

	private FiliereDto mapFiliere(Filiere filiere) {

		FiliereDto filiereDto = modelMapper.map(filiere, FiliereDto.class);
		filiereDto.setFamilles(new ArrayList<>());
		filiereDto.setClasseDObjet(new ArrayList<>());
		filiereDto.setTermes(new ArrayList<>());
		filiereDto.setGisements(new ArrayList<>());

		if (filiere.getSuperRessources() != null) {
			filiereDto.setSuperRessource(new ArrayList<>());
			filiereDto.getSuperRessource().addAll(filiere.getSuperRessources().stream()
					.filter(item -> item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)).map(item -> {
						// item.setIdentiteNumerique(null);

						item.setFiliere(null);

						if (item.getProfilAutorisations() == null) {
							item.setProfilAutorisations(new HashSet<>());
						}

						item.setProfilAutorisations(item.getProfilAutorisations().stream().map(element -> {

							element.setAssociationProfilAutorisationTypeAutorisations(element
									.getAssociationProfilAutorisationTypeAutorisations().stream().map(profilType -> {
										profilType.setProfilAutorisation(null);
										profilType.getTypeAutorisation()
												.setAssociationProfilAutorisationTypeAutorisations(null);
										return profilType;
									}).collect(Collectors.toSet()));

							element.setProfilAutorisationAbstraits(element.getProfilAutorisationAbstraits().stream()
									.map(profilAutorisationAbstrait -> {
										profilAutorisationAbstrait.setAssociationProfilAutorisationAbstraitRoles(
												profilAutorisationAbstrait
														.getAssociationProfilAutorisationAbstraitRoles().stream()
														.map(profilRole -> {
															profilRole.setProfilAutorisationAbstrait(null);
															if (profilRole.getRole() != null) {
																profilRole.getRole()
																		.setAssociationProfilAutorisationAbstraitRoles(
																				null);
																profilRole.getRole().setUtilisateurRoles(null);
																profilRole.getRole().setGroupeRoleRoles(null);
															}
															return profilRole;
														}).collect(Collectors.toSet()));

										profilAutorisationAbstrait.setAutorisations(profilAutorisationAbstrait
												.getAutorisations().stream().map(autorisation -> {
													autorisation.setProfilAutorisationAbstrait(null);
													if (autorisation.getIdentiteNumerique() != null) {
														// autorisation.getIdentiteNumerique().setSuperRessources(null);
														autorisation.getIdentiteNumerique()
																.setAssociationClasseDobjetUtilisateurs(null);
														autorisation.getIdentiteNumerique()
																.setAssociationTermeUtilisateur(null);
														autorisation.getIdentiteNumerique().setAutorisations(null);
														autorisation.getIdentiteNumerique().setCoordinateurs(null);
														autorisation.getIdentiteNumerique().setResponsables(null);
														autorisation.getIdentiteNumerique().setObjets(null);
														autorisation.getIdentiteNumerique()
																.setResponsablesDelegues(null);
														autorisation.getIdentiteNumerique().setTermes(null);
														autorisation.getIdentiteNumerique().setUtilisateurRoles(null);
														autorisation.getIdentiteNumerique().setValideurs(null);
														autorisation.getIdentiteNumerique().setCoordinateurs(null);
														autorisation.getIdentiteNumerique()
																.setFamilleResponsables(null);
														autorisation.getIdentiteNumerique()
																.setStatutValideurClasseDobjets(null);
														autorisation.getIdentiteNumerique()
																.setStatutSuppleantClasseDobjets(null);
														autorisation.getIdentiteNumerique()
																.setStatutValideurTermes(null);
														autorisation.getIdentiteNumerique()
																.setStatutSuppleantTermes(null);
														autorisation.getIdentiteNumerique().getUtilisateur()
																.setIdentiteNumerique(null);
													}

													return autorisation;
												}).collect(Collectors.toSet()));

										profilAutorisationAbstrait.setProfilAutorisation(null);
										return profilAutorisationAbstrait;
									}).collect(Collectors.toSet()));

							element.setSuperRessource(null);

							return element;
						}).collect(Collectors.toSet()));
						return item;
					}).collect(Collectors.toList()));

		}

		if (filiere.getFiliereFamilles() != null) {

			for (FiliereFamille item : filiere.getFiliereFamilles()) {
				if (item.getFamille().getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
					filiereDto.getFamilles().add(mapFamille(item.getFamille()));
				}
			}
			filiereDto.setFamilles(filiereDto.getFamilles().stream().sorted().collect(Collectors.toList()));
		}

		if (filiere.getAssociationClasseDobjetFilieres() != null) {
			filiere.getAssociationClasseDobjetFilieres().forEach(
					item -> filiereDto.getClasseDObjet().add(modelMapper.map(item.getClasseDobjet(), ObjetDto.class)));
		}

		if (filiere.getAssociationTermeFilieres() != null) {
			filiere.getAssociationTermeFilieres()
					.forEach(item -> filiereDto.getTermes().add(modelMapper.map(item.getTerme(), TermeDto.class)));
		}

		if (filiere.getAssociationFiliereGisements() != null) {
			List<AssociationFiliereGisement> list = filiere.getAssociationFiliereGisements().stream()
					.filter(predicate -> predicate.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE))
					.collect(Collectors.toList());
			list.forEach(item -> filiereDto.getGisements().add(modelMapper.map(item.getGisement(), GisementDto.class)));
		}

		if (filiere.getFiliereResponsables() != null) {
			for (FiliereResponsable item : filiere.getFiliereResponsables()) {
				if (item.getResponsable() != null) {
					UtilisateurDto dto = modelMapper.map(item.getResponsable().getUtilisateur(), UtilisateurDto.class);
					dto.setIdIdentiteNumerique(item.getResponsable().getIdIdentiteNumerique());
					filiereDto.setResponsableDeFiliere(dto);
				}

				if (item.getResponsableDelegue() != null) {
					UtilisateurDto dto = modelMapper.map(item.getResponsableDelegue().getUtilisateur(),
							UtilisateurDto.class);
					dto.setIdIdentiteNumerique(item.getResponsableDelegue().getIdIdentiteNumerique());
					filiereDto.setResponsableDeFiliereDelegue(dto);
				}

				if (item.getCoordinateur() != null) {
					UtilisateurDto dto = modelMapper.map(item.getCoordinateur().getUtilisateur(), UtilisateurDto.class);
					dto.setIdIdentiteNumerique(item.getCoordinateur().getIdIdentiteNumerique());
					filiereDto.setCoordinateurDeFiliere(dto);
				}

				if (item.getValideur() != null) {
					ValideurDto valideur = modelMapper.map(item.getValideur().getUtilisateur(), ValideurDto.class);
					valideur.setIdIdentiteNumerique(item.getValideur().getIdIdentiteNumerique());
					valideur.setOrdre(item.getOrdre());
					filiereDto.getValideursDeFiliere().add(valideur);
				}
			}

		}

		return filiereDto;

	}

	public FiliereDto get(Long id) {
		Optional<Filiere> entity = filiereRepository.findById(id);
		if (entity.isPresent()) {
			return mapFiliere(entity.get());
		} else {
			throw new NoSuchElementException();
		}
	}

	public FiliereDto getWithMapper(Long id) {
		Optional<Filiere> entity = filiereRepository.findById(id);
		if (entity.isPresent()) {
			return modelMapper.map(entity.get(), FiliereDto.class);
		} else {
			throw new NoSuchElementException();
		}
	}

	private void updateEndDate(Optional<Filiere> entity) {
		if (entity.isPresent()) {
			entity.get().getAssociationTermeFilieres().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
			filiereTermeRepository.saveAll(entity.get().getAssociationTermeFilieres());

			entity.get().getAssociationClasseDobjetFilieres()
					.forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
			filiereObjetRepository.saveAll(entity.get().getAssociationClasseDobjetFilieres());

			entity.get().getAssociationFiliereGisements().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
			associationFiliereGisementRepository.saveAll(entity.get().getAssociationFiliereGisements());

			entity.get().getFiliereResponsables().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));

			entity.get().setDateFinActivite(LocalDateTime.now());
			filiereRepository.save(entity.get());
		}
	}

	private void updateJoins(@Valid FiliereDto filiereDto, Filiere model, Filiere filiere) {
		boolean update = false;
		if (model.getFiliereResponsables() != null || model.getAssociationClasseDobjetFilieres() != null
				|| model.getAssociationTermeFilieres() != null || model.getFiliereFamilles() != null) {
			update = true;
		}
		final boolean flag = update;

		model.setFiliereResponsables(new HashSet<>());
		model.setAssociationClasseDobjetFilieres(new HashSet<>());
		model.setAssociationTermeFilieres(new HashSet<>());
		model.setFiliereFamilles(new HashSet<>());
		model.setSuperRessources(new HashSet<>());

		if (filiereDto.getResponsableDeFiliere() != null) {
			FiliereResponsable filiereResponsable = new FiliereResponsable();
			IdentiteNumerique id = identiteNumeriqueRepository
					.findById(filiereDto.getResponsableDeFiliere().getIdIdentiteNumerique()).get();
			filiereResponsable.setResponsable(id);
			filiereResponsable.setFiliere(filiere);
			ariane(filiereDto, filiereResponsable, flag);
			filiereResponsableRepository.save(filiereResponsable);
			model.getFiliereResponsables().add(filiereResponsable);

		}

		if (filiereDto.getResponsableDeFiliereDelegue() != null) {
			FiliereResponsable filiereResponsable = new FiliereResponsable();
			IdentiteNumerique id = identiteNumeriqueRepository
					.findById(filiereDto.getResponsableDeFiliereDelegue().getIdIdentiteNumerique()).get();
			filiereResponsable.setResponsableDelegue(id);
			filiereResponsable.setFiliere(filiere);
			ariane(filiereDto, filiereResponsable, flag);
			filiereResponsableRepository.save(filiereResponsable);
			model.getFiliereResponsables().add(filiereResponsable);
		}

		if (filiereDto.getCoordinateurDeFiliere() != null) {
			FiliereResponsable filiereResponsable = new FiliereResponsable();
			IdentiteNumerique id = identiteNumeriqueRepository
					.findById(filiereDto.getCoordinateurDeFiliere().getIdIdentiteNumerique()).get();
			filiereResponsable.setCoordinateur(id);
			filiereResponsable.setFiliere(filiere);
			ariane(filiereDto, filiereResponsable, flag);
			filiereResponsableRepository.save(filiereResponsable);
			model.getFiliereResponsables().add(filiereResponsable);
		}

		if (filiereDto.getValideursDeFiliere() != null) {
			List<FiliereResponsable> valideurs = new ArrayList<>();
			for (ValideurDto element : filiereDto.getValideursDeFiliere()) {
				FiliereResponsable filiereResponsable = new FiliereResponsable();
				IdentiteNumerique id = identiteNumeriqueRepository.findById(element.getIdIdentiteNumerique())
						.orElseGet(null);
				filiereResponsable.setValideur(id);
				filiereResponsable.setFiliere(filiere);
				filiereResponsable.setOrdre(element.getOrdre());
				ariane(filiereDto, filiereResponsable, flag);
				valideurs.add(filiereResponsable);

			}
			filiereResponsableRepository.saveAll(valideurs);
			model.getFiliereResponsables().addAll(valideurs);

		}

		if (filiereDto.getClasseDObjet() != null) {
			filiereDto.getClasseDObjet().forEach(item -> {
				if (item.getIdClasseDobjet() != null) {
					AssociationClasseDobjetFiliere filiereObjet = new AssociationClasseDobjetFiliere();
					filiereObjet.setClasseDobjet(modelMapper.map(item, ClasseDobjet.class));
					filiereObjet.setFiliere(filiere);
					ariane(filiereDto, filiereObjet, flag);
					filiereObjetRepository.save(filiereObjet);
					model.getAssociationClasseDobjetFilieres().add(filiereObjet);
				}
			});
		}

		if (filiereDto.getTermes() != null) {
			filiereDto.getTermes().forEach(item -> {
				if (item.getIdTerme() != null) {
					AssociationTermeFiliere filiereTerme = new AssociationTermeFiliere();
					filiereTerme.setTerme(modelMapper.map(item, Terme.class));
					filiereTerme.setFiliere(filiere);
					ariane(filiereDto, filiereTerme, flag);
					filiereTermeRepository.save(filiereTerme);
					model.getAssociationTermeFilieres().add(filiereTerme);
				}
			});
		}

		if (filiereDto.getFamilles() != null) {
			filiereDto.getFamilles().forEach(item -> {
				if (item.getIdFamille() != null) {
					FiliereFamille filiereFamille = new FiliereFamille();
					filiereFamille.setFamille(modelMapper.map(item, Famille.class));
					filiereFamille.setFiliere(filiere);
					ariane(filiereDto, filiereFamille, flag);
					filiereFamilleRepository.save(filiereFamille);
					model.getFiliereFamilles().add(filiereFamille);
				}
			});
		}

		if (filiereDto.getGisements() != null) {
			for (GisementDto item : filiereDto.getGisements()) {
				if (item.getIdGisement() != null) {
					AssociationFiliereGisement filiereGisement = new AssociationFiliereGisement();
					filiereGisement.setGisement(modelMapper.map(item, Gisement.class));
					filiereGisement.setFiliere(filiere);
					ariane(filiereDto, filiereGisement, flag);
					associationFiliereGisementRepository.save(filiereGisement);
					model.getAssociationFiliereGisements().add(filiereGisement);
				}
			}
		}

	}

	<T extends RessourceAbstraite> void ariane(T obj, T response, boolean flag) {
		if (!flag) {
			ArianeHelper.setNewEntityDetails(response);
		} else {
			ArianeHelper.updateEntityDetails(obj, response);
		}
	}

	public FamilleDto mapFamille(Famille famille) {
		
		if(famille.getNom().equals("testFamille")) {
			System.out.println("stop");
		}
		FamilleDto familleDto = modelMapper.map(famille, FamilleDto.class);
		familleDto.setObjets(new ArrayList<>());
		familleDto.setTermes(new ArrayList<>());
		familleDto.setSousFamilles(new ArrayList<>());

		if (famille.getFiliereFamilles() != null) {
			famille.getFiliereFamilles()
					.forEach(item -> familleDto.setFiliere(modelMapper.map(item.getFiliere(), FiliereDto.class)));
		}

		if (famille.getFamilles() == null) {
			famille.setFamilles(familleRepository.findByFamilleAndDateFinActivite(famille, ArianeHelper.FUTURE_DATE));
		}

		if (famille.getFamilles() != null) {
			for (Famille item : famille.getFamilles()) {
				if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
					familleDto.getSousFamilles().add(mapFamille(item));
				}
			}
		}

		if (famille.getFamille() != null) {
			familleDto.setFamilleMere(modelMapper.map(famille.getFamille(), FamilleDto.class));
		}

		/*if (famille.getFamilles() != null) {
			famille.getFamilles().forEach(item -> familleDto.setFamilleMere(modelMapper.map(item, FamilleDto.class)));
		}*/

		if (famille.getAssociationClasseDobjetFamilles() != null) {
			famille.getAssociationClasseDobjetFamilles().forEach(
					item -> familleDto.getObjets().add(modelMapper.map(item.getClasseDobjet(), ObjetDto.class)));
		}

		if (famille.getAssociationClasseDobjetSousFamilles() != null) {
			famille.getAssociationClasseDobjetSousFamilles().forEach(
					item -> familleDto.getObjets().add(modelMapper.map(item.getClasseDobjet(), ObjetDto.class)));
		}

		if (famille.getFamilleResponsables() != null) {
			for (FamilleResponsable item : famille.getFamilleResponsables()) {
				if (item.getIdentiteNumerique() != null) {
					familleDto.setResponsableDeDonnees(
							modelMapper.map(item.getIdentiteNumerique().getUtilisateur(), UtilisateurDto.class));
				}
			}
		}

		return familleDto;
	}

	public FiliereDto addIntervenant(FiliereDto filiereDto) {

		persistIntervenant(filiereDto, null);
		return mapFiliere(filiereRepository.findById(filiereDto.getIdFiliere()).get());
	}

	public FiliereDto updateIntervenant(FiliereDto filiereDto, Filiere newFiliere) {

		persisitUpdatedIntervenant(filiereDto, newFiliere);

		return mapFiliere(filiereRepository.findById(filiereDto.getIdFiliere()).get());
	}

	public FiliereDto persisitUpdatedIntervenant(FiliereDto filiereDto, Filiere newFiliere) {
		endDateIntervenant(filiereDto, false);
		persistIntervenant(filiereDto, newFiliere);

		return mapFiliere(filiereRepository.findById(filiereDto.getIdFiliere()).get());
	}

	@Transactional
	public void endDateIntervenant(FiliereDto filiereDto, boolean operation) {

		Optional<Filiere> filiere = filiereRepository.findById(filiereDto.getIdFiliere());

		if (filiere.isPresent() && filiereDto.getSuperRessource() != null
				&& !filiereDto.getSuperRessource().isEmpty()) {
			List<Long> ressources = filiereDto.getSuperRessource().stream().map(item -> item.getIdSuperRessource())
					.collect(Collectors.toList());
			filiere.get()
					.setSuperRessources(filiere.get().getSuperRessources().stream()
							.filter(ressource -> operation ? !ressources.contains(ressource.getIdSuperRessource())
									: ressources.contains(ressource.getIdSuperRessource()))
							.collect(Collectors.toSet()));

			for (SuperRessource super_ressource : filiere.get().getSuperRessources()) {

				super_ressource.setDateFinActivite(ArianeHelper.getCurrentDate());
				superRessourceRepository.save(super_ressource);

				for (ProfilAutorisation profil_autorisation : super_ressource.getProfilAutorisations()) {

					profil_autorisation.setDateFinActivite(ArianeHelper.getCurrentDate());
					profilAutorisationRepository.save(profil_autorisation);

					for (AssociationProfilAutorisationTypeAutorisation associationProfilAutorisationTypeAutorisation : profil_autorisation
							.getAssociationProfilAutorisationTypeAutorisations()) {

						associationProfilAutorisationTypeAutorisation.setDateFinActivite(ArianeHelper.getCurrentDate());
						associationProfilAutorisationTypeAutorisationRepository
								.save(associationProfilAutorisationTypeAutorisation);

					}

					for (ProfilAutorisationAbstrait profilAutorisationAbstrait : profil_autorisation
							.getProfilAutorisationAbstraits()) {

						profilAutorisationAbstrait.setDateFinActivite(ArianeHelper.getCurrentDate());
						profilAutorisationAbstraitRepository.save(profilAutorisationAbstrait);

						for (AssociationProfilAutorisationAbstraitRole associationProfilAutorisationAbstraitRole : profilAutorisationAbstrait
								.getAssociationProfilAutorisationAbstraitRoles()) {
							associationProfilAutorisationAbstraitRole.setDateFinActivite(ArianeHelper.getCurrentDate());
							associationProfilAutorisationAbstraitRoleRepository
									.save(associationProfilAutorisationAbstraitRole);

						}

						for (Autorisation autorisation : profilAutorisationAbstrait.getAutorisations()) {
							autorisation.setDateFinActivite(ArianeHelper.getCurrentDate());
							autorisationRepository.save(autorisation);

						}
					}
				}
			}

		}
	}

	@Transactional
	private void persistIntervenant(FiliereDto filiereDto, Filiere newFiliere) {
		Optional<Filiere> filiere = filiereRepository.findById(filiereDto.getIdFiliere());

		if (filiere.isPresent() && filiereDto.getSuperRessource() != null) {
			for (SuperRessource super_ressource : filiereDto.getSuperRessource()) {

				SuperRessource sr = new SuperRessource();
				sr.setFiliere(newFiliere == null ? filiere.get() : newFiliere);
				ArianeHelper.setNewEntityDetails(sr);
				sr = superRessourceRepository.save(sr);

				for (ProfilAutorisation profil_autorisation : super_ressource.getProfilAutorisations()) {
					ProfilAutorisation pa = new ProfilAutorisation();
					ArianeHelper.setNewEntityDetails(pa);
					pa.setSuperRessource(sr);
					pa.setLibelle(profil_autorisation.getLibelle());
					pa = profilAutorisationRepository.save(pa);

					if (profil_autorisation.getAssociationProfilAutorisationTypeAutorisations() != null) {
						for (AssociationProfilAutorisationTypeAutorisation associationProfilAutorisationTypeAutorisation : profil_autorisation
								.getAssociationProfilAutorisationTypeAutorisations()) {
							AssociationProfilAutorisationTypeAutorisation apta = new AssociationProfilAutorisationTypeAutorisation();
							ArianeHelper.setNewEntityDetails(apta);

							TypeAutorisation typeAutorisation = modelMapper.map(
									associationProfilAutorisationTypeAutorisation.getTypeAutorisation(),
									TypeAutorisation.class);

							apta.setProfilAutorisation(pa);
							apta.setTypeAutorisation(typeAutorisation);

							apta = associationProfilAutorisationTypeAutorisationRepository.save(apta);

						}
					}

					if (profil_autorisation.getProfilAutorisationAbstraits() != null) {
						for (ProfilAutorisationAbstrait profilAutorisationAbstrait : profil_autorisation
								.getProfilAutorisationAbstraits()) {
							ProfilAutorisationAbstrait pra = new ProfilAutorisationAbstrait();
							ArianeHelper.setNewEntityDetails(pra);

							pra.setProfilAutorisation(pa);

							pra = profilAutorisationAbstraitRepository.save(pra);

							if (profilAutorisationAbstrait.getAssociationProfilAutorisationAbstraitRoles() != null) {
								for (AssociationProfilAutorisationAbstraitRole associationProfilAutorisationAbstraitRole : profilAutorisationAbstrait
										.getAssociationProfilAutorisationAbstraitRoles()) {
									AssociationProfilAutorisationAbstraitRole apaar = new AssociationProfilAutorisationAbstraitRole();
									ArianeHelper.setNewEntityDetails(apaar);
									apaar.setProfilAutorisationAbstrait(pra);
									apaar.setRole(associationProfilAutorisationAbstraitRole.getRole());
									apaar = associationProfilAutorisationAbstraitRoleRepository.save(apaar);

								}
							}

							if (profilAutorisationAbstrait.getAutorisations() != null) {
								for (Autorisation autorisation : profilAutorisationAbstrait.getAutorisations()) {
									Autorisation au = new Autorisation();
									ArianeHelper.setNewEntityDetails(au);
									au.setProfilAutorisationAbstrait(pra);
									if (autorisation.getIdentiteNumerique() != null && autorisation
											.getIdentiteNumerique().getUtilisateur().getIdUtilisateur() != null) {
										Optional<Utilisateur> user = utilisateurRepository.findById(autorisation
												.getIdentiteNumerique().getUtilisateur().getIdUtilisateur());
										if (user.isPresent()) {
											au.setIdentiteNumerique(user.get().getIdentiteNumerique());
										}
									}

									autorisationRepository.save(au);

								}
							}
						}
					}
				}
			}
		}
	}

	@Transactional
	public FiliereDto delete(Long idFiliere) {
		Optional<Filiere> filiere = filiereRepository.findById(idFiliere);
		updateEndDate(filiere);
		endDateIntervenant(mapFiliere(filiere.get()), false);
		return mapFiliere(filiereRepository.findById(idFiliere).get());
	}

}
