package sncf.reseau.cdsdpadsir.agora.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sncf.reseau.cdsdpadsir.agora.entity.*;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;
import sncf.reseau.cdsdpadsir.agora.model.AttributDto;
import sncf.reseau.cdsdpadsir.agora.model.DefinitionDto;
import sncf.reseau.cdsdpadsir.agora.model.FamilleDto;
import sncf.reseau.cdsdpadsir.agora.model.FamilleStructApi;
import sncf.reseau.cdsdpadsir.agora.model.FiliereDto;
import sncf.reseau.cdsdpadsir.agora.model.FiliereStructApi;
import sncf.reseau.cdsdpadsir.agora.model.ObjetAPIDTO;
import sncf.reseau.cdsdpadsir.agora.model.ObjetDto;
import sncf.reseau.cdsdpadsir.agora.model.StatutClasseDobjetDto;
import sncf.reseau.cdsdpadsir.agora.model.UtilisateurDto;
import sncf.reseau.cdsdpadsir.agora.repository.*;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;

@Service
public class ObjetService {

	private ObjetRepository objetRepository;

	private ModelMapper modelMapper;

	private FiliereObjetRepository filiereObjetRepository;

	private FamilleObjetRepository familleObjetRepository;

	private AttributRepository attributRepository;

	private DefinitionRepository definitionRepository;

	private AssociationClasseDobjetUtilisateurRepository associationClasseDobjetUtilisateurRepository;

	private TermeClasseDobjetRepository termeClasseDobjetRepository;

	private DomaineObjetRepository domaineObjetRepository;

	private StatutClasseDobjetRepository statutClasseDobjetRepository;

	private IdentiteNumeriqueRepository identiteNumeriqueRepository;

	private ValeurStatutClasseDobjetRepository valeurStatutClasseDobjetRepository;

	private SousFamilleObjetRepository sousFamilleObjetRepository;

	private UtilisateurRepository utilisateurRepository;

	private FiliereRepository filiereRepository;

	private SharedService sharedService;

	private RelationClasseDobjetRepository relationClasseDobjetRepository;
	private RelationClasseMereRepository relationClasseMereRepository;

	@Autowired
	public ObjetService(ObjetRepository objetRepository, ModelMapper modelMapper,
			FiliereObjetRepository filiereObjetRepository, FamilleObjetRepository familleObjetRepository,
			AttributRepository attributRepository, DefinitionRepository definitionRepository,
			AssociationClasseDobjetUtilisateurRepository associationClasseDobjetUtilisateurRepository,
			TermeClasseDobjetRepository termeClasseDobjetRepository,
			SousFamilleObjetRepository sousFamilleObjetRepository,
			StatutClasseDobjetRepository statutClasseDobjetRepository,
			ValeurStatutClasseDobjetRepository valeurStatutClasseDobjetRepository,
			UtilisateurRepository utilisateurRepository, IdentiteNumeriqueRepository identiteNumeriqueRepository,
			DomaineObjetRepository domaineObjetRepository, SharedService sharedService,
			FiliereRepository filiereRepository, RelationClasseDobjetRepository relationClasseDobjetRepository,
						RelationClasseMereRepository relationClasseMereRepository) {
		super();
		this.objetRepository = objetRepository;
		this.modelMapper = modelMapper;
		this.filiereObjetRepository = filiereObjetRepository;
		this.familleObjetRepository = familleObjetRepository;
		this.attributRepository = attributRepository;
		this.definitionRepository = definitionRepository;
		this.associationClasseDobjetUtilisateurRepository = associationClasseDobjetUtilisateurRepository;
		this.termeClasseDobjetRepository = termeClasseDobjetRepository;
		this.statutClasseDobjetRepository = statutClasseDobjetRepository;
		this.valeurStatutClasseDobjetRepository = valeurStatutClasseDobjetRepository;
		this.identiteNumeriqueRepository = identiteNumeriqueRepository;
		this.domaineObjetRepository = domaineObjetRepository;
		this.utilisateurRepository = utilisateurRepository;
		this.sousFamilleObjetRepository = sousFamilleObjetRepository;
		this.sharedService = sharedService;
		this.filiereRepository = filiereRepository;
        this.relationClasseDobjetRepository = relationClasseDobjetRepository;
		this.relationClasseMereRepository = relationClasseMereRepository;
		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	public PagingResponse<ObjetDto> findAll(Specification<ClasseDobjet> objetSpec, Sort sort, @Valid BigDecimal page,
			@Valid BigDecimal size) {

		if (isRequestPaged(page, size))
			return findAll(objetSpec, buildPageRequest(page, size, sort));

		List<ObjetDto> entities = findAll(objetSpec, sort);
		return new PagingResponse<>((long) entities.size(), 0L, 0L, 0L, 0L, entities);
	}

	private PagingResponse<ObjetDto> findAll(Specification<ClasseDobjet> spec, Pageable pageable) {
		Page<ClasseDobjet> page = objetRepository.findAll(spec, pageable);
		List<ClasseDobjet> content = page.getContent();
		return new PagingResponse<>(page.getTotalElements(), (long) page.getNumber(), (long) page.getNumberOfElements(),
				pageable.getOffset(), (long) page.getTotalPages(),
				content.stream().map(mapper -> mapObjet(mapper)).collect(Collectors.toList()));
	}

	public ObjetDto mapObjet(ClasseDobjet mapper) {
		ObjetDto objetDto = modelMapper.map(mapper, ObjetDto.class);
		objetDto.setFilieres(new HashSet<>());
		objetDto.setFamilles(new HashSet<>());
		objetDto.setSousFamilles(new HashSet<>());
		objetDto.setAttributClasses(new HashSet<>());
		objetDto.setDefinitions(new HashSet<>());
		objetDto.setResponsables(new HashSet<>());
		objetDto.setTermes(new HashSet<>());

		if (mapper.getProprietaire() != null) {
			objetDto.setProprietaire(modelMapper.map(mapper.getProprietaire().getUtilisateur(), UtilisateurDto.class));
		}

		mapper.getAssociationClasseDobjetFilieres().forEach(item -> {
			if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
				objetDto.getFilieres().add(modelMapper.map(item.getFiliere(), FiliereDto.class));
			}
		});
		mapper.getAssociationClasseDobjetFamilles().forEach(item -> {
			if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
				objetDto.getFamilles().add(modelMapper.map(item.getFamille(), FamilleDto.class));
			}
		});
		mapper.getAssociationClasseDobjetSousFamilles().forEach(item -> {
			if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
				objetDto.getSousFamilles().add(modelMapper.map(item.getFamille(), FamilleDto.class));
			}
		});
		mapper.getAttributClasses().forEach(item -> {
			if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
				objetDto.getAttributClasses().add(modelMapper.map(item, AttributDto.class));
			}
		});
		mapper.getDefinitionClasseDobjets().forEach(item -> {
			if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
				objetDto.getDefinitions().add(modelMapper.map(item, DefinitionDto.class));
			}
		});
		mapper.getAssociationClasseDobjetUtilisateurs().forEach(item -> {
			if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
				objetDto.getResponsables()
						.add(modelMapper.map(item.getIdentiteNumerique().getUtilisateur(), UtilisateurDto.class));
			}
		});

		mapper.getAssociationTermeClasseDobjets().forEach(item -> {
			if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
				objetDto.getTermes().add(sharedService.mapTerme(item.getTerme()));
			}
		});

		for (StatutClasseDobjet item : mapper.getStatutClasseDobjets()) {
			StatutClasseDobjetDto statut = modelMapper.map(item, StatutClasseDobjetDto.class);
			statut.setValideur(modelMapper.map(item.getValideur().getUtilisateur(), UtilisateurDto.class));
			statut.setSuppleant(modelMapper.map(item.getSuppleant().getUtilisateur(), UtilisateurDto.class));
			statut.setLibelle(item.getValeurStatutClasseDobjet().getLibelle());
			objetDto.setStatut(statut);
		}

		return objetDto;
	}

	public List<ObjetDto> findAll(Specification<ClasseDobjet> spec, Sort sort) {
		return objetRepository.findAll(spec, sort).stream().map(mapper -> mapObjet(mapper))
				.collect(Collectors.toList());
	}

	public PagingResponse<ObjetAPIDTO> findAllObjetAPI(Specification<ClasseDobjet> objetSpec, Sort sort) {
		List<ObjetAPIDTO> entities = findAll(objetSpec, sort).stream()
				.filter(objet -> objet.getStatut() != null && objet.getStatut().getLibelle() != null
						&& objet.getStatut().getLibelle().equals("Publié"))
				.map(this::mapObjetAPi).collect(Collectors.toList());
		return new PagingResponse<>((long) entities.size(), 0L, 0L, 0L, 0L, entities);

	}

	private ObjetAPIDTO mapObjetAPi(ObjetDto objet) {
		ObjetAPIDTO objetAPI = modelMapper.map(objet, ObjetAPIDTO.class);
		objetAPI.setTermes(new HashSet<>());
		if (objet.getDefinitions() != null && !objet.getDefinitions().isEmpty()) {
			Optional<DefinitionDto> definition = objet.getDefinitions().stream().findFirst();
			if (definition.isPresent()) {
				objetAPI.setDefinition(definition.get().getDefinition());
			}
		}
		if (objet.getFilieres() != null && !objet.getFilieres().isEmpty()) {
			FiliereStructApi struct = new FiliereStructApi();
			Optional<FiliereDto> filiere = objet.getFilieres().stream().findFirst();
			if (filiere.isPresent()) {
				struct.setId(filiere.get().getId());
				struct.setLibelle(filiere.get().getNom());
			}
			objetAPI.setFiliere(struct);
		}
		if (objet.getFamilles() != null && !objet.getFamilles().isEmpty()) {
			FamilleStructApi struct = new FamilleStructApi();
			FamilleStructApi sousStruct = new FamilleStructApi();
			Optional<FamilleDto> famille = objet.getFamilles().stream().findFirst();
			if (famille.isPresent()) {
				struct.setId(famille.get().getId());
				struct.setLibelle(famille.get().getNom());
			}

			Optional<FamilleDto> sousFamille = objet.getSousFamilles().stream().findFirst();
			if (sousFamille.isPresent()) {
				sousStruct.setId(sousFamille.get().getId());
				sousStruct.setLibelle(sousFamille.get().getNom());
			}

			objetAPI.setSousFamille(sousStruct);
			objetAPI.setFamille(struct);
		}

		if (objet.getTermes() != null && !objet.getTermes().isEmpty())
			objetAPI.setTermes(
					objet.getTermes().stream().filter(terme -> terme.getStatut().getLibelle().equals("Publié"))
							.map(terme -> sharedService.mapTermeAPI(terme)).collect(Collectors.toSet()));

		return objetAPI;
	}

	private boolean isRequestPaged(BigDecimal page, BigDecimal size) {
		return (page != null && size != null);
	}

	private Pageable buildPageRequest(BigDecimal page, BigDecimal size, Sort sort) {
		return PageRequest.of(page.intValue(), size.intValue(), sort);
	}

	@Transactional
	public ObjetDto add(@Valid ObjetDto objetDto) {

		ArianeHelper.setNewEntityDetails(objetDto);
		ClasseDobjet model = modelMapper.map(objetDto, ClasseDobjet.class);
		model.setProprietaire(identiteNumeriqueRepository
				.findByUtilisateur(modelMapper.map(objetDto.getProprietaire(), Utilisateur.class)).get());
		ClasseDobjet objet = objetRepository.save(model);

		updateJoins(objetDto, model, objet);
		return mapObjet(objet);
	}

	private void updateJoins(ObjetDto objetDto, ClasseDobjet model, ClasseDobjet objet) {

		boolean update = false;
		if (model.getAssociationClasseDobjetFilieres() != null || model.getAssociationClasseDobjetFamilles() != null
				|| model.getAttributClasses() != null || model.getDefinitionClasseDobjets() != null) {
			update = true;
		}
		final boolean flag = update;
		model.setAssociationClasseDobjetFilieres(new HashSet<>());
		model.setAssociationClasseDobjetFamilles(new HashSet<>());
		model.setDefinitionClasseDobjets(new HashSet<>());
		model.setAttributClasses(new HashSet<>());
		model.setAssociationClasseDobjetUtilisateurs(new HashSet<>());
		model.setAssociationTermeClasseDobjets(new HashSet<>());
		model.setAssociationClasseDobjetSousFamilles(new HashSet<>());

		model.setStatutClasseDobjets(new HashSet<>());
		StatutClasseDobjet statut = new StatutClasseDobjet();

		Optional<Utilisateur> valideur = this.utilisateurRepository
				.findById(objetDto.getStatut().getValideur().getIdUtilisateur());
		Optional<Utilisateur> suppleant = this.utilisateurRepository
				.findById(objetDto.getStatut().getSuppleant().getIdUtilisateur());
		IdentiteNumerique idValideur = null;
		IdentiteNumerique idSuppleant = null;
		if (valideur.isPresent()) {
			idValideur = valideur.get().getIdentiteNumerique();
		}

		if (suppleant.isPresent()) {
			idSuppleant = suppleant.get().getIdentiteNumerique();
		}

		ariane(objetDto, idValideur, flag);
		ariane(objetDto, idSuppleant, flag);

		ValeurStatutClasseDobjet valeurStatutClasseDobjet = valeurStatutClasseDobjetRepository
				.findByLibelle(objetDto.getStatut().getLibelle()).get();
		ariane(objetDto, valeurStatutClasseDobjet, flag);

		statut.setValideur(idValideur);
		statut.setSuppleant(idSuppleant);
		statut.setValeurStatutClasseDobjet(valeurStatutClasseDobjet);
		statut.setClasseDobjet(model);
		statut.setCommentaire(objetDto.getStatut().getCommentaire());
		statut.setNom(objetDto.getStatut().getNom());
		statut.setPrenom(objetDto.getStatut().getPrenom());
		ariane(objetDto, statut, flag);

		model.getStatutClasseDobjets().add(statutClasseDobjetRepository.save(statut));

		objetDto.getFilieres().forEach(item -> {
			if (item.getIdFiliere() != null) {
				AssociationClasseDobjetFiliere filiereObjet = new AssociationClasseDobjetFiliere();
				filiereObjet.setClasseDobjet(objet);
				filiereObjet.setFiliere(modelMapper.map(item, Filiere.class));
				ariane(objetDto, filiereObjet, flag);
				filiereObjetRepository.save(filiereObjet);
				model.getAssociationClasseDobjetFilieres().add(filiereObjet);
			}
		});
		objetDto.getFamilles().forEach(item -> {
			if (item.getIdFamille() != null) {
				AssociationClasseDobjetFamille familleObjet = new AssociationClasseDobjetFamille();
				familleObjet.setClasseDobjet(objet);
				familleObjet.setFamille(modelMapper.map(item, Famille.class));
				ariane(objetDto, familleObjet, flag);
				familleObjetRepository.save(familleObjet);
				model.getAssociationClasseDobjetFamilles().add(familleObjet);
			}
		});

		objetDto.getSousFamilles().forEach(item -> {
			if (item.getIdFamille() != null) {
				AssociationClasseDobjetSousFamille sousFamilleObjet = new AssociationClasseDobjetSousFamille();
				sousFamilleObjet.setClasseDobjet(objet);
				sousFamilleObjet.setFamille(modelMapper.map(item, Famille.class));
				ariane(objetDto, sousFamilleObjet, flag);
				sousFamilleObjetRepository.save(sousFamilleObjet);
				model.getAssociationClasseDobjetSousFamilles().add(sousFamilleObjet);
			}
		});

		objetDto.getAttributClasses().forEach(item -> {
			AttributClasse attribut = modelMapper.map(item, AttributClasse.class);
			attribut.setIdAttributClasse(null);
			attribut.setClasseDobjet(objet);
			ariane(objetDto, attribut, flag);
			attribut = attributRepository.save(attribut);
			model.getAttributClasses().add(attribut);
		});

		objetDto.getTermes().forEach(item -> {
			AssociationTermeClasseDobjet termClass = new AssociationTermeClasseDobjet();
			termClass.setClasseDobjet(objet);
			termClass.setTerme(modelMapper.map(item, Terme.class));
			ariane(objetDto, termClass, flag);
			termClass = termeClasseDobjetRepository.save(termClass);
			model.getAssociationTermeClasseDobjets().add(termClass);
		});

		objetDto.getDefinitions().forEach(item -> {
			DefinitionClasseDobjet definition = modelMapper.map(item, DefinitionClasseDobjet.class);
			definition.setIdDefinitionClasseDobjet(null);
			definition.setClasseDobjet(objet);
			ariane(objetDto, definition, flag);
			definition = definitionRepository.save(definition);
			model.getDefinitionClasseDobjets().add(definition);
		});

		objetDto.getResponsables().forEach(item -> {
			if (item.getIdUtilisateur() != null) {
				AssociationClasseDobjetIdentiteNumerique responsable = new AssociationClasseDobjetIdentiteNumerique();
				IdentiteNumerique id = this.utilisateurRepository.findById(item.getIdUtilisateur()).get()
						.getIdentiteNumerique();
				responsable.setIdentiteNumerique(id);
				responsable.setClasseDobjet(objet);
				ariane(objetDto, responsable, flag);
				associationClasseDobjetUtilisateurRepository.save(responsable);
				model.getAssociationClasseDobjetUtilisateurs().add(responsable);
			}
		});

		if (update) {
			List<RelationClasseDobjet> relationsClasseDobjetsUsingObjectAsSource = relationClasseDobjetRepository.findAllByClasseDobjetSource_IdClasseDobjet(objetDto.getIdClasseDobjet());
			if (!relationsClasseDobjetsUsingObjectAsSource.isEmpty()) {
				relationsClasseDobjetsUsingObjectAsSource.stream().forEach(
						relationClasseDobjet -> {
							relationClasseDobjet.setClasseDobjetSource(objet);
							relationClasseDobjetRepository.save(relationClasseDobjet);
						}
				);
			}
			List<RelationClasseDobjet> relationsClasseDobjetsUsingObjectAsDestination = relationClasseDobjetRepository.findAllByClasseDobjetDestination_IdClasseDobjet(objetDto.getIdClasseDobjet());
			if (!relationsClasseDobjetsUsingObjectAsDestination.isEmpty()) {
				relationsClasseDobjetsUsingObjectAsDestination.stream().forEach(
						relationClasseDobjet -> {
							relationClasseDobjet.setClasseDobjetDestination(objet);
							relationClasseDobjetRepository.save(relationClasseDobjet);
						}
				);
			}
			List<RelationClasseMere> relationsClasseMereUsingObjectAsMere = relationClasseMereRepository.findAllByClasseDobjeMere_IdClasseDobjet(objetDto.getIdClasseDobjet());
			if (!relationsClasseMereUsingObjectAsMere.isEmpty()) {
				relationsClasseMereUsingObjectAsMere.stream().forEach(
						relationClasseMere -> {
							relationClasseMere.setClasseDobjeMere(objet);
							relationClasseMereRepository.save(relationClasseMere);
						}
				);
			}
			List<RelationClasseMere> relationsClasseMereUsingObjectAsFille = relationClasseMereRepository.findAllByClasseDobjetFille_IdClasseDobjet(objetDto.getIdClasseDobjet());
			if (!relationsClasseMereUsingObjectAsFille.isEmpty()) {
				relationsClasseMereUsingObjectAsFille.stream().forEach(
						relationClasseFille -> {
							relationClasseFille.setClasseDobjetFille(objet);
							relationClasseMereRepository.save(relationClasseFille);
						}
				);
			}
		}
	}

	<T extends RessourceAbstraite> void ariane(T obj, T response, boolean flag) {
		if (!flag) {
			ArianeHelper.setNewEntityDetails(response);
		} else {
			ArianeHelper.updateEntityDetails(obj, response);
		}
	}

	@Transactional
	public ObjetDto update(@Valid ObjetDto objetDto) throws ConflictException {

		Optional<ClasseDobjet> entity = objetRepository.findByIdClasseDobjet(objetDto.getIdClasseDobjet());
		Filiere filiere = filiereRepository.findById(objetDto.getFilieres().iterator().next().getIdFiliere()).get();
		Optional<ClasseDobjet> objetByLibelle = objetRepository.findFirstByLibelleAndDateFinActiviteByFiliere(
				objetDto.getLibelle(), ArianeHelper.FUTURE_DATE, filiere.getNom());
		if (entity.isPresent()) {
			if (objetByLibelle.isPresent() && !objetByLibelle.get().getId().equals(entity.get().getId())) {
				throw new ConflictException();
			}
			ClasseDobjet objet = new ClasseDobjet();
			modelMapper.map(entity.get(), objet);
			objet.setProprietaire(entity.get().getProprietaire());
			objet.setIdClasseDobjet(null);
			objet.setLibelle(objetDto.getLibelle());
			objet.setCommentaire(objetDto.getCommentaire());
			objet.setAbreviation(objetDto.getAbreviation());
			ArianeHelper.updateEntityDetails(objetDto, objet);
			objet = objetRepository.save(objet);
			updateJoins(objetDto, objet, objet);
			updateEndDate(entity);

			return mapObjet(objet);
		} else {
			throw new NoSuchElementException();
		}
	}

	private void updateEndDate(Optional<ClasseDobjet> entity) {
		entity.get().getAssociationClasseDobjetFilieres().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
		filiereObjetRepository.saveAll(entity.get().getAssociationClasseDobjetFilieres());

		entity.get().getAssociationClasseDobjetFamilles().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
		familleObjetRepository.saveAll(entity.get().getAssociationClasseDobjetFamilles());

		entity.get().getAssociationClasseDobjetUtilisateurs()
				.forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
		associationClasseDobjetUtilisateurRepository.saveAll(entity.get().getAssociationClasseDobjetUtilisateurs());

		entity.get().getDefinitionClasseDobjets().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
		definitionRepository.saveAll(entity.get().getDefinitionClasseDobjets());

		entity.get().getAttributClasses().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
		attributRepository.saveAll(entity.get().getAttributClasses());

		entity.get().getAssociationTermeClasseDobjets().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
		termeClasseDobjetRepository.saveAll(entity.get().getAssociationTermeClasseDobjets());

		entity.get().getStatutClasseDobjets().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
		statutClasseDobjetRepository.saveAll(entity.get().getStatutClasseDobjets());

		entity.get().setDateFinActivite(LocalDateTime.now());
		objetRepository.save(entity.get());
	}

	public ObjetDto get(Long id) {
		Optional<ClasseDobjet> entity = objetRepository.findById(id);
		if (entity.isPresent()) {
			return mapObjet(entity.get());
		} else {
			throw new NoSuchElementException();
		}
	}

	public boolean delete(Long id) {
		Optional<ClasseDobjet> entity = objetRepository.findByIdClasseDobjet(id);
		if (entity.isPresent()) {

			updateEndDate(entity);
			return true;
		} else {
			throw new NoSuchElementException();
		}
	}

	public boolean checkUniquenessByFiliere(@Valid ObjetDto objetDto) {
		if (objetDto.getFilieres().isEmpty() || objetDto.getFilieres().iterator().next().getIdFiliere() == null) {
			return false;
		}
		Optional<Filiere> filiere = filiereRepository.findById(objetDto.getFilieres().iterator().next().getIdFiliere());
		return filiere.isPresent()
				&& objetRepository.findFirstByLibelleAndDateFinActiviteByFiliere(objetDto.getLibelle(),
						ArianeHelper.FUTURE_DATE, filiere.get().getNom()).isPresent();
	}

	public boolean checkUniqueness(@Valid ObjetDto objetDto) {
		return objetRepository
				.findFirstByLibelleIgnoreCaseAndDateFinActivite(objetDto.getLibelle(), ArianeHelper.FUTURE_DATE)
				.isPresent();
	}

	public boolean checkUniquenessForEdit(ObjetDto objetDto) {
		Optional<ClasseDobjet> entity = objetRepository.findByIdClasseDobjet(objetDto.getIdClasseDobjet());
		Optional<ClasseDobjet> objetByLibelle = objetRepository
				.findFirstByLibelleIgnoreCaseAndDateFinActivite(objetDto.getLibelle(), ArianeHelper.FUTURE_DATE);

		return (entity.isPresent() && objetByLibelle.isPresent()
				&& !objetByLibelle.get().getId().equals(entity.get().getId()));

	}

	public List<StatutClasseDobjetDto> getStatuts(String id) {

		List<StatutClasseDobjetDto> list = statutClasseDobjetRepository.findById(id).stream().map(item -> {
			StatutClasseDobjetDto statut = modelMapper.map(item, StatutClasseDobjetDto.class);
			statut.setValideur(modelMapper.map(item.getValideur().getUtilisateur(), UtilisateurDto.class));
			statut.setSuppleant(modelMapper.map(item.getSuppleant().getUtilisateur(), UtilisateurDto.class));
			statut.setLibelle(item.getValeurStatutClasseDobjet().getLibelle());
			return statut;
		}).collect(Collectors.toList());

		Collections.sort(list, (o1, o2) -> o1.getDateFinActivite().compareTo(o2.getDateFinActivite()));

		return list;

	}

	public ClasseDobjet getLastVersionReturningEntity(String id) {
		Optional<ClasseDobjet> entity = objetRepository.findTopByIdOrderByIdClasseDobjetDesc(id);
		if (entity.isPresent()) {
			return entity.get();
		} else {
			throw new NoSuchElementException();
		}
	}

	public ObjetDto getLastVersion(String id) {
		Optional<ClasseDobjet> entity = objetRepository.findTopByIdOrderByIdClasseDobjetDesc(id);
		if (entity.isPresent()) {
			return mapObjet(entity.get());
		} else {
			throw new NoSuchElementException();
		}
	}
}