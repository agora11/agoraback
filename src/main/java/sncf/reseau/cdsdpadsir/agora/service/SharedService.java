package sncf.reseau.cdsdpadsir.agora.service;

import java.util.HashSet;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sncf.reseau.cdsdpadsir.agora.entity.ClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.entity.StatutClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.entity.StatutTerme;
import sncf.reseau.cdsdpadsir.agora.entity.Terme;
import sncf.reseau.cdsdpadsir.agora.model.*;

@Service
public class SharedService {

	private ModelMapper modelMapper;

	@Autowired
	public SharedService(ModelMapper modelMapper) {
		super();
		this.modelMapper = modelMapper;
		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	public ObjetDto mapObjet(ClasseDobjet mapper) {
		ObjetDto objetDto = modelMapper.map(mapper, ObjetDto.class);
		objetDto.setFilieres(new HashSet<>());
		objetDto.setFamilles(new HashSet<>());
		objetDto.setAttributClasses(new HashSet<>());
		objetDto.setDefinitions(new HashSet<>());
		objetDto.setResponsables(new HashSet<>());
		objetDto.setTermes(new HashSet<>());

		if (mapper.getAssociationClasseDobjetFilieres() != null)
			mapper.getAssociationClasseDobjetFilieres().forEach(item -> objetDto.getFilieres().add(modelMapper.map(item.getFiliere(), FiliereDto.class)));
		if (mapper.getAssociationClasseDobjetFamilles() != null)
			mapper.getAssociationClasseDobjetFamilles().forEach(item -> objetDto.getFamilles().add(modelMapper.map(item.getFamille(), FamilleDto.class)));
		if (mapper.getAttributClasses() != null)
			mapper.getAttributClasses().forEach(item -> objetDto.getAttributClasses().add(modelMapper.map(item, AttributDto.class)));
		if (mapper.getDefinitionClasseDobjets() != null)
			mapper.getDefinitionClasseDobjets().forEach(item -> objetDto.getDefinitions().add(modelMapper.map(item, DefinitionDto.class)));
		if (mapper.getAssociationClasseDobjetUtilisateurs() != null)
			mapper.getAssociationClasseDobjetUtilisateurs().forEach(item -> objetDto.getResponsables()
						.add(modelMapper.map(item.getIdentiteNumerique().getUtilisateur(), UtilisateurDto.class)));

		if (mapper.getStatutClasseDobjets() != null) {
			for (StatutClasseDobjet item : mapper.getStatutClasseDobjets()) {
				StatutClasseDobjetDto statut = modelMapper.map(item, StatutClasseDobjetDto.class);
				statut.setValideur(modelMapper.map(item.getValideur().getUtilisateur(), UtilisateurDto.class));
				statut.setSuppleant(modelMapper.map(item.getSuppleant().getUtilisateur(), UtilisateurDto.class));
				statut.setLibelle(item.getValeurStatutClasseDobjet().getLibelle());
				objetDto.setStatut(statut);
			}
		}

		return objetDto;
	}

	public ObjetAssocAPIDTO mapObjetAPi(ObjetDto objet) {
		ObjetAssocAPIDTO objetAPI = modelMapper.map(objet, ObjetAssocAPIDTO.class);
		if (objet.getDefinitions() != null && !objet.getDefinitions().isEmpty()) {
			objetAPI.setDefinition(objet.getDefinitions().stream().findFirst().get().getDefinition());
		}
		if (objet.getFilieres() != null && !objet.getFilieres().isEmpty()) {
			FiliereStructApi struct = new FiliereStructApi();
			struct.setId(objet.getFilieres().stream().findFirst().get().getId());
			struct.setLibelle(objet.getFilieres().stream().findFirst().get().getNom());
			objetAPI.setFiliere(struct);
		}
		if (objet.getFamilles() != null && !objet.getFamilles().isEmpty()) {
			FamilleStructApi struct = new FamilleStructApi();
			struct.setId(objet.getFamilles().stream().findFirst().get().getId());
			struct.setLibelle(objet.getFamilles().stream().findFirst().get().getNom());
			if (objet.getFamilles().iterator().next().getSousFamilles() != null
					&& !objet.getFamilles().iterator().next().getSousFamilles().isEmpty()) {
				FamilleStructApi subStruct = new FamilleStructApi();
				subStruct.setId(objet.getFamilles().stream().findFirst().get().getSousFamilles().stream().findFirst()
						.get().getId());
				subStruct.setLibelle(objet.getFamilles().stream().findFirst().get().getSousFamilles().stream()
						.findFirst().get().getNom());
				objetAPI.setSousFamille(subStruct);
			}
			objetAPI.setFamille(struct);
		}

		return objetAPI;
	}

	public TermeDto mapTerme(Terme mapper) {
		TermeDto termeDto = modelMapper.map(mapper, TermeDto.class);
		termeDto.setFilieres(new HashSet<>());
		termeDto.setDocuments(new HashSet<>());
		termeDto.setUtilisateurs(new HashSet<>());
		termeDto.setDefinitions(new HashSet<>());
		termeDto.setObjets(new HashSet<>());

		if (mapper.getAssociationTermeFilieres() != null)
			mapper.getAssociationTermeFilieres()
					.forEach(item -> termeDto.getFilieres().add(modelMapper.map(item.getFiliere(), FiliereDto.class)));

		if (mapper.getAssociationTermeDocuments() != null)
			mapper.getAssociationTermeDocuments().forEach(
					item -> termeDto.getDocuments().add(modelMapper.map(item.getDocument(), DocumentDto.class)));

		if (mapper.getDefinitionTermes() != null)
			mapper.getDefinitionTermes()
					.forEach(item -> termeDto.getDefinitions().add(modelMapper.map(item, DefinitionDto.class)));

		if (mapper.getAssociationTermeUtilisateurs() != null)
			mapper.getAssociationTermeUtilisateurs().forEach(item -> termeDto.getUtilisateurs()
					.add(modelMapper.map(item.getIdentiteNumerique().getUtilisateur(), UtilisateurDto.class)));

		if (mapper.getStatutTermes() != null)
			mapper.getStatutTermes().forEach(
					item -> termeDto.setStatut(modelMapper.map(item.getIdStatutTerme(), StatutTermeDto.class)));

		if (mapper.getStatutTermes() != null) {
			for (StatutTerme item : mapper.getStatutTermes()) {
				StatutTermeDto statut = modelMapper.map(item, StatutTermeDto.class);
				statut.setValideur(modelMapper.map(item.getValideur().getUtilisateur(), UtilisateurDto.class));
				statut.setSuppleant(modelMapper.map(item.getSuppleant().getUtilisateur(), UtilisateurDto.class));
				statut.setLibelle(item.getValeurStatutTerme().getLibelle());
				termeDto.setStatut(statut);
			}
		}

		return termeDto;
	}

	public ObjetAssocAPIDTO mapTermeAPI(TermeDto terme) {
		ObjetAssocAPIDTO termeAPI = modelMapper.map(terme, ObjetAssocAPIDTO.class);
		if (terme.getDefinitions() != null && !terme.getDefinitions().isEmpty()) {
			termeAPI.setDefinition(terme.getDefinitions().stream().findFirst().get().getDefinition());
		}
		if (terme.getFilieres() != null && !terme.getFilieres().isEmpty()) {
			FiliereStructApi struct = new FiliereStructApi();
			struct.setId(terme.getFilieres().stream().findFirst().get().getId());
			struct.setLibelle(terme.getFilieres().stream().findFirst().get().getNom());
			termeAPI.setFiliere(struct);
		}

		return termeAPI;
	}

}
