package sncf.reseau.cdsdpadsir.agora.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sncf.reseau.cdsdpadsir.agora.entity.TypeAttributClasse;
import sncf.reseau.cdsdpadsir.agora.model.TypeAttributClasseDto;
import sncf.reseau.cdsdpadsir.agora.repository.TypeAttributClasseRepository;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;

@Service
public class TypeAttributClasseService {

	private TypeAttributClasseRepository typeAttributClasseRepository;
	private ModelMapper modelMapper;

	@Autowired
	public TypeAttributClasseService(TypeAttributClasseRepository typeAttributClasseRepository,
			ModelMapper modelMapper) {
		this.typeAttributClasseRepository = typeAttributClasseRepository;
		this.modelMapper = modelMapper;
		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}
		
	public Optional<TypeAttributClasseDto> getByTypeAttribut(String type) {
		Optional<TypeAttributClasse> typeAttribut = typeAttributClasseRepository.getByType(type);		
		 return typeAttribut.isPresent() ? Optional.of(typeAttribut.get().mapToDto())  : Optional.empty();
	}

	@Transactional
	public TypeAttributClasseDto add(TypeAttributClasseDto typeAttributDto) {
		Optional<TypeAttributClasseDto> typeExist = getByTypeAttribut(typeAttributDto.getType());
		if (typeExist.isPresent()) {
			return typeExist.get();
		} else {
			ArianeHelper.setNewEntityDetails(typeAttributDto);			
			TypeAttributClasse typeAttributClasse = typeAttributClasseRepository.save(modelMapper.map(typeAttributDto, TypeAttributClasse.class));			
			return null !=typeAttributDto ? modelMapper.map(typeAttributClasse, TypeAttributClasseDto.class) : null;
			
		}
	}
	
}
