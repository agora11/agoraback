package sncf.reseau.cdsdpadsir.agora.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import sncf.reseau.cdsdpadsir.agora.entity.Domaine;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.model.DomaineDto;
import sncf.reseau.cdsdpadsir.agora.repository.DomaineRepository;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DomaineService {

    @Autowired
    private DomaineRepository domaineRepository;

    @Autowired
    private ModelMapper modelMapper;


    public PagingResponse<DomaineDto> findAll(Specification<Domaine> domaineSpec, Sort sort, @Valid BigDecimal page,
                                              @Valid BigDecimal size) {

        if (isRequestPaged(page, size))
            return findAll(domaineSpec, buildPageRequest(page, size, sort));

        List<DomaineDto> entities = findAll(domaineSpec, sort);
        return new PagingResponse<>((long) entities.size(), 0L, 0L, 0L, 0L, entities);
    }

    private PagingResponse<DomaineDto> findAll(Specification<Domaine> spec, Pageable pageable) {
        Page<Domaine> page = domaineRepository.findAll(spec, pageable);
        List<Domaine> content = page.getContent();
        return new PagingResponse<>(page.getTotalElements(), (long) page.getNumber(), (long) page.getNumberOfElements(),
                pageable.getOffset(), (long) page.getTotalPages(),
                content.stream().map(mapper -> modelMapper.map(mapper, DomaineDto.class)).collect(Collectors.toList()));
    }

    public List<DomaineDto> findAll(Specification<Domaine> spec, Sort sort) {
        return domaineRepository.findAll(spec, sort).stream().map(mapper -> modelMapper.map(mapper, DomaineDto.class))
                .collect(Collectors.toList());
    }

    private boolean isRequestPaged(BigDecimal page, BigDecimal size) {
        return (page != null && size != null);
    }

    private Pageable buildPageRequest(BigDecimal page, BigDecimal size, Sort sort) {
        return PageRequest.of(page.intValue(), size.intValue(), sort);
    }
}
