package sncf.reseau.cdsdpadsir.agora.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sncf.reseau.cdsdpadsir.agora.entity.Acteur;
import sncf.reseau.cdsdpadsir.agora.entity.AssociationActeurGisement;
import sncf.reseau.cdsdpadsir.agora.entity.AssociationFiliereGisement;
import sncf.reseau.cdsdpadsir.agora.entity.AssociationLienUtileGisement;
import sncf.reseau.cdsdpadsir.agora.entity.Filiere;
import sncf.reseau.cdsdpadsir.agora.entity.Gisement;
import sncf.reseau.cdsdpadsir.agora.entity.LienUtile;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;
import sncf.reseau.cdsdpadsir.agora.model.ActeurDto;
import sncf.reseau.cdsdpadsir.agora.model.FiliereGisementDto;
import sncf.reseau.cdsdpadsir.agora.model.GisementDto;
import sncf.reseau.cdsdpadsir.agora.model.LienUtileDto;
import sncf.reseau.cdsdpadsir.agora.repository.ActeurRepository;
import sncf.reseau.cdsdpadsir.agora.repository.AssociationActeurGisementRepository;
import sncf.reseau.cdsdpadsir.agora.repository.AssociationFiliereGisementRepository;
import sncf.reseau.cdsdpadsir.agora.repository.AssociationLienUtileGisementRepository;
import sncf.reseau.cdsdpadsir.agora.repository.GisementRepository;
import sncf.reseau.cdsdpadsir.agora.repository.LienUtileRepository;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;

@Service
public class GisementService {

	private ModelMapper modelMapper;
	private GisementRepository gisementRepository;
	private ActeurRepository acteurRepository;
	private LienUtileRepository lienUtileRepository;
	private AssociationActeurGisementRepository associationActeurGisementRepository;
	private AssociationFiliereGisementRepository associationFiliereGisementRepository;
	private AssociationLienUtileGisementRepository associationLienUtileGisementRepository;

	@Autowired
	public GisementService(GisementRepository gisementRepository, ActeurRepository acteurRepository,
			AssociationActeurGisementRepository associationActeurGisementRepository,
			AssociationFiliereGisementRepository associationFiliereGisementRepository,
			LienUtileRepository lienUtileRepository,
			AssociationLienUtileGisementRepository associationLienUtileGisementRepository, ModelMapper modelMapper) {
		super();
		this.modelMapper = modelMapper;
		this.gisementRepository = gisementRepository;
		this.lienUtileRepository = lienUtileRepository;
		this.acteurRepository = acteurRepository;
		this.associationActeurGisementRepository = associationActeurGisementRepository;
		this.associationLienUtileGisementRepository = associationLienUtileGisementRepository;
		this.associationFiliereGisementRepository = associationFiliereGisementRepository;
		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	public PagingResponse<GisementDto> findAll(Specification<Gisement> gisementSpec, Sort sort, @Valid BigDecimal page,
			@Valid BigDecimal size) {

		if (isRequestPaged(page, size))
			return findAll(gisementSpec, buildPageRequest(page, size, sort));

		List<GisementDto> entities = findAll(gisementSpec, sort);
		return new PagingResponse<>((long) entities.size(), 0L, 0L, 0L, 0L, entities);
	}

	public List<GisementDto> findAll(Specification<Gisement> spec, Sort sort) {
		return gisementRepository.findAll(spec, sort).stream().map(this::mapGisement).collect(Collectors.toList());
	}

	private boolean isRequestPaged(BigDecimal page, BigDecimal size) {
		return (page != null && size != null);
	}

	private Pageable buildPageRequest(BigDecimal page, BigDecimal size, Sort sort) {
		return PageRequest.of(page.intValue(), size.intValue(), sort);
	}

	private PagingResponse<GisementDto> findAll(Specification<Gisement> spec, Pageable pageable) {
		Page<Gisement> page = gisementRepository.findAll(spec, pageable);
		List<Gisement> content = page.getContent();
		return new PagingResponse<>(page.getTotalElements(), (long) page.getNumber(), (long) page.getNumberOfElements(),
				pageable.getOffset(), (long) page.getTotalPages(),
				content.stream().map(this::mapGisement).collect(Collectors.toList()));
	}

	@Transactional
	public GisementDto add(GisementDto gisementDto) {
		ArianeHelper.setNewEntityDetails(gisementDto);
		Gisement model = modelMapper.map(gisementDto, Gisement.class);
		Gisement gisement = gisementRepository.save(model);
		updateJoins(gisementDto, model, gisement);
		return mapGisement(model);
	}

	@Transactional
	public GisementDto update(GisementDto gisementDto) throws ConflictException {
		Optional<Gisement> entity = gisementRepository.findByIdGisement(gisementDto.getIdGisement());

		if (entity.isPresent()) {

			Gisement gisement = new Gisement();
			modelMapper.map(entity.get(), gisement);
			gisement.setIdGisement(null);

			gisement.setLibelle(gisementDto.getLibelle());

			gisement.setNiveauDeCompletude(gisementDto.getNiveauDeCompletude());

			gisement.setNombreDeJeuxDonnees(gisementDto.getNombreDeJeuxDonnees());

			gisement.setNombreDeClients(gisementDto.getNombreDeClients());

			gisement.setNombreDeFournisseurs(gisementDto.getNombreDeFournisseurs());

			gisement.setNombreDeServices(gisementDto.getNombreDeServices());

			gisement.setNombreObjetsReferences(gisementDto.getNombreObjetsReferences());

			gisement.setRaisonEtre(gisementDto.getRaisonEtre());

			gisement.setRaisonEtreCourte(gisementDto.getRaisonEtreCourte());

			gisement.setServicesDocumentes(gisementDto.getServicesDocumentes());

			gisement.setStatut(gisementDto.getStatut());

			gisement.setDescription(gisementDto.getDescription());

			gisement.setDateMiseEnService(gisementDto.getDateMiseEnService());

			gisement.setDetailsClientsExternes(gisementDto.getDetailsClientsExternes());

			gisement.setNombreClientsInternes(gisementDto.getNombreClientsInternes());

			gisement.setNombreClientsExternes(gisementDto.getNombreClientsExternes());

			gisement.setNombreServicesWebExposesDataLab(gisementDto.getNombreServicesWebExposesDataLab());

			gisement.setNombreObjetsReferencesEnProjet(gisementDto.getNombreObjetsReferencesEnProjet());

			gisement.setNombreServicesEnProjet(gisementDto.getNombreServicesEnProjet());

			gisement.setNombreServicesWebExposesDataLabEnProjet(
					gisementDto.getNombreServicesWebExposesDataLabEnProjet());

			ArianeHelper.updateEntityDetails(gisementDto, gisement);
			gisement = gisementRepository.save(gisement);
			updateJoins(gisementDto, gisement, gisement);
			updateEndDate(entity);

			return mapGisement(gisement);
		} else {
			throw new NoSuchElementException();
		}

	}

	public boolean delete(long id) {
		Optional<Gisement> entity = gisementRepository.findByIdGisement(id);
		if (entity.isPresent()) {
			updateEndDate(entity);
			return true;
		} else {
			throw new NoSuchElementException();
		}
	}

	private void updateJoins(GisementDto gisementDto, Gisement model, Gisement gisement) {

		boolean update = false;

		if (model.getAssociationActeurGisements() != null || model.getAssociationFiliereGisements() != null
				|| model.getAssociationLienUtileGisements() != null) {
			update = true;
		}
		final boolean flag = update;

		model.setAssociationActeurGisements(new HashSet<>());
		if (gisementDto.getActeurs() != null) {
			gisementDto.getActeurs().forEach(item -> {
				Acteur acteur = modelMapper.map(item, Acteur.class);
				acteur.setIdActeur(null);
				ariane(gisementDto, acteur, flag);
				AssociationActeurGisement acteurGisement = new AssociationActeurGisement();
				acteurGisement.setGisement(gisement);
				acteurGisement.setActeur(acteurRepository.save(acteur));
				ariane(gisementDto, acteurGisement, flag);
				associationActeurGisementRepository.save(acteurGisement);
				model.getAssociationActeurGisements().add(acteurGisement);
			});
		}

		model.setAssociationFiliereGisements(new HashSet<>());
		if (gisementDto.getFilieres() != null) {
			gisementDto.getFilieres().forEach(item -> {
				AssociationFiliereGisement filiereGisement = new AssociationFiliereGisement();
				filiereGisement.setGisement(gisement);
				filiereGisement.setFiliere(modelMapper.map(item, Filiere.class));
				ariane(gisementDto, filiereGisement, flag);
				associationFiliereGisementRepository.save(filiereGisement);
				model.getAssociationFiliereGisements().add(filiereGisement);
			});
		}

		model.setAssociationLienUtileGisements(new HashSet<>());
		if (gisementDto.getLiensUtiles() != null) {
			gisementDto.getLiensUtiles().forEach(item -> {
				LienUtile lienUtile = modelMapper.map(item, LienUtile.class);
				ariane(gisementDto, lienUtile, flag);
				AssociationLienUtileGisement associationLienUtileGisement = new AssociationLienUtileGisement();
				associationLienUtileGisement.setGisement(gisement);
				associationLienUtileGisement.setLienUtile(lienUtileRepository.save(lienUtile));
				ariane(gisementDto, associationLienUtileGisement, flag);
				associationLienUtileGisement = associationLienUtileGisementRepository
						.save(associationLienUtileGisement);
				model.getAssociationLienUtileGisements().add(associationLienUtileGisement);
			});
		}

	}

	private void updateEndDate(Optional<Gisement> entity) {

		entity.get().getAssociationActeurGisements().forEach(item -> {
			item.setDateFinActivite(LocalDateTime.now());
			item.getActeur().setDateDebutActivite(LocalDateTime.now());
		});
		acteurRepository.saveAll(entity.get().getAssociationActeurGisements().stream().map(item -> item.getActeur())
				.collect(Collectors.toList()));
		associationActeurGisementRepository.saveAll(entity.get().getAssociationActeurGisements());

		entity.get().getAssociationFiliereGisements().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
		associationFiliereGisementRepository.saveAll(entity.get().getAssociationFiliereGisements());

		entity.get().getAssociationLienUtileGisements().forEach(item -> {
			item.getLienUtile().setDateDebutActivite(LocalDateTime.now());
			item.setDateFinActivite(LocalDateTime.now());
		});
		lienUtileRepository.saveAll(entity.get().getAssociationLienUtileGisements().stream()
				.map(item -> item.getLienUtile()).collect(Collectors.toList()));
		associationLienUtileGisementRepository.saveAll(entity.get().getAssociationLienUtileGisements());

		entity.get().setDateFinActivite(LocalDateTime.now());
		gisementRepository.save(entity.get());

	}

	private GisementDto mapGisement(Gisement gisement) {

		GisementDto gisementDto = modelMapper.map(gisement, GisementDto.class);
		gisementDto.setActeurs(new ArrayList<>());
		gisementDto.setLiensUtiles(new ArrayList<>());
		gisementDto.setFilieres(new ArrayList<>());

		if (gisement.getAssociationActeurGisements() != null)
			gisement.getAssociationActeurGisements().forEach(item -> {
				if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
					ActeurDto acteurDto = modelMapper.map(item.getActeur(), ActeurDto.class);
					gisementDto.getActeurs().add(acteurDto);
				}
			});

		if (gisement.getAssociationLienUtileGisements() != null)
			gisement.getAssociationLienUtileGisements().forEach(item -> {
				if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
					gisementDto.getLiensUtiles().add(modelMapper.map(item.getLienUtile(), LienUtileDto.class));
				}
			});

		if (gisement.getAssociationFiliereGisements() != null)
			gisement.getAssociationFiliereGisements().forEach(item -> {
				if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
					gisementDto.getFilieres().add(modelMapper.map(item.getFiliere(), FiliereGisementDto.class));
				}
			});

		return gisementDto;

	}

	<T extends RessourceAbstraite> void ariane(T obj, T response, boolean flag) {
		if (!flag) {
			ArianeHelper.setNewEntityDetails(response);
		} else {
			ArianeHelper.updateEntityDetails(obj, response);
		}
	}

	public ActeurDto deleteActeur(Long id) {
		Optional<Acteur> acteur = acteurRepository.findById(id);
		if (!acteur.isPresent()) {
			throw new NoSuchElementException();
		}
		acteur.get().getAssociationActeurGisements().forEach(item -> {
			item.getActeur().setDateFinActivite(ArianeHelper.FUTURE_DATE);
			item.setDateFinActivite(ArianeHelper.FUTURE_DATE);
		});

		acteurRepository.saveAll(acteur.get().getAssociationActeurGisements().stream().map(item -> item.getActeur())
				.collect(Collectors.toList()));
		associationActeurGisementRepository.saveAll(acteur.get().getAssociationActeurGisements());

		return modelMapper.map(acteur, ActeurDto.class);
	}

	public ActeurDto deleteLienUtile(Long id) {
		Optional<LienUtile> lienUtile = lienUtileRepository.findById(id);
		if (!lienUtile.isPresent()) {
			throw new NoSuchElementException();
		}
		lienUtile.get().getAssociationLienUtileGisements().forEach(item -> {
			item.getLienUtile().setDateFinActivite(ArianeHelper.FUTURE_DATE);
			item.setDateFinActivite(ArianeHelper.FUTURE_DATE);
		});

		lienUtileRepository.saveAll(lienUtile.get().getAssociationLienUtileGisements().stream()
				.map(item -> item.getLienUtile()).collect(Collectors.toList()));
		associationLienUtileGisementRepository.saveAll(lienUtile.get().getAssociationLienUtileGisements());

		return modelMapper.map(lienUtile, ActeurDto.class);
	}
}
