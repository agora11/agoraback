package sncf.reseau.cdsdpadsir.agora.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sncf.reseau.cdsdpadsir.agora.entity.ClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.entity.Paquet;
import sncf.reseau.cdsdpadsir.agora.model.ClasseDobjetDto;
import sncf.reseau.cdsdpadsir.agora.repository.ClasseDobjetRepository;
import sncf.reseau.cdsdpadsir.agora.repository.PaquetRepository;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;

@Service
public class ClasseDobjetService {
	
	private ClasseDobjetRepository classeDobjetRepository;
	private PaquetRepository paquetRepository ;
	private ModelMapper modelMapper;
	
	@Autowired
	public ClasseDobjetService(
			ClasseDobjetRepository classeDobjetRepository,
			PaquetRepository paquetRepository ,
			 ModelMapper modelMapper) {
		
		this.classeDobjetRepository = classeDobjetRepository ;
		this.modelMapper = modelMapper ;
		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STANDARD);		
		this.paquetRepository = paquetRepository ;
		
	}
	
	public List<ClasseDobjetDto> addAll(List<ClasseDobjetDto> classesDobjetDto){
		 
		return classesDobjetDto
			.stream()
			.map(classe -> add(classe)).collect(Collectors.toList());
	}
	
	@Transactional
	public ClasseDobjetDto add(ClasseDobjetDto classeDobjetDto){
		
		classeDobjetDto.setDateCreation(LocalDateTime.now());
		classeDobjetDto.setRevisionFonctionnel(Timestamp.valueOf(LocalDateTime.now()));
		ClasseDobjet classeEntity = modelMapper.map(classeDobjetDto, ClasseDobjet.class);
		List<Paquet> paquets = null ;
		if(null != classeDobjetDto.getPaquet()) {			
			paquets = paquetRepository.findByIdExterneAndMaxRevisionFonctionnelle(classeDobjetDto.getPaquet().getIdExterne());
			if(null != paquets && !paquets.isEmpty()) {
				classeEntity.setPaquet(paquets.get(0));
			}
		}
		
		// get id agora id exist
		List<ClasseDobjet> classesDobjetAgora = classeDobjetRepository.findByIdExterneAndMaxRevisionFonctionnelle(classeDobjetDto.getIdExterne()) ;		

		if(null != classesDobjetAgora && !classesDobjetAgora.isEmpty()) {
			ArianeHelper.updateEntityDetails(classesDobjetAgora.get(0), classeEntity);
			classesDobjetAgora.get(0).setDateFinActivite(LocalDateTime.now());
			classeDobjetRepository.save(classesDobjetAgora.get(0));
		}else {
			ArianeHelper.setNewEntityDetails(classeEntity);
		}
		classeEntity.setRevisionFonctionnel(Timestamp.valueOf(LocalDateTime.now()));
		
		classeEntity = classeDobjetRepository.save(classeEntity);
		classeDobjetRepository.flush();
		return classeEntity != null ? classeEntity.mapToDto() : null  ;
		
	}
	
	public List<ClasseDobjetDto> getByIdExterneAndVersionRevisionFonctionnel(String idExterne , String version , Timestamp revisionFonctionnel){
		return classeDobjetRepository
				.findByIdExterneVersionRevisionFonctionnel(idExterne, version, revisionFonctionnel)
				.stream()
				.map(classeDobjet -> classeDobjet.mapToDto())
				.collect(Collectors.toList());
	}
	
	public List<ClasseDobjetDto> getByIdExterneAndMaxRevisionFonctionnelle(String idExterne){
		return classeDobjetRepository
				.findByIdExterneAndMaxRevisionFonctionnelle(idExterne)
				.stream()
				.map(classeDobjet -> classeDobjet.mapToDto())
				.collect(Collectors.toList());
	}
	

}
