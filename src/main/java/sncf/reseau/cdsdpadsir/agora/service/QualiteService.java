package sncf.reseau.cdsdpadsir.agora.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javassist.NotFoundException;
import sncf.reseau.cdsdpadsir.agora.entity.ClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.entity.Critere;
import sncf.reseau.cdsdpadsir.agora.entity.CritereQualite;
import sncf.reseau.cdsdpadsir.agora.entity.PolitiqueQualite;
import sncf.reseau.cdsdpadsir.agora.entity.PolitiqueQualiteParUsage;
import sncf.reseau.cdsdpadsir.agora.entity.TauxQualiteMesure;
import sncf.reseau.cdsdpadsir.agora.entity.UsageMetier;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.model.CritereQualiteDto;
import sncf.reseau.cdsdpadsir.agora.model.ObjetDto;
import sncf.reseau.cdsdpadsir.agora.model.PolitiqueQualiteDto;
import sncf.reseau.cdsdpadsir.agora.model.PolitiqueQualiteParUsageDto;
import sncf.reseau.cdsdpadsir.agora.model.TauxQualiteMesureDto;
import sncf.reseau.cdsdpadsir.agora.repository.CritereQualiteRepository;
import sncf.reseau.cdsdpadsir.agora.repository.CritereRepository;
import sncf.reseau.cdsdpadsir.agora.repository.ObjetRepository;
import sncf.reseau.cdsdpadsir.agora.repository.PolitiqueQualiteParUsageRepository;
import sncf.reseau.cdsdpadsir.agora.repository.PolitiqueQualiteRepository;
import sncf.reseau.cdsdpadsir.agora.repository.TauxQualiteMesureRepository;
import sncf.reseau.cdsdpadsir.agora.repository.UsageMetierRepository;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;

@Service
public class QualiteService {

	private ModelMapper modelMapper;
	private CritereQualiteRepository critereQualiteRepository;
	private PolitiqueQualiteParUsageRepository politiqueQualiteParUsageRepository;
	private PolitiqueQualiteRepository politiqueQualiteRepository;
	private TauxQualiteMesureRepository tauxQualiteMesureRepository;
	private UsageMetierRepository usageMetierRepository;
	private CritereRepository critereRepository;
	private ObjetRepository objetRepository;

	@Autowired
	public QualiteService(ModelMapper modelMapper, CritereQualiteRepository critereQualiteRepository,
			PolitiqueQualiteParUsageRepository politiqueQualiteParUsageRepository,
			UsageMetierRepository usageMetierRepository, PolitiqueQualiteRepository politiqueQualiteRepository,
			CritereRepository critereRepository, TauxQualiteMesureRepository tauxQualiteMesureRepository,
			ObjetRepository objetRepository) {
		super();
		this.modelMapper = modelMapper;
		this.critereQualiteRepository = critereQualiteRepository;
		this.politiqueQualiteParUsageRepository = politiqueQualiteParUsageRepository;
		this.politiqueQualiteRepository = politiqueQualiteRepository;
		this.tauxQualiteMesureRepository = tauxQualiteMesureRepository;
		this.usageMetierRepository = usageMetierRepository;
		this.critereRepository = critereRepository;
		this.objetRepository = objetRepository;
		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	public PagingResponse<ObjetDto> findAll(Specification<ClasseDobjet> objetSpec, Sort sort, @Valid BigDecimal page,
			@Valid BigDecimal size) {

		if (isRequestPaged(page, size))
			return findAll(buildPageRequest(page, size, sort));

		List<ObjetDto> entities = findAll(sort);
		return new PagingResponse<>((long) entities.size(), 0L, 0L, 0L, 0L, entities);
	}

	public List<ObjetDto> findAll(Sort sort) {
		return objetRepository.findObjetsFonctionnels(ArianeHelper.FUTURE_DATE).stream().map(mapper -> mapObjet(mapper))
				.collect(Collectors.toList());
	}

	private boolean isRequestPaged(BigDecimal page, BigDecimal size) {
		return (page != null && size != null);
	}

	private Pageable buildPageRequest(BigDecimal page, BigDecimal size, Sort sort) {
		return PageRequest.of(page.intValue(), size.intValue(), sort);
	}

	private PagingResponse<ObjetDto> findAll(Pageable pageable) {
		Page<ClasseDobjet> page = objetRepository.findObjetsFonctionnels(ArianeHelper.FUTURE_DATE, pageable);
		List<ClasseDobjet> content = page.getContent();
		return new PagingResponse<>(page.getTotalElements(), (long) page.getNumber(), (long) page.getNumberOfElements(),
				pageable.getOffset(), (long) page.getTotalPages(),
				content.stream().map(mapper -> mapObjet(mapper)).collect(Collectors.toList()));
	}

	public void updateObjectsJoins(List<ClasseDobjet> objectsToUpdate, List<ClasseDobjet> newObjects) {

		for (ClasseDobjet classeDobjet : objectsToUpdate) {
			ClasseDobjet obj = newObjects.stream().filter(predicate -> predicate.getId().equals(classeDobjet.getId()))
					.collect(Collectors.toList()).get(0);
			if (obj != null) {
				updateJoins(classeDobjet, obj);
			}
		}

	}

	public ObjetDto mapObjet(ClasseDobjet objet) {
		ObjetDto dto = new ObjetDto();
		modelMapper.map(objet, dto);
		dto.setCritereQualites(new HashSet<>());

		if (objet.getCritereQualites() != null) {
			for (CritereQualite critereQualite : objet.getCritereQualites().stream()
					.filter(critere -> critere.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE))
					.collect(Collectors.toSet())) {

				CritereQualiteDto critereQualiteDto = new CritereQualiteDto();
				modelMapper.map(critereQualite, critereQualiteDto);
				critereQualiteDto.setPolitiqueQualites(new ArrayList<>());
				dto.getCritereQualites().add(critereQualiteDto);

				if (critereQualite.getPolitiqueQualites() != null) {
					for (PolitiqueQualite politiqueQualite : critereQualite.getPolitiqueQualites()) {

						PolitiqueQualiteDto politiqueQualiteDto = new PolitiqueQualiteDto();
						modelMapper.map(politiqueQualite, politiqueQualiteDto);
						critereQualiteDto.getPolitiqueQualites().add(politiqueQualiteDto);
						politiqueQualiteDto.setPolitiqueQualiteParUsages(new ArrayList<>());
						politiqueQualiteDto.setTauxQualiteMesures(new ArrayList<>());

						if (politiqueQualite.getPolitiqueQualiteParUsages() != null) {
							for (PolitiqueQualiteParUsage politiqueQualiteParUsage : politiqueQualite
									.getPolitiqueQualiteParUsages()) {
								PolitiqueQualiteParUsageDto politiqueQualiteParUsagedto = new PolitiqueQualiteParUsageDto();
								modelMapper.map(politiqueQualiteParUsage, politiqueQualiteParUsagedto);
								politiqueQualiteDto.getPolitiqueQualiteParUsages().add(politiqueQualiteParUsagedto);
							}
						}

						if (politiqueQualite.getTauxQualiteMesures() != null) {
							for (TauxQualiteMesure tauxQualiteMesure : politiqueQualite.getTauxQualiteMesures()) {

								TauxQualiteMesureDto tauxQualiteMesureDto = new TauxQualiteMesureDto();
								modelMapper.map(tauxQualiteMesure, tauxQualiteMesureDto);
								politiqueQualiteDto.getTauxQualiteMesures().add(tauxQualiteMesureDto);
							}
						}
					}
				}

			}
		}

		if (dto.getCritereQualites() != null) {
			dto.getCritereQualites().forEach(critere -> {
				if (critere.getPolitiqueQualites() != null) {
					critere.getPolitiqueQualites().forEach(politique -> {
						if (politique.getTauxQualiteMesures() != null) {
							Collections.sort(politique.getTauxQualiteMesures());
						}
					});
				}
			});
		}

		return dto;
	}

	private ClasseDobjet mapObjetDto(ObjetDto objetDto) {
		ClasseDobjet objet = new ClasseDobjet();
		modelMapper.map(objetDto, objet);
		objet.setCritereQualites(new HashSet<>());

		for (CritereQualiteDto critereQualiteDto : objetDto.getCritereQualites()) {

			CritereQualite critereQualite = new CritereQualite();
			modelMapper.map(critereQualiteDto, critereQualite);
			critereQualite.setPolitiqueQualites(new HashSet<>());
			objet.getCritereQualites().add(critereQualite);

			for (PolitiqueQualiteDto politiqueQualiteDto : critereQualiteDto.getPolitiqueQualites()) {

				PolitiqueQualite politiqueQualite = new PolitiqueQualite();
				modelMapper.map(politiqueQualiteDto, politiqueQualite);
				critereQualite.getPolitiqueQualites().add(politiqueQualite);
				politiqueQualite.setPolitiqueQualiteParUsages(new HashSet<>());
				politiqueQualite.setTauxQualiteMesures(new HashSet<>());

				for (PolitiqueQualiteParUsageDto politiqueQualiteParUsageDto : politiqueQualiteDto
						.getPolitiqueQualiteParUsages()) {
					PolitiqueQualiteParUsage politiqueQualiteParUsage = new PolitiqueQualiteParUsage();
					modelMapper.map(politiqueQualiteParUsageDto, politiqueQualiteParUsage);
					politiqueQualite.getPolitiqueQualiteParUsages().add(politiqueQualiteParUsage);
				}

				for (TauxQualiteMesureDto tauxQualiteMesureDto : politiqueQualiteDto.getTauxQualiteMesures()) {

					TauxQualiteMesure tauxQualiteMesure = new TauxQualiteMesure();
					modelMapper.map(tauxQualiteMesureDto, tauxQualiteMesure);
					politiqueQualite.getTauxQualiteMesures().add(tauxQualiteMesure);
				}

			}
		}

		return objet;
	}

	@Transactional(propagation = org.springframework.transaction.annotation.Propagation.REQUIRES_NEW)
	public ObjetDto addCritere(ObjetDto objetDto) throws NotFoundException {
		Optional<ClasseDobjet> classe = objetRepository.findById(objetDto.getIdClasseDobjet());

		if (!classe.isPresent()) {
			throw new NotFoundException("Objet introuvable dans la base", new NoSuchElementException());
		}

		if (objetDto.getCriteresToDelete() != null) {
			List<CritereQualite> criteres = critereQualiteRepository.findAllById(objetDto.getCriteresToDelete().stream()
					.map(CritereQualiteDto::getIdCritereQualite).collect(Collectors.toList()));
			for (CritereQualite critere : criteres) {
				archiverCriteres(critere);
			}
		}

		if (objetDto.getCritereQualites() != null) {
			for (CritereQualiteDto critereQualiteDto : objetDto.getCritereQualites()) {
				CritereQualite cq = new CritereQualite();
				cq.setClasseDobjet(classe.get());
				modelMapper.map(critereQualiteDto, cq);
				cq.setIdCritereQualite(null);
				if (critereQualiteDto.getIdCritereQualite() != null) {
					Optional<CritereQualite> oldCritere = critereQualiteRepository
							.findById(critereQualiteDto.getIdCritereQualite());
					if (oldCritere.isPresent()) {
						ArianeHelper.updateEntityDetails(oldCritere.get(), cq);
						oldCritere.get().setDateFinActivite(ArianeHelper.getCurrentDate());
						critereQualiteRepository.save(oldCritere.get());
					}
				} else {
					ArianeHelper.setNewEntityDetails(cq);
				}

				cq = critereQualiteRepository.save(cq);

				if (critereQualiteDto.getPolitiqueQualites() != null) {
					for (PolitiqueQualiteDto politiqueQualiteDto : critereQualiteDto.getPolitiqueQualites()) {
						PolitiqueQualite pq = new PolitiqueQualite();
						modelMapper.map(politiqueQualiteDto, pq);
						pq.setIdPolitiqueQualite(null);
						if (politiqueQualiteDto.getIdPolitiqueQualite() != null) {
							Optional<PolitiqueQualite> oldPolitiqueQualite = politiqueQualiteRepository
									.findById(politiqueQualiteDto.getIdPolitiqueQualite());
							if (oldPolitiqueQualite.isPresent()) {
								ArianeHelper.updateEntityDetails(oldPolitiqueQualite.get(), pq);
								oldPolitiqueQualite.get().setDateFinActivite(ArianeHelper.getCurrentDate());
								politiqueQualiteRepository.save(oldPolitiqueQualite.get());
							}
						} else {
							ArianeHelper.setNewEntityDetails(pq);
						}

						pq.setCritereQualite(cq);
						pq = politiqueQualiteRepository.save(pq);

						if (politiqueQualiteDto.getPolitiqueQualiteParUsages() != null) {
							for (PolitiqueQualiteParUsageDto politiqueQualiteParUsageDto : politiqueQualiteDto
									.getPolitiqueQualiteParUsages()) {
								PolitiqueQualiteParUsage pqpu = new PolitiqueQualiteParUsage();
								modelMapper.map(politiqueQualiteParUsageDto, pqpu);
								pqpu.setIdPolitiqueQualiteParUsage(null);
								if (politiqueQualiteParUsageDto.getIdPolitiqueQualiteParUsage() != null) {
									Optional<PolitiqueQualiteParUsage> oldPolitiqueQualiteParUsage = politiqueQualiteParUsageRepository
											.findById(politiqueQualiteParUsageDto.getIdPolitiqueQualiteParUsage());
									if (oldPolitiqueQualiteParUsage.isPresent()) {
										ArianeHelper.updateEntityDetails(oldPolitiqueQualiteParUsage.get(), pqpu);
										oldPolitiqueQualiteParUsage.get()
												.setDateFinActivite(ArianeHelper.getCurrentDate());
										politiqueQualiteParUsageRepository.save(oldPolitiqueQualiteParUsage.get());
									}
								} else {
									ArianeHelper.setNewEntityDetails(pqpu);
								}
								Optional<UsageMetier> usageMetier = usageMetierRepository
										.findById(politiqueQualiteParUsageDto.getUsageMetier().getIdUsageMetier());
								pqpu.setUsageMetier(usageMetier.get());
								pqpu.setPolitiqueQualite(pq);
								politiqueQualiteParUsageRepository.save(pqpu);
							}
						}

						if (politiqueQualiteDto.getTauxQualiteMesures() != null) {
							for (TauxQualiteMesureDto tauxQualiteMesure : politiqueQualiteDto.getTauxQualiteMesures()) {
								TauxQualiteMesure tq = new TauxQualiteMesure();
								modelMapper.map(tauxQualiteMesure, tq);
								tq.setIdTauxQualiteMesure(null);
								if (tauxQualiteMesure.getIdTauxQualiteMesure() != null) {
									Optional<TauxQualiteMesure> oldTauxQualiteMesure = tauxQualiteMesureRepository
											.findById(tauxQualiteMesure.getIdTauxQualiteMesure());
									if (oldTauxQualiteMesure.isPresent()) {
										ArianeHelper.updateEntityDetails(oldTauxQualiteMesure.get(), tq);
										oldTauxQualiteMesure.get().setDateFinActivite(ArianeHelper.getCurrentDate());
										tauxQualiteMesureRepository.save(oldTauxQualiteMesure.get());
									}
								} else {
									ArianeHelper.setNewEntityDetails(tq);
								}

								tq.setPolitiqueQualite(pq);
								tauxQualiteMesureRepository.save(tq);
							}
						}
					}
				}
			}
		}

		return objetDto;
	}

	@Transactional
	public ObjetDto updateCritere(ObjetDto objetDto) {
		Optional<ClasseDobjet> oldObjet = objetRepository.findById(objetDto.getIdClasseDobjet());
		ClasseDobjet newObjet = mapObjetDto(objetDto);
		if (oldObjet.isPresent()) {
			this.updateJoins(oldObjet.get(), newObjet);
		}
		return mapObjet(objetRepository.findById(objetDto.getIdClasseDobjet()).get());
	}

	@Transactional
	public ObjetDto archiver(long id) {

		Optional<ClasseDobjet> objet = objetRepository.findById(id);

		if (!objet.isPresent())
			throw new NoSuchElementException();

		for (CritereQualite critereQualite : objet.get().getCritereQualites()) {
			archiverCriteres(critereQualite);

		}

		return mapObjet(objetRepository.findById(objet.get().getIdClasseDobjet()).get());

	}

	private void archiverCriteres(CritereQualite critereQualite) {
		critereQualite.setDateFinActivite(ArianeHelper.getCurrentDate());
		critereQualiteRepository.save(critereQualite);

		for (PolitiqueQualite politiqueQualite : critereQualite.getPolitiqueQualites()) {
			politiqueQualite.setDateFinActivite(ArianeHelper.getCurrentDate());
			politiqueQualiteRepository.save(politiqueQualite);

			for (PolitiqueQualiteParUsage politiqueQualiteParUsage : politiqueQualite.getPolitiqueQualiteParUsages()) {
				politiqueQualiteParUsage.setDateDebutActivite(ArianeHelper.getCurrentDate());
				politiqueQualiteParUsageRepository.save(politiqueQualiteParUsage);
			}

			for (TauxQualiteMesure tauxQualiteMesure : politiqueQualite.getTauxQualiteMesures()) {
				tauxQualiteMesure.setDateFinActivite(ArianeHelper.getCurrentDate());
				tauxQualiteMesureRepository.save(tauxQualiteMesure);
			}
		}
	}

	@Transactional
	public void updateJoins(ClasseDobjet oldObject, ClasseDobjet newObject) {

		if (oldObject.getCritereQualites() != null) {
			for (CritereQualite critereQualite : oldObject.getCritereQualites()) {
				CritereQualite cq = new CritereQualite();
				modelMapper.map(critereQualite, cq);
				ArianeHelper.updateEntityDetails(critereQualite, cq);
				cq.setIdCritereQualite(null);
				cq.setClasseDobjet(newObject);
				critereQualite.setDateFinActivite(ArianeHelper.getCurrentDate());
				critereQualiteRepository.save(critereQualite);
				cq = critereQualiteRepository.save(cq);

				if (critereQualite.getPolitiqueQualites() != null) {
					for (PolitiqueQualite politiqueQualite : critereQualite.getPolitiqueQualites()) {
						PolitiqueQualite pq = new PolitiqueQualite();
						modelMapper.map(politiqueQualite, pq);
						ArianeHelper.updateEntityDetails(politiqueQualite, pq);
						pq.setIdPolitiqueQualite(null);
						pq.setCritereQualite(cq);
						politiqueQualite.setDateFinActivite(ArianeHelper.getCurrentDate());
						politiqueQualiteRepository.save(politiqueQualite);
						pq = politiqueQualiteRepository.save(pq);

						if (politiqueQualite.getPolitiqueQualiteParUsages() != null) {
							for (PolitiqueQualiteParUsage politiqueQualiteParUsage : politiqueQualite
									.getPolitiqueQualiteParUsages()) {
								PolitiqueQualiteParUsage pqpu = new PolitiqueQualiteParUsage();
								modelMapper.map(politiqueQualiteParUsage, pqpu);
								ArianeHelper.updateEntityDetails(politiqueQualiteParUsage, pqpu);
								pqpu.setIdPolitiqueQualiteParUsage(null);
								pqpu.setPolitiqueQualite(pq);
								politiqueQualiteParUsage.setDateDebutActivite(ArianeHelper.getCurrentDate());
								politiqueQualiteParUsageRepository.save(politiqueQualiteParUsage);
								politiqueQualiteParUsageRepository.save(pqpu);
							}
						}

						if (politiqueQualite.getTauxQualiteMesures() != null) {
							for (TauxQualiteMesure tauxQualiteMesure : politiqueQualite.getTauxQualiteMesures()) {
								TauxQualiteMesure tq = new TauxQualiteMesure();
								modelMapper.map(tauxQualiteMesure, tq);
								ArianeHelper.updateEntityDetails(tauxQualiteMesure, tq);
								tq.setIdTauxQualiteMesure(null);
								tq.setPolitiqueQualite(pq);
								tauxQualiteMesure.setDateFinActivite(ArianeHelper.getCurrentDate());
								tauxQualiteMesureRepository.save(tauxQualiteMesure);
								tauxQualiteMesureRepository.save(tq);
							}
						}
					}
				}
			}
		}
	}

	public List<String> findAllCriteres() {
		return critereRepository.findAll().stream().map(item -> item.getLibelle()).collect(Collectors.toList());
	}

	public List<UsageMetier> findAllUsage() {
		return usageMetierRepository.findAll();
	}

}
