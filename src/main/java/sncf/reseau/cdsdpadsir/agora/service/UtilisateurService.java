package sncf.reseau.cdsdpadsir.agora.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.ImmutableList;

import sncf.reseau.cdsdpadsir.agora.configuration.CpError;
import sncf.reseau.cdsdpadsir.agora.entity.AssociationClasseDobjetIdentiteNumerique;
import sncf.reseau.cdsdpadsir.agora.entity.Autorisation;
import sncf.reseau.cdsdpadsir.agora.entity.ClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.entity.FiliereResponsable;
import sncf.reseau.cdsdpadsir.agora.entity.IdentiteNumerique;
import sncf.reseau.cdsdpadsir.agora.entity.ProfilAutorisation;
import sncf.reseau.cdsdpadsir.agora.entity.ProfilAutorisationAbstrait;
import sncf.reseau.cdsdpadsir.agora.entity.Role;
import sncf.reseau.cdsdpadsir.agora.entity.StatutClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.entity.StatutTerme;
import sncf.reseau.cdsdpadsir.agora.entity.Terme;
import sncf.reseau.cdsdpadsir.agora.entity.Utilisateur;
import sncf.reseau.cdsdpadsir.agora.entity.UtilisateurRole;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.model.ActeurApiDto;
import sncf.reseau.cdsdpadsir.agora.model.AutorisationDto;
import sncf.reseau.cdsdpadsir.agora.model.FiliereDto;
import sncf.reseau.cdsdpadsir.agora.model.GroupeRoleDto;
import sncf.reseau.cdsdpadsir.agora.model.ProfilAutorisationDto;
import sncf.reseau.cdsdpadsir.agora.model.RoleApiDto;
import sncf.reseau.cdsdpadsir.agora.model.RoleDto;
import sncf.reseau.cdsdpadsir.agora.model.UtilisateurDto;
import sncf.reseau.cdsdpadsir.agora.repository.AssociationClasseDobjetUtilisateurRepository;
import sncf.reseau.cdsdpadsir.agora.repository.AutorisationRepository;
import sncf.reseau.cdsdpadsir.agora.repository.FiliereResponsableRepository;
import sncf.reseau.cdsdpadsir.agora.repository.GroupeRoleRepository;
import sncf.reseau.cdsdpadsir.agora.repository.GroupeRoleRoleRepository;
import sncf.reseau.cdsdpadsir.agora.repository.IdentiteNumeriqueRepository;
import sncf.reseau.cdsdpadsir.agora.repository.ObjetRepository;
import sncf.reseau.cdsdpadsir.agora.repository.ProfilAutorisationAbstraitRepository;
import sncf.reseau.cdsdpadsir.agora.repository.ProfilAutorisationRepository;
import sncf.reseau.cdsdpadsir.agora.repository.RoleRepository;
import sncf.reseau.cdsdpadsir.agora.repository.StatutClasseDobjetRepository;
import sncf.reseau.cdsdpadsir.agora.repository.StatutTermeRepository;
import sncf.reseau.cdsdpadsir.agora.repository.TermeRepository;
import sncf.reseau.cdsdpadsir.agora.repository.UtilisateurRepository;
import sncf.reseau.cdsdpadsir.agora.repository.UtilisateurRoleRepository;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;

@Service
public class UtilisateurService {

	private UtilisateurRepository utilisateurRepository;
	private IdentiteNumeriqueRepository identiteNumeriqueRepository;
	private GroupeRoleRepository groupeRoleRepository;
	private GroupeRoleRoleRepository groupeRoleRoleRepository;
	private RoleRepository roleRepository;
	private UtilisateurRoleRepository utilisateurRoleRepository;
	private StatutTermeRepository statutTermesRepository;
	private FiliereResponsableRepository filiereResponsableRepository;
	private StatutClasseDobjetRepository statutClasseDobjetRepository;
	private AssociationClasseDobjetUtilisateurRepository associationClasseDobjetUtilisateurRepository;
	private TermeRepository termeRepository;
	private ObjetRepository objetRepository;
	private AutorisationRepository autorisationRepository;
	private FiliereService filiereService;
	private ProfilAutorisationRepository profilAutorisationRepository;
	private ProfilAutorisationAbstraitRepository profilAutorisationAbstraitRepository;
	private static final String ADMIN = "admin";

	private ModelMapper modelMapper;

	@Autowired
	public UtilisateurService(UtilisateurRepository utilisateurRepository,
			IdentiteNumeriqueRepository identiteNumeriqueRepository, GroupeRoleRepository groupeRoleRepository,
			RoleRepository roleRepository, GroupeRoleRoleRepository groupeRoleRoleRepository,
			UtilisateurRoleRepository utilisateurRoleRepository, TermeRepository termeRepository,
			FiliereResponsableRepository filiereResponsableRepository, ObjetRepository objetRepository,
			AssociationClasseDobjetUtilisateurRepository associationClasseDobjetUtilisateurRepository,
			StatutClasseDobjetRepository statutClasseDobjetRepository, StatutTermeRepository statutTermesRepository,
			AutorisationRepository autorisationRepository, FiliereService filiereService, ModelMapper modelMapper,
			ProfilAutorisationRepository profilAutorisationRepository,
			ProfilAutorisationAbstraitRepository profilAutorisationAbstraitRepository) {
		super();
		this.utilisateurRepository = utilisateurRepository;
		this.identiteNumeriqueRepository = identiteNumeriqueRepository;
		this.groupeRoleRepository = groupeRoleRepository;
		this.groupeRoleRoleRepository = groupeRoleRoleRepository;
		this.roleRepository = roleRepository;
		this.utilisateurRoleRepository = utilisateurRoleRepository;
		this.statutTermesRepository = statutTermesRepository;
		this.filiereResponsableRepository = filiereResponsableRepository;
		this.statutClasseDobjetRepository = statutClasseDobjetRepository;
		this.associationClasseDobjetUtilisateurRepository = associationClasseDobjetUtilisateurRepository;
		this.termeRepository = termeRepository;
		this.objetRepository = objetRepository;
		this.autorisationRepository = autorisationRepository;
		this.filiereService = filiereService;
		this.profilAutorisationRepository = profilAutorisationRepository;
		this.profilAutorisationAbstraitRepository = profilAutorisationAbstraitRepository;
		this.modelMapper = modelMapper;

		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	public PagingResponse<UtilisateurDto> findAll(Specification<Utilisateur> utilisateurSpec, Sort sort,
			@Valid BigDecimal page, @Valid BigDecimal size) {

		if (isRequestPaged(page, size))
			return findAll(utilisateurSpec, buildPageRequest(page, size, sort));

		List<UtilisateurDto> entities = findAll(utilisateurSpec, sort);
		return new PagingResponse<>((long) entities.size(), 0L, 0L, 0L, 0L, entities);
	}

	public PagingResponse<ActeurApiDto> findAllApi(Specification<Utilisateur> utilisateurSpec, Sort sort,
			@Valid BigDecimal page, @Valid BigDecimal size) {
		PagingResponse<UtilisateurDto> response = findAll(utilisateurSpec, sort, page, size);
		List<ActeurApiDto> listApi = response.getElements().stream().map(item -> {
			ActeurApiDto acteur = modelMapper.map(item, ActeurApiDto.class);

			if (item.getFilieresResponsable() != null && !item.getFilieresResponsable().isEmpty()) {

				acteur.setFilieres(
						item.getFilieresResponsable().stream().map(FiliereDto::getNom).collect(Collectors.toList()));

			} else if (item.getFilieresResponsableDelegue() != null
					&& !item.getFilieresResponsableDelegue().isEmpty()) {
				acteur.setFilieres(item.getFilieresResponsableDelegue().stream().map(FiliereDto::getNom)
						.collect(Collectors.toList()));

			} else if (item.getFilieresCoordinateur() != null && !item.getFilieresCoordinateur().isEmpty()) {
				acteur.setFilieres(
						item.getFilieresCoordinateur().stream().map(FiliereDto::getNom).collect(Collectors.toList()));
			}
			acteur.setRoles(new ArrayList<>());
			item.getRoles().forEach(role -> acteur.getRoles().add(modelMapper.map(role, RoleApiDto.class)));
			return acteur;
		}).collect(Collectors.toList());

		return new PagingResponse<>((long) response.getCount(), response.getPageNumber(), response.getPageSize(),
				response.getPageOffset(), response.getPageTotal(), listApi);
	}

	private PagingResponse<UtilisateurDto> findAll(Specification<Utilisateur> spec, Pageable pageable) {
		Page<Utilisateur> page = utilisateurRepository.findAll(spec, pageable);
		List<Utilisateur> content = page.getContent();
		return new PagingResponse<>(page.getTotalElements(), (long) page.getNumber(), (long) page.getNumberOfElements(),
				pageable.getOffset(), (long) page.getTotalPages(),
				content.stream().map(this::mapUtilisateur).collect(Collectors.toList()));
	}

	private UtilisateurDto mapUtilisateur(final Utilisateur mapper) {
		boolean isAdmin = false;
		try {
			isAdmin = mapper.getIdentiteNumerique().getAutorisations().stream()
					.anyMatch(autorisation -> autorisation.getProfilAutorisationAbstrait().getProfilAutorisation()
							.getLibelle().equals(ADMIN)
							&& autorisation.getProfilAutorisationAbstrait().getProfilAutorisation().getDateFinActivite()
									.equals(ArianeHelper.FUTURE_DATE));
		} catch (Exception exp) {
			isAdmin = false;
		}

		UtilisateurDto utilisateurDto = modelMapper.map(mapper, UtilisateurDto.class);
		utilisateurDto.setFilieresResponsable(new ArrayList<>());
		utilisateurDto.setFilieresResponsableDelegue(new ArrayList<>());
		utilisateurDto.setFilieresCoordinateur(new ArrayList<>());
		utilisateurDto.setRoles(new ArrayList<>());
		utilisateurDto.setFamilleRoles(new ArrayList<>());
		ImmutableList<Set<FiliereResponsable>> responsables = null;
		ImmutableList<Set<FiliereResponsable>> responsablesDelegues = null;
		ImmutableList<Set<FiliereResponsable>> coordinateurs = null;

		utilisateurDto.setAdmin(isAdmin);

		if (mapper.getIdentiteNumerique() != null && mapper.getIdentiteNumerique().getResponsablesDelegues() != null) {
			responsablesDelegues = ImmutableList.of(mapper.getIdentiteNumerique().getResponsablesDelegues());
		}

		if (mapper.getIdentiteNumerique() != null && mapper.getIdentiteNumerique().getCoordinateurs() != null) {
			coordinateurs = ImmutableList.of(mapper.getIdentiteNumerique().getCoordinateurs());
		}

		if (mapper.getIdentiteNumerique() != null && mapper.getIdentiteNumerique().getAutorisations() != null) {
			utilisateurDto.setAutorisations(new ArrayList<>());

			for (Autorisation autorisation : mapper.getIdentiteNumerique().getAutorisations().stream()
					.filter(predicate -> predicate.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE))
					.collect(Collectors.toList())) {
				AutorisationDto autorisationDto = modelMapper.map(autorisation, AutorisationDto.class);

				if (autorisation.getProfilAutorisationAbstrait() != null
						&& autorisation.getProfilAutorisationAbstrait().getProfilAutorisation() != null) {
					autorisationDto.setProfilAutorisation(
							modelMapper.map(autorisation.getProfilAutorisationAbstrait().getProfilAutorisation(),
									ProfilAutorisationDto.class));
				}

				if (autorisation.getProfilAutorisationAbstrait() != null
						&& autorisation.getProfilAutorisationAbstrait().getProfilAutorisation() != null && autorisation
								.getProfilAutorisationAbstrait().getProfilAutorisation().getSuperRessource() != null) {
					autorisationDto.setFiliere(modelMapper.map(autorisation.getProfilAutorisationAbstrait()
							.getProfilAutorisation().getSuperRessource().getFiliere(), FiliereDto.class));
				}

				utilisateurDto.getAutorisations().add(autorisationDto);
			}
		}

		if (mapper.getIdentiteNumerique() != null)
			utilisateurDto.setIdIdentiteNumerique(mapper.getIdentiteNumerique().getIdIdentiteNumerique());

		if (coordinateurs != null && coordinateurs.get(0) != null) {
			for (FiliereResponsable item : coordinateurs.get(0)) {
				if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE))
					utilisateurDto.getFilieresCoordinateur()
							.add(filiereService.getWithMapper(item.getFiliere().getIdFiliere()));
			}
		}

		if (responsablesDelegues != null && responsablesDelegues.get(0) != null) {
			for (FiliereResponsable item : responsablesDelegues.get(0)) {
				if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE))
					utilisateurDto.getFilieresResponsableDelegue()
							.add(filiereService.getWithMapper(item.getFiliere().getIdFiliere()));
			}
		}

		if (mapper.getIdentiteNumerique() != null && mapper.getIdentiteNumerique().getUtilisateurRoles() != null) {
			for (UtilisateurRole role : mapper.getIdentiteNumerique().getUtilisateurRoles()) {
				utilisateurDto.getRoles().add(modelMapper.map(role.getRole(), RoleDto.class));
				groupeRoleRoleRepository.findByRole(role.getRole());
				utilisateurDto.getFamilleRoles()
						.add(modelMapper.map(groupeRoleRoleRepository.findByRole(role.getRole()).get().getGroupeRole(),
								GroupeRoleDto.class));
			}
		}

		if (mapper.getIdentiteNumerique() != null && mapper.getIdentiteNumerique().getResponsables() != null) {
			responsables = ImmutableList.of(mapper.getIdentiteNumerique().getResponsables());
		}

		if (responsables != null && responsables.get(0) != null) {
			responsables.get(0).forEach(item -> {
				if (item.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
					utilisateurDto.getFilieresResponsable()
							.add(filiereService.getWithMapper(item.getFiliere().getIdFiliere()));
				}
			});
		}

		return utilisateurDto;
	}

	public List<UtilisateurDto> findAll(Specification<Utilisateur> spec, Sort sort) {
		List<Utilisateur> list = utilisateurRepository.findAll(spec, sort).stream().filter(Objects::nonNull)
				.collect(Collectors.toList());
		return list.stream().filter(Objects::nonNull).map(this::mapUtilisateur).collect(Collectors.toList());

	}

	private boolean isRequestPaged(BigDecimal page, BigDecimal size) {
		return (page != null && size != null);
	}

	private Pageable buildPageRequest(BigDecimal page, BigDecimal size, Sort sort) {
		return PageRequest.of(page.intValue(), size.intValue(), sort);
	}

	@Transactional
	public UtilisateurDto add(@Valid UtilisateurDto utilisateurDto) throws CpError {

		Utilisateur utilisateur = modelMapper.map(utilisateurDto, Utilisateur.class);
		Optional<Utilisateur> cp = utilisateurRepository.findByCpAndDateFinActivite(utilisateur.getCp(),
				ArianeHelper.FUTURE_DATE);

		if (cp.isPresent()) {
			throw new CpError("code cp existe deja");
		}

		ArianeHelper.setNewEntityDetails(utilisateur);
		Utilisateur save = utilisateurRepository.save(utilisateur);

		IdentiteNumerique id = new IdentiteNumerique();
		id.setUtilisateur(save);
		id.setUtilisateurRoles(new HashSet<>());
		id.setAssociationClasseDobjetUtilisateurs(new HashSet<>());
		id.setFamilleResponsables(new HashSet<>());
		id.setCoordinateurs(new HashSet<>());
		id.setResponsablesDelegues(new HashSet<>());
		id.setResponsables(new HashSet<>());
		id.setStatutValideurClasseDobjets(new HashSet<>());
		id.setStatutSuppleantClasseDobjets(new HashSet<>());
		id.setStatutValideurTermes(new HashSet<>());
		id.setStatutSuppleantTermes(new HashSet<>());
		ArianeHelper.setNewEntityDetails(id);
		IdentiteNumerique idSave = identiteNumeriqueRepository.save(id);
		save.setIdentiteNumerique(idSave);

		if (utilisateurDto.getRoles() != null && !utilisateurDto.getRoles().isEmpty()) {
			id.setUtilisateurRoles(new HashSet<>());
			for (RoleDto role : utilisateurDto.getRoles()) {
				UtilisateurRole utilisateurRole = new UtilisateurRole();
				utilisateurRole.setIdentiteNumerique(idSave);
				utilisateurRole.setRole(modelMapper.map(role, Role.class));
				ArianeHelper.setNewEntityDetails(utilisateurRole);
				utilisateurRoleRepository.save(utilisateurRole);
				id.getUtilisateurRoles().add(utilisateurRole);
			}
		}

		if (utilisateurDto.isAdmin()) {
			Autorisation autorisation = new Autorisation();
			ProfilAutorisationAbstrait profilAutorisationAbstrait = new ProfilAutorisationAbstrait();
			ProfilAutorisation profilAutorisation = new ProfilAutorisation();

			ArianeHelper.setNewEntityDetails(profilAutorisation);
			profilAutorisation.setLibelle(ADMIN);
			profilAutorisationRepository.save(profilAutorisation);

			ArianeHelper.setNewEntityDetails(profilAutorisationAbstrait);
			profilAutorisationAbstrait.setProfilAutorisation(profilAutorisation);
			profilAutorisationAbstraitRepository.save(profilAutorisationAbstrait);

			autorisation.setProfilAutorisationAbstrait(profilAutorisationAbstrait);
			ArianeHelper.setNewEntityDetails(autorisation);
			autorisation.setIdentiteNumerique(idSave);
			autorisationRepository.save(autorisation);

		}

		return mapUtilisateur(save);
	}

	@Transactional
	public UtilisateurDto update(UtilisateurDto utilisateurDto) throws CpError {

		Utilisateur utilisateurObjet = modelMapper.map(utilisateurDto, Utilisateur.class);
		Optional<Utilisateur> cp = utilisateurRepository.findByCpAndDateFinActivite(utilisateurObjet.getCp(),
				ArianeHelper.FUTURE_DATE);

		if (cp.isPresent() && !cp.get().getIdUtilisateur().equals(utilisateurObjet.getIdUtilisateur())
				&& !cp.get().getId().equals(utilisateurObjet.getId())) {
			throw new CpError("code cp existe deja");
		}

		IdentiteNumerique id = identiteNumeriqueRepository.findByUtilisateurAndDateFinActivite(
				modelMapper.map(utilisateurDto, Utilisateur.class), ArianeHelper.FUTURE_DATE).get();

		List<UtilisateurRole> utilisateurRoles = null;
		if (!id.getUtilisateurRoles().isEmpty()) {
			utilisateurRoles = utilisateurRoleRepository.findAllById(id.getUtilisateurRoles().stream()
					.map(UtilisateurRole::getIdUtilisateurRole).collect(Collectors.toList()));
		}

		IdentiteNumerique newId = new IdentiteNumerique();
		// id.setSuperRessources(null);

		copyIdentifiant(id, newId);
		newId.setIdIdentiteNumerique(null);
		ArianeHelper.updateEntityDetails(id, newId);

		id.setDateFinActivite(ArianeHelper.getCurrentDate());
		identiteNumeriqueRepository.save(id);

		List<Autorisation> autorisations = new ArrayList<>();

		Utilisateur utilisateur = utilisateurRepository.findById(utilisateurDto.getIdUtilisateur()).get();
		Utilisateur newUtilisateur = modelMapper.map(utilisateurDto, Utilisateur.class);
		newUtilisateur.setIdUtilisateur(null);
		ArianeHelper.updateEntityDetails(utilisateur, newUtilisateur);
		newUtilisateur = utilisateurRepository.save(newUtilisateur);
		utilisateur.setDateFinActivite(ArianeHelper.getCurrentDate());
		utilisateurRepository.save(utilisateur);
		newId.setUtilisateur(newUtilisateur);

		IdentiteNumerique newIdNum = identiteNumeriqueRepository.save(newId);

		for (Autorisation element : id.getAutorisations()) {
			Autorisation autorisation = new Autorisation();

			autorisation.setDateCreation(element.getDateCreation());
			autorisation.setDateDebutActivite(element.getDateDebutActivite());
			autorisation.setDateFinActivite(element.getDateFinActivite());
			autorisation.setId(element.getId());
			autorisation.setIdExterne(element.getIdExterne());
			autorisation.setRevision(element.getRevision());

			// modelMapper.map(element, autorisation);
			autorisation.setIdentiteNumerique(newIdNum);
			autorisation.setProfilAutorisationAbstrait(element.getProfilAutorisationAbstrait());
			autorisation.setIdAutorisation(null);
			ArianeHelper.updateEntityDetails(element, autorisation);
			autorisations.add(autorisation);
			element.setDateFinActivite(ArianeHelper.getCurrentDate());
		}

		autorisationRepository.saveAll(autorisations);
		autorisationRepository.saveAll(id.getAutorisations());
		newIdNum.getAutorisations().clear();
		newIdNum.getAutorisations().addAll(autorisations);

		if (id.getTermes() != null) {
			List<Terme> termes = new ArrayList<>();
			List<Terme> oldTermes = new ArrayList<>();
			for (Terme element : id.getTermes()) {
				Terme terme = new Terme();
				modelMapper.map(element, terme);
				terme.setProprietaire(newIdNum);
				termes.add(terme);
				element.setDateFinActivite(ArianeHelper.getCurrentDate());
				oldTermes.add(element);
			}
			termeRepository.saveAll(termes);
			termeRepository.saveAll(oldTermes);
		}

		if (id.getObjets() != null) {
			List<ClasseDobjet> objets = new ArrayList<>();
			List<ClasseDobjet> oldObjets = new ArrayList<>();
			for (ClasseDobjet element : id.getObjets()) {
				ClasseDobjet objet = new ClasseDobjet();
				modelMapper.map(element, objet);
				objet.setProprietaire(newIdNum);
				objets.add(objet);
				element.setDateFinActivite(ArianeHelper.getCurrentDate());
				oldObjets.add(element);
			}
			objetRepository.saveAll(objets);
			objetRepository.saveAll(oldObjets);
		}

		if (id.getStatutValideurTermes() != null) {
			List<StatutTerme> statutTermes = new ArrayList<>();
			List<StatutTerme> oldStatutTermes = new ArrayList<>();
			for (StatutTerme element : id.getStatutValideurTermes()) {
				StatutTerme statutTerme = new StatutTerme();
				modelMapper.map(element, statutTerme);
				statutTerme.setTerme(element.getTerme());
				statutTerme.setValideur(newIdNum);
				statutTerme.setNom(newIdNum.getUtilisateur().getNom());
				statutTerme.setPrenom(newIdNum.getUtilisateur().getPrenom());
				statutTermes.add(statutTerme);
				element.setDateFinActivite(ArianeHelper.getCurrentDate());
				oldStatutTermes.add(element);
			}
			statutTermesRepository.saveAll(statutTermes);
			statutTermesRepository.saveAll(oldStatutTermes);
		}

		if (id.getStatutSuppleantTermes() != null) {
			List<StatutTerme> statutTermes = new ArrayList<>();
			List<StatutTerme> oldStatutTermes = new ArrayList<>();
			for (StatutTerme element : id.getStatutSuppleantTermes()) {
				StatutTerme statutTerme = new StatutTerme();
				modelMapper.map(element, statutTerme);
				statutTerme.setTerme(element.getTerme());
				statutTerme.setSuppleant(newIdNum);
				statutTerme.setNom(newIdNum.getUtilisateur().getNom());
				statutTerme.setPrenom(newIdNum.getUtilisateur().getPrenom());
				statutTermes.add(statutTerme);
				element.setDateFinActivite(ArianeHelper.getCurrentDate());
				oldStatutTermes.add(element);
			}
			statutTermesRepository.saveAll(statutTermes);
			statutTermesRepository.saveAll(oldStatutTermes);
		}

		if (id.getStatutValideurClasseDobjets() != null) {
			List<StatutClasseDobjet> statutClasseDobjets = new ArrayList<>();
			List<StatutClasseDobjet> oldStatutClasseDobjets = new ArrayList<>();
			for (StatutClasseDobjet element : id.getStatutValideurClasseDobjets()) {
				StatutClasseDobjet statutClasseDobjet = new StatutClasseDobjet();
				modelMapper.map(element, statutClasseDobjet);
				statutClasseDobjet.setClasseDobjet(element.getClasseDobjet());
				statutClasseDobjet.setValideur(newIdNum);
				statutClasseDobjet.setNom(newIdNum.getUtilisateur().getNom());
				statutClasseDobjet.setPrenom(newIdNum.getUtilisateur().getPrenom());
				statutClasseDobjets.add(statutClasseDobjet);
				element.setDateFinActivite(ArianeHelper.getCurrentDate());
				oldStatutClasseDobjets.add(element);
			}
			statutClasseDobjetRepository.saveAll(statutClasseDobjets);
			statutClasseDobjetRepository.saveAll(oldStatutClasseDobjets);
		}

		if (id.getStatutSuppleantClasseDobjets() != null) {
			List<StatutClasseDobjet> statutClasseDobjets = new ArrayList<>();
			List<StatutClasseDobjet> oldStatutClasseDobjets = new ArrayList<>();
			for (StatutClasseDobjet element : id.getStatutSuppleantClasseDobjets()) {
				StatutClasseDobjet statutClasseDobjet = new StatutClasseDobjet();
				modelMapper.map(element, statutClasseDobjet);
				statutClasseDobjet.setClasseDobjet(element.getClasseDobjet());
				statutClasseDobjet.setSuppleant(newIdNum);
				statutClasseDobjet.setNom(newIdNum.getUtilisateur().getNom());
				statutClasseDobjet.setPrenom(newIdNum.getUtilisateur().getPrenom());
				statutClasseDobjets.add(statutClasseDobjet);
				element.setDateFinActivite(ArianeHelper.getCurrentDate());
				oldStatutClasseDobjets.add(element);
			}
			statutClasseDobjetRepository.saveAll(statutClasseDobjets);
			statutClasseDobjetRepository.saveAll(oldStatutClasseDobjets);
		}

		if (id.getCoordinateurs() != null) {
			List<FiliereResponsable> coordinateurs = new ArrayList<>();
			List<FiliereResponsable> oldCoordinateurs = new ArrayList<>();
			for (FiliereResponsable element : id.getCoordinateurs()) {
				FiliereResponsable filiereResponsable = new FiliereResponsable();
				modelMapper.map(element, filiereResponsable);
				filiereResponsable.setFiliere(element.getFiliere());
				filiereResponsable.setCoordinateur(newIdNum);
				coordinateurs.add(filiereResponsable);
				element.setDateFinActivite(ArianeHelper.getCurrentDate());
				oldCoordinateurs.add(element);
			}
			filiereResponsableRepository.saveAll(coordinateurs);
			filiereResponsableRepository.saveAll(oldCoordinateurs);
		}

		if (id.getResponsablesDelegues() != null) {
			List<FiliereResponsable> responsablesDelegues = new ArrayList<>();
			List<FiliereResponsable> oldResponsablesDelegues = new ArrayList<>();
			for (FiliereResponsable element : id.getResponsablesDelegues()) {
				FiliereResponsable filiereResponsable = new FiliereResponsable();
				modelMapper.map(element, filiereResponsable);
				filiereResponsable.setFiliere(element.getFiliere());
				filiereResponsable.setResponsableDelegue(newIdNum);
				responsablesDelegues.add(filiereResponsable);
				element.setDateFinActivite(ArianeHelper.getCurrentDate());
				oldResponsablesDelegues.add(element);
			}
			filiereResponsableRepository.saveAll(responsablesDelegues);
			filiereResponsableRepository.saveAll(oldResponsablesDelegues);
		}

		if (id.getResponsables() != null) {
			List<FiliereResponsable> responsables = new ArrayList<>();
			List<FiliereResponsable> oldResponsables = new ArrayList<>();
			for (FiliereResponsable element : id.getResponsables()) {
				FiliereResponsable filiereResponsable = new FiliereResponsable();
				modelMapper.map(element, filiereResponsable);
				filiereResponsable.setFiliere(element.getFiliere());
				filiereResponsable.setResponsable(newIdNum);
				responsables.add(filiereResponsable);
				element.setDateFinActivite(ArianeHelper.getCurrentDate());
				oldResponsables.add(element);
			}
			filiereResponsableRepository.saveAll(responsables);
			filiereResponsableRepository.saveAll(oldResponsables);
		}

		if (id.getAssociationClasseDobjetUtilisateurs() != null) {
			for (AssociationClasseDobjetIdentiteNumerique element : id.getAssociationClasseDobjetUtilisateurs()) {
				AssociationClasseDobjetIdentiteNumerique association = new AssociationClasseDobjetIdentiteNumerique();
				modelMapper.map(element, association);
				association.setClasseDobjet(element.getClasseDobjet());
				association.setIdentiteNumerique(newIdNum);
				element.setDateFinActivite(ArianeHelper.getCurrentDate());
				associationClasseDobjetUtilisateurRepository.save(association);
				associationClasseDobjetUtilisateurRepository.save(element);
			}

		}

		if (utilisateurDto.getRoles() != null && !utilisateurDto.getRoles().isEmpty()) {

			if (utilisateurRoles != null) {
				for (UtilisateurRole role : utilisateurRoles) {
					role.setDateFinActivite(ArianeHelper.getCurrentDate());
				}
				utilisateurRoleRepository.saveAll(utilisateurRoles);
			}

			newIdNum.getUtilisateurRoles().clear();
			List<UtilisateurRole> newUtilisateurRoles = new ArrayList<>();

			for (RoleDto roleDto : utilisateurDto.getRoles()) {
				UtilisateurRole newUtilisateurRole = new UtilisateurRole();
				ArianeHelper.setNewEntityDetails(newUtilisateurRole);
				newIdNum.getUtilisateurRoles().add(newUtilisateurRole);
				newUtilisateurRole.setIdentiteNumerique(newIdNum);
				newUtilisateurRole.setRole(modelMapper.map(roleDto, Role.class));
				newUtilisateurRoles.add(newUtilisateurRole);
			}

			utilisateurRoleRepository.saveAll(newUtilisateurRoles);

		}

		boolean isAdmin = id.getAutorisations().stream()
				.anyMatch(autorisation -> autorisation.getProfilAutorisationAbstrait().getProfilAutorisation()
						.getLibelle().equals(ADMIN)
						&& autorisation.getProfilAutorisationAbstrait().getProfilAutorisation().getDateFinActivite()
								.equals(ArianeHelper.FUTURE_DATE));

		if (!utilisateurDto.isAdmin() && isAdmin) {
			newIdNum.getAutorisations().forEach(autorisation -> {
				if (autorisation.getProfilAutorisationAbstrait().getProfilAutorisation().getLibelle().equals(ADMIN)) {
					autorisation.setDateFinActivite(ArianeHelper.getCurrentDate());
					autorisation.getProfilAutorisationAbstrait().setDateFinActivite(ArianeHelper.getCurrentDate());
					autorisation.getProfilAutorisationAbstrait().getProfilAutorisation()
							.setDateFinActivite(ArianeHelper.getCurrentDate());
					autorisationRepository.save(autorisation);
					profilAutorisationAbstraitRepository.save(autorisation.getProfilAutorisationAbstrait());
					profilAutorisationRepository
							.save(autorisation.getProfilAutorisationAbstrait().getProfilAutorisation());
				}
			});
		} else if (utilisateurDto.isAdmin() && !isAdmin) {
			Autorisation autorisation = new Autorisation();
			ProfilAutorisationAbstrait profilAutorisationAbstrait = new ProfilAutorisationAbstrait();
			ProfilAutorisation profilAutorisation = new ProfilAutorisation();

			ArianeHelper.setNewEntityDetails(profilAutorisation);
			profilAutorisation.setLibelle(ADMIN);
			profilAutorisationRepository.save(profilAutorisation);

			ArianeHelper.setNewEntityDetails(profilAutorisationAbstrait);
			profilAutorisationAbstrait.setProfilAutorisation(profilAutorisation);
			profilAutorisationAbstraitRepository.save(profilAutorisationAbstrait);

			autorisation.setProfilAutorisationAbstrait(profilAutorisationAbstrait);
			ArianeHelper.setNewEntityDetails(autorisation);
			autorisation.setIdentiteNumerique(newIdNum);
			autorisationRepository.save(autorisation);
			newIdNum.getAutorisations().add(autorisation);
		}

		newUtilisateur.setIdentiteNumerique(newIdNum);
		return mapUtilisateur(utilisateurRepository.findById(newUtilisateur.getIdUtilisateur()).get());
	}

	private void copyIdentifiant(IdentiteNumerique id, IdentiteNumerique newId) {

		newId.setIdIdentiteNumerique(id.getIdIdentiteNumerique());
		newId.setUtilisateur(id.getUtilisateur());
		newId.setAssociationClasseDobjetUtilisateurs(new HashSet<>());
		newId.setAssociationTermeUtilisateur(id.getAssociationTermeUtilisateur());
		newId.setFamilleResponsables(new HashSet<>());
		newId.setCoordinateurs(new HashSet<>());
		newId.setResponsablesDelegues(new HashSet<>());
		newId.setResponsables(new HashSet<>());
		newId.setValideurs(new HashSet<>());
		newId.setStatutValideurClasseDobjets(new HashSet<>());
		newId.setStatutSuppleantClasseDobjets(new HashSet<>());
		newId.setStatutValideurTermes(new HashSet<>());
		newId.setStatutSuppleantTermes(new HashSet<>());
		newId.setUtilisateurRoles(new HashSet<>());
		newId.setTermes(new HashSet<>());
		newId.setObjets(new HashSet<>());
		newId.setAutorisations(new HashSet<>());
		newId.setId(id.getId());
		newId.setDateCreation(id.getDateCreation());
		newId.setDateDebutActivite(id.getDateDebutActivite());
		newId.setDateFinActivite(id.getDateFinActivite());
		newId.setRevision(id.getRevision());

	}

	public String delete(String id) {
		List<Utilisateur> list = utilisateurRepository.findById(id);
		for (Utilisateur util : list) {
			util.setNom(ArianeHelper.STARS);
			util.setPrenom(ArianeHelper.STARS);
			util.setCp(ArianeHelper.STARS);
			util.setAdresseMail(ArianeHelper.STARS);
			if (util.getDateFinActivite().equals(ArianeHelper.FUTURE_DATE)) {
				util.setDateFinActivite(ArianeHelper.getCurrentDate());
			}
		}

		utilisateurRepository.saveAll(list);
		return id;
	}

	public PagingResponse<GroupeRoleDto> findAllFamillesRoles(Sort sort) {
		List<GroupeRoleDto> entities = groupeRoleRepository.findAll(sort).stream()
				.map(item -> modelMapper.map(item, GroupeRoleDto.class)).collect(Collectors.toList());
		return new PagingResponse<>((long) entities.size(), 0L, 0L, 0L, 0L, entities);
	}

	public PagingResponse<RoleDto> findAllRoles(Specification<Role> roleSpec, Sort sort) {
		List<RoleDto> entities = roleRepository.findAll(roleSpec, sort).stream()
				.map(item -> modelMapper.map(item, RoleDto.class)).collect(Collectors.toList());
		return new PagingResponse<>((long) entities.size(), 0L, 0L, 0L, 0L, entities);
	}

	public PagingResponse<RoleApiDto> findAllApiRoles(Specification<Role> roleSpec, Sort sort) {
		List<RoleApiDto> entities = roleRepository.findAll(roleSpec, sort).stream()
				.map(item -> modelMapper.map(item, RoleApiDto.class)).collect(Collectors.toList());
		return new PagingResponse<>((long) entities.size(), 0L, 0L, 0L, 0L, entities);
	}

}
