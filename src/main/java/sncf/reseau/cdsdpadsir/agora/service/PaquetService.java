package sncf.reseau.cdsdpadsir.agora.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import sncf.reseau.cdsdpadsir.agora.entity.Paquet;
import sncf.reseau.cdsdpadsir.agora.model.PaquetDto;
import sncf.reseau.cdsdpadsir.agora.repository.PaquetRepository;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;

@Service
public class PaquetService {

	private PaquetRepository paquetRepository;
	private ModelMapper modelMapper;
	
	private static Logger log = LoggerFactory.getLogger(PaquetService.class);

	@Autowired
	public PaquetService(PaquetRepository paquetRepository, ModelMapper modelMapper , NamedParameterJdbcTemplate jdbc) {

		this.paquetRepository = paquetRepository;
		this.modelMapper = modelMapper;
		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	@Transactional
	public List<PaquetDto> saveAll(List<PaquetDto> paquetDtoList) {
		
		Map<Integer , List<PaquetDto>> paquetNiveaux = getPaquetAndLevel(paquetDtoList) ;
		paquetDtoList.stream().forEach(p -> log.debug("paquet envoyé idExterne {} , nom paquet {} ",p.getIdExterne() , p.getNom()));
		paquetNiveaux.entrySet()
		.stream()	
		.flatMap(e -> e.getValue().stream())
		.map(paquetDto -> modelMapper.map(paquetDto, Paquet.class))		
		.forEach(paquet -> {
			String parentIdExterne = paquet.getPaquetParent() != null ? paquet.getPaquetParent().getIdExterne() : null;
			if(parentIdExterne != null) {
				List<Paquet> parentsPaquets = null;
				boolean hasRootParentWithVersion = paquet.getPaquetParent() != null && paquet.getPaquetParent().getVersion() != null;
				if(hasRootParentWithVersion) {
					parentsPaquets = paquetRepository.findByIdExterneAndMaxRevisionFonctionnelle(parentIdExterne);
				} else {
					parentsPaquets = paquetRepository.findByIdExterne(parentIdExterne);
				}
		
				Paquet parentPaquet = parentsPaquets.isEmpty() ? null : parentsPaquets.get(0);
				paquet.setPaquetParent(parentPaquet);
			}

			List<Paquet> paquetEntity = paquetRepository.findByIdExterneAndMaxRevisionFonctionnelle(paquet.getIdExterne());			
			if(null != paquetEntity && !paquetEntity.isEmpty()) {
				ArianeHelper.updateEntityDetails(paquetEntity.get(0), paquet);
				paquetEntity.get(0).setDateFinActivite(LocalDateTime.now());
				paquetRepository.save(paquetEntity.get(0));
			}else {
				ArianeHelper.setNewEntityDetails(paquet);
			}
			paquet.setRevisionFonctionnel(Timestamp.valueOf(LocalDateTime.now()));
			paquetRepository.save(paquet);
			log.debug("save idExterne {} , nom paquet {} ",paquet.getIdExterne() , paquet.getNom());
			paquetRepository.flush();
		});
		
		return paquetDtoList;
	}
	
	private Map<Integer , List<PaquetDto>> getPaquetAndLevel(List<PaquetDto> paquetDtoList) {

		Map<Integer , List<PaquetDto>> paquetNiveaux = new HashMap<Integer, List<PaquetDto>>();
		// 1 list
		Optional<PaquetDto> optionalParent = paquetDtoList.stream().filter(p -> p.getPaquetParent() == null).findFirst();
		if(optionalParent.isPresent()) {
			PaquetDto parentPaquet = optionalParent.get();
			paquetNiveaux.put(0, Arrays.asList(parentPaquet));
			
			tree(paquetDtoList, parentPaquet, paquetNiveaux, 0);
		}
		
		Map<Integer, List<PaquetDto>> map =  paquetNiveaux.entrySet()
		 .stream()
		 .sorted(Map.Entry.comparingByKey())
		 .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		return map;
	}
		
		private void tree(List<PaquetDto> paquets, PaquetDto paquetParent, Map<Integer, List<PaquetDto>> niveaux, int niveau) {
			List<PaquetDto> paquetsNiveau = paquets.stream().filter(p -> p.getPaquetParent() != null && p.getPaquetParent().getId().equals(paquetParent.getId())).collect(Collectors.toList());
			if(!paquetsNiveau.isEmpty()) {
				++niveau;
				if(niveaux.get(niveau) != null) {
					niveaux.get(niveau).addAll(paquetsNiveau);
				} else {
					niveaux.put(niveau, paquetsNiveau);
				}
			}
			for(PaquetDto paquet : paquetsNiveau) {
				tree(paquets, paquet, niveaux, niveau);
			}
		}
	
}
