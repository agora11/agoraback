package sncf.reseau.cdsdpadsir.agora.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import sncf.reseau.cdsdpadsir.agora.entity.ClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.entity.Filiere;
import sncf.reseau.cdsdpadsir.agora.entity.Gisement;
import sncf.reseau.cdsdpadsir.agora.entity.Terme;
import sncf.reseau.cdsdpadsir.agora.entity.Utilisateur;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.model.FamilleDto;
import sncf.reseau.cdsdpadsir.agora.model.FiliereDto;
import sncf.reseau.cdsdpadsir.agora.model.GisementDto;
import sncf.reseau.cdsdpadsir.agora.model.ObjetDto;
import sncf.reseau.cdsdpadsir.agora.model.PolitiqueQualiteDto;
import sncf.reseau.cdsdpadsir.agora.model.PolitiqueQualiteParUsageDto;
import sncf.reseau.cdsdpadsir.agora.model.RechercheDto;
import sncf.reseau.cdsdpadsir.agora.model.RechercheLigneDto;
import sncf.reseau.cdsdpadsir.agora.model.TauxQualiteMesureDto;
import sncf.reseau.cdsdpadsir.agora.model.TermeDto;
import sncf.reseau.cdsdpadsir.agora.model.UtilisateurDto;

@Service
public class RechercheService {

	@Autowired
	RechercheService recherceService;

	@Autowired
	ObjetService objetService;

	@Autowired
	TermeService termeService;

	@Autowired
	FiliereService filiereService;

	@Autowired
	UtilisateurService utilisateurService;

	@Autowired
	GisementService gisementService;

	public RechercheDto search(String keyWord, Sort sort, BigDecimal page, BigDecimal size,
			Specification<Gisement> activeGisement, Specification<Filiere> activeFiliere,
			Specification<ClasseDobjet> activeObjet, Specification<ClasseDobjet> activeObjetFonctionnel,
			Specification<Terme> activeTerme, Specification<Utilisateur> activeUtilisateur) {

		final PagingResponse<GisementDto> responseGisement = gisementService.findAll(activeGisement, sort, page, size);
		final PagingResponse<UtilisateurDto> responseUtilisateur = utilisateurService.findAll(activeUtilisateur, sort,
				page, size);
		final PagingResponse<FiliereDto> responseFiliere = filiereService.findAll(activeFiliere, sort, page, size);
		final PagingResponse<ObjetDto> responseObjet = objetService.findAll(activeObjet, sort, page, size);
		final PagingResponse<ObjetDto> responseObjetFonctionnel = objetService.findAll(activeObjetFonctionnel, sort,
				page, size);

		final PagingResponse<TermeDto> responseTerme = termeService.findAll(activeTerme, sort, page, size);

		RechercheDto rechercheDto = new RechercheDto();

		responseGisement.getElements().forEach(element -> {
			RechercheLigneDto ligne = new RechercheLigneDto();
			ligne.setMotRecheche(keyWord);
			ligne.setObject(element);
			searchMotif(keyWord, element, ligne);
			rechercheDto.getGisement().add(ligne);
		});

		responseUtilisateur.getElements().forEach(element -> {
			RechercheLigneDto ligne = new RechercheLigneDto();
			ligne.setMotRecheche(keyWord);
			ligne.setObject(element);
			searchMotif(keyWord, element, ligne);
			rechercheDto.getActeurs().add(ligne);
		});

		responseFiliere.getElements().forEach(element -> {
			RechercheLigneDto ligne = new RechercheLigneDto();
			ligne.setMotRecheche(keyWord);
			ligne.setObject(element);
			searchMotif(keyWord, element, ligne);
			rechercheDto.getFiliere().add(ligne);
		});

		responseObjet.getElements().forEach(element -> {
			RechercheLigneDto ligne = new RechercheLigneDto();
			ligne.setMotRecheche(keyWord);
			ligne.setObject(element);
			searchMotif(keyWord, element, ligne);
			rechercheDto.getObjets().add(ligne);
		});

		responseObjetFonctionnel.getElements().forEach(element -> {
			RechercheLigneDto ligne = new RechercheLigneDto();
			ligne.setMotRecheche(keyWord);
			ligne.setObject(element);
			searchMotif(keyWord, element, ligne);
			rechercheDto.getQualite().add(ligne);
		});

		responseTerme.getElements().forEach(element -> {
			RechercheLigneDto ligne = new RechercheLigneDto();
			ligne.setMotRecheche(keyWord);
			ligne.setObject(element);
			searchMotif(keyWord, element, ligne);
			rechercheDto.getGlossaire().add(ligne);
		});

		return rechercheDto;

	}

	private void searchMotif(String keyWord, Object element, RechercheLigneDto ligne) {

		final String keyWordRegex = ".*" + keyWord.toUpperCase() + ".*";

		if (element instanceof ObjetDto) {

			if (((ObjetDto) element).getCritereQualites() != null
					&& !((ObjetDto) element).getCritereQualites().isEmpty()) {
				((ObjetDto) element).getCritereQualites().forEach(critere -> {

					if (critere.getLibelle() != null && critere.getLibelle().toUpperCase().matches(keyWordRegex)) {
						ligne.getMotifRecherche().put("libelle", keyWord);
					}

					if (critere.getUrl() != null && critere.getUrl().toUpperCase().matches(keyWordRegex)) {
						ligne.getMotifRecherche().put("url", keyWord);
					}

					if (critere.getPolitiqueQualites() != null) {
						for (PolitiqueQualiteDto politiqueQualite : critere.getPolitiqueQualites()) {

							if (politiqueQualite.getIndicateurDeQualite().toUpperCase().matches(keyWordRegex)) {
								ligne.getMotifRecherche().put("indicateur de qualité", keyWord);
							}

							if (politiqueQualite.getDescription().toUpperCase().matches(keyWordRegex)) {
								ligne.getMotifRecherche().put("description", keyWord);
							}
							
							if (politiqueQualite.getTauxPromis().toUpperCase().matches(keyWordRegex)) {
								ligne.getMotifRecherche().put("taux promis", keyWord);
							}

							if (politiqueQualite.getTauxQualiteMesures() != null) {
								for (TauxQualiteMesureDto tauxQualiteMesure : politiqueQualite
										.getTauxQualiteMesures()) {

									if (tauxQualiteMesure.getDateDeMesure()!=null && tauxQualiteMesure.getDateDeMesure().toUpperCase().matches(keyWordRegex)) {
										ligne.getMotifRecherche().put("date de mesure ", keyWord);
									}

									if (tauxQualiteMesure.getTauxMesure()!=null && tauxQualiteMesure.getTauxMesure().toUpperCase().matches(keyWordRegex)) {
										ligne.getMotifRecherche().put("taux de mesure ", keyWord);
									}
								}
							}

							if (politiqueQualite.getPolitiqueQualiteParUsages() != null) {
								for (PolitiqueQualiteParUsageDto politiqueQualiteParUsage : politiqueQualite
										.getPolitiqueQualiteParUsages()) {

									if (politiqueQualiteParUsage.getTauxSouhaite() != null && politiqueQualiteParUsage
											.getTauxSouhaite().toUpperCase().matches(keyWordRegex)) {
										ligne.getMotifRecherche().put("taux souhaité", keyWord);
									}

									if (politiqueQualiteParUsage.getUsageMetier() != null && politiqueQualiteParUsage
											.getUsageMetier().getLibelle().toUpperCase().matches(keyWordRegex)) {
										ligne.getMotifRecherche().put("usage metier", keyWord);
									}
								}
							}
						}
					}
				});
			}

			if (((ObjetDto) element).getFamilles() != null) {
				((ObjetDto) element).getFamilles().forEach((famille) -> {
					if (famille.getNom().toUpperCase().matches(keyWordRegex)) {
						ligne.getMotifRecherche().put("famille", keyWord);
					}
				});
			}

			if (((ObjetDto) element).getSousFamilles() != null) {
				for (FamilleDto sousFamille : ((ObjetDto) element).getSousFamilles()) {
					if (sousFamille.getNom().toUpperCase().matches(keyWordRegex)) {
						ligne.getMotifRecherche().put("sous famille", keyWord);
					}
				}
			}

			if (((ObjetDto) element).getFilieres() != null) {
				((ObjetDto) element).getFilieres().forEach(filiere -> {
					if (filiere.getNom().toUpperCase().matches(keyWordRegex)) {
						ligne.getMotifRecherche().put("filiere", keyWord);
					}
				});
			}

			if (((ObjetDto) element).getAbreviation() != null
					&& ((ObjetDto) element).getAbreviation().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("abreviation", keyWord);
			}

			if (!((ObjetDto) element).getDefinitions().isEmpty()
					&& ((ObjetDto) element).getDefinitions().iterator().next().getDefinition() != null
					&& ((ObjetDto) element).getDefinitions().iterator().next().getDefinition().toUpperCase()
							.matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("abreviation", keyWord);
			}

			if (((ObjetDto) element).getCommentaire() != null
					&& ((ObjetDto) element).getCommentaire().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("commentaire", keyWord);
			}

			if (((ObjetDto) element).getProprietaire() != null
					&& ((ObjetDto) element).getProprietaire().getNom().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("proprietaire nom", keyWord);
			}

			if (((ObjetDto) element).getProprietaire() != null
					&& ((ObjetDto) element).getProprietaire().getPrenom().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("proprietaire prenom", keyWord);
			}

			if (((ObjetDto) element).getLibelle() != null
					&& ((ObjetDto) element).getLibelle().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("libelle Objet", keyWord);
			}

			if (((ObjetDto) element).getStatut() != null) {
				if (((ObjetDto) element).getStatut().getLibelle().toUpperCase().matches(keyWordRegex)) {
					ligne.getMotifRecherche().put("statut", keyWord);
				}

				if (((ObjetDto) element).getStatut().getValideur() != null && ((ObjetDto) element).getStatut()
						.getValideur().getNom().toUpperCase().matches(keyWordRegex)) {
					ligne.getMotifRecherche().put("valideur nom", keyWord);
				}

				if (((ObjetDto) element).getStatut().getValideur() != null && ((ObjetDto) element).getStatut()
						.getValideur().getPrenom().toUpperCase().matches(keyWordRegex)) {
					ligne.getMotifRecherche().put("valideur prenom", keyWord);
				}

				if (((ObjetDto) element).getStatut().getSuppleant() != null && ((ObjetDto) element).getStatut()
						.getValideur().getNom().toUpperCase().matches(keyWordRegex)) {
					ligne.getMotifRecherche().put("suppleant nom", keyWord);
				}

				if (((ObjetDto) element).getStatut().getSuppleant() != null && ((ObjetDto) element).getStatut()
						.getValideur().getPrenom().toUpperCase().matches(keyWordRegex)) {
					ligne.getMotifRecherche().put("suppleant prenom", keyWord);
				}
			}
		}

		if (element instanceof TermeDto) {

			if (((TermeDto) element).getFilieres() != null) {
				((TermeDto) element).getFilieres().forEach(filiere -> {
					if (filiere.getNom().toUpperCase().matches(keyWordRegex)) {
						ligne.getMotifRecherche().put("filiere", keyWord);
					}
				});
			}

			if (((TermeDto) element).getAbreviation() != null
					&& ((TermeDto) element).getAbreviation().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("abreviation", keyWord);
			}

			if (!((TermeDto) element).getDefinitions().isEmpty()
					&& ((TermeDto) element).getDefinitions().iterator().next().getDefinition() != null
					&& ((TermeDto) element).getDefinitions().iterator().next().getDefinition().toUpperCase()
							.matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("abreviation", keyWord);
			}

			if (((TermeDto) element).getCommentaire() != null
					&& ((TermeDto) element).getCommentaire().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("commentaire", keyWord);
			}

			if (((TermeDto) element).getProprietaire() != null
					&& ((TermeDto) element).getProprietaire().getNom().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("proprietaire nom", keyWord);
			}

			if (((TermeDto) element).getProprietaire() != null
					&& ((TermeDto) element).getProprietaire().getPrenom().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("proprietaire prenom", keyWord);
			}

			if (((TermeDto) element).getLibelle() != null
					&& ((TermeDto) element).getLibelle().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("libelle Objet", keyWord);
			}

			if (((TermeDto) element).getStatut() != null) {
				if (((TermeDto) element).getStatut().getLibelle().toUpperCase().matches(keyWordRegex)) {
					ligne.getMotifRecherche().put("statut", keyWord);
				}

				if (((TermeDto) element).getStatut().getValideur() != null && ((TermeDto) element).getStatut()
						.getValideur().getNom().toUpperCase().matches(keyWordRegex)) {
					ligne.getMotifRecherche().put("valideur nom", keyWord);
				}

				if (((TermeDto) element).getStatut().getValideur() != null && ((TermeDto) element).getStatut()
						.getValideur().getPrenom().toUpperCase().matches(keyWordRegex)) {
					ligne.getMotifRecherche().put("valideur prenom", keyWord);
				}

				if (((TermeDto) element).getStatut().getSuppleant() != null && ((TermeDto) element).getStatut()
						.getValideur().getNom().toUpperCase().matches(keyWordRegex)) {
					ligne.getMotifRecherche().put("suppleant nom", keyWord);
				}

				if (((TermeDto) element).getStatut().getSuppleant() != null && ((TermeDto) element).getStatut()
						.getValideur().getPrenom().toUpperCase().matches(keyWordRegex)) {
					ligne.getMotifRecherche().put("suppleant prenom", keyWord);
				}
			}

			if (((TermeDto) element).getDocuments() != null) {
				((TermeDto) element).getDocuments().forEach(document -> {
					if (document.getUrl().toUpperCase().matches(keyWordRegex)) {
						ligne.getMotifRecherche().put("document url", keyWord);
					}
				});
			}

			if (((TermeDto) element).getDocuments() != null) {
				((TermeDto) element).getDocuments().forEach(document -> {
					if (document.getTitre().toUpperCase().matches(keyWordRegex)) {
						ligne.getMotifRecherche().put("document titre", keyWord);
					}
				});
			}

		}
		if (element instanceof UtilisateurDto) {

			if (((UtilisateurDto) element).getNom() != null
					&& ((UtilisateurDto) element).getNom().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("acteur nom", keyWord);
			}

			if (((UtilisateurDto) element).getPrenom() != null
					&& ((UtilisateurDto) element).getPrenom().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("acteur prenom", keyWord);
			}

			if (((UtilisateurDto) element).getCp() != null
					&& ((UtilisateurDto) element).getCp().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("acteur cp", keyWord);
			}

			if (((UtilisateurDto) element).getAdresseMail() != null
					&& ((UtilisateurDto) element).getAdresseMail().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("acteur adresse email", keyWord);
			}

			if (((UtilisateurDto) element).getPoste() != null
					&& ((UtilisateurDto) element).getPoste().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("acteur poste", keyWord);
			}

			if (((UtilisateurDto) element).getOrganisation() != null
					&& ((UtilisateurDto) element).getOrganisation().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("acteur organisation", keyWord);
			}

		}
		if (element instanceof GisementDto) {

			if (((GisementDto) element).getLibelle() != null
					&& ((GisementDto) element).getLibelle().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement libelle", keyWord);
			}

			if (((GisementDto) element).getLibelle() != null
					&& ((GisementDto) element).getLibelle().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement libelle", keyWord);
			}

			if (((GisementDto) element).getNiveauDeCompletude() != null
					&& ((GisementDto) element).getNiveauDeCompletude().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement libelle", keyWord);
			}

			if (((GisementDto) element).getNiveauDeCompletude() != null
					&& ((GisementDto) element).getNiveauDeCompletude().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement niveau de completude", keyWord);
			}

			if (((GisementDto) element).getNombreDeJeuxDonnees() != null
					&& ((GisementDto) element).getNombreDeJeuxDonnees().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement nombre de jeux données", keyWord);
			}

			if (((GisementDto) element).getNombreDeClients() != null
					&& ((GisementDto) element).getNombreDeClients().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement nombre de clients", keyWord);
			}

			if (((GisementDto) element).getNombreDeFournisseurs() != null
					&& ((GisementDto) element).getNombreDeFournisseurs().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement nombre de fournisseurs", keyWord);
			}

			if (((GisementDto) element).getNombreDeServices() != null
					&& ((GisementDto) element).getNombreDeServices().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement nombre de services", keyWord);
			}

			if (((GisementDto) element).getDetailsClientsExternes() != null
					&& ((GisementDto) element).getDetailsClientsExternes().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement détails clients externes", keyWord);
			}

			if (((GisementDto) element).getNombreClientsExternes() != null
					&& ((GisementDto) element).getNombreClientsExternes().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement nombre clients externes", keyWord);
			}

			if (((GisementDto) element).getNombreClientsInternes() != null
					&& ((GisementDto) element).getNombreClientsInternes().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement nombre clients internes", keyWord);
			}

			if (((GisementDto) element).getNombreObjetsReferences() != null
					&& ((GisementDto) element).getNombreObjetsReferences().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement nombre objets references", keyWord);
			}

			if (((GisementDto) element).getNombreServicesWebExposesDataLab() != null && ((GisementDto) element)
					.getNombreServicesWebExposesDataLab().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement nombre services web exposes dataLab", keyWord);
			}

			if (((GisementDto) element).getNombreObjetsReferencesEnProjet() != null && ((GisementDto) element)
					.getNombreObjetsReferencesEnProjet().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement nombre objets references en projet", keyWord);
			}

			if (((GisementDto) element).getNombreServicesEnProjet() != null
					&& ((GisementDto) element).getNombreServicesEnProjet().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement nombre services en projet", keyWord);
			}

			if (((GisementDto) element).getNombreServicesWebExposesDataLabEnProjet() != null && ((GisementDto) element)
					.getNombreServicesWebExposesDataLabEnProjet().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement nombre de services web exposes dataLab en projet", keyWord);
			}

			if (((GisementDto) element).getRaisonEtre() != null
					&& ((GisementDto) element).getRaisonEtre().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement raison d'etre", keyWord);
			}

			if (((GisementDto) element).getRaisonEtreCourte() != null
					&& ((GisementDto) element).getRaisonEtreCourte().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement raison d'etre courte", keyWord);
			}

			if (((GisementDto) element).getServicesDocumentes() != null
					&& ((GisementDto) element).getServicesDocumentes().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement services doculentés", keyWord);
			}

			if (((GisementDto) element).getStatut() != null
					&& ((GisementDto) element).getStatut().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement statut", keyWord);
			}

			if (((GisementDto) element).getDescription() != null
					&& ((GisementDto) element).getDescription().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement description", keyWord);
			}

			if (((GisementDto) element).getDateMiseEnService() != null
					&& ((GisementDto) element).getDateMiseEnService().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("gisement date mise en service", keyWord);
			}

			if (((GisementDto) element).getActeurs() != null) {
				((GisementDto) element).getActeurs().forEach(acteur -> {
					if (acteur.getNom().toUpperCase().matches(keyWordRegex)) {
						ligne.getMotifRecherche().put("gisement acteur nom", keyWord);
					}
					if (acteur.getContact().toUpperCase().matches(keyWordRegex)) {
						ligne.getMotifRecherche().put("gisement acteur contact", keyWord);
					}
					if (acteur.getRole().toUpperCase().matches(keyWordRegex)) {
						ligne.getMotifRecherche().put("gisement acteur role", keyWord);
					}
				});
			}

			if (((GisementDto) element).getFilieres() != null) {
				((GisementDto) element).getFilieres().forEach(filiere -> {
					if (filiere.getNom().toUpperCase().matches(keyWordRegex)) {
						ligne.getMotifRecherche().put("gisement filiere nom", keyWord);
					}
				});
			}

			if (((GisementDto) element).getLiensUtiles() != null) {
				((GisementDto) element).getLiensUtiles().forEach(lienUtile -> {
					if (lienUtile.getLibelle().toUpperCase().matches(keyWordRegex)) {
						ligne.getMotifRecherche().put("gisement lien utile libelle", keyWord);
					}
					if (lienUtile.getUrl().toUpperCase().matches(keyWordRegex)) {
						ligne.getMotifRecherche().put("gisement lien utile url", keyWord);
					}
				});
			}
		}
		if (element instanceof FiliereDto) {

			if (((FiliereDto) element).getNom() != null
					&& ((FiliereDto) element).getNom().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("filiere nom", keyWord);
			}

			if (((FiliereDto) element).getDescription() != null
					&& ((FiliereDto) element).getDescription().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("filiere description", keyWord);
			}

			if (((FiliereDto) element).getFamilles() != null) {
				((FiliereDto) element).getFamilles().forEach(famille -> {
					if (famille.getNom().toUpperCase().matches(keyWordRegex)) {
						ligne.getMotifRecherche().put("filiere famille nom", keyWord);
					}
					if (famille.getSousFamilles() != null) {
						famille.getSousFamilles().forEach(sousFamille -> {
							if (sousFamille.getNom().toUpperCase().matches(keyWordRegex)) {
								ligne.getMotifRecherche().put("filiere sous famille nom", keyWord);
							}
						});
					}
				});
			}

			if (((FiliereDto) element).getCoordinateurDeFiliere() != null
					&& ((FiliereDto) element).getCoordinateurDeFiliere().getNom().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("filiere nom coordinateur de filiere", keyWord);
			}

			if (((FiliereDto) element).getResponsableDeFiliere() != null
					&& ((FiliereDto) element).getResponsableDeFiliere().getNom().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("filiere nom responsable de filiere", keyWord);
			}

			if (((FiliereDto) element).getResponsableDeFiliereDelegue() != null && ((FiliereDto) element)
					.getResponsableDeFiliereDelegue().getNom().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("filiere nom responsable de filiere delegue", keyWord);
			}

			if (((FiliereDto) element).getCoordinateurDeFiliere() != null && ((FiliereDto) element)
					.getCoordinateurDeFiliere().getPrenom().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("filiere prenom coordinateur de filiere", keyWord);
			}

			if (((FiliereDto) element).getResponsableDeFiliere() != null && ((FiliereDto) element)
					.getResponsableDeFiliere().getPrenom().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("filiere prenom responsable de filiere", keyWord);
			}

			if (((FiliereDto) element).getResponsableDeFiliereDelegue() != null && ((FiliereDto) element)
					.getResponsableDeFiliereDelegue().getPrenom().toUpperCase().matches(keyWordRegex)) {
				ligne.getMotifRecherche().put("filiere prenom responsable de filiere delegue", keyWord);
			}

		}

	}

}
