package sncf.reseau.cdsdpadsir.agora.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sncf.reseau.cdsdpadsir.agora.entity.RelationClasseMere;
import sncf.reseau.cdsdpadsir.agora.model.ClasseDobjetDto;
import sncf.reseau.cdsdpadsir.agora.model.RelationClasseMereDto;
import sncf.reseau.cdsdpadsir.agora.repository.RelationClasseMereRepository;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;
import sncf.reseau.cdsdpadsir.agora.xmi.util.DistinctByKey;

@Service
public class RelationClasseMereService {

	Logger log = LoggerFactory.getLogger(RelationClasseMereService.class);
	private RelationClasseMereRepository relationClasseMereRepository;
	private ClasseDobjetService classeDobjetService;
	private ModelMapper modelMapper;

	@Autowired
	public RelationClasseMereService(RelationClasseMereRepository relationClasseMereRepository,
			ClasseDobjetService classeDobjetService, ModelMapper modelMapper) {

		this.relationClasseMereRepository = relationClasseMereRepository;
		this.classeDobjetService = classeDobjetService;
		this.modelMapper = modelMapper;
		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	public List<RelationClasseMereDto> addAll(List<RelationClasseMereDto> relationsDto) {
		return relationsDto.stream().filter(new DistinctByKey<>(RelationClasseMereDto::getIdExterne))
				.map(relationDto -> {
					log.debug("addAll Relation id {} , idStart  {} , idEnd {} , type {}", relationDto.getId(),
							null != relationDto.getClasseDobjeMere() ? relationDto.getClasseDobjeMere().getIdExterne()
									: null,
							null != relationDto.getClasseDobjetFille()
									? relationDto.getClasseDobjetFille().getIdExterne()
									: null);

					if (null != relationDto.getClasseDobjeMere()) {
						List<ClasseDobjetDto> classesMereDto = classeDobjetService
								.getByIdExterneAndMaxRevisionFonctionnelle(
										relationDto.getClasseDobjeMere().getIdExterne());
						if (!classesMereDto.isEmpty()) {
							relationDto.setClasseDobjeMere(classesMereDto.get(0));
						}
					}
					if (null != relationDto.getClasseDobjetFille()) {
						List<ClasseDobjetDto> classesFilleDto = classeDobjetService
								.getByIdExterneAndMaxRevisionFonctionnelle(
										relationDto.getClasseDobjetFille().getIdExterne());
						if (!classesFilleDto.isEmpty()) {
							relationDto.setClasseDobjetFille(classesFilleDto.get(0));
						}
					}
					return add(relationDto);
				}).collect(Collectors.toList());
	}

	@Transactional
	public RelationClasseMereDto add(RelationClasseMereDto relationDto) {

		RelationClasseMere relationClasseMere = null;
		// get id agora id exist
		List<RelationClasseMere> relationsClasseMere = relationClasseMereRepository
				.findByIdExterneAndMaxRevisionFonctionnelle(relationDto.getIdExterne());

		if (null != relationsClasseMere && !relationsClasseMere.isEmpty()) {
			ArianeHelper.updateEntityDetails(relationsClasseMere.get(0), relationDto);
			relationsClasseMere.get(0).setDateFinActivite(LocalDateTime.now());
			relationClasseMereRepository.save(relationsClasseMere.get(0));
		} else {
			ArianeHelper.setNewEntityDetails(relationDto);
		}
		relationDto.setRevisionFonctionnel(Timestamp.valueOf(LocalDateTime.now()));

		relationClasseMere = relationClasseMereRepository.save(modelMapper.map(relationDto, RelationClasseMere.class));
		return relationClasseMere != null ? relationClasseMere.mapToDto() : null;

	}

	public RelationClasseMereDto getByIdExterneVersionRevisionFonctionnel(String idExterne, String version,
			Timestamp revisionFonctionnel) {
		List<RelationClasseMere> relations = relationClasseMereRepository
				.findByIdExterneAndMaxRevisionFonctionnelle(idExterne);
		if (null != relations && !relations.isEmpty()) {
			return relations.get(0).mapToDto();
		} else {
			return null;
		}
	}

}
