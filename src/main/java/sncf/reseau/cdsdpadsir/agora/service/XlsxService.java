package sncf.reseau.cdsdpadsir.agora.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sncf.reseau.cdsdpadsir.agora.api.StructureFromXmiController;
import sncf.reseau.cdsdpadsir.agora.entity.Famille;
import sncf.reseau.cdsdpadsir.agora.entity.Filiere;
import sncf.reseau.cdsdpadsir.agora.entity.Utilisateur;
import sncf.reseau.cdsdpadsir.agora.model.AttributDto;
import sncf.reseau.cdsdpadsir.agora.model.DefinitionDto;
import sncf.reseau.cdsdpadsir.agora.model.FamilleDto;
import sncf.reseau.cdsdpadsir.agora.model.FiliereDto;
import sncf.reseau.cdsdpadsir.agora.model.ObjetDto;
import sncf.reseau.cdsdpadsir.agora.model.StatutClasseDobjetDto;
import sncf.reseau.cdsdpadsir.agora.model.StatutTermeDto;
import sncf.reseau.cdsdpadsir.agora.model.TermeDto;
import sncf.reseau.cdsdpadsir.agora.model.UtilisateurDto;
import sncf.reseau.cdsdpadsir.agora.repository.FamilleRepository;
import sncf.reseau.cdsdpadsir.agora.repository.FiliereRepository;
import sncf.reseau.cdsdpadsir.agora.repository.UtilisateurRepository;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;

@Service
public class XlsxService {

	private FiliereRepository filiereRepository;
	private UtilisateurRepository utilisateurRepository;
	private TermeService termeService;
	private FamilleRepository familleRepository;
	private ObjetService objetService;
	private static final Logger logger = LoggerFactory.getLogger(StructureFromXmiController.class);
	private static final String libelle_statut = "Brouillon";

	@Autowired
	public XlsxService(FiliereRepository filiereRepository, TermeService termeService,
			FamilleRepository familleRepository, ObjetService objetService,
			UtilisateurRepository utilisateurRepository) {
		super();
		this.filiereRepository = filiereRepository;
		this.utilisateurRepository = utilisateurRepository;
		this.termeService = termeService;
		this.familleRepository = familleRepository;
		this.objetService = objetService;
	}

	@Transactional
	public void loadTermes(String nomFiliere, String nomStatut, String prenomStatut, String fileName) throws Exception {

		Filiere filiere = filiereRepository.findTopByNomOrderByDateFinActiviteDesc(nomFiliere).get();
		Utilisateur utilisateur = utilisateurRepository.findByNomIgnoreCaseAndPrenomIgnoreCaseAndDateFinActivite(
				nomStatut, prenomStatut, ArianeHelper.FUTURE_DATE).get();

		logger.info("filiere");
		File excelFile = new File("/home/sbt000p1/inst001/app/" + fileName + ".xlsx");
		FileInputStream fis = new FileInputStream(excelFile);

		logger.info(excelFile.getName());
		// we create an XSSF Workbook object for our XLSX Excel File
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		// we get first sheet
		XSSFSheet sheet = workbook.getSheetAt(0);

		// we iterate on rows
		Iterator<Row> rowIt = sheet.iterator();

		while (rowIt.hasNext()) {
			Row row = rowIt.next();

			// iterate on cells for the current row
			Iterator<Cell> cellIterator = row.cellIterator();

			List<String> line = new ArrayList<>();

			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				try {
					line.add(cell.getStringCellValue());
				} catch (Exception exp) {
					logger.error(exp.getMessage());
				}
			}

			TermeDto terme = new TermeDto();
			terme.setLibelle(line.get(0).equals(" ") ? "" : line.get(0));
			terme.setDefinitions(new HashSet<>());
			DefinitionDto definition = new DefinitionDto();
			definition.setDefinition(line.get(1).equals(" ") ? "" : line.get(1));
			terme.getDefinitions().add(definition);
			try {
				terme.setAbreviation(line.get(2).equals(" ") ? "" : line.get(2));
			} catch (Exception exp) {
				logger.error(exp.getMessage());
			}

			terme.setCommentaire(line.get(3).equals(" ") ? "" : line.get(3));

			FiliereDto filiereDto = new FiliereDto();
			filiereDto.setIdFiliere(filiere.getIdFiliere());

			terme.setFilieres(new HashSet<>());
			terme.getFilieres().add(filiereDto);

			StatutTermeDto statut = new StatutTermeDto();
			UtilisateurDto valideur = new UtilisateurDto();
			valideur.setIdUtilisateur(utilisateur.getIdUtilisateur());
			valideur.setIdIdentiteNumerique(utilisateur.getIdentiteNumerique().getIdIdentiteNumerique());
			UtilisateurDto suppleant = valideur;
			UtilisateurDto proprietaire = valideur;
			terme.setProprietaire(proprietaire);

			statut.setValideur(valideur);
			statut.setSuppleant(suppleant);
			statut.setLibelle(libelle_statut);
			statut.setPrenom(prenomStatut);
			statut.setNom(nomStatut);

			terme.setStatut(statut);
			terme.setDocuments(new HashSet<>());
			terme.setObjets(new HashSet<>());
			terme.setUtilisateurs(new HashSet<>());

			termeService.add(terme);
		}

		workbook.close();
		fis.close();
	}

	@Transactional
	public void loadObjets() throws IOException {

		Filiere filiere = filiereRepository.findByIdFiliere(182L).get();
		File excelFile = new File("/home/sbt000p1/inst001/app/termes.xlsx");
		FileInputStream fis = new FileInputStream(excelFile);

		// we create an XSSF Workbook object for our XLSX Excel File
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		// we get first sheet
		XSSFSheet sheet = workbook.getSheetAt(1);

		// we iterate on rows
		Iterator<Row> rowIt = sheet.iterator();
		List<List<String>> grid = new ArrayList<>();

		while (rowIt.hasNext()) {
			Row row = rowIt.next();

			// iterate on cells for the current row
			Iterator<Cell> cellIterator = row.cellIterator();

			List<String> line = new ArrayList<>();

			while (cellIterator.hasNext()) {
				try {
					Cell cell = cellIterator.next();
					line.add(cell.getStringCellValue());
				} catch (Exception exp) {

				}
			}

			grid.add(line);

		}

		for (int i = 0; i < grid.size(); i++) {
			ObjetDto objet = new ObjetDto();

			objet.setLibelle(grid.get(i).get(0));
			DefinitionDto definition = new DefinitionDto();
			definition.setDefinition(grid.get(i).get(1));
			objet.setDefinitions(new HashSet<>());
			objet.getDefinitions().add(definition);

			Famille famille = familleRepository.findByNomAndDateFinActivite(grid.get(i).get(4),
					ArianeHelper.FUTURE_DATE);
			objet.setFamilles(new HashSet<>());
			if (famille != null) {
				FamilleDto familleDto = new FamilleDto();
				familleDto.setIdFamille(famille.getIdFamille());
				objet.getFamilles().add(familleDto);
			}

			FiliereDto filiereDto = new FiliereDto();
			filiereDto.setIdFiliere(filiere.getIdFiliere());
			objet.setFilieres(new HashSet<>());
			objet.getFilieres().add(filiereDto);

			StatutClasseDobjetDto statut = new StatutClasseDobjetDto();
			UtilisateurDto valideur = new UtilisateurDto();
			valideur.setIdUtilisateur(830L);
			valideur.setIdIdentiteNumerique(831L);
			UtilisateurDto suppleant = valideur;
			UtilisateurDto proprietaire = valideur;
			objet.setProprietaire(proprietaire);

			statut.setValideur(valideur);
			statut.setSuppleant(suppleant);
			statut.setLibelle("Brouillon");
			statut.setPrenom("Aminata");
			statut.setNom("DIONE");

			objet.setStatut(statut);

			objet.setResponsables(new HashSet<>());
			objet.setTermes(new HashSet<>());

			objet.setAttributClasses(new HashSet<>());
			for (int j = i; j < grid.size(); j++) {
				if (grid.get(i).get(0).equals(grid.get(j).get(0))) {
					AttributDto attribut = new AttributDto();
					attribut.setLibelle(grid.get(j).get(2));
					attribut.setValeur(grid.get(j).get(3));
					objet.getAttributClasses().add(attribut);
				} else {
					i = j - 1;
					break;
				}
			}

			objetService.add(objet);
		}

		workbook.close();
		fis.close();
	}

}
