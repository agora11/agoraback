package sncf.reseau.cdsdpadsir.agora.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sncf.reseau.cdsdpadsir.agora.entity.AttributClasse;
import sncf.reseau.cdsdpadsir.agora.model.AttributClasseDto;
import sncf.reseau.cdsdpadsir.agora.model.ClasseDobjetDto;
import sncf.reseau.cdsdpadsir.agora.model.TypeAttributClasseDto;
import sncf.reseau.cdsdpadsir.agora.repository.AttributClasseRepository;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;

@Service
public class AttributClasseService {
	
	private AttributClasseRepository attributClasseRepository;
	private TypeAttributClasseService typeAttributClasseService ;
	private ClasseDobjetService classeDobjetService ;
	private ModelMapper modelMapper;
	
	@Autowired
	public AttributClasseService(
			AttributClasseRepository attributClasseRepository ,
			TypeAttributClasseService typeAttributClasseService ,
			ClasseDobjetService classeDobjetService,
			ModelMapper modelMapper
			) {
		
		this.attributClasseRepository =  attributClasseRepository ;
		this.typeAttributClasseService = typeAttributClasseService ;
		this.classeDobjetService = classeDobjetService;
		this.modelMapper = modelMapper ;
		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}
	
	public Set<AttributClasseDto> addAll(Set<AttributClasseDto> attributClasseDtoList) {

		attributClasseDtoList.stream().forEach(attrDto -> add(attrDto));
		return attributClasseDtoList ;
	}
	
	@Transactional
	public AttributClasseDto add(AttributClasseDto attributClasseDto) {
		
		if(null != attributClasseDto.getTypeAttributClasse() && null != attributClasseDto.getTypeAttributClasse().getType()) {	 // propriete sans type exp : referenceMetier
			
			Optional<TypeAttributClasseDto> typeAttribut = typeAttributClasseService
					.getByTypeAttribut(attributClasseDto.getTypeAttributClasse().getType());
			if( typeAttribut.isPresent() ) {
				attributClasseDto.setTypeAttributClasse(typeAttribut.get());
			}else {				
				TypeAttributClasseDto typeAttributDto = new TypeAttributClasseDto();
				typeAttributDto.setType(attributClasseDto.getTypeAttributClasse().getType());
				typeAttributDto.setTypeElementModeleObjet(attributClasseDto.getTypeElementModeleObjet());
				typeAttributDto.setDateCreation(LocalDateTime.now());
				typeAttributDto = typeAttributClasseService.add(typeAttributDto) ;				
				attributClasseDto.setTypeAttributClasse(typeAttributDto);
			}
			
			if(null != attributClasseDto.getClasseDobjet()) {				
				List<ClasseDobjetDto> classesDobjetDtoDB = classeDobjetService
						.getByIdExterneAndMaxRevisionFonctionnelle(attributClasseDto.getClasseDobjet().getIdExterne());				
				if(!classesDobjetDtoDB.isEmpty()) {
					attributClasseDto.setClasseDobjet(classesDobjetDtoDB.get(0));
				}
			}			
		}
		
		// get id agora id exist
		List<AttributClasse> attributsClasseEntity = attributClasseRepository.findByIdExterneAndMaxRevisionFonctionnelle(attributClasseDto.getIdExterne());
		if(null != attributsClasseEntity && !attributsClasseEntity.isEmpty()) {
			ArianeHelper.updateEntityDetails(attributsClasseEntity.get(0), attributClasseDto);
			attributsClasseEntity.get(0).setDateFinActivite(LocalDateTime.now());
			attributClasseRepository.save(attributsClasseEntity.get(0));
		}else {
			ArianeHelper.setNewEntityDetails(attributClasseDto);
		}
		attributClasseDto.setRevisionFonctionnel(Timestamp.valueOf(LocalDateTime.now()));
		
		AttributClasse model = modelMapper.map(attributClasseDto, AttributClasse.class);
		attributClasseRepository.save(model) ;			
		attributClasseRepository.flush();
		return attributClasseDto ;
	}
	
	public AttributClasseDto getByIdExterneAndMaxRevisionFonctionnelle(String idExterne) {
		List<AttributClasse> attrs = attributClasseRepository.findByIdExterneAndMaxRevisionFonctionnelle(idExterne) ;
		return mapEntityToDto(attrs);
	}
	
	public AttributClasseDto getByIdExterneVersionRevisionFonctionnel(String idExterne, String version , Timestamp revisionFonctionnel ) {
		List<AttributClasse> attrs = attributClasseRepository.findByIdExterneVersionRevisionFonctionnel(idExterne, version, revisionFonctionnel) ;
		return mapEntityToDto(attrs);
	}

	private AttributClasseDto mapEntityToDto(List<AttributClasse> attrs) {
		if(null != attrs && !attrs.isEmpty()) {
			return attrs.get(0).mapToDto();
		}else {
			return null ;
		}
	}

	
}
