package sncf.reseau.cdsdpadsir.agora.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import sncf.reseau.cdsdpadsir.agora.entity.AssociationClasseDobjetFamille;
import sncf.reseau.cdsdpadsir.agora.entity.AssociationClasseDobjetSousFamille;
import sncf.reseau.cdsdpadsir.agora.entity.ClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.entity.Famille;
import sncf.reseau.cdsdpadsir.agora.entity.FamilleResponsable;
import sncf.reseau.cdsdpadsir.agora.entity.Filiere;
import sncf.reseau.cdsdpadsir.agora.entity.FiliereFamille;
import sncf.reseau.cdsdpadsir.agora.entity.IdentiteNumerique;
import sncf.reseau.cdsdpadsir.agora.entity.Utilisateur;
import sncf.reseau.cdsdpadsir.agora.entity.utils.PagingResponse;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;
import sncf.reseau.cdsdpadsir.agora.model.FamilleDto;
import sncf.reseau.cdsdpadsir.agora.model.FiliereDto;
import sncf.reseau.cdsdpadsir.agora.model.ObjetDto;
import sncf.reseau.cdsdpadsir.agora.model.UtilisateurDto;
import sncf.reseau.cdsdpadsir.agora.repository.FamilleObjetRepository;
import sncf.reseau.cdsdpadsir.agora.repository.FamilleRepository;
import sncf.reseau.cdsdpadsir.agora.repository.FamilleResponsableRepository;
import sncf.reseau.cdsdpadsir.agora.repository.FiliereFamilleRepository;
import sncf.reseau.cdsdpadsir.agora.repository.IdentiteNumeriqueRepository;
import sncf.reseau.cdsdpadsir.agora.repository.SousFamilleObjetRepository;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;

@Service
public class FamilleService {

	private FamilleRepository familleRepository;
	private FamilleObjetRepository familleObjetRepository;
	private SousFamilleObjetRepository sousFamilleObjetRepository;
	private FamilleResponsableRepository familleResponsableRepository;
	private FiliereFamilleRepository filiereFamilleRepository;
	private IdentiteNumeriqueRepository identiteNumeriqueRepository;

	private ModelMapper modelMapper;

	@Autowired
	public FamilleService(FamilleRepository familleRepository, FamilleObjetRepository familleObjeteRepository,
			FiliereFamilleRepository filiereFamilleRepository, IdentiteNumeriqueRepository identiteNumeriqueRepository,
			SousFamilleObjetRepository sousFamilleObjetRepository,
			FamilleResponsableRepository familleResponsableRepository, ModelMapper modelMapper) {
		super();
		this.familleRepository = familleRepository;
		this.familleObjetRepository = familleObjeteRepository;
		this.filiereFamilleRepository = filiereFamilleRepository;
		this.identiteNumeriqueRepository = identiteNumeriqueRepository;
		this.familleResponsableRepository = familleResponsableRepository;
		this.sousFamilleObjetRepository = sousFamilleObjetRepository;
		this.modelMapper = modelMapper;

		this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	public PagingResponse<FamilleDto> findAll(Specification<Famille> activeFamille, Sort sort, @Valid BigDecimal page,
			@Valid BigDecimal size) {
		if (isRequestPaged(page, size))
			return findAll(activeFamille, buildPageRequest(page, size, sort));

		List<FamilleDto> entities = findAll(activeFamille, sort);
		return new PagingResponse<>((long) entities.size(), 0L, 0L, 0L, 0L, entities);
	}

	private PagingResponse<FamilleDto> findAll(Specification<Famille> spec, Pageable pageable) {
		Page<Famille> famille = familleRepository.findAll(spec, pageable);
		List<Famille> content = famille.getContent();
		return new PagingResponse<>(famille.getTotalElements(), (long) famille.getNumber(),
				(long) famille.getNumberOfElements(), pageable.getOffset(), (long) famille.getTotalPages(),
				content.stream().map(mapper -> mapFamille(mapper)).collect(Collectors.toList()));
	}

	public List<FamilleDto> findAll(Specification<Famille> spec, Sort sort) {
		return familleRepository.findAll(spec, sort).stream().map(mapper -> mapFamille(mapper))
				.collect(Collectors.toList());
	}

	private boolean isRequestPaged(BigDecimal page, BigDecimal size) {
		return (page != null && size != null);
	}

	private Pageable buildPageRequest(BigDecimal page, BigDecimal size, Sort sort) {
		return PageRequest.of(page.intValue(), size.intValue(), sort);
	}

	public FamilleDto add(@Valid FamilleDto familleDto) {
		ArianeHelper.setNewEntityDetails(familleDto);
		Famille model = modelMapper.map(familleDto, Famille.class);
		if (familleDto.getFamilleMere() != null) {
			model.setFamille(modelMapper.map(familleDto.getFamilleMere(), Famille.class));
		}
		Famille famille = familleRepository.save(model);
		updateJoins(familleDto, model, famille);
		return mapFamille(famille);
	}

	private void updateJoins(@Valid FamilleDto familleDto, Famille model, Famille famille) {
		boolean update = false;
		if (model.getAssociationClasseDobjetFamilles() != null || model.getFamilleResponsables() != null
				|| model.getFiliereFamilles() != null || model.getFamilles() != null) {
			update = true;
		}
		final boolean flag = update;

		model.setAssociationClasseDobjetFamilles(new HashSet<>());
		model.setAssociationClasseDobjetSousFamilles(new HashSet<>());
		model.setFamilleResponsables(new HashSet<>());
		model.setFiliereFamilles(new HashSet<>());
		model.setFamilles(new HashSet<>());

		if (familleDto.getObjets() != null) {
			familleDto.getObjets().forEach(item -> {
				if (item.getIdClasseDobjet() != null) {
					if (familleDto.getFamilleMere() == null) {
						AssociationClasseDobjetFamille familleObjet = new AssociationClasseDobjetFamille();
						familleObjet.setClasseDobjet(modelMapper.map(item, ClasseDobjet.class));
						familleObjet.setFamille(famille);
						ariane(familleDto, familleObjet, flag);
						familleObjetRepository.save(familleObjet);
						model.getAssociationClasseDobjetFamilles().add(familleObjet);
					} else {
						AssociationClasseDobjetSousFamille sousFamilleObjet = new AssociationClasseDobjetSousFamille();
						sousFamilleObjet.setClasseDobjet(modelMapper.map(item, ClasseDobjet.class));
						sousFamilleObjet.setFamille(famille);
						ariane(familleDto, sousFamilleObjet, flag);
						sousFamilleObjetRepository.save(sousFamilleObjet);
						model.getAssociationClasseDobjetSousFamilles().add(sousFamilleObjet);
					}

				}
			});
		}

		if (familleDto.getResponsableDeDonnees() != null) {
			FamilleResponsable familleResponsable = new FamilleResponsable();
			Optional<IdentiteNumerique> id = identiteNumeriqueRepository
					.findByUtilisateur(modelMapper.map(familleDto.getResponsableDeDonnees(), Utilisateur.class));
			if (id.isPresent()) {
				familleResponsable.setIdentiteNumerique(id.get());
				familleResponsable.setFamille(famille);
				ariane(familleDto, familleResponsable, flag);
				familleResponsableRepository.save(familleResponsable);
				model.getFamilleResponsables().add(familleResponsable);
			}
		}

		if (familleDto.getFiliere() != null && familleDto.getFiliere().getIdFiliere() != null) {
			FiliereFamille filiereFamille = new FiliereFamille();
			filiereFamille.setFiliere(modelMapper.map(familleDto.getFiliere(), Filiere.class));
			filiereFamille.setFamille(famille);
			ariane(familleDto, filiereFamille, flag);
			filiereFamilleRepository.save(filiereFamille);
			model.getFiliereFamilles().add(filiereFamille);
		}

		if (familleDto.getSousFamilles() != null) {
			familleDto.getSousFamilles().forEach(item -> {
				Famille sousFamille = modelMapper.map(item, Famille.class);
				sousFamille.setFamille(famille);
				familleRepository.save(sousFamille);
				model.getFamilles().add(sousFamille);
			});
		}

	}

	<T extends RessourceAbstraite> void ariane(T obj, T response, boolean flag) {
		if (!flag) {
			ArianeHelper.setNewEntityDetails(response);
		} else {
			ArianeHelper.updateEntityDetails(obj, response);
		}
	}

	public FamilleDto update(FamilleDto familleDto) {
		Optional<Famille> entity = familleRepository.findByIdFamille(familleDto.getIdFamille());

		if (entity.isPresent()) {
			Famille famille = new Famille();
			modelMapper.map(entity.get(), famille);
			famille.setIdFamille(null);
			famille.setNom(familleDto.getNom());
			ArianeHelper.updateEntityDetails(familleDto, famille);
			famille = familleRepository.save(famille);
			updateJoins(familleDto, famille, famille);
			updateEndDate(entity);

			return mapFamille(famille);
		} else {
			throw new NoSuchElementException();
		}
	}

	private FamilleDto mapFamille(Famille famille) {
		FamilleDto familleDto = modelMapper.map(famille, FamilleDto.class);
		familleDto.setObjets(new ArrayList<>());
		familleDto.setTermes(new ArrayList<>());
		familleDto.setSousFamilles(new ArrayList<>());

		if (famille.getFiliereFamilles() != null) {
			famille.getFiliereFamilles()
					.forEach(item -> familleDto.setFiliere(modelMapper.map(item.getFiliere(), FiliereDto.class)));
		}

		if (famille.getFamilles() != null) {
			for (Famille item : famille.getFamilles()) {
				familleDto.getSousFamilles().add(mapFamille(item));
			}
		}

		if (famille.getFamille() != null) {
			familleDto.setFamilleMere(modelMapper.map(famille.getFamille(), FamilleDto.class));
		}

		if (famille.getAssociationClasseDobjetFamilles() != null) {
			famille.getAssociationClasseDobjetFamilles().forEach(
					item -> familleDto.getObjets().add(modelMapper.map(item.getClasseDobjet(), ObjetDto.class)));
		}

		if (famille.getFamilleResponsables() != null) {
			for (FamilleResponsable item : famille.getFamilleResponsables()) {
				if (item.getIdentiteNumerique() != null) {
					familleDto.setResponsableDeDonnees(
							modelMapper.map(item.getIdentiteNumerique().getUtilisateur(), UtilisateurDto.class));
				}
			}
		}

		return familleDto;
	}

	private void updateEndDate(Optional<Famille> entity) {
		if (entity.isPresent()) {
			entity.get().getAssociationClasseDobjetFamilles()
					.forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
			familleObjetRepository.saveAll(entity.get().getAssociationClasseDobjetFamilles());

			entity.get().getAssociationClasseDobjetSousFamilles()
					.forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
			sousFamilleObjetRepository.saveAll(entity.get().getAssociationClasseDobjetSousFamilles());

			entity.get().getFamilleResponsables().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
			familleResponsableRepository.saveAll(entity.get().getFamilleResponsables());

			entity.get().getFiliereFamilles().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
			filiereFamilleRepository.saveAll(entity.get().getFiliereFamilles());

			entity.get().setDateFinActivite(LocalDateTime.now());

			familleRepository.save(entity.get());
		}
	}

	public FamilleDto delete(FamilleDto familleDto) {
		Optional<Famille> entity = familleRepository.findByIdFamille(familleDto.getIdFamille());

		if (entity.get().getFamilles() != null) {
			entity.get().getFamilles().forEach(item -> item.setDateFinActivite(LocalDateTime.now()));
			familleRepository.saveAll(entity.get().getFamilles());
		}

		this.updateEndDate(entity);
		return mapFamille(entity.get());
	}

}
