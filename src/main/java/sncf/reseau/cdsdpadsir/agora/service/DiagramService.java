package sncf.reseau.cdsdpadsir.agora.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sncf.reseau.cdsdpadsir.agora.entity.*;
import sncf.reseau.cdsdpadsir.agora.generic.model.RessourceAbstraite;
import sncf.reseau.cdsdpadsir.agora.model.DiagramDto;
import sncf.reseau.cdsdpadsir.agora.model.ObjetDto;
import sncf.reseau.cdsdpadsir.agora.repository.*;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DiagramService {

	@Autowired
	private DiagramRepository diagramRepository;
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private ObjetService objetService;
	@Autowired
	private RelationClasseDobjetRepository relationClasseDobjetRepository;
	@Autowired
	private RelationClasseMereRepository relationClasseMereRepository;
	@Autowired
	private CellRepository cellRepository;
	@Autowired
	private FiliereRepository filiereRepository;

	public DiagramDto saveDiagram(DiagramDto diagram) {

		List<Cell> updatedCells = new ArrayList<>();
		Diagram diagramEntity = new Diagram();
		ArianeHelper.setNewEntityDetails(diagramEntity);
		Diagram savedDiagram = mapCellObjectAndSaveDiagram(diagramEntity, diagram, updatedCells, false);
		return modelMapper.map(savedDiagram, DiagramDto.class);

	}

	public List<DiagramDto> getAllDiagrams() {
		List<Diagram> diagramEntities = diagramRepository.findAll();
		diagramEntities = diagramEntities.stream()
				.filter(diagram -> diagram.getDateFinActivite().isEqual(ArianeHelper.FUTURE_DATE))
				.collect(Collectors.toList());
		return diagramEntities.stream()
				.filter(diagram -> diagram.getDateFinActivite().isEqual(ArianeHelper.FUTURE_DATE)).map(diagram -> {
					DiagramDto diagramDto = new DiagramDto();
					modelMapper.map(diagram, diagramDto);
					List<String> updatedCells = diagram.getCells().stream().map(cell -> addObjectDataToCell(cell))
							.collect(Collectors.toList());
					diagramDto.setCells(updatedCells);
					List<String> associationsIds = extractRelationsIds(updatedCells, true);
					List<String> inheritancesIds = extractRelationsIds(updatedCells, false);
					diagramDto.setAssociations(associationsIds);
					diagramDto.setInheritances(inheritancesIds);
					return diagramDto;
				}).collect(Collectors.toList());
	}

	public DiagramDto getDiagram(Long id) {
		Optional<Diagram> optionalDiagram = diagramRepository.findById(id);
		DiagramDto diagramDto = new DiagramDto();
		if (optionalDiagram.isPresent()) {
			Diagram diagram = optionalDiagram.get();
			modelMapper.map(diagram, diagramDto);
			List<String> updatedCells = diagram.getCells().stream().map(cell -> addObjectDataToCell(cell))
					.collect(Collectors.toList());
			diagramDto.setCells(updatedCells);
			List<String> associationsIds = extractRelationsIds(updatedCells, true);
			List<String> inheritancesIds = extractRelationsIds(updatedCells, false);
			diagramDto.setAssociations(associationsIds);
			diagramDto.setInheritances(inheritancesIds);

		}
		return diagramDto;
	}

	private List<String> extractRelationsIds(List<String> cells, boolean isAssociation) {

		List<String> relationsIds = new ArrayList<>();
		for (String cell : cells) {
			try {
				JsonNode cellJsonNode = objectMapper.readTree(cell);
				if (cellJsonNode.get("type").asText().equals("devs.Link")
						&& cellJsonNode.get("isAssociationType").asBoolean() == isAssociation) {
					relationsIds.add(cellJsonNode.get("id").asText());
				}
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}

		return relationsIds;
	}

	@Transactional
	public DiagramDto updateDiagram(DiagramDto diagram) {
		List<Cell> updatedCells = new ArrayList<>();
		Diagram diagramEntity = diagramRepository.getOne(diagram.getDiagramId());
		Diagram updatedDiagram = mapCellObjectAndSaveDiagram(diagramEntity, diagram, updatedCells, true);
		List<String> associationsIds = extractRelationsIds(
				updatedDiagram.getCells().stream().map(Cell::getCell).collect(Collectors.toList()), true);
		List<String> inheritancesIds = extractRelationsIds(
				updatedDiagram.getCells().stream().map(Cell::getCell).collect(Collectors.toList()), false);
		DiagramDto diagramDto = new DiagramDto();
		modelMapper.map(updatedDiagram, diagramDto);
		diagramDto.setAssociations(associationsIds);
		diagramDto.setInheritances(inheritancesIds);
		return diagramDto;
	}

	@Transactional
	public Diagram mapCellObjectAndSaveDiagram(Diagram diagramEntity, DiagramDto diagram, List<Cell> updatedCells,
			boolean isUpdate) {
		// get existing relations to check for changes
		List<String> existingAssociationsIds = new ArrayList<>();
		List<String> existingInheritancesIds = new ArrayList<>();
		if (isUpdate && diagram.getAssociations() != null && diagram.getInheritances() != null) {
			existingAssociationsIds = diagram.getAssociations();
			existingInheritancesIds = diagram.getInheritances();
		}

		// matching object container id with object id
		Map<String, String> idsMatcher = new HashMap<>();
		for (String cell : diagram.getCells()) {
			try {
				JsonNode cellJsonNode = objectMapper.readTree(cell);
				if (cellJsonNode.has("data") && cellJsonNode.get("data").has("id")) {
					idsMatcher.put(cellJsonNode.get("id").asText(), cellJsonNode.get("data").get("id").asText());
				} else if (cellJsonNode.has("data")) {
					idsMatcher.put(cellJsonNode.get("id").asText(), cellJsonNode.get("data").asText());
				}
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
		for (String cell : diagram.getCells()) {
			try {
				JsonNode cellJsonNode = objectMapper.readTree(cell);
				if (cellJsonNode.has("data") && cellJsonNode.get("data").has("id")) {
					JsonNode objectData = cellJsonNode.get("data");
					String objectId = objectData.get("id").asText();
					ObjectNode cellObjectNode = ((ObjectNode) cellJsonNode).put("data", objectId);
					String updatedCell = cellObjectNode.toString();
					Cell cellObj = new Cell();
					cellObj.setCell(updatedCell);
					cellObj.setDiagram(diagramEntity);
					updatedCells.add(cellObj);
				} else if (cellJsonNode.has("data")) {
					String objectId = cellJsonNode.get("id").asText();
					ObjectNode cellObjectNode = ((ObjectNode) cellJsonNode).put("data", objectId);
					String updatedCell = cellObjectNode.toString();
					Cell cellObj = new Cell();
					cellObj.setCell(updatedCell);
					cellObj.setDiagram(diagramEntity);
					updatedCells.add(cellObj);
				} else if (cellJsonNode.has("isAssociationType")) {
					Cell cellObj = new Cell();
					cellObj.setCell(cell);
					cellObj.setDiagram(diagramEntity);
					updatedCells.add(cellObj);

					String sourceId = idsMatcher.get(cellJsonNode.get("source").get("id").asText());
					ClasseDobjet sourceObject = objetService.getLastVersionReturningEntity(sourceId);
					String targetId = idsMatcher.get(cellJsonNode.get("target").get("id").asText());
					ClasseDobjet targetObject = objetService.getLastVersionReturningEntity(targetId);
					String uID = cellJsonNode.get("id").asText();
					if (cellJsonNode.get("isAssociationType").asBoolean() && !existingAssociationsIds.contains(uID)) {
						RelationClasseDobjet relation = new RelationClasseDobjet();
						ArianeHelper.setNewEntityDetails(relation);
						relation.setId(uID);
						relation.setClasseDobjetSource(sourceObject);
						relation.setClasseDobjetDestination(targetObject);
						relationClasseDobjetRepository.save(relation);
					} else if (!cellJsonNode.get("isAssociationType").asBoolean()
							&& !existingInheritancesIds.contains(uID)) {
						RelationClasseMere relation = new RelationClasseMere();
						ArianeHelper.setNewEntityDetails(relation);
						relation.setId(uID);
						relation.setClasseDobjeMere(targetObject);
						relation.setClasseDobjetFille(sourceObject);
						relationClasseMereRepository.save(relation);
					} else if (cellJsonNode.get("isAssociationType").asBoolean()
							&& existingAssociationsIds.contains(uID)) {
						RelationClasseDobjet existingRelation = relationClasseDobjetRepository
								.findTopByIdOrderByIdRelationClasseDobjetDesc(uID)
								.orElseThrow(NoSuchElementException::new);
						ClasseDobjet existingSourceObject = existingRelation.getClasseDobjetSource();
						ClasseDobjet existingTargetObject = existingRelation.getClasseDobjetDestination();
						updateExistingRelation(existingRelation, existingSourceObject, existingTargetObject,
								cellJsonNode, idsMatcher);
						existingAssociationsIds.remove(uID);
					} else if (!cellJsonNode.get("isAssociationType").asBoolean()
							&& existingInheritancesIds.contains(uID)) {
						RelationClasseMere existingRelation = relationClasseMereRepository
								.findTopByIdOrderByIdRelationClasseMereDesc(uID)
								.orElseThrow(NoSuchElementException::new);
						ClasseDobjet existingChildObject = existingRelation.getClasseDobjetFille();
						ClasseDobjet existingParentObject = existingRelation.getClasseDobjeMere();
						updateExistingRelation(existingRelation, existingChildObject, existingParentObject,
								cellJsonNode, idsMatcher);
						existingInheritancesIds.remove(uID);
					}
				}
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

		}
		if (isUpdate) {
			cellRepository.deleteByDiagram_DiagramId(diagramEntity.getDiagramId());
			relationClasseDobjetRepository.deleteByIdIn(existingAssociationsIds);
			relationClasseMereRepository.deleteByIdIn(existingInheritancesIds);
		}

		ArianeHelper.updateEntityDetails(diagram, diagramEntity);
		Filiere filiere = filiereRepository.findTopByNomOrderByIdFiliereDesc(diagram.getFiliere())
				.orElseThrow(NoSuchElementException::new);
		diagramEntity.setFiliere(filiere);
		diagramEntity.setLibelle(diagram.getLibelle());
		diagramEntity.setDescription(diagram.getDescription());
		diagramEntity.setCells(updatedCells);
		return diagramRepository.save(diagramEntity);
	}

	public <T extends RessourceAbstraite> void updateExistingRelation(T existingRelation,
			ClasseDobjet existingSourceObject, ClasseDobjet existingTargetObject, JsonNode cellJsonNode,
			Map<String, String> idsMatcher) {
		if (existingRelation instanceof RelationClasseDobjet) {
			if (!existingSourceObject.getId().equals(cellJsonNode.get("source").get("id").asText())) {
				ClasseDobjet newSourceObject = objetService
						.getLastVersionReturningEntity(idsMatcher.get(cellJsonNode.get("source").get("id").asText()));
				((RelationClasseDobjet) existingRelation).setClasseDobjetSource(newSourceObject);
			}
			if (!existingTargetObject.getId().equals(cellJsonNode.get("target").get("id").asText())) {
				ClasseDobjet newTargetObject = objetService
						.getLastVersionReturningEntity(idsMatcher.get(cellJsonNode.get("target").get("id").asText()));
				((RelationClasseDobjet) existingRelation).setClasseDobjetDestination(newTargetObject);
			}
			ArianeHelper.updateEntityDetails(existingRelation, existingRelation);
			relationClasseDobjetRepository.save((RelationClasseDobjet) existingRelation);
		}
		if (existingRelation instanceof RelationClasseMere) {
			if (!existingSourceObject.getId().equals(cellJsonNode.get("source").get("id").asText())) {
				ClasseDobjet newSourceObject = objetService
						.getLastVersionReturningEntity(idsMatcher.get(cellJsonNode.get("source").get("id").asText()));
				((RelationClasseMere) existingRelation).setClasseDobjetFille(newSourceObject);
			}
			if (!existingTargetObject.getId().equals(cellJsonNode.get("target").get("id").asText())) {
				ClasseDobjet newTargetObject = objetService
						.getLastVersionReturningEntity(idsMatcher.get(cellJsonNode.get("target").get("id").asText()));
				((RelationClasseMere) existingRelation).setClasseDobjeMere(newTargetObject);
			}
			ArianeHelper.updateEntityDetails(existingRelation, existingRelation);
			relationClasseMereRepository.save((RelationClasseMere) existingRelation);
		}
	}

	public String addObjectDataToCell(Cell cell) {
		try {
			JsonNode cellJsonNode = objectMapper.readTree(cell.getCell());
			if (cellJsonNode.has("data") && !cellJsonNode.get("type").asText().equals("container.Parent")) {
				String objectId = cellJsonNode.get("data").asText();
				ObjetDto object = objetService.getLastVersion(objectId);
				((ObjectNode) (cellJsonNode.get("attrs").get("label").get("textWrap"))).put("text", object.getLibelle());
				String updatedCellData = ((ObjectNode) cellJsonNode).putPOJO("data", object).toString();
				return updatedCellData;

			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return cell.getCell();
	}

	public DiagramDto updateDiagramMetaData(DiagramDto diagram) {
		Diagram diagramEntity = diagramRepository.getOne(diagram.getDiagramId());
		diagramEntity.setLibelle(diagram.getLibelle());
		diagramEntity.setDescription(diagram.getDescription());
		Filiere filiere = filiereRepository.findTopByNomOrderByIdFiliereDesc(diagram.getFiliere())
				.orElseThrow(NoSuchElementException::new);
		diagramEntity.setFiliere(filiere);
		ArianeHelper.updateEntityDetails(diagram, diagramEntity);
		diagramRepository.save(diagramEntity);
		return modelMapper.map(diagramEntity, DiagramDto.class);
	}

	public DiagramDto archiveDiagram(DiagramDto diagram) {
		Diagram diagramEntity = diagramRepository.getOne(diagram.getDiagramId());
		diagramEntity.setDateFinActivite(LocalDateTime.now());
		diagramRepository.save(diagramEntity);
		diagram.getAssociations().stream().forEach(association -> {
			RelationClasseDobjet existingRelation = relationClasseDobjetRepository
					.findTopByIdOrderByIdRelationClasseDobjetDesc(association).orElseThrow(NoSuchElementException::new);
			existingRelation.setDateFinActivite(LocalDateTime.now());
			relationClasseDobjetRepository.save(existingRelation);
		});
		diagram.getInheritances().stream().forEach(inheritance -> {
			RelationClasseMere existingRelation = relationClasseMereRepository
					.findTopByIdOrderByIdRelationClasseMereDesc(inheritance).orElseThrow(NoSuchElementException::new);
			existingRelation.setDateFinActivite(LocalDateTime.now());
			relationClasseMereRepository.save(existingRelation);
		});
		return modelMapper.map(diagramEntity, DiagramDto.class);
	}
}
