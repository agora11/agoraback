package sncf.reseau.cdsdpadsir.agora.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;

import sncf.reseau.cdsdpadsir.agora.entity.AssociationClasseDobjetFiliere;
import sncf.reseau.cdsdpadsir.agora.entity.AssociationTermeFiliere;
import sncf.reseau.cdsdpadsir.agora.entity.ClasseDobjet;
import sncf.reseau.cdsdpadsir.agora.entity.Document;
import sncf.reseau.cdsdpadsir.agora.entity.Domaine;
import sncf.reseau.cdsdpadsir.agora.entity.Etiquette;
import sncf.reseau.cdsdpadsir.agora.entity.Famille;
import sncf.reseau.cdsdpadsir.agora.entity.Filiere;
import sncf.reseau.cdsdpadsir.agora.entity.Gisement;
import sncf.reseau.cdsdpadsir.agora.entity.Terme;
import sncf.reseau.cdsdpadsir.agora.entity.TypeElementModeleObjet;
import sncf.reseau.cdsdpadsir.agora.entity.Utilisateur;
import sncf.reseau.cdsdpadsir.agora.utils.ArianeHelper;

public class Specifications {

	private static final String REVISION = "revision";

	public static org.springframework.data.jpa.domain.Specification<Utilisateur> getUtilisateursByRevision() {
		return (root, query, criteriaBuilder) -> {
			Subquery<Integer> sq = query.subquery(Integer.class);
			Root<Utilisateur> maxRevision = sq.from(Utilisateur.class);

			List<Predicate> predicates = new ArrayList<>();
			List<Predicate> subPredicates = new ArrayList<>();

			subPredicates.add(criteriaBuilder.equal(maxRevision.get("id"), root.get("id")));

			sq.select(criteriaBuilder.max(maxRevision.<Integer>get(REVISION)))
					.where(subPredicates.toArray(new Predicate[predicates.size()]));

			predicates.add(criteriaBuilder.equal(root.get("dateFinActivite"), ArianeHelper.FUTURE_DATE));
			predicates.add(criteriaBuilder.equal(root.get(REVISION), sq));

			Predicate equalPredicate = criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			return equalPredicate;
		};
	}

	public static org.springframework.data.jpa.domain.Specification<ClasseDobjet> getObjetsByFiliere(
			List<String> filieres) {
		return (root, query, criteriaBuilder) -> {

			Join<ClasseDobjet, AssociationClasseDobjetFiliere> filiereObjets = root
					.join("associationClasseDobjetFilieres");
			Join<AssociationClasseDobjetFiliere, Filiere> associate = filiereObjets.join("filiere");
			List<Predicate> conditions = new ArrayList<>();

			In<String> inClause = criteriaBuilder.in(associate.get("nom"));

			for (String filiere : filieres) {
				inClause.value(filiere);
			}
			conditions.add(inClause);

			Predicate equalPredicate = criteriaBuilder.and(conditions.toArray(new Predicate[] {}));

			return equalPredicate;
		};
	}

	public static org.springframework.data.jpa.domain.Specification<ClasseDobjet> getObjetsFonctionels() {
		return (root, query, criteriaBuilder) -> {

			List<Predicate> conditions = new ArrayList<>();

			Join<ClasseDobjet, TypeElementModeleObjet> associate = root.join("typeElementModeleObjet");

			Predicate clause = criteriaBuilder.equal(associate.get("libelle"), "fonctionnel");

			query.multiselect(associate.get("id"), criteriaBuilder.max(associate.get("revisionFonctionnel")));
			query.groupBy(associate.get("id"));

			conditions.add(clause);

			Predicate equalPredicate = criteriaBuilder.and(conditions.toArray(new Predicate[] {}));

			return equalPredicate;
		};
	}

	public static org.springframework.data.jpa.domain.Specification<Terme> getTermesByRevision() {
		return (root, query, criteriaBuilder) -> {
			Subquery<Integer> sq = query.subquery(Integer.class);
			Root<Terme> maxRevision = sq.from(Terme.class);

			return querry(root, criteriaBuilder, sq, maxRevision);
		};
	}

	public static org.springframework.data.jpa.domain.Specification<ClasseDobjet> getObjetsByRevision() {
		return (root, query, criteriaBuilder) -> {
			Subquery<Integer> sq = query.subquery(Integer.class);
			Root<ClasseDobjet> maxRevision = sq.from(ClasseDobjet.class);

			return querry(root, criteriaBuilder, sq, maxRevision);
		};
	}

	public static org.springframework.data.jpa.domain.Specification<Etiquette> getTagsByRevision() {
		return (root, query, criteriaBuilder) -> {
			Subquery<Integer> sq = query.subquery(Integer.class);
			Root<Etiquette> maxRevision = sq.from(Etiquette.class);

			return querry(root, criteriaBuilder, sq, maxRevision);
		};
	}

	public static org.springframework.data.jpa.domain.Specification<Filiere> getFilieresByRevision() {
		return (root, query, criteriaBuilder) -> {
			Subquery<Integer> sq = query.subquery(Integer.class);
			Root<Filiere> maxRevision = sq.from(Filiere.class);

			return querry(root, criteriaBuilder, sq, maxRevision);
		};
	}

	public static org.springframework.data.jpa.domain.Specification<Document> getDocumentsByRevision() {
		return (root, query, criteriaBuilder) -> {
			Subquery<Integer> sq = query.subquery(Integer.class);
			Root<Document> maxRevision = sq.from(Document.class);

			return querry(root, criteriaBuilder, sq, maxRevision);
		};
	}

	public static org.springframework.data.jpa.domain.Specification<Famille> getFamillesByRevision() {
		return (root, query, criteriaBuilder) -> {
			Subquery<Integer> sq = query.subquery(Integer.class);
			Root<Famille> maxRevision = sq.from(Famille.class);

			return querry(root, criteriaBuilder, sq, maxRevision);
		};
	}

	public static org.springframework.data.jpa.domain.Specification<Domaine> getDomainesByRevision() {
		return (root, query, criteriaBuilder) -> {
			Subquery<Integer> sq = query.subquery(Integer.class);
			Root<Domaine> maxRevision = sq.from(Domaine.class);

			return querry(root, criteriaBuilder, sq, maxRevision);
		};
	}

	private static Predicate querry(Root<?> root, CriteriaBuilder criteriaBuilder, Subquery<Integer> sq,
			Root<?> maxRevision) {
		List<Predicate> predicates = new ArrayList<>();
		List<Predicate> subPredicates = new ArrayList<>();

		subPredicates.add(criteriaBuilder.equal(maxRevision.get("id"), root.get("id")));

		sq.select(criteriaBuilder.max(maxRevision.<Integer>get(REVISION)))
				.where(subPredicates.toArray(new Predicate[predicates.size()]));

		predicates.add(criteriaBuilder.equal(root.get("dateFinActivite"), ArianeHelper.FUTURE_DATE));
		predicates.add(criteriaBuilder.equal(root.get(REVISION), sq));

		Predicate equalPredicate = criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
		return equalPredicate;
	}

	public static org.springframework.data.jpa.domain.Specification<Terme> getTermesByFiliere(List<String> filieres) {
		return (root, query, criteriaBuilder) -> {

			Join<Terme, AssociationTermeFiliere> filiereTermes = root.join("associationTermeFilieres");
			Join<AssociationTermeFiliere, Filiere> associate = filiereTermes.join("filiere");
			List<Predicate> conditions = new ArrayList<>();

			In<String> inClause = criteriaBuilder.in(associate.get("nom"));

			for (String filiere : filieres) {
				inClause.value(filiere);
			}
			conditions.add(inClause);

			Predicate equalPredicate = criteriaBuilder.and(conditions.toArray(new Predicate[] {}));

			return equalPredicate;
		};
	}

	public static Specification<Gisement> getGisementsByRevision() {
		return (root, query, criteriaBuilder) -> {
			Subquery<Integer> sq = query.subquery(Integer.class);
			Root<Gisement> maxRevision = sq.from(Gisement.class);
			return querry(root, criteriaBuilder, sq, maxRevision);
		};
	}
}
