//Version templatefile maven : 1.9.0

//déclaration de la shared library build_libs
@Library('build_libs') _

def options = [
  //Paramètre optionnel :Id du fichier de configuration maven à utiliser
  //'MAVEN_CONFIG_ID': '{{group-repository-nexus-id}}',
  //Paramètre optionnel : Clé SonarQube pour les analyses
  //PROJECT_SONAR_KEY: '{{project-sonar-key}}',
  //Propriétés spécifiques au job
  //Paramètre optionnel : Nom de la connexion à Gitlab
  //GITLAB_NAME: '{{gitlab}}',
  // Paramètre obligatoire
  //Nombre de build a conserver dans l'historique
  NUM_TO_KEEP   : 10,
  //Paramètre obligatoire
  //La branche par défaut à partir de laquelle, le déploiement nexus et l'analyse sonar doivent être fait
  BRANCH_DEVELOP: 'develop',
  //Paramètre obligatoire
  //version de l'image android utilisés pour le build
  MAVEN_VERSION : '3.5.3-jdk-8'
]

/** Spécifique pour les builds docker */

def prepareBuildDocker = [
  // Paramètre optionnel :
  // Id du credentials Jenkins à utiliser pour que le build docker puisse tirer les images From des dockerfiles lorsque présentes sur une organisation privée de la DTR et non publique.
  // 'credentialsIdBuild': '{{credentialsIdBuild }}',

  // Paramètre optionnel :
  // Nom du dépôt maven du service nexus sur lequel seront poussés les binaires lors de ce build.
  // Il sera injecté en tant que build-arg "REPO_NAME" lors du build de l'image. Les dockerfiles doivent contenir l'instruction ARG REPO_NAME
  // 'repoBinaires':'{{ maven-repo }}',

  // Paramètre optionnel :
  // id du credential permettant de downloader les binaires présents sur le service Nexus
  // 'credentialsIdDownload':'{{credentialsId-download}}',

  // Paramètre optionnel :
  // Le nom des images est construit à partir du nom du dépôt git. Ce paramètre permet d'utiliser une valeur différente à la place du nom du dépot.
  // Pour les projets multi-modules, le nom de l'image sera construite à partir de ce préfixe et du nom du module dans les sources sous le dossier docker.
  // Pour les projets non modulaire, le nom de l'image correspondra à ce préfixe.
  //'prefixImage': '{{ prefix-Image }}',

  //Paramètre optionnel :
  //Permet de passer en argument du build docker une variable no_proxy, qui par défaut est valorisée avec les adresses en .sncf.fr et l'adresse du nexus metier
  //'no_proxy': '{{ no-proxy}}',

  //Paramètre optionnel:
  //Permet de passer des arguments au build des images docker. 
  // Les valeurs possibles sont des listes de dictionnaires disposant des clés 'name' et 'value'.
  // Par exemple, [ [ 'name':'BUILD', 'value':'docker' ], ['name': 'OS', 'value': 'ubuntu' ] ] 
  // permettra d'ajouter à la commande build "--build-arg BUILD='docker' --build-arg OS='ubuntu'"
  //'buildArgs': [ ],

  // Chemin du pom maven local au workspace
  'pomPath': 'pom.xml'
]


//Valorisation des options par défaut
options = defaultBuildOptions(options)


//Propriétés du job
properties([[$class: 'BuildConfigProjectProperty'],
            //Connexion Gitlab
            gitLabConnection("${options['GITLAB_NAME']}"),
            //Conservation des 10 dernières éxecutions
            buildDiscarder(logRotator(numToKeepStr: "${options['NUM_TO_KEEP']}")),
            //Déclenchement du job via webhook lors de push de nouveau code ou de creation de merge request
            pipelineTriggers([[$class: 'GitLabPushTrigger', triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All']])])

//Plugin timestamp pour afficher le temps à chaque ligne de log
timestamps {
  withTools([name: 'maven', version: "${options['MAVEN_VERSION']}"]) {
    try {
      stage('Récupération code source') {
        scmInfo = checkout scm
        env.GIT_URL = scmInfo.GIT_URL
        env.GIT_COMMIT = scmInfo.GIT_COMMIT

      }
      //Décommenter pour activer le build docker
      
      stage('Preparation build docker'){
          def pom = readMavenPom file: prepareBuildDocker['pomPath']
          prepareBuildDocker['version'] = pom.getVersion()


      }


      //Phase de compilation, publication du nombre de TODOs
      withMaven(mavenSettingsConfig: "${options['MAVEN_CONFIG_ID']}", options: [openTasksPublisher()], publisherStrategy: 'EXPLICIT') {
        container('maven') {
          stage('Compilation') {
            sh "mvn $MAVEN_CONFIG clean compile -U"
          }
        }
      }
      //Phase de tests, publication des résultats de TUs
      withMaven(mavenSettingsConfig: "${options['MAVEN_CONFIG_ID']}", publisherStrategy: 'EXPLICIT', options: [junitPublisher(healthScaleFactor: 1.0)]) {
        container('maven') {
          stage('TUs') {
            sh "mvn $MAVEN_CONFIG test -Dmaven.test.failure.ignore=true"
          }
        }
      }
      //Phase d'upload, publication sur la page du jobs, des artefacts poussés uniquement sur la branche de developpement par defaut
      if (env.BRANCH_NAME == options['BRANCH_DEVELOP']) {
        withMaven(mavenSettingsConfig: "${options['MAVEN_CONFIG_ID']}", options: [artifactsPublisher()], publisherStrategy: 'EXPLICIT') {
          container('maven') {
            stage('Upload binaires') {
              sh "mvn $MAVEN_CONFIG deploy -DskipTests=true"
            }
          }
        }
        //phase n'analyse qualité uniquement sur la branche de developpement par defaut
        withMaven(mavenSettingsConfig: "${options['MAVEN_CONFIG_ID']}", publisherStrategy: 'EXPLICIT') {
          container('maven') {
            stage('analyse SonarQube') {
              withSonarQubeEnv("sonarqube") {
                sh "mvn $MAVEN_CONFIG sonar:sonar -Dsonar.projectKey=${options['PROJECT_SONAR_KEY_BRANCH']} -DskipTests=true"
              }
            }
          }
        }
        //Décommenter pour activer le build docker
        
        if ( currentBuild.result != 'FAILURE'){

        // Réalise les étapes de builds et de push des images des modules du projet
            buildDocker(prepareBuildDocker)
        }

      }
      //voir https://jenkins.io/doc/pipeline/steps/gitlab-plugin/ pour plus de précisions
      updateGitlabCommitStatus name: 'build', state: 'success'
      currentBuild.result = 'SUCCESS'

    } catch (all) {
      //voir https://jenkins.io/doc/pipeline/steps/gitlab-plugin/ pour plus de précisions
      updateGitlabCommitStatus name: 'build', state: 'failed'
      currentBuild.result = 'FAILURE'
      //Envoi d'un mail en cas d'échec - COMMENTE EN ATTENTE DE L'INSTALLATION DU PLUGIN EMAIL EXT
      //voir https://jenkins.io/doc/pipeline/steps/email-ext/ pour plus de précisions
      emailext(
        body: '${DEFAULT_CONTENT}',
        subject: '${DEFAULT_SUBJECT}',
        //envoie du mail aux développeurs responsables d'une modification du code
        recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
        //Pour également envoyer le mail à une liste d'adresses (séparées par des ,)
        //  to : "adressmail1, adressmail2"
      )
      throw all
    }
  }
}
